// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Sml.Common;
using Sml.Common.Visitors;

namespace Sml.Algebra.Data
{
	public class SparceMatrix<T> : IMatrix<T>, ICollection<T>, IVisitable<T>
	{
		protected int _rowsCount;
		protected int _colsCount;

		private static readonly T DefValue = default(T);

		private Dictionary<long, T> _data = new Dictionary<long, T>();

		/// <summary>
		/// Создание нового экземпляра класса <see cref="ObjectMatrix&lt;T&gt;"/>.
		/// </summary>
		/// <param name="rowCount">Количество строк матрицы</param>
		/// <param name="columns">Количество колонок матрицы</param>
		public SparceMatrix(int rowCount, int columns)
		{
			if (rowCount <= 0)
				throw new ArgumentException("RowsOrColumnsInvalid", "rowCount");

			if (columns <= 0)
				throw new ArgumentException("RowsOrColumnsInvalid", "columns");

			_colsCount = columns;
			_rowsCount = rowCount;
		}

		private long GetLocation(int row, int col)
		{
			//todo возможно использование OR
			return ((long)row << 32) + col; 
		}

		internal void SetValue(int row, int column, T value)
		{
			long location = GetLocation(row, column);
			if (!value.Equals(DefValue))
				_data[location] = value;
			else
				_data.Remove(location);
		}

		internal T GetValue(int row, int column)
		{
			long location = GetLocation(row, column);
			return _data.ContainsKey(location) ? _data[location] : DefValue;
		}

		public void AddColumn()
		{
			AddColumns(1);
		}

		public void AddColumn(params T[] values)
		{
			Guard.ArgumentNotNull(values, "values");
			if (values.Length > _rowsCount)
				throw new ArgumentException("NumberOfValuesDoNotAgreeWithNumberOfRows", "values");

			AddColumn();
			int col = _colsCount - 1;
			for (int i = 0; i < values.Length; i++)
				SetValue(i, col, values[i]);
		}

		public void AddColumns(int columnCount)
		{
			Guard.ArgumentIsNotNegative(columnCount, "columnCount");
			_colsCount += columnCount;
		}

		public void AddRow()
		{
			AddRows(1);
		}

		public void AddRow(params T[] values)
		{
			Guard.ArgumentNotNull(values, "values");
			if (values.Length > _colsCount)
				throw new ArgumentException("NumberOfValuesDoNotAgreeWithNumberOfColumns", "values");

			AddRow();

			int row = _rowsCount - 1;
			for (int i = 0; i < values.Length; i++)
				SetValue(row, i, values[i]);
		}

		public void AddRows(int rowCount)
		{
			Guard.ArgumentIsNotNegative(rowCount, "rowCount");
			_rowsCount += rowCount;
		}

		public void DeleteColumn(int colIndex)
		{
			if (_colsCount == 1)
				throw new InvalidOperationException("MatrixDeleteOnlyColumn");

			if ((colIndex >= _colsCount) || (colIndex < 0))
				throw new ArgumentOutOfRangeException("colIndex");

			for (int row = 0; row < _rowsCount; row++)
			{
				long location = GetLocation(row, colIndex);
				_data.Remove(location);
			}
			_colsCount--;
		}

		public void DeleteRow(int rowIndex)
		{
			if (_rowsCount == 1)
				throw new InvalidOperationException("MatrixDeleteOnlyRow");

			if ((rowIndex >= _rowsCount) || (rowIndex < 0))
				throw new ArgumentOutOfRangeException("rowIndex");

			for (int col = 0; col < _rowsCount; col++)
			{
				long location = GetLocation(rowIndex, col);
				_data.Remove(location);
			}
			_rowsCount--;
		}

		public T[] GetColumn(int colIndex)
		{
			if ((colIndex < 0) || (colIndex >= _colsCount))
				throw new ArgumentOutOfRangeException("colIndex");

			T[] localArray = new T[_rowsCount];
			for (int i = 0; i < _rowsCount; i++)
				localArray[i] = GetValue(i, colIndex);

			return localArray;
		}

		public T[] GetRow(int rowIndex)
		{
			if ((rowIndex < 0) || (rowIndex >= _rowsCount))
				throw new ArgumentOutOfRangeException("rowIndex");

			T[] localArray = new T[_colsCount];
			for (int i = 0; i < _colsCount; i++)
				localArray[i] = GetValue(rowIndex, i);

			return localArray;
		}

		public IMatrix<T> GetSubMatrix(int rowStart, int columnStart, int rowCount, int columnCount)
		{
			Guard.ArgumentIsPositive(rowCount, "rowCount");
			Guard.ArgumentIsPositive(columnCount, "columnCount");

			Guard.ArgumentIsNotNegative(rowStart, "rowStart");
			Guard.ArgumentIsNotNegative(columnStart, "columnStart");

			if (((rowStart + rowCount) > RowCount) || ((columnStart + columnCount) > ColumnCount))
				throw new ArgumentOutOfRangeException("rowStart", "TooManyRowsOrColumns");

			SparceMatrix<T> matrix = new SparceMatrix<T>(rowCount, columnCount);
			for (int row = rowStart; row < (rowStart + rowCount); row++)
			{
				for (int col = columnStart; col < (columnStart + columnCount); col++)
					matrix.SetValue(row - rowStart, col - columnStart, GetValue(row, col));
			}
			return matrix;
		}

		public void SwapColumns(int firstColumn, int secondColumn)
		{
			if (firstColumn == secondColumn) return;

			if ((firstColumn < 0) || (firstColumn >= _colsCount))
				throw new ArgumentOutOfRangeException("firstColumn");

			if ((secondColumn < 0) || (secondColumn >= _colsCount))
				throw new ArgumentOutOfRangeException("secondColumn");

			for (int i = 0; i < _rowsCount; i++)
			{
				T local = GetValue(i, firstColumn);
				SetValue(i, firstColumn, GetValue(i, secondColumn));
				SetValue(i, secondColumn, local);
			}
		}

		public void SwapRows(int firstRow, int secondRow)
		{
			if (firstRow == secondRow) return;

			if ((firstRow < 0) || (firstRow >= _rowsCount))
				throw new ArgumentOutOfRangeException("firstRow");

			if ((secondRow < 0) || (secondRow >= _rowsCount))
				throw new ArgumentOutOfRangeException("secondRow");

			for (int i = 0; i < _colsCount; i++)
			{
				T local = GetValue(firstRow, i);
				SetValue(firstRow, i, GetValue(secondRow, i));
				SetValue(secondRow, i, local);
			}
		}

		public void Resize(int newNumberOfRows, int newNumberOfColumns)
		{
			Guard.ArgumentIsNotNegative(newNumberOfRows, "newNumberOfRows");
			Guard.ArgumentIsNotNegative(newNumberOfColumns, "newNumberOfColumns");

			Dictionary<long, T> newData = new Dictionary<long, T>();

			int rowMin	 = Math.Min(_rowsCount, newNumberOfRows);
			int colMin	 = Math.Min(_colsCount, newNumberOfColumns);


			for (int row = 0; row < rowMin; row++)
				for (int col = 0; col < colMin; col++)
				{
					long location = GetLocation(row, col);
					if (_data.ContainsKey(location))
						newData[location] = _data[location];
				}
			_data = newData;
			_rowsCount = newNumberOfRows;
			_colsCount = newNumberOfColumns;
		}

		public int ColumnCount
		{
			get { return _colsCount; }
		}

		public bool IsSquare
		{
			get { return (_colsCount == _rowsCount); }
		}

		public T this[int row, int column]
		{
			get { return GetValue(row, column); }
			set { SetValue(row, column, value); }
		}

		public int RowCount
		{
			get { return _rowsCount; }
		}

		public IEnumerator<T> GetEnumerator()
		{
			for (int i = 0; i < _colsCount; i++)
				for (int j = 0; j < _rowsCount; j++)
					yield return GetValue(j, i);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		void ICollection<T>.Add(T item)
		{
			throw new NotSupportedException();
		}

		bool ICollection<T>.Remove(T item)
		{
			throw new NotSupportedException();
		}

		public void Clear()
		{
			_data.Clear();
		}

		public bool Contains(T item)
		{
			foreach (T value in _data.Values)
			{
				if (value.Equals(item))
					return true;
			}
			return false;
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
			if ((array.Length - arrayIndex) < Count)
				throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");

			for (int i = 0; i < _colsCount; i++)
				for (int j = 0; j < _rowsCount; j++)
					array[j * _colsCount + i] = GetValue(j, i);
		}

		public int RealCount
		{
			get { return _data.Count; }
		}

		public int Count
		{
			get { return _colsCount * _rowsCount; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public void Accept(IVisitor<T> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");

			foreach (T value in _data.Values)
			{
				visitor.Visit(value);
			}
		}
	}
}

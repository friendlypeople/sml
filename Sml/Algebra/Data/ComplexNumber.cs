// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Globalization;
using Sml.Common;

namespace Sml.Algebra.Data
{
	[Serializable]
	public struct ComplexNumber : ICloneable, IEquatable<ComplexNumber>
	{
		private double _real;
		private double _imaginary;

		public ComplexNumber(double real, double imaginary)
		{
			_real = real;
			_imaginary = imaginary;
		}

		public bool Equals(ComplexNumber other)
		{
			return (this == other);
		}

		public ComplexNumber Conjugate
		{
			get { return new ComplexNumber(_real, -1.0*_imaginary); }
		}

		public ComplexNumber Multiply(ComplexNumber complex)
		{
			return MultiplyInternal(this, complex);
		}

		public ComplexNumber Multiply(double number)
		{
			return MultiplyInternal(this, number);
		}

		public ComplexNumber Divide(ComplexNumber number)
		{
			return DivideInternal(this, number);
		}

		public ComplexNumber Divide(double number)
		{
			return DivideInternal(this, number);
		}

		public ComplexNumber Add(ComplexNumber number)
		{
			return AddInternal(this, number);
		}

		public ComplexNumber Subtract(ComplexNumber complex)
		{
			return SubtractInternal(this, complex);
		}

		public IMatrix<double> ToMatrix()
		{
			Matrix matrix = new Matrix(2, 2);
			matrix[0, 0] = _real;
			matrix[0, 1] = -1.0*_imaginary;
			matrix[1, 0] = _imaginary;
			matrix[1, 1] = _real;
			return matrix;
		}

		public double Modulus
		{
			get { return Math.Sqrt((_real*_real) + (_imaginary*_imaginary)); }
		}

		public double Real
		{
			get { return _real; }
			set { _real = value; }
		}

		public double Imaginary
		{
			get { return _imaginary; }
			set { _imaginary = value; }
		}

		public ComplexNumber AdditiveInverse
		{
			get { return new ComplexNumber(_real*-1.0, _imaginary*-1.0); }
		}

		public double AbsoluteValue
		{
			get { return Math.Sqrt((_real*_real) + (_imaginary*_imaginary)); }
		}

		public ComplexNumber Reciprocal
		{
			get
			{
				if ((_real == 0.0) && (_imaginary == 0.0))
				{
					throw new InvalidOperationException("ComplexReciprocalNonZero");
				}
				double div = (_real*_real) + (_imaginary*_imaginary);
				return new ComplexNumber(_real/div, (_imaginary*-1.0)/div);
			}
		}

		public static ComplexNumber AdditiveIdentity
		{
			get { return new ComplexNumber(0.0, 0.0); }
		}

		public static ComplexNumber MultiplicativeIdentity
		{
			get { return new ComplexNumber(1.0, 0.0); }
		}

		public static ComplexNumber operator +(ComplexNumber left, ComplexNumber right)
		{
			return AddInternal(left, right);
		}

		public static ComplexNumber operator -(ComplexNumber left, ComplexNumber right)
		{
			return SubtractInternal(left, right);
		}

		public static ComplexNumber operator *(ComplexNumber left, ComplexNumber right)
		{
			return MultiplyInternal(left, right);
		}

		public static ComplexNumber operator /(ComplexNumber left, ComplexNumber right)
		{
			return DivideInternal(left, right);
		}

		public static ComplexNumber operator /(ComplexNumber complexNumber, double number)
		{
			return DivideInternal(complexNumber, number);
		}

		public static ComplexNumber operator *(ComplexNumber complexNumber, double number)
		{
			return MultiplyInternal(complexNumber, number);
		}

		public static ComplexNumber operator *(double number, ComplexNumber complexNumber)
		{
			return new ComplexNumber(complexNumber._real*number, complexNumber._imaginary*number);
		}

		public static bool operator ==(ComplexNumber left, ComplexNumber right)
		{
			return ((left._real == right._real) && (left._imaginary == right._imaginary));
		}

		public static bool operator !=(ComplexNumber left, ComplexNumber right)
		{
			return !(left == right);
		}

		public static explicit operator string(ComplexNumber complexNumber)
		{
			return complexNumber.ToString();
		}

		public static implicit operator ComplexNumber(double real)
		{
			return new ComplexNumber(real, 0.0);
		}

		bool IEquatable<ComplexNumber>.Equals(ComplexNumber other)
		{
			return ((_real == other._real) && (_imaginary == other._imaginary));
		}

		public object Clone()
		{
			return new ComplexNumber(_real, _imaginary);
		}

		public override string ToString()
		{
			return string.Format(CultureInfo.InvariantCulture, "{0} + {1}i", new object[] {_real, _imaginary});
		}

		public override bool Equals(object obj)
		{
			return (((obj != null) && (obj.GetType() == GetType())) && (this == ((ComplexNumber) obj)));
		}

		public override int GetHashCode()
		{
			return HashCode.GetHashCode(_real.GetHashCode(),_imaginary.GetHashCode());
		}

		private static ComplexNumber MultiplyInternal(ComplexNumber left, ComplexNumber right)
		{
			return new ComplexNumber((left.Real*right.Real) - (left.Imaginary*right.Imaginary),
			                         (left.Real*right.Imaginary) + (left.Imaginary*right.Real));
		}

		private static ComplexNumber MultiplyInternal(ComplexNumber left, double right)
		{
			return new ComplexNumber(left.Real*right, left.Imaginary*right);
		}

		private static ComplexNumber DivideInternal(ComplexNumber left, double right)
		{
			return new ComplexNumber(left.Real/right, left.Imaginary/right);
		}

		private static ComplexNumber DivideInternal(ComplexNumber left, ComplexNumber right)
		{
			ComplexNumber conjugate = right.Conjugate;
			ComplexNumber t1 = left.Multiply(conjugate);
			ComplexNumber t2 = right.Multiply(conjugate);
			return new ComplexNumber(t1.Real/t2.Real, t1.Imaginary/t2.Real);
		}

		private static ComplexNumber AddInternal(ComplexNumber left, ComplexNumber right)
		{
			return new ComplexNumber(left.Real + right.Real, left.Imaginary + right.Imaginary);
		}

		private static ComplexNumber SubtractInternal(ComplexNumber left, ComplexNumber right)
		{
			return new ComplexNumber(left.Real - right.Real, left.Imaginary - right.Imaginary);
		}
	}
}

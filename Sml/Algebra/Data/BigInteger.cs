// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Sml.Common;

namespace Sml.Algebra.Data
{
	public class BigInteger
	{
		private const int	MaxLength = 70;
		private const int	LastPos = MaxLength - 1;

		private readonly UInt32[]	_data = new uint[MaxLength];
		private int					_actualLength;

		public BigInteger()
		{
			_actualLength = 1;
		}

		public BigInteger(long value)
		{
			while (value != 0 && _actualLength < MaxLength)
			{
				Data[_actualLength] = (uint)(value & 0xFFFFFFFF);
				value >>= 32;
				_actualLength++;
			}
			CheckActualLength(this);
		}

		public BigInteger(ulong value)
		{
			while (value != 0 && _actualLength < MaxLength)
			{
				Data[_actualLength] = (uint)(value & 0xFFFFFFFF);
				value >>= 32;
				_actualLength++;
			}
			CheckActualLength(this);			
		}

		public BigInteger(BigInteger value)
		{
			CloneImpl(value, this);
		}

		public BigInteger(byte[] value) : this(value, 0, value.Length) { }


		public BigInteger(byte[] value, int index, int length)
		{
			const int size = sizeof(UInt32);
			_actualLength = length / size;
			int leftOver = length & 0x3;

			int stIndex = length - leftOver;
			Utils.TransformToUints(value, Data, index, stIndex);

			switch (leftOver)
			{
				case 1:
					Data[_actualLength] = value[stIndex];
					break;
				case 2:
					Data[_actualLength] = (uint)(value[stIndex++] + (value[stIndex] << 8));
					break;
				case 3:
					Data[_actualLength] = (uint)(value[stIndex++] + (value[stIndex++] << 8) + (value[stIndex] << 16));
					break;
			}
			_actualLength++;
			CollapseZeroSequence(this);
		}

		public BigInteger(uint[] value)
		{
			Array.Copy(value, _data, value.Length);
		}

		public BigInteger(string value) : this(value, 10) { }

		public BigInteger(string value, int radix)
		{
			BigInteger multiplier = new BigInteger(1);
			BigInteger result = new BigInteger();
			value = (value.ToUpper()).Trim();
			int limit = 0;

			if (value[0] == '-')
				limit = 1;

			for (int i = value.Length - 1; i >= limit; i--)
			{
				int posVal = value[i];

				if (posVal >= '0' && posVal <= '9')
					posVal -= '0';

				else if (posVal >= 'A' && posVal <= 'Z')
					posVal = (posVal - 'A') + 10;
				else
					posVal = 9999999;       // arbitrary large


				if (posVal >= radix)
					throw (new ArithmeticException("Invalid string in constructor."));
				
				if (value[0] == '-')
					posVal = -posVal;

				result = result + (multiplier * posVal);

				if ((i - 1) >= limit)
					multiplier = multiplier * radix;
			}

			if (value[0] == '-')
			{
				if (IsPositive(result))
					throw (new ArithmeticException("Negative underflow in constructor."));
			}
			else
			{
				if (IsNegative(result))
					throw (new ArithmeticException("Positive overflow in constructor."));
			}

			CloneImpl(result, this);
		}


		private static void CloneImpl(BigInteger src, BigInteger dst)
		{
			Array.Copy(src.Data, dst.Data, MaxLength);
			dst._actualLength = src._actualLength;
		}

		private static bool IsPositive(BigInteger bi)
		{
			return ReturnWithMask(bi) == 0;
		}

		private static bool IsNegative(BigInteger bi)
		{
			return ReturnWithMask(bi) != 0;
		}

		private static bool IsZero(BigInteger bi)
		{
			return bi._actualLength == 1 && bi._data[0] == 0;
		}

		private static bool IsOne(BigInteger bi)
		{
			return bi._actualLength == 1 && bi._data[0] == 1;
		}

		public uint[] Data
		{
			get { return _data; }
		}

		private static void CollapseZeroSequence(BigInteger src)
		{
			while (src._actualLength > 1 && src._data[src._actualLength - 1] == 0)
				src._actualLength--;
		}

		private static void CheckActualLength(BigInteger src)
		{
			if (src._actualLength == 0)
				src._actualLength = 1;
		}

		private static uint ReturnWithMask(BigInteger src)
		{
			
			return src._data[LastPos] & 0x80000000;
		}

		private static int ShiftRight(uint[] buffer, int shiftVal)
		{
			int shiftAmount = 32;
			int invShift = 0;
			int bufLen = buffer.Length;

			while (bufLen > 1 && buffer[bufLen - 1] == 0)
				bufLen--;

			for (int count = shiftVal; count > 0; )
			{
				if (count < shiftAmount)
				{
					shiftAmount = count;
					invShift = 32 - shiftAmount;
				}

				ulong carry = 0;
				for (int i = bufLen - 1; i >= 0; i--)
				{
					ulong val = ((ulong)buffer[i]) >> shiftAmount;
					val |= carry;

					carry = ((ulong)buffer[i]) << invShift;
					buffer[i] = (uint)(val);
				}

				count -= shiftAmount;
			}

			while (bufLen > 1 && buffer[bufLen - 1] == 0)
				bufLen--;

			return bufLen;
		}

		private static int ShiftLeft(uint[] buffer, int shiftVal)
		{
			int shiftAmount = 32;
			int bufLen = buffer.Length;

			while (bufLen > 1 && buffer[bufLen - 1] == 0)
				bufLen--;

			for (int count = shiftVal; count > 0; )
			{
				if (count < shiftAmount)
					shiftAmount = count;

				ulong carry = 0;
				for (int i = 0; i < bufLen; i++)
				{
					ulong val = ((ulong)buffer[i]) << shiftAmount;
					val |= carry;

					buffer[i] = (uint)(val & 0xFFFFFFFF);
					carry = val >> 32;
				}

				if (carry != 0)
				{
					if (bufLen + 1 <= buffer.Length)
					{
						buffer[bufLen] = (uint)carry;
						bufLen++;
					}
				}
				count -= shiftAmount;
			}
			return bufLen;
		}

		public static implicit operator BigInteger(long value)
		{
			return (new BigInteger(value));
		}

		public static implicit operator BigInteger(ulong value)
		{
			return (new BigInteger(value));
		}

		public static implicit operator BigInteger(int value)
		{
			return (new BigInteger(value));
		}

		public static implicit operator BigInteger(uint value)
		{
			return (new BigInteger(value));
		}

		public static BigInteger operator +(BigInteger bi1, long bi2)
		{
			return bi1 + new BigInteger(bi2);
		}

		public static BigInteger operator +(BigInteger bi1, ulong bi2)
		{
			return bi1 + new BigInteger(bi2);
		}

		public static BigInteger operator +(BigInteger bi1, BigInteger bi2)
		{
			BigInteger result = new BigInteger();

			int length = Math.Max(bi1._actualLength, bi2._actualLength);
			
			long carry = 0;
			for (int i = 0; i < length; i++)
			{
				long sum = (long)bi1._data[i] + bi2._data[i] + carry;
				carry = sum >> 32;
				result._data[i] = (uint)(sum & 0xFFFFFFFF);
			}

			if (carry != 0 && length < MaxLength)
			{
				result._data[length] = (uint)(carry);
				length++;
			}

			if (length > result._actualLength)
				result._actualLength = length;
			else
				CollapseZeroSequence(result);

			// overflow check
			if (ReturnWithMask(bi1) == ReturnWithMask(bi2) && ReturnWithMask(result) != ReturnWithMask(bi1))
				throw (new ArithmeticException());

			return result;
		}

		public static BigInteger operator ++(BigInteger bi1)
		{
			BigInteger result = new BigInteger(bi1);

			long carry = 1;
			int index = 0;

			while (carry != 0 && index < MaxLength)
			{
				long val = result._data[index];
				val++;
				result._data[index] = (uint)(val & 0xFFFFFFFF);
				carry = val >> 32;
				index++;
			}


			if (index > result._actualLength)
				result._actualLength = index;
			else
				CollapseZeroSequence(result);

			if (IsPositive(bi1) && ReturnWithMask(result) != ReturnWithMask(bi1))
				throw (new ArithmeticException("Overflow in ++ "));

			return result;
		}

		public static BigInteger operator -(BigInteger bi1, BigInteger bi2)
		{
			BigInteger result = new BigInteger();

			int length = Math.Max(bi1._actualLength, bi2._actualLength);

			long carryIn = 0;
			for (int i = 0; i < length; i++)
			{
				long diff = (long)bi1._data[i] - bi2._data[i] - carryIn;
				result._data[i] = (uint)(diff & 0xFFFFFFFF);
				carryIn = diff < 0 ? 1 : 0;
			}

			// Обрабатываем отрицательное число
			if (carryIn != 0)
			{
				for (int i = length; i < MaxLength; i++)
					result._data[i] = 0xFFFFFFFF;
				length = MaxLength;
			}

			result._actualLength = length;
			CollapseZeroSequence(result);

			if (ReturnWithMask(bi1) != ReturnWithMask(bi2) && ReturnWithMask(result) != ReturnWithMask(bi1))
				throw (new ArithmeticException());

			return result;
		}

		public static BigInteger operator -(BigInteger bi1, long bi2)
		{
			return bi1 - new BigInteger(bi2);
		}

		public static BigInteger operator -(BigInteger bi1, ulong bi2)
		{
			return bi1 - new BigInteger(bi2);
		}

		public static BigInteger operator --(BigInteger bi1)
		{
			BigInteger result = new BigInteger(bi1);

			bool carryIn = true;
			int index = 0;

			while (carryIn && index < MaxLength)
			{
				long val = result._data[index];
				val--;

				result._data[index] = (uint)(val & 0xFFFFFFFF);

				if (val >= 0) carryIn = false;
				index++;
			}

			if (index > result._actualLength)
				result._actualLength = index;

			CollapseZeroSequence(result);
			if (IsNegative(bi1) && ReturnWithMask(result) != ReturnWithMask(bi1))
				throw (new ArithmeticException("Underflow in --."));

			return result;
		}

		public static BigInteger operator -(BigInteger bi1)
		{
			if (bi1._actualLength == 1 && bi1._data[0] == 0)
				return (new BigInteger());

			BigInteger result = new BigInteger(bi1);
			for (int i = 0; i < MaxLength; i++)
				result._data[i] = ~(bi1._data[i]);

			long carry = 1;
			int index = 0;

			while (carry != 0 && index < MaxLength)
			{
				long val = result._data[index];
				val++;

				result._data[index] = (uint)(val & 0xFFFFFFFF);
				carry = val >> 32;
				index++;
			}

			if (ReturnWithMask(bi1) == ReturnWithMask(result))
				throw (new ArithmeticException("Overflow in negation.\n"));

			result._actualLength = MaxLength;
			CollapseZeroSequence(result);
			return result;
		}

		public static BigInteger operator ~(BigInteger bi1)
		{
			BigInteger result = new BigInteger(bi1);

			for (int i = 0; i < MaxLength; i++)
				result._data[i] = ~(bi1._data[i]);

			result._actualLength = MaxLength;
			CollapseZeroSequence(result);
			return result;
		}

		public static BigInteger operator *(BigInteger bi1, long bi2)
		{
			return bi1 * new BigInteger(bi2);
		}

		public static BigInteger operator *(BigInteger bi1, ulong bi2)
		{
			return bi1 * new BigInteger(bi2);
		}

		public static BigInteger operator *(BigInteger bi1, BigInteger bi2)
		{
			bool bi1Neg = false;
			bool bi2Neg = false;

			// take the absolute value of the inputs
			try
			{
				if (IsNegative(bi1))
				{
					bi1Neg = true;
					bi1 = -bi1;
				}
				if (IsNegative(bi2))
				{
					bi2Neg = true;
					bi2 = -bi2;
				}
			}
			catch (Exception) { }

			BigInteger result = new BigInteger();
			try
			{
				for (int i = 0; i < bi1._actualLength; i++)
				{
					if (bi1._data[i] == 0) continue;

					ulong mcarry = 0;
					for (int j = 0, k = i; j < bi2._actualLength; j++, k++)
					{
						ulong val = ((ulong)bi1._data[i] * bi2._data[j]) + result._data[k] + mcarry;
						result._data[k] = (uint)(val & 0xFFFFFFFF);
						mcarry = (val >> 32);
					}

					if (mcarry != 0)
						result._data[i + bi2._actualLength] = (uint)mcarry;
				}
			}
			catch (Exception)
			{
				throw (new ArithmeticException("Multiplication overflow."));
			}


			result._actualLength = bi1._actualLength + bi2._actualLength;
			if (result._actualLength > MaxLength)
				result._actualLength = MaxLength;

			CollapseZeroSequence(result);
			
			if (IsNegative(result))
			{
				if (bi1Neg != bi2Neg && IsPositive(result))    // different sign
				{
					if (result._actualLength == 1)
						return result;

					bool isMaxNeg = true;
					for (int i = 0; i < result._actualLength - 1 && isMaxNeg; i++)
					{
						if (result._data[i] != 0)
							isMaxNeg = false;
					}
					if (isMaxNeg)
						return result;
				}

				throw (new ArithmeticException("Multiplication overflow."));
			}
			if (bi1Neg != bi2Neg)
				return -result;

			return result;
		}

		public static BigInteger operator << (BigInteger bi1, int shiftVal)
		{
			BigInteger result = new BigInteger(bi1);
			result._actualLength = ShiftLeft(result._data, shiftVal);
			return result;
		}

		public static BigInteger operator >>(BigInteger bi1, int shiftVal)
		{
			BigInteger result = new BigInteger(bi1);
			result._actualLength = ShiftRight(result._data, shiftVal);

			if (IsNegative(bi1))
			{
				for (int i = MaxLength - 1; i >= result._actualLength; i--)
					result._data[i] = 0xFFFFFFFF;

				uint mask = 0x80000000;
				for (int i = 0; i < 32; i++)
				{
					if ((result._data[result._actualLength - 1] & mask) != 0)
						break;

					result._data[result._actualLength - 1] |= mask;
					mask >>= 1;
				}
				result._actualLength = MaxLength;
			}
			return result;
		}

		

		public static bool operator > (BigInteger bi1, BigInteger bi2)
		{
			if (IsNegative(bi1) && IsPositive(bi2))
				return false;

			if (IsPositive(bi1) && IsNegative(bi2))
				return true;

			int pos = IterateArray(bi1, bi2);
			return pos >= 0 && bi1._data[pos] > bi2._data[pos];
		}


		public static bool operator < (BigInteger bi1, BigInteger bi2)
		{
			if (IsNegative(bi1) && IsPositive(bi2))
				return true;

			if (IsPositive(bi1) && IsNegative(bi2))
				return false;

			int pos = IterateArray(bi1, bi2);
			return pos >= 0 && bi1._data[pos] < bi2._data[pos];
		}

		private static int IterateArray(BigInteger bi1, BigInteger bi2)
		{
			int len = Math.Max(bi1._actualLength, bi2._actualLength);
			int pos;
			for (pos = len - 1; pos >= 0 && bi1._data[pos] == bi2._data[pos]; pos--) { }
			return pos;
		}


		public static bool operator >=(BigInteger bi1, BigInteger bi2)
		{
			return (bi1 == bi2 || bi1 > bi2);
		}


		public static bool operator <=(BigInteger bi1, BigInteger bi2)
		{
			return (bi1 == bi2 || bi1 < bi2);
		}

		public static BigInteger operator /(BigInteger bi1, long bi2)
		{
			return bi1 / new BigInteger(bi2);
		}

		public static BigInteger operator /(BigInteger bi1, ulong bi2)
		{
			return bi1 / new BigInteger(bi2);
		}


		public BigInteger DivRem(BigInteger bi, out BigInteger remainder)
		{
			BigInteger bi1 = new BigInteger(this);
			BigInteger quotient = new BigInteger();
			remainder = new BigInteger();

			bool divisorNeg = false;
			bool dividendNeg = false;

			if (IsNegative(bi1))
			{
				bi1 = -bi1;
				dividendNeg = true;
			}
			if (IsNegative(bi))
			{
				bi = -bi;
				divisorNeg = true;
			}

			if (bi1 < bi)
				return quotient;

			DivideImpl(bi1, bi, remainder, quotient);

			if (dividendNeg)
				remainder = - remainder;
			return dividendNeg != divisorNeg ? -quotient : quotient;
		}

		public static BigInteger operator /(BigInteger bi1, BigInteger bi2)
		{
			BigInteger quotient = new BigInteger();
			BigInteger remainder = new BigInteger();

			bool divisorNeg = false, dividendNeg = false;

			if (IsNegative(bi1))
			{
				bi1 = -bi1;
				dividendNeg = true;
			}
			if (IsNegative(bi2))
			{
				bi2 = -bi2;
				divisorNeg = true;
			}

			if (bi1 < bi2)
				return quotient;

			DivideImpl(bi1, bi2, remainder, quotient);
			return dividendNeg != divisorNeg ? -quotient : quotient;
		}

		public static BigInteger operator %(BigInteger bi1, BigInteger bi2)
		{
			BigInteger quotient = new BigInteger();
			BigInteger remainder = new BigInteger(bi1);

			bool dividendNeg = false;

			if (IsNegative(bi1))
			{
				bi1 = -bi1;
				dividendNeg = true;
			}
			if (IsNegative(bi2))
				bi2 = -bi2;

			if (bi1 < bi2)
				return remainder;
			
			DivideImpl(bi1, bi2, remainder, quotient);
			return dividendNeg ? -remainder : remainder;
		}

		private static void DivideImpl(BigInteger bi1, BigInteger bi2, BigInteger remainder, BigInteger quotient)
		{
			if (bi2._actualLength == 1)
				SingleByteDivide(bi1, bi2, quotient, remainder);
			else
				MultiByteDivide(bi1, bi2, quotient, remainder);
		}

		public override bool Equals(object o)
		{
			BigInteger bi = (BigInteger)o;

			if (_actualLength != bi._actualLength)
				return false;

			for (int i = 0; i < _actualLength; i++)
			{
				if (_data[i] != bi._data[i])
					return false;
			}
			return true;
		}

		public override int GetHashCode()
		{
			return ToString().GetHashCode();
		}

		public override string ToString()
		{
			return ToString(10);
		}


		public string ToString(int radix)
		{
			if (radix < 2 || radix > 36)
				throw (new ArgumentException("Radix must be >= 2 and <= 36"));

			const string charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			string result = "";
			bool negative = false;

			BigInteger a = new BigInteger(this);
			BigInteger quotient = new BigInteger();
			BigInteger remainder = new BigInteger();
			BigInteger biRadix = new BigInteger(radix);

			if (IsNegative(a))
			{
				negative = true;
				try
				{
					a = -a;
				}
				catch (Exception) { }
			}

			if (IsZero(a))
				result = "0";
			else
			{
				while (a._actualLength > 1 || (a._actualLength == 1 && a._data[0] != 0))
				{
					SingleByteDivide(a, biRadix, quotient, remainder);

					if (remainder._data[0] < 10)
						result = remainder._data[0] + result;
					else
						result = charSet[(int)remainder._data[0] - 10] + result;

					a = quotient;
				}
				if (negative)
					result = "-" + result;
			}

			return result;
		}

		public static BigInteger operator &(BigInteger bi1, BigInteger bi2)
		{
			BigInteger result = new BigInteger();

			int len = Math.Max(bi1._actualLength, bi2._actualLength);

			for (int i = 0; i < len; i++)
			{
				uint sum = bi1._data[i] & bi2._data[i];
				result._data[i] = sum;
			}

			result._actualLength = MaxLength;
			CollapseZeroSequence(result);
			return result;
		}

		public static BigInteger operator |(BigInteger bi1, BigInteger bi2)
		{
			BigInteger result = new BigInteger();

			int len = Math.Max(bi1._actualLength, bi2._actualLength);
			for (int i = 0; i < len; i++)
			{
				uint sum = bi1._data[i] | bi2._data[i];
				result._data[i] = sum;
			}

			result._actualLength = MaxLength;
			CollapseZeroSequence(result);
			return result;
		}

		public static BigInteger operator ^(BigInteger bi1, BigInteger bi2)
		{
			BigInteger result = new BigInteger();

			int len = Math.Max(bi1._actualLength, bi2._actualLength);

			for (int i = 0; i < len; i++)
			{
				uint sum = bi1._data[i] ^ bi2._data[i];
				result._data[i] = sum;
			}

			result._actualLength = MaxLength;
			CollapseZeroSequence(result);
			return result;
		}

		public static BigInteger MaxValue
		{
			get
			{
				BigInteger bi = new BigInteger();

				for (int i = 0; i < LastPos; i++)
					bi.Data[i] = uint.MaxValue;

				bi.Data[LastPos] = uint.MaxValue >> 1;
				bi._actualLength = MaxLength;
				return bi;
			}
		}

		public static BigInteger MinValue
		{
			get
			{
				BigInteger bi = new BigInteger();
				bi.Data[LastPos] = 0x80000000;
				bi._actualLength = MaxLength;
				return bi;
			}
		}



		public BigInteger Max(BigInteger bi)
		{
			return this > bi ? (new BigInteger(this)) : (new BigInteger(bi));
		}


		public BigInteger Min(BigInteger bi)
		{
			return this < bi ? (new BigInteger(this)) : (new BigInteger(bi));
		}

		public BigInteger Abs()
		{
			return (IsNegative(this) ? -this : new BigInteger(this));
		}


		private static void SingleByteDivide(BigInteger bi1, BigInteger bi2,
			BigInteger outQuotient, BigInteger outRemainder)
		{
			uint[] result = new uint[MaxLength];
			int resultPos = 0;

			CloneImpl(bi1, outRemainder);
			CollapseZeroSequence(outRemainder);

			ulong divisor = bi2._data[0];
			int pos = outRemainder._actualLength - 1;
			ulong dividend = outRemainder._data[pos];

			if (dividend >= divisor)
			{
				ulong quotient = dividend / divisor;
				result[resultPos++] = (uint)quotient;
				outRemainder._data[pos] = (uint)(dividend % divisor);
			}
			pos--;

			while (pos >= 0)
			{
				dividend = ((ulong)outRemainder._data[pos + 1] << 32) + outRemainder._data[pos];
				ulong quotient = dividend / divisor;
				result[resultPos++] = (uint)quotient;

				outRemainder._data[pos + 1] = 0;
				outRemainder._data[pos--] = (uint)(dividend % divisor);
			}

			outQuotient._actualLength = resultPos;
			
			int j = 0;
			for (int i = outQuotient._actualLength - 1; i >= 0; i--, j++)
				outQuotient._data[j] = result[i];
			
			for (; j < MaxLength; j++)
				outQuotient._data[j] = 0;


			CollapseZeroSequence(outQuotient);
			CheckActualLength(outQuotient);
			CollapseZeroSequence(outRemainder);
		}

		private static void MultiByteDivide(BigInteger bi1, BigInteger bi2, 
			BigInteger outQuotient, BigInteger outRemainder)
		{
			uint[] result = new uint[MaxLength];

			int remainderLen = bi1._actualLength + 1;
			uint[] remainder = new uint[remainderLen];

			uint mask = 0x80000000;
			uint val = bi2._data[bi2._actualLength - 1];
			int shift = 0, resultPos = 0;

			while (mask != 0 && (val & mask) == 0)
			{
				shift++; mask >>= 1;
			}

			Array.Copy(bi1._data, remainder, bi1._actualLength);
			ShiftLeft(remainder, shift);
			bi2 = bi2 << shift;


			int j = remainderLen - bi2._actualLength;
			int pos = remainderLen - 1;

			ulong firstDivisorByte = bi2._data[bi2._actualLength - 1];
			ulong secondDivisorByte = bi2._data[bi2._actualLength - 2];

			int divisorLen = bi2._actualLength + 1;
			uint[] dividendPart = new uint[divisorLen];

			while (j > 0)
			{
				ulong dividend = ((ulong)remainder[pos] << 32) + remainder[pos - 1];

				ulong qHat = dividend / firstDivisorByte;
				ulong rHat = dividend % firstDivisorByte;

				bool done = false;
				while (!done)
				{
					done = true;

					if (qHat != 0x100000000 && (qHat * secondDivisorByte) <= ((rHat << 32) + remainder[pos - 2]))
						continue;

					qHat--;
					rHat += firstDivisorByte;

					if (rHat < 0x100000000)
						done = false;
				}

				for (int h = 0; h < divisorLen; h++)
					dividendPart[h] = remainder[pos - h];

				BigInteger kk = new BigInteger(dividendPart);
				BigInteger ss = bi2 * (long)qHat;
				while (ss > kk)
				{
					qHat--;
					ss -= bi2;
				}
				BigInteger yy = kk - ss;

				for (int h = 0; h < divisorLen; h++)
					remainder[pos - h] = yy._data[bi2._actualLength - h];

				result[resultPos++] = (uint)qHat;
				pos--;
				j--;
			}

			outQuotient._actualLength = resultPos;
			int y = 0;
			for (int x = outQuotient._actualLength - 1; x >= 0; x--, y++)
				outQuotient._data[y] = result[x];
			Utils.ZeroMemory(outQuotient._data);

			CollapseZeroSequence(outQuotient);
			CheckActualLength(outQuotient);
			
			outRemainder._actualLength = ShiftRight(remainder, shift);
			Array.Copy(remainder, outRemainder._data, outRemainder._actualLength);
			Utils.ZeroMemory(outRemainder._data);
		}

		public int BitCount()
		{
			CollapseZeroSequence(this);
			
			uint value = _data[_actualLength - 1];
			uint mask = 0x80000000;
			int bits = 32;

			while (bits > 0 && (value & mask) == 0)
			{
				bits--;
				mask >>= 1;
			}
			bits += ((_actualLength - 1) << 5);
			return bits;
		}

		public byte[] GetBytes()
		{
			int numBits = BitCount();

			int numBytes = numBits >> 3;
			if ((numBits & 0x7) != 0)
				numBytes++;

			byte[] result = new byte[numBytes];


			int pos = 0;
			

			uint val;
			for (int i = 0; i < _actualLength - 1; i++, pos += 4)
			{
				val = _data[i];
				result[pos] = (byte)(val & 0xFF);
				val >>= 8;
				result[pos + 1] = (byte)(val & 0xFF);
				val >>= 8;
				result[pos + 2] = (byte)(val & 0xFF);
				val >>= 8;
				result[pos + 3] = (byte)(val & 0xFF);
			}

			uint tempVal;
			val = _data[_actualLength - 1];

			if ((tempVal = (val & 0xFF)) != 0)
				result[pos++] = (byte)tempVal;
			if ((tempVal = (val >> 8 & 0xFF)) != 0)
				result[pos++] = (byte)tempVal;
			if ((tempVal = (val >> 16 & 0xFF)) != 0)
				result[pos++] = (byte)tempVal;
			if ((tempVal = (val >> 24 & 0xFF)) != 0)
				result[pos] = (byte)tempVal;
			
			return result;
		}

		public static int Jacobi(BigInteger a, BigInteger b)
		{
			if ((b._data[0] & 0x1) == 0)
				throw (new ArgumentException("Jacobi defined only for odd integers."));

			if (a >= b) a %= b;

			if (IsZero(a)) return 0;
			if (IsOne(a)) return 1;

			if (a < 0)
			{
				if ((((b - 1)._data[0]) & 2) == 0)
					return Jacobi(-a, b);
				return -Jacobi(-a, b);
			}

			int e = 0;
			for (int index = 0; index < a._actualLength; index++)
			{
				uint mask = 1;
				for (int i = 0; i < 32; i++)
				{
					if ((a._data[index] & mask) != 0)
					{
						index = a._actualLength;
						break;
					}
					mask <<= 1;
					e++;
				}
			}

			BigInteger a1 = a >> e;

			int s = 1;
			if ((e & 1) != 0 && ((b._data[0] & 7) == 3 || (b._data[0] & 7) == 5))
				s = -1;

			if ((b._data[0] & 3) == 3 && (a1._data[0] & 3) == 3)
				s = -s;
			return a1._actualLength == 1 && a1._data[0] == 1 ? s : s * Jacobi(b%a1, a1);
		}

		private BigInteger BarrettReduction(BigInteger x, BigInteger n, BigInteger constant)
		{
			int k = n._actualLength,
				kPlusOne = k + 1,
				kMinusOne = k - 1;

			BigInteger q1 = new BigInteger();

			// q1 = x / b^(k-1)
			for (int i = kMinusOne, j = 0; i < x._actualLength; i++, j++)
				q1._data[j] = x._data[i];
			q1._actualLength = x._actualLength - kMinusOne;
			if (q1._actualLength <= 0)
				q1._actualLength = 1;


			BigInteger q2 = q1 * constant;
			BigInteger q3 = new BigInteger();

			// q3 = q2 / b^(k+1)
			for (int i = kPlusOne, j = 0; i < q2._actualLength; i++, j++)
				q3._data[j] = q2._data[i];
			q3._actualLength = q2._actualLength - kPlusOne;
			if (q3._actualLength <= 0)
				q3._actualLength = 1;


			// r1 = x mod b^(k+1)
			// i.e. keep the lowest (k+1) words
			BigInteger r1 = new BigInteger();
			int lengthToCopy = (x._actualLength > kPlusOne) ? kPlusOne : x._actualLength;
			for (int i = 0; i < lengthToCopy; i++)
				r1._data[i] = x._data[i];
			r1._actualLength = lengthToCopy;


			// r2 = (q3 * n) mod b^(k+1)
			// partial multiplication of q3 and n

			BigInteger r2 = new BigInteger();
			for (int i = 0; i < q3._actualLength; i++)
			{
				if (q3._data[i] == 0) continue;

				ulong mcarry = 0;
				int t = i;
				for (int j = 0; j < n._actualLength && t < kPlusOne; j++, t++)
				{
					// t = i + j
					ulong val = ((ulong)q3._data[i] * n._data[j]) + r2._data[t] + mcarry;

					r2._data[t] = (uint)(val & 0xFFFFFFFF);
					mcarry = (val >> 32);
				}

				if (t < kPlusOne)
					r2._data[t] = (uint)mcarry;
			}
			r2._actualLength = kPlusOne;
			while (r2._actualLength > 1 && r2._data[r2._actualLength - 1] == 0)
				r2._actualLength--;

			r1 -= r2;
			if (IsNegative(r1))  
			{
				BigInteger val = new BigInteger();
				val._data[kPlusOne] = 0x00000001;
				val._actualLength = kPlusOne + 1;
				r1 += val;
			}

			while (r1 >= n)
				r1 -= n;

			return r1;
		}

		private static BigInteger[] LucasSequenceHelper(BigInteger p, BigInteger q,
														BigInteger k, BigInteger n,
														BigInteger constant, int s)
		{
			BigInteger[] result = new BigInteger[3];

			if ((k._data[0] & 0x00000001) == 0)
				throw (new ArgumentException("Argument k must be odd."));

			int numbits = k.BitCount();
			uint mask = (uint) 0x1 << ((numbits & 0x1F) - 1);

			// v = v0, v1 = v1, u1 = u1, Q_k = Q^0
			BigInteger v = 2%n;
			BigInteger qK = 1%n;
			BigInteger v1 = p%n;
			BigInteger u1 = qK;
			bool flag = true;

			for (int i = k._actualLength - 1; i >= 0; i--)
			{
				while (mask != 0)
				{
					if (i == 0 && mask == 0x00000001) // last bit
						break;

					if ((k._data[i] & mask) != 0) // bit is set
					{
						// index doubling with addition

						u1 = (u1*v1)%n;

						v = ((v*v1) - (p*qK))%n;
						v1 = n.BarrettReduction(v1*v1, n, constant);
						v1 = (v1 - ((qK*q) << 1))%n;

						if (flag)
							flag = false;
						else
							qK = n.BarrettReduction(qK*qK, n, constant);

						qK = (qK*q)%n;
					}
					else
					{
						// index doubling
						u1 = ((u1*v) - qK)%n;

						v1 = ((v*v1) - (p*qK))%n;
						v = n.BarrettReduction(v*v, n, constant);
						v = (v - (qK << 1))%n;

						if (flag)
						{
							qK = q%n;
							flag = false;
						}
						else
							qK = n.BarrettReduction(qK*qK, n, constant);
					}

					mask >>= 1;
				}
				mask = 0x80000000;
			}

			// at this point u1 = u(n+1) and v = v(n)
			// since the last bit always 1, we need to transform u1 to u(2n+1) and v to v(2n+1)

			u1 = ((u1*v) - qK)%n;
			v = ((v*v1) - (p*qK))%n;
			
			if (!flag)
				qK = n.BarrettReduction(qK*qK, n, constant);

			qK = (qK*q)%n;


			for (int i = 0; i < s; i++)
			{
				// index doubling
				u1 = (u1*v)%n;
				v = ((v*v) - (qK << 1))%n;

				qK = n.BarrettReduction(qK*qK, n, constant);
			}

			result[0] = u1;
			result[1] = v;
			result[2] = qK;

			return result;
		}

		public static BigInteger[] LucasSequence(BigInteger p, BigInteger q,
												 BigInteger k, BigInteger n)
		{
			if (IsZero(k))
			{
				BigInteger[] result = new BigInteger[3];

				result[0] = 0; result[1] = 2 % n; result[2] = 1 % n;
				return result;
			}

			// calculate constant = b^(2k) / m for Barrett Reduction
			BigInteger constant = new BigInteger();

			int nLen = n._actualLength << 1;
			constant._data[nLen] = 0x00000001;
			constant._actualLength = nLen + 1;
			constant = constant / n;

			// calculate values of s and t
			int s = 0;

			for (int index = 0; index < k._actualLength; index++)
			{
				uint mask = 0x01;

				for (int i = 0; i < 32; i++)
				{
					if ((k._data[index] & mask) != 0)
					{
						index = k._actualLength;
						break;
					}
					mask <<= 1;
					s++;
				}
			}

			BigInteger t = k >> s;
			return LucasSequenceHelper(p, q, t, n, constant, s);
		}


		public Int64 AsInt64()
		{
			Int64 val = _data[0];
			try
			{       // exception if maxLength = 1
				val |= (Int64)_data[1] << 32;
			}
			catch (Exception)
			{
				if ((_data[0] & 0x80000000) != 0) // negative
					val = (int)_data[0];
			}
			return val;
		}

		public Int32 AsInt32()
		{
			return (int)_data[0];
		}

		public UInt32 AsUint32()
		{
			return _data[0];
		}

	}
}

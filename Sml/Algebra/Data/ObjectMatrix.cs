// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Sml.Common;
using Sml.Common.Visitors;

namespace Sml.Algebra.Data
{
	[Serializable]
	public class ObjectMatrix<T> : IMatrix<T>, ICollection<T>, IVisitable<T>
	{
		protected T[] _data;
		protected int _columnCount;
		protected int _rowCount;

		/// <summary>
		/// Создание нового экземпляра класса <see cref="ObjectMatrix&lt;T&gt;"/>.
		/// </summary>
		/// <param name="rowCount">Количество строк матрицы</param>
		/// <param name="columns">Количество колонок матрицы</param>
		public ObjectMatrix(int rowCount, int columns)
		{
			if (rowCount <= 0)
				throw new ArgumentException("RowsOrColumnsInvalid", "rowCount");

			if (columns <= 0)
				throw new ArgumentException("RowsOrColumnsInvalid", "columns");

			_columnCount = columns;
			_rowCount = rowCount;
			_data = new T[_rowCount * _columnCount];
		}

	    /// <summary>
	    /// Создание нового экземпляра класса <see cref="ObjectMatrix&lt;T&gt;"/>.
	    /// </summary>
	    /// <param name="rowCount">Количество строк матрицы</param>
	    /// <param name="columns">Количество колонок матрицы</param>
        /// <param name="data">Данные в виде двумерного массива</param>
	    internal ObjectMatrix(int rowCount, int columns, T[,] data)
			: this(rowCount, columns)
		{
			for (int i = 0; i < rowCount; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					_data[(i*columns) + j] = data[i, j];
				}
			}
		}

	    /// <summary>
	    /// Создание нового экземпляра класса <see cref="ObjectMatrix&lt;T&gt;"/>.
	    /// </summary>
	    /// <param name="rowCount">Количество строк матрицы</param>
	    /// <param name="columns">Количество колонок матрицы</param>
        /// <param name="pData">Данные в виде одномерного массива</param>
	    internal ObjectMatrix(int rowCount, int columns, T[] pData)
		{
			_columnCount = columns;
			_rowCount = rowCount;
			_data = pData;
		}

		/// <summary>
		/// Получить количество строк
		/// </summary>
		public int RowCount
		{
			get { return _rowCount; }
		}

		/// <summary>
		/// Получить количество колонок
		/// </summary>
		public int ColumnCount
		{
			get { return _columnCount; }
		}

		/// <summary>
		/// Получить количество элементов матрицы.
		/// </summary>
		/// <returns> Количество элементов матрицы.</returns>
		public int Count
		{
			get { return _data.Length; }
		}

		/// <summary>
		/// Получить данные матрицы в виде массива
		/// </summary>
		internal T[] Data
		{
			get { return _data; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		/// <summary>
		/// Получить или задать значение, указывающее, является ли данная матрица квадратной
		/// </summary>
		/// <value> <c>true</c> если матрица квадратная; иначе, <c>false</c>. </value>
		public bool IsSquare
		{
			get { return (_rowCount == _columnCount); }
		}

		/// <summary>
		/// Получить или задать данные в определенной ячейке
		/// </summary>
		public T this[int row, int column]
		{
			get
			{
				CheckIndexValid(row, column);
				return _data[GetOffset(row, column)];
			}
			set
			{
				CheckIndexValid(row, column);
				_data[GetOffset(row, column)] = value;
			}
		}


		internal T GetValue(int row, int column)
		{
			return _data[GetOffset(row, column)];
		}

		protected int GetOffset(int row, int column)
		{
			return ((row * _columnCount) + column);
		}

		private void CheckIndexValid(int i, int j)
		{
			if ((i < 0) || (i > (_rowCount - 1)))
				throw new ArgumentOutOfRangeException("i");

			if ((j < 0) || (j > (_columnCount - 1)))
				throw new ArgumentOutOfRangeException("j");
		}

		private void CopyData(T[] newData, int newColumnCount)
		{
			int noOfColumns;
			if (_columnCount < newColumnCount)
			{
				noOfColumns = _columnCount;
			}
			else
			{
				noOfColumns = newColumnCount;
			}
			for (int i = 0; i < _rowCount; i++)
			{
				Array.Copy(_data, i * _columnCount, newData, i * newColumnCount, noOfColumns);
			}
		}

		internal void SetValue(int row, int column, T value)
		{
			_data[GetOffset(row, column)] = value;
		}

		public void Accept(IVisitor<T> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			for (int i = 0; i < _data.Length; i++)
			{
				visitor.Visit(_data[i]);
			}
		}

		/// <summary>
		/// Добавить колонку в матрицу
		/// </summary>
		public void AddColumn()
		{
			AddColumns(1);
		}


		/// <summary>
		///  Добавить колонку в матрицу
		/// </summary>
		/// <param name="values">Данные для колонки.</param>
		/// <remarks>Количество данных не должно привышать количества строк</remarks>
		public void AddColumn(params T[] values)
		{
			Guard.ArgumentNotNull(values, "values");
			if (values.Length > _rowCount)
				throw new ArgumentException("NumberOfValuesDoNotAgreeWithNumberOfRows", "values");

			AddColumn();
			for (int i = 0; i < values.Length; i++)
				SetValue(i, _columnCount - 1, values[i]);
		}

		/// <summary>
		/// Добавить колонки в матрицу
		/// </summary>
		/// <param name="columnCount">Количество добавленых колонок</param>
		public void AddColumns(int columnCount)
		{
			if (columnCount <= 0)
				throw new ArgumentOutOfRangeException("columnCount");
			int newColumnCount = _columnCount + columnCount;
			T[] newData = new T[_rowCount*newColumnCount];
			CopyData(newData, newColumnCount);
			_columnCount = newColumnCount;
			_data = newData;
		}

		/// <summary>
		/// Добавить строку в матрицу
		/// </summary>
		public void AddRow()
		{
			AddRows(1);
		}

		/// <summary>
		///Добавить строку в матрицу
		/// </summary>
		/// <param name="values">Данные для строки</param>
		/// <remarks>Количество данных не должно привышать количества колонок</remarks>
		public void AddRow(params T[] values)
		{
			Guard.ArgumentNotNull(values, "values");
			if (values.Length > _columnCount)
			{
				throw new ArgumentException("NumberOfValuesDoNotAgreeWithNumberOfColumns", "values");
			}
			AddRow();
			for (int i = 0; i < values.Length; i++)
			{
				SetValue(_rowCount - 1, i, values[i]);
			}
		}

		/// <summary>
		/// Добавить строки в матрицу
		/// </summary>
		/// <param name="rowCount">Количество добавленых строк</param>
		public void AddRows(int rowCount)
		{
			if (rowCount <= 0)
				throw new ArgumentOutOfRangeException("rowCount");

			int num = _rowCount + rowCount;
			T[] newData = new T[num*_columnCount];
			CopyData(newData, _columnCount);
			_rowCount = num;
			_data = newData;
		}

		

		/// <summary>
		/// Очистка матрицы.
		/// </summary>
		public void Clear()
		{
			_data = new T[_data.Length];
		}

		/// <summary>
		/// Проверка содержит ли матрица указанное значение.
		/// </summary>
		/// <param name="item">Значение для поиска.</param>
		/// <returns> <c>true</c> если значение найдено; иначе, <c>false</c>. </returns>
		public bool Contains(T item)
		{
			for (int i = 0; i < _data.Length; i++)
			{
				if (_data[i].Equals(item))
				{
					return true;
				}
			}
			return false;
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
			if ((array.Length - arrayIndex) < Count)
			{
				throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");
			}
			Array.Copy(_data, 0, array, arrayIndex, _data.Length);
		}

		/// <summary>
		/// Удалить колонку матрицы по индексу
		/// </summary>
		/// <param name="colIndex">Индекс колонки для удаления</param>
		public void DeleteColumn(int colIndex)
		{
			if (_columnCount == 1)
				throw new InvalidOperationException("MatrixDeleteOnlyColumn");

			if ((colIndex > (_columnCount - 1)) || (colIndex < 0))
				throw new ArgumentOutOfRangeException("column");

			T[] localArray = new T[_rowCount*(_columnCount - 1)];
			for (int i = 0; i < _rowCount; i++)
			{
				int num2 = 0;
				for (int j = 0; j < _columnCount; j++)
				{
					if (j != colIndex)
					{
						localArray[(i*(_columnCount - 1)) + num2] = GetValue(i, j);
						num2++;
					}
				}
			}
			_data = localArray;
			_columnCount--;
		}

		/// <summary>
		/// Удалить строку матрицы по индексу
		/// </summary>
		/// <param name="rowIndex">Индекс строки для удаления</param>
		public void DeleteRow(int rowIndex)
		{
			if (_rowCount == 1)
				throw new InvalidOperationException("MatrixDeleteOnlyRow");

			int num = _rowCount - 1;
			if ((rowIndex > num) || (rowIndex < 0))
				throw new ArgumentOutOfRangeException("rowIndex");

			T[] destinationArray = new T[num*ColumnCount];
			Array.Copy(_data, 0, destinationArray, 0, rowIndex*ColumnCount);
			Array.Copy(_data, (rowIndex + 1)*ColumnCount, destinationArray, rowIndex*ColumnCount,
			           ColumnCount*(num - rowIndex));
			_data = destinationArray;
			_rowCount--;
		}

		/// <summary>
		/// Получить данные колонки в виде массива
		/// </summary>
		/// <param name="colIndex">Индекс колонки.</param>
		/// <returns>Массив данных колонки</returns>
		public T[] GetColumn(int colIndex)
		{
			if ((colIndex < 0) || (colIndex > (_columnCount - 1)))
			{
				throw new ArgumentOutOfRangeException("colIndex");
			}
			T[] localArray = new T[_rowCount];
			for (int i = 0; i < _rowCount; i++)
			{
				localArray[i] = GetValue(i, colIndex);
			}
			return localArray;
		}

		/// <summary>
		/// Получить данные строки в виде массива
		/// </summary>
		/// <param name="rowIndex">Индекс строки.</param>
		/// <returns>Массив данных строки</returns>
		public T[] GetRow(int rowIndex)
		{
			if ((rowIndex < 0) || (rowIndex > (_rowCount - 1)))
			{
				throw new ArgumentOutOfRangeException("rowIndex");
			}
			T[] localArray = new T[_columnCount];
			for (int i = 0; i < _columnCount; i++)
			{
				localArray[i] = GetValue(rowIndex, i);
			}
			return localArray;
		}

		public IEnumerator<T> GetEnumerator()
		{
			int index = 0;
			while (true)
			{
				if (index >= _data.Length)
				{
					yield break;
				}
				yield return _data[index];
				index++;
			}
		}

		/// <summary>
		/// Получить подматрицу
		/// </summary>
		/// <param name="rowStart">Стартовая строка</param>
		/// <param name="columnStart">Стартовая колонка.</param>
		/// <param name="rowCount">Количество строк.</param>
		/// <param name="columnCount">Количество колонок.</param>
		/// <returns></returns>
		public ObjectMatrix<T> GetSubMatrix(int rowStart, int columnStart, int rowCount, int columnCount)
		{
			if (rowCount <= 0)
			{
				throw new ArgumentOutOfRangeException("rowCount", "ColumnAndRowCountBiggerThan");
			}
			if (columnCount <= 0)
			{
				throw new ArgumentOutOfRangeException("columnCount", "ColumnAndRowCountBiggerThan");
			}
			if (rowStart < 0)
			{
				throw new ArgumentOutOfRangeException("rowStart", "RowStartCanNotBeSmallerThanZero");
			}
			if (columnStart < 0)
			{
				throw new ArgumentOutOfRangeException("columnCount", "ColumnStartCanNotBeSmallerThanZero");
			}
			if (((rowStart + rowCount) > RowCount) || ((columnStart + columnCount) > ColumnCount))
			{
				throw new ArgumentOutOfRangeException("rowStart", "TooManyRowsOrColumns");
			}
			ObjectMatrix<T> matrix = new ObjectMatrix<T>(rowCount, columnCount);
			for (int i = rowStart; i < (rowStart + rowCount); i++)
			{
				for (int j = columnStart; j < (columnStart + columnCount); j++)
				{
					matrix.SetValue(i - rowStart, j - columnStart, GetValue(i, j));
				}
			}
			return matrix;
		}



		/// <summary>
		/// Поменять колонки местами
		/// </summary>
		/// <param name="firstColumn">Индекс первой колонки</param>
		/// <param name="secondColumn">Индекс второй колонки.</param>
		public void SwapColumns(int firstColumn, int secondColumn)
		{
			if ((firstColumn < 0) || (firstColumn > (_columnCount - 1)))
			{
				throw new ArgumentOutOfRangeException("firstColumn");
			}
			if ((secondColumn < 0) || (secondColumn > (_columnCount - 1)))
			{
				throw new ArgumentOutOfRangeException("secondColumn");
			}
			if (firstColumn != secondColumn)
			{
				for (int i = 0; i < _rowCount; i++)
				{
					T local = GetValue(i, firstColumn);
					SetValue(i, firstColumn, GetValue(i, secondColumn));
					SetValue(i, secondColumn, local);
				}
			}
		}

		/// <summary>
		/// Поменять строки местами
		/// </summary>
		/// <param name="firstRow">Индекс первой строки.</param>
		/// <param name="secondRow">Индекс второй строки.</param>
		public void SwapRows(int firstRow, int secondRow)
		{
			if ((firstRow < 0) || (firstRow > (_rowCount - 1)))
			{
				throw new ArgumentOutOfRangeException("firstRow");
			}
			if ((secondRow < 0) || (secondRow > (_rowCount - 1)))
			{
				throw new ArgumentOutOfRangeException("secondRow");
			}
			if (firstRow != secondRow)
			{
				for (int i = 0; i < _columnCount; i++)
				{
					T local = GetValue(firstRow, i);
					SetValue(firstRow, i, GetValue(secondRow, i));
					SetValue(secondRow, i, local);
				}
			}
		}

		IMatrix<T> IMatrix<T>.GetSubMatrix(int rowStart, int noOfColumnStart, int rowCount, int columnCount)
		{
			return GetSubMatrix(rowStart, noOfColumnStart, rowCount, columnCount);
		}

		/// <summary>
		/// Изменить размер матрицы
		/// </summary>
		/// <param name="newNumberOfRows">Количество строк новой матрицы</param>
		/// <param name="newNumberOfColumns">Количество колонок новой матрицы</param>
		public void Resize(int newNumberOfRows, int newNumberOfColumns)
		{
			if (newNumberOfRows <= 0)
				throw new ArgumentException("RowsOrColumnsInvalid", "newNumberOfRows");

			if (newNumberOfColumns <= 0)
				throw new ArgumentException("RowsOrColumnsInvalid", "newNumberOfColumns");

			T[] localArray = new T[newNumberOfRows*newNumberOfColumns];
			int num = Math.Min(_rowCount, newNumberOfRows);
			int num2 = Math.Min(_columnCount, newNumberOfColumns);
			for (int i = 0; i < num; i++)
			{
				for (int j = 0; j < num2; j++)
					localArray[(newNumberOfRows*i) + j] = GetValue(i, j);
			}
			_data = localArray;
			_rowCount = newNumberOfRows;
			_columnCount = newNumberOfColumns;
		}

		

		void ICollection<T>.Add(T item)
		{
			throw new NotSupportedException();
		}

		bool ICollection<T>.Remove(T item)
		{
			throw new NotSupportedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		/// <summary>
		/// Представить матрицу в виде двумерного массива
		/// </summary>
		/// <returns>Двумерный массив содержащий данные о матрице</returns>
		public T[,] ToArray()
		{
			T[,] localArray = new T[RowCount,ColumnCount];
			for (int i = 0; i < RowCount; i++)
			{
				for (int j = 0; j < ColumnCount; j++)
					localArray[i, j] = GetValue(i, j);
			}
			return localArray;
		}

		/// <summary>
		/// Представить матрицу в виде строки
		/// </summary>
		/// <returns> Строка содержащая данные о матрице </returns>
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder((_rowCount*_columnCount)*2);
			for (int i = 0; i < _rowCount; i++)
			{
				for (int j = 0; j < _columnCount; j++)
				{
					builder.Append(GetValue(i, j)).Append("\t");
				}
				builder.AppendLine();
			}
			return builder.ToString();
		}

		

		
	}
}

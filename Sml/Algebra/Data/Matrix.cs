// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Sml.Algebra.Algorithms.MatrixDecomposition;
using Sml.Common;

namespace Sml.Algebra.Data
{
    [Serializable]
    public class Matrix : ObjectMatrix<double>, IMathematicalMatrix, ICloneable, IEquatable<Matrix>, ICollection<double>
    {
        /// <summary>
        /// Создание нового экземпляра класса <see cref="Matrix"/>.
        /// </summary>
        /// <param name="rowCount">Количество строк матрицы</param>
        /// <param name="colCount">Количество колонок матрицы</param>
        public Matrix(int rowCount, int colCount)
            : base(rowCount, colCount)
        {
        }

        /// <summary>
        /// Создание нового экземпляра класса <see cref="Matrix"/>.
        /// </summary>
        /// <param name="rowCount">Количество строк матрицы</param>
        /// <param name="colCount">Количество колонок матрицы</param>
        /// <param name="data">Данные в виде двумерного массива</param>
        public Matrix(int rowCount, int colCount, double[,] data)
            : base(rowCount, colCount, data)
        {
        }

        /// <summary>
        /// Создание нового экземпляра класса <see cref="Matrix"/>.
        /// </summary>
        /// <param name="rowCount">Количество строк матрицы</param>
        /// <param name="colCount">Количество колонок матрицы</param>
        /// <param name="data">Данные в виде одномерного массива</param>
        public Matrix(int rowCount, int colCount, double[] data)
            : base(rowCount, colCount, data)
        {
        }

        /// <summary>
        /// Создание нового экземпляра класса <see cref="Matrix"/>.
        /// </summary>
        /// <param name="squareSide">Сторона квадратной матрицы</param>
        public Matrix(int squareSide)
            : this(squareSide, squareSide)
        {
        }

        /// <summary>
        /// Копирующий конструктор
        /// </summary>
        /// <param name="origin">Исходная матрица</param>
        public Matrix(Matrix origin)
            : this(origin._rowCount, origin._columnCount, (double[]) origin._data.Clone())
        {
        }

        public Matrix Add(IMatrix<double> matrix)
        {
            return AddInternal(this, matrix);
        }

        IMathematicalMatrix IMathematicalMatrix.Add(IMathematicalMatrix matrix)
        {
            return AddInternal(this, matrix);
        }

        private static Matrix AddInternal(IMatrix<double> leftMatrix, IMatrix<double> rightMatrix)
        {
            Guard.ArgumentNotNull(leftMatrix, "leftMatrix");
            Guard.ArgumentNotNull(rightMatrix, "rightMatrix");
            if ((leftMatrix.RowCount != rightMatrix.RowCount) || (leftMatrix.ColumnCount != rightMatrix.ColumnCount))
            {
                throw new ArgumentException("IncompatibleMatricesSameSize", "rightMatrix");
            }
            Matrix ret = new Matrix(leftMatrix.RowCount, leftMatrix.ColumnCount);
            for (int i = 0; i < leftMatrix.RowCount; i++)
            {
                for (int j = 0; j < leftMatrix.ColumnCount; j++)
                {
                    ret.SetValue(i, j, leftMatrix[i, j] + rightMatrix[i, j]);
                }
            }
            return ret;
        }

        /// <summary>
        /// Получить сопряженную матрицу
        /// </summary>
        /// <returns></returns>
        public IMathematicalMatrix Adjoint()
        {
            if (!IsSquare)
            {
                throw new InvalidOperationException("MatrixMustBeSquare");
            }
            Matrix tmp = new Matrix(RowCount, ColumnCount);
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    tmp.SetValue(i, j, Math.Pow(-1.0, i + j)*Minor(i, j).Determinant());
                }
            }
            return tmp.Transpose();
        }

        public void ChangeSignColumn(int columnIndex)
        {
            if ((columnIndex < 0) || (columnIndex > (_columnCount - 1)))
            {
                throw new ArgumentOutOfRangeException("columnIndex");
            }
            for (int i = 0; i < _rowCount; i++)
            {
                SetValue(i, columnIndex, -GetValue(i, columnIndex));
            }
        }

        public void ChangeSignRow(int rowIndex)
        {
            if ((rowIndex < 0) || (rowIndex > (_rowCount - 1)))
            {
                throw new ArgumentOutOfRangeException("rowIndex");
            }
            for (int i = 0; i < _columnCount; i++)
            {
                SetValue(rowIndex, i, -GetValue(rowIndex, i));
            }
        }

        public Matrix Clone()
        {
            return new Matrix(_rowCount, _columnCount, (double[]) _data.Clone());
        }

        public Matrix Concatenate(IMatrix<double> rightMatrix)
        {
            return ConcatenateInternal(this, rightMatrix);
        }

        IMathematicalMatrix IMathematicalMatrix.Concatenate(IMathematicalMatrix rightMatrix)
        {
            return ConcatenateInternal(this, rightMatrix);
        }

        private static Matrix ConcatenateInternal(IMatrix<double> leftMatrix, IMatrix<double> rightMatrix)
        {
            Guard.ArgumentNotNull(rightMatrix, "rightMatrix");
            if (leftMatrix.RowCount != rightMatrix.RowCount)
                throw new ArgumentException("MatrixRowCountDiffers");
            Matrix newMatrix = new Matrix(leftMatrix.RowCount, leftMatrix.ColumnCount + rightMatrix.ColumnCount);
            for (int i = 0; i < leftMatrix.RowCount; i++)
            {
                for (int j = 0; j < leftMatrix.ColumnCount; j++)
                {
                    newMatrix.SetValue(i, j, leftMatrix[i, j]);
                }
                for (int j = 0; j < rightMatrix.ColumnCount; j++)
                {
                    newMatrix.SetValue(i, j + leftMatrix.ColumnCount, rightMatrix[i, j]);
                }
            }
            return newMatrix;
        }

        public double Determinant()
        {
            return new LUDecomposition(this).Determinant();
        }

        public static Matrix Diagonal(int rows, int columns, double value)
        {
            Matrix matrix = new Matrix(rows, columns);
            int n = Math.Min(rows, columns);
            for (int i = 0; i < n; i++)
            {
                matrix.SetValue(i, i, value);
            }
            return matrix;
        }

        public override bool Equals(object obj)
        {
            Matrix mtx = obj as Matrix;
            if (mtx == null)
                return false;
            return Equals(mtx);
        }

        public bool Equals(Matrix other)
        {
            return Equals(other, 0);
        }

        public bool Equals(Matrix other, double precision)
        {
            if (other == null)
                return false;

            if (other.RowCount != RowCount)
                return false;

            if (other.ColumnCount != ColumnCount)
                return false;

            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    if (!(Math.Abs(GetValue(i, j) - other[i, j]) <= precision))
                        return false;
                }
            }
            return true;
        }

        internal Matrix GetSubMatrix(int[] rows, int columnStart, int columnEnd)
        {
            Matrix tmp = new Matrix(rows.Length, (columnEnd - columnStart) + 1);
            for (int i = 0; i < rows.Length; i++)
            {
                for (int j = columnStart; j <= columnEnd; j++)
                    tmp.SetValue(i, j - columnStart, GetValue(rows[i], j));
            }
            return tmp;
        }

        public new Matrix GetSubMatrix(int rowStart, int columnStart, int rowCount, int columnCount)
        {
            ObjectMatrix<double> objectMatrix = base.GetSubMatrix(rowStart, columnStart, rowCount, columnCount);
            return new Matrix(rowCount, columnCount, objectMatrix.Data);
        }

        /// <summary>
        /// Получить подматрицу, ограниченную столбцами и строками с заданными номерами.
        /// (!) Граничные столбцы и строки включаются
        /// </summary>
        public Matrix GetSubMatrixLimited(int topRow, int bottomRow, int leftColumn, int rightColumn)
        {
            if (topRow < 0 || bottomRow >= _rowCount || leftColumn < 0 || rightColumn >= _columnCount)
                throw new Exception("Выход за границу матрицы");
            if (topRow > bottomRow || leftColumn > rightColumn)
                throw new Exception("Неправильное выделение");

            Matrix res = new Matrix(bottomRow - topRow + 1, rightColumn - leftColumn + 1);
            for (int i = 0; i < res._rowCount; i++)
                for (int j = 0; j < res._columnCount; j++)
                    res[i, j] = this[i + topRow, j + leftColumn];
            return res;
        }

        /// <summary>
        /// Получить подматрицу, ограниченную столбцами и строками с заданными номерами.
        /// (!) Граничные столбцы и строки включаются
        /// </summary>
        public Matrix GetSubMatrixLimited(int bottomRow, int rightColumn)
        {
            return GetSubMatrixLimited(0, bottomRow, 0, rightColumn);
        }

        public static Matrix IdentityMatrix(int rows, int columns)
        {
            return Diagonal(rows, columns, 1.0);
        }

        Matrix Inverse()
        {
            return Solve(IdentityMatrix(_rowCount, _rowCount));
        }

        IMathematicalMatrix IMathematicalMatrix.Inverse()
        {
            return Solve(IdentityMatrix(_rowCount, _rowCount));
        }

        public static Matrix LinearSolve(Matrix leftMatrix, Matrix rightMatrix)
        {
            Guard.ArgumentNotNull(leftMatrix, "leftMatrix");
            return (leftMatrix.Inverse()*rightMatrix);
        }

        /// <summary>
        /// Получение минора матрицы
        /// </summary>
        /// <param name="row">Строка, которая вычеркивается</param>
        /// <param name="column">Колонка, которая вычеркивается</param>
        /// <returns>Минор</returns>
        public Matrix Minor(int row, int column)
        {
            if ((row > (RowCount - 1)) || (row < 0))
                throw new ArgumentOutOfRangeException("row");
            if ((column > (ColumnCount - 1)) || (column < 0))
                throw new ArgumentOutOfRangeException("column");

            Matrix minor = new Matrix(RowCount - 1, ColumnCount - 1);
            int m = 0;
            for (int i = 0; i < RowCount; i++)
            {
                if (i != row)
                {
                    int n = 0;
                    for (int j = 0; j < ColumnCount; j++)
                    {
                        if (j != column)
                        {
                            minor.SetValue(m, n, GetValue(i, j));
                            n++;
                        }
                    }
                    m++;
                }
            }
            return minor;
        }

        IMathematicalMatrix IMathematicalMatrix.Minor(int row, int column)
        {
            return Minor(row, column);
        }

        public Matrix Multiply(IMatrix<double> matrix)
        {
            Guard.ArgumentNotNull(matrix, "matrix");
            if (_columnCount != matrix.RowCount)
                throw new ArgumentException("IncompatibleMatricesTimes", "matrix");
            Matrix ret = new Matrix(_rowCount, matrix.ColumnCount);
            for (int i = 0; i < _rowCount; i++)
            {
                for (int j = 0; j < matrix.ColumnCount; j++)
                {
                    double sum = 0.0;
                    for (int k = 0; k < _columnCount; k++)
                    {
                        sum += GetValue(i, k) * matrix[k, j];
                    }
                    ret.SetValue(i, j, sum);
                }
            }
            return ret;
        }

        IMathematicalMatrix IMathematicalMatrix.Multiply(IMathematicalMatrix matrix)
        {
            return Multiply(matrix);
        }

        public Matrix Multiply(double number)
        {
            Matrix ret = new Matrix(_rowCount, _columnCount);
            for (int i = 0; i < _rowCount; i++)
            {
                for (int j = 0; j < _columnCount; j++)
                {
                    ret.SetValue(i, j, GetValue(i, j)*number);
                }
            }
            return ret;
        }

        IMathematicalMatrix IMathematicalMatrix.Multiply(double number)
        {
            return Multiply(number);
        }

        public void MultiplyColumn(int column, double factor)
        {
            if ((column > (ColumnCount - 1)) || (column < 0))
                throw new ArgumentOutOfRangeException("column");

            for (int i = 0; i < RowCount; i++)
                SetValue(i, column, GetValue(i, column)*factor);
        }

        public void MultiplyRow(int row, double factor)
        {
            if ((row > (RowCount - 1)) || (row < 0))
            {
                throw new ArgumentOutOfRangeException("row");
            }
            for (int i = 0; i < ColumnCount; i++)
            {
                SetValue(row, i, GetValue(row, i)*factor);
            }
        }

        public Matrix Negate()
        {
            return this * -1.0;
        }

        IMathematicalMatrix IMathematicalMatrix.Negate()
        {
            return Negate();
        }

        //IMatrix<double> IMatrix<double>.GetSubMatrix(int rowStart, int columnStart, int rowCount, int columnCount)
        //{
        //    return GetSubMatrix(rowStart, columnStart, rowCount, columnCount);
        //}

        public Matrix Subtract(IMatrix<double> matrix)
        {
            Guard.ArgumentNotNull(matrix, "matrix");
            if ((_rowCount != matrix.RowCount) || (_columnCount != matrix.ColumnCount))
                throw new ArgumentException("IncompatibleMatricesSameSize", "matrix");

            Matrix ret = new Matrix(_rowCount, _columnCount);
            for (int i = 0; i < _rowCount; i++)
            {
                for (int j = 0; j < _columnCount; j++)
                    ret.SetValue(i, j, GetValue(i, j) - matrix[i, j]);
            }
            return ret;
        }

        IMathematicalMatrix IMathematicalMatrix.Subtract(IMathematicalMatrix matrix)
        {
            return Subtract(matrix);
        }

        /// <summary>
        /// Перегруженный оператор (-) для получения противоположной матрицы.
        /// </summary>
        public static Matrix operator -(Matrix op)
        {
            return op.Negate();
        }

        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            Guard.ArgumentNotNull(m1, "m1");
            return m1.Add(m2);
        }

        /// <summary>
        /// Перегруженный оператор (+) для сложения матрицы и числа.
        /// </summary>
        public static Matrix operator +(Matrix matrix, double number)
        {
            if (matrix._rowCount != matrix._columnCount)
                throw new ArgumentException("Матрица должна быть квадратной");
            Matrix result = new Matrix(matrix);
            for (int i = 0; i < result._rowCount; i++)
                result[i, i] += number;
            return result;
        }

        /// <summary>
        /// Перегруженный оператор (+) для сложения числа и матрицы.
        /// </summary>
        public static Matrix operator +(double number, Matrix matrix)
        {
            return matrix + number;
        }

        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            Guard.ArgumentNotNull(m1, "m1");
            return m1.Subtract(m2);
        }

        /// <summary>
        /// Перегруженный оператор (-) для вычитания матрицы и числа.
        /// </summary>
        public static Matrix operator -(Matrix m, double number)
        {
            return m + -number;
        }

        /// <summary>
        /// Перегруженный оператор (-) для вычитания числа и матрицы.
        /// </summary>
        public static Matrix operator -(double number, Matrix matrix)
        {
            if (matrix._rowCount != matrix._columnCount)
                throw new ArgumentException("Матрица должна быть квадратной");
            Matrix result = -matrix;
            for (int i = 0; i < result._rowCount; i++)
                result[i, i] += number;
            return result;
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            Guard.ArgumentNotNull(m1, "m1");
            Guard.ArgumentNotNull(m1, "m2");
            return m1.Multiply(m2);
        }

        public static Matrix operator *(Matrix matrix, double number)
        {
            Guard.ArgumentNotNull(matrix, "matrix");
            return matrix.Multiply(number);
        }

        public static Matrix operator *(double number, Matrix matrix)
        {
            Guard.ArgumentNotNull(matrix, "matrix");
            return matrix.Multiply(number);
        }

        /// <summary>
        /// Перегруженный оператор (==) для сравнения двух матриц.
        /// </summary>
        public static bool operator ==(Matrix m1, Matrix m2)
        {
            if (ReferenceEquals(m1, m2))
                return true;
            if (ReferenceEquals(m1, null))
                return false;
            if (ReferenceEquals(m2, null))
                return false;
            return m1.Equals(m2);
        }

        /// <summary>
        /// Перегруженный оператор (!=) для сравнения двух матриц.
        /// </summary>
        public static bool operator !=(Matrix m1, Matrix m2)
        {
            return !(m1 == m2);
        }

        public double Rank()
        {
            return new LUDecomposition(this).Rank();
        }

        public Matrix Subtract(Matrix matrix)
        {
            Guard.ArgumentNotNull(matrix, "matrix");
            if ((_rowCount != matrix._rowCount) || (_columnCount != matrix._columnCount))
                throw new ArgumentException("IncompatibleMatricesSameSize", "matrix");
            Matrix ret = new Matrix(_rowCount, _columnCount);
            for (int i = 0; i < _rowCount; i++)
            {
                for (int j = 0; j < _columnCount; j++)
                {
                    ret.SetValue(i, j, GetValue(i, j) - matrix.GetValue(i, j));
                }
            }
            return ret;
        }

        void ICollection<double>.Add(double item)
        {
            throw new NotSupportedException();
        }

        bool ICollection<double>.Remove(double item)
        {
            throw new NotSupportedException();
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public Matrix Transpose()
        {
            Matrix ret = new Matrix(_columnCount, _rowCount);
            for (int i = 0; i < _rowCount; i++)
            {
                for (int j = 0; j < _columnCount; j++)
                {
                    ret.SetValue(j, i, GetValue(i, j));
                }
            }
            return ret;
        }

        IMathematicalMatrix IMathematicalMatrix.Transpose()
        {
            return Transpose();
        }

        /// <summary>
        /// Возвращает вторую норму матрицы - максимум суммы модулей элементов столбца.
        /// </summary>
        public double SecondNorm()
        {
            if (_rowCount != _columnCount)
                throw new InvalidOperationException("Матрица должна быть квадратной");
            double max = -1; // Правильно, норма матрицы всегда положительна
            for (int i = 0; i < _rowCount; i++)
            {
                double sum = 0;
                for (int j = 0; j < _columnCount; j++)
                    sum += Math.Abs(this[i, j]);
                if (max < sum)
                    max = sum;
            }
            return max;
        }

        public double FrobeniusNorm
        {
            get
            {
                double f = 0.0;
                for (int i = 0; i < _rowCount; i++)
                {
                    for (int j = 0; j < _columnCount; j++)
                    {
                        double d = GetValue(i, j);
                        f = Math.Sqrt((f*f) + (d*d));
                    }
                }
                return f;
            }
        }

        public double InfinityNorm
        {
            get
            {
                double f = 0.0;
                for (int i = 0; i < _rowCount; i++)
                {
                    double s = 0.0;
                    for (int j = 0; j < _columnCount; j++)
                        s += Math.Abs(GetValue(i, j));
                    if (f < s)
                        f = s;
                }
                return f;
            }
        }

        public bool IsDiagonal
        {
            get
            {
                if (_rowCount != _columnCount)
                    return false;
                for (int i = 0; i < _rowCount; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        if ((GetValue(i, j) != 0.0) || (GetValue(j, i) != 0.0))
                            return false;
                    }
                }
                return true;
            }
        }

        public bool IsPositiveDefinite
        {
            get
            {
                if (_rowCount != _columnCount)
                {
                    return false;
                }
                for (int i = 0; i < _rowCount; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        if (GetValue(i, j) != GetValue(j, i))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        public bool IsSingular
        {
            get { return (Determinant() == 0.0); }
        }

        public bool IsSymmetric
        {
            get
            {
                if (_rowCount != _columnCount)
                    return false;
                for (int i = 0; i < _rowCount; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        if (GetValue(i, j) != GetValue(j, i))
                            return false;
                    }
                }
                return true;
            }
        }

        public TriangularMatrixType IsTriangular
        {
            get
            {
                if (_rowCount != _columnCount)
                    return TriangularMatrixType.None;

                bool upper = true;
                bool lower = true;
                for (int i = 0; i < _rowCount; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        if (GetValue(i, j) != 0.0)
                        {
                            if (!lower) return TriangularMatrixType.None;
                            upper = false;
                        }
                        if (GetValue(j, i) != 0.0)
                        {
                            if (!upper) return TriangularMatrixType.None;
                            lower = false;

                        }
                    }
                }
                return CalculateType(upper, lower);
            }
        }

        private TriangularMatrixType CalculateType(bool upper, bool lower)
        {
            if (upper)
                return lower ? TriangularMatrixType.Diagonal : TriangularMatrixType.Upper;
            return lower ? TriangularMatrixType.Lower : TriangularMatrixType.None;
        }

        public double OneNorm
        {
            get
            {
                double f = 0.0;
                for (int j = 0; j < _columnCount; j++)
                {
                    double s = 0.0;
                    for (int i = 0; i < _rowCount; i++)
                        s += Math.Abs(GetValue(i, j));
                    if (f < s)
                        f = s;
                }
                return f;
            }
        }

        public double Trace
        {
            get
            {
                double trace = 0.0;
                int direction = Math.Min(_rowCount, _columnCount);
                for (int i = 0; i < direction; i++)
                    trace += GetValue(i, i);
                return trace;
            }
        }

        public Matrix Solve(Matrix rightHandSide)
        {
            IDecomposition decomposition;
            if (IsSquare)
                decomposition = new LUDecomposition(this);
            else
                decomposition = new QRDecomposition(this);
            return decomposition.Solve(rightHandSide);
        }

        public override int GetHashCode()
        {
            return _data.GetHashCode();
        }

        /// <summary>
        /// Является ли матрица матрицей с диагональным преобладанием.
        /// </summary>
        public bool HasDiagonalDominance()
        {
            if (_rowCount != _columnCount)
                throw new InvalidOperationException("Матрица должна быть квадратной");
            for (int i = 0; i < _rowCount; i++)
            {
                double sum = 0;
                for (int j = 0; j < _columnCount; j++)
                    sum += Math.Abs(this[i, j]);
                sum -= Math.Abs(this[i, i]);
                if (Math.Abs(this[i, i]) <= sum)
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Задать элементы матрицы через одномерный массив
        /// </summary>
        /// <param name="data">Элементы матрицы</param>
        public void SetNativeForm(params double[] data)
        {
            if (data.Length != _columnCount*_rowCount)
                throw new ArgumentException("Неверная длина массива");
            _data = (double[]) data.Clone();
            //int k = 0;
            //for (int i = 1; i <= _rowCount; i++)
            //{
            //    for (int j = 1; j <= _columnCount; j++)
            //    {
            //        this[i, j] = values[k];
            //        k++;
            //    }
            //}
        }

        /// <summary>
        /// Прибавляет к строке destinationRowIndex строку sourceRowIndex, умноженную на factor.
        /// </summary>
        public void SumRows(int sourceRowIndex, int destinationRowIndex, double factor)
        {
            if (sourceRowIndex == destinationRowIndex)
                throw new ArgumentException("Операция применима только к разным строкам");
            for (int i = 0; i < _columnCount; i++)
                this[destinationRowIndex, i] += this[sourceRowIndex, i]*factor;
        }
    }
}

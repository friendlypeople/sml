// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Sml.Algebra.Data;
using Sml.Common;

namespace Sml.Algebra.Algorithms.MatrixDecomposition
{
	[Serializable]
	public class QRDecomposition : IDecomposition
	{
		private double[] _diagonal;
		private Matrix _qr;

		public QRDecomposition(Matrix matrix)
		{
			Guard.ArgumentNotNull(matrix, "matrix");
			Decompose(matrix);
		}

		public void Decompose(Matrix matrix)
		{
			_qr = matrix.Clone();
			_diagonal = new double[_qr.ColumnCount];
			for (int k = 0; k < _qr.ColumnCount; k++)
			{
				double nrm = 0.0;
				for (int i = k; i < _qr.RowCount; i++)
				{
					double d = _qr[i, k];
					nrm = Math.Sqrt((nrm * nrm) + (d * d));
				}
				if (nrm != 0.0)
				{
					if (_qr.GetValue(k, k) < 0.0)
					{
						nrm = -nrm;
					}
					for (int i = k; i < _qr.RowCount; i++)
					{
						_qr.SetValue(i, k, _qr.GetValue(i, k)/nrm);
					}
					_qr.SetValue(k, k, _qr.GetValue(k, k) + 1.0);
					for (int j = k + 1; j < _qr.ColumnCount; j++)
					{
						double s = 0.0;
						for (int i = k; i < _qr.RowCount; i++)
						{
							s += _qr.GetValue(i, k)*_qr.GetValue(i, j);
						}
						s = -s/_qr.GetValue(k, k);
						for (int i = k; i < _qr.RowCount; i++)
						{
							_qr.SetValue(i, j, _qr.GetValue(i, j) + (s*_qr.GetValue(i, k)));
						}
					}
				}
				_diagonal[k] = -nrm;
			}
		}

		public Matrix Solve(Matrix right)
		{
			Guard.ArgumentNotNull(right, "right");
			if (right.RowCount != _qr.RowCount)
			{
				throw new ArgumentException("MatrixRowCountDiffers");
			}
			if (!IsFullRank)
			{
				throw new ArgumentException("MatrixIsRankDeficient");
			}
			int nx = right.ColumnCount;
			Matrix cloneMatrix = right.Clone();
			for (int k = 0; k < _qr.ColumnCount; k++)
			{
				for (int j = 0; j < nx; j++)
				{
					double s = 0.0;
					for (int i = k; i < _qr.RowCount; i++)
					{
						s += _qr.GetValue(i, k)*cloneMatrix.GetValue(i, j);
					}
					s = -s/_qr.GetValue(k, k);
					for (int i = k; i < _qr.RowCount; i++)
					{
						cloneMatrix.SetValue(i, j, cloneMatrix.GetValue(i, j) + (s*_qr.GetValue(i, k)));
					}
				}
			}
			for (int k = _qr.ColumnCount - 1; k >= 0; k--)
			{
				for (int j = 0; j < nx; j++)
				{
					cloneMatrix.SetValue(k, j, cloneMatrix.GetValue(k, j)/_diagonal[k]);
				}
				for (int i = 0; i < k; i++)
				{
					for (int j = 0; j < nx; j++)
					{
						cloneMatrix.SetValue(i, j, cloneMatrix.GetValue(i, j) - (cloneMatrix.GetValue(k, j)*_qr.GetValue(i, k)));
					}
				}
			}
			return cloneMatrix.GetSubMatrix(0, 0, _qr.ColumnCount, nx);
		}

		public Matrix H
		{
			get
			{
				Matrix matrix = new Matrix(_qr.RowCount, _qr.ColumnCount);
				for (int i = 0; i < _qr.RowCount; i++)
				{
					for (int j = 0; j < _qr.ColumnCount; j++)
					{
						matrix.SetValue(i, j, i >= j ? _qr.GetValue(i, j) : 0.0);
					}
				}
				return matrix;
			}
		}

		public bool IsFullRank
		{
			get
			{
				for (int j = 0; j < _qr.ColumnCount; j++)
				{
					if (_diagonal[j] == 0.0)
					{
						return false;
					}
				}
				return true;
			}
		}

		public Matrix LeftFactorMatrix
		{
			get { return OrthogonalFactor; }
		}

		public Matrix OrthogonalFactor
		{
			get
			{
				Matrix matrix = new Matrix(_qr.RowCount, _qr.ColumnCount);
				for (int k = _qr.ColumnCount - 1; k >= 0; k--)
				{
					for (int i = 0; i < _qr.RowCount; i++)
					{
						matrix.SetValue(i, k, 0.0);
					}
					matrix.SetValue(k, k, 1.0);
					for (int j = k; j < _qr.ColumnCount; j++)
					{
						if (_qr.GetValue(k, k) != 0.0)
						{
							double s = 0.0;
							for (int i = k; i < _qr.RowCount; i++)
							{
								s += _qr.GetValue(i, k)*matrix.GetValue(i, j);
							}
							s = -s/_qr.GetValue(k, k);
							for (int i = k; i < _qr.RowCount; i++)
							{
								matrix.SetValue(i, j, matrix.GetValue(i, j) + (s*_qr.GetValue(i, k)));
							}
						}
					}
				}
				return matrix;
			}
		}

		public Matrix RightFactorMatrix
		{
			get { return UpperTriangularMatrix; }
		}

		public Matrix UpperTriangularMatrix
		{
			get
			{
				Matrix matrix = new Matrix(_qr.ColumnCount, _qr.ColumnCount);
				for (int i = 0; i < _qr.ColumnCount; i++)
				{
					for (int j = 0; j < _qr.ColumnCount; j++)
					{
						if (i < j)
						{
							matrix.SetValue(i, j, _qr.GetValue(i, j));
						}
						else if (i == j)
						{
							matrix.SetValue(i, j, _diagonal[i]);
						}
						else
						{
							matrix.SetValue(i, j, 0.0);
						}
					}
				}
				return matrix;
			}
		}
	}
}

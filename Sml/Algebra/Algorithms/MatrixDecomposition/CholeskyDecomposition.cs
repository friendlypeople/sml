// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Sml.Algebra.Data;
using Sml.Common;

namespace Sml.Algebra.Algorithms.MatrixDecomposition
{
	public class CholeskyDecomposition : IDecomposition
	{
		private Matrix _leftFactorMatrix;
		private readonly int _dimension;

		public CholeskyDecomposition(Matrix matrix)
		{
			Guard.ArgumentNotNull(matrix, "matrix");
			_dimension = matrix.RowCount;
			Decompose(matrix);
		}

		public void Decompose(Matrix matrix)
		{
			Guard.ArgumentNotNull(matrix, "a");
			if (!matrix.IsSymmetric)
			{
				throw new ArgumentException("MatrixMustBeSymmetric");
			}
			int aRows = matrix.RowCount;
			Matrix res = new Matrix(aRows, aRows);
			for (int i = 0; i < aRows; i++)
			{
				for (int j = i; j < aRows; j++)
				{
					double sum = matrix[i, j];
					for (int k = i - 1; k >= 0; k--)
					{
						sum -= res[i, k] * res[j, k];
					}
					if (i == j)
					{
						if (sum <= 0.0)
						{
							throw new ArgumentException("InputMatrixNotPositiveDefinite");
						}
						res[i, i] = Math.Sqrt(sum);
					}
					else
					{
						res[j, i] = sum / res[i, i];
					}
				}
			}
			LeftFactorMatrix = res;
		}

		public static Matrix QuickDecompose(Matrix a)
		{
			Guard.ArgumentNotNull(a, "a");
			if (!a.IsSymmetric)
			{
				throw new ArgumentException("MatrixMustBeSymmetric");
			}
			int aRowCount = a.RowCount;
			Matrix res = new Matrix(aRowCount, aRowCount);
			for (int i = 0; i < aRowCount; i++)
			{
				for (int j = i; j < aRowCount; j++)
				{
					double sum = a[i, j];
					for (int k = i - 1; k >= 0; k--)
					{
						sum -= res[i, k] * res[j, k];
					}
					if (i == j)
					{
						if (sum <= 0.0)
						{
							throw new ArgumentException("InputMatrixNotPositiveDefinite");
						}
						res[i, i] = Math.Sqrt(sum);
					}
					else
					{
						res[j, i] = sum / res[i, i];
					}
				}
			}
			return res;
		}

		public static double[] QuickSolveLinearEquation(Matrix a, double[] b)
		{
			int k;
			double sum;
			Guard.ArgumentNotNull(a, "a");
			Guard.ArgumentNotNull(b, "b");
			if (!a.IsSymmetric)
			{
				throw new ArgumentException("MatrixMustBeSymmetric");
			}
			int aRowCount = a.RowCount;
			if (b.Length != aRowCount)
			{
				throw new ArgumentException("NonMatchingDimensions");
			}
			Matrix decomposeMatrix = QuickDecompose(a);
			double[] x = new double[aRowCount];
			for (int i = 0; i < aRowCount; i++)
			{
				sum = b[i];
				k = i - 1;
				while (k >= 0)
				{
					sum -= decomposeMatrix[i, k] * x[k];
					k--;
				}
				x[i] = sum / decomposeMatrix[i, i];
			}
			for (int i = aRowCount - 1; i >= 0; i--)
			{
				sum = x[i];
				for (k = i + 1; k < aRowCount; k++)
				{
					sum -= decomposeMatrix[k, i] * x[k];
				}
				x[i] = sum / decomposeMatrix[i, i];
			}
			return x;
		}

		public Matrix Solve(Matrix right)
		{
			int k;
			double sum;
			Guard.ArgumentNotNull(right, "b");
			if ((right.ColumnCount != 1) || (right.RowCount != _dimension))
			{
				throw new ArgumentException("NonMatchingDimensions");
			}
			double[] x = new double[_dimension];
			double[] m = new double[_dimension];
			for (int j = 0; j < _dimension; j++)
			{
				m[j] = right.GetValue(j, 0);
			}
			for (int i = 0; i < _dimension; i++)
			{
				sum = m[i];
				k = i - 1;
				while (k >= 0)
				{
					sum -= LeftFactorMatrix[i, k] * x[k];
					k--;
				}
				x[i] = sum / LeftFactorMatrix[i, i];
			}
			for (int i = _dimension - 1; i >= 0; i--)
			{
				sum = x[i];
				for (k = i + 1; k < _dimension; k++)
				{
					sum -= LeftFactorMatrix[k, i] * x[k];
				}
				x[i] = sum / LeftFactorMatrix[i, i];
			}
			return new Matrix(_dimension, 1, x);
		}

		public Matrix LeftFactorMatrix
		{
			get { return _leftFactorMatrix; }
			private set { _leftFactorMatrix = value; }
		}

		public Matrix RightFactorMatrix
		{
			get { return LeftFactorMatrix.Transpose(); }
		}
	}
}

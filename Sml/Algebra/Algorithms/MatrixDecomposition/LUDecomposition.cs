// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Sml.Algebra.Data;
using Sml.Common;

namespace Sml.Algebra.Algorithms.MatrixDecomposition
{
	public class LUDecomposition : IDecomposition
	{
		private Matrix _lu;
		private int[] _pivots;
		private int _pivotSign;

		public LUDecomposition(Matrix matrix)
		{
			Guard.ArgumentNotNull(matrix, "matrix");
			if (!matrix.IsSquare)
			{
				throw new ArgumentException("MatrixMustBeSquare");
			}
			Decompose(matrix);
		}

		public void Decompose(Matrix matrix)
		{
			_lu = matrix.Clone();
			_pivots = new int[_lu.RowCount];
			for (int i = 0; i < _lu.RowCount; i++)
			{
				_pivots[i] = i;
			}
			_pivotSign = 1;
			double[] column = new double[_lu.RowCount];
			for (int j = 0; j < _lu.ColumnCount; j++)
			{
				for (int i = 0; i < _lu.RowCount; i++)
				{
					column[i] = _lu.GetValue(i, j);
				}
				for (int i = 0; i < _lu.RowCount; i++)
				{
					int kmax = Math.Min(i, j);
					double s = 0.0;
					for (int k = 0; k < kmax; k++)
					{
						s += _lu.GetValue(i, k)*column[k];
					}
					_lu.SetValue(i, j, column[i] - s);
					column[i] -= s;
				}
				int p = j;
				for (int i = j + 1; i < _lu.RowCount; i++)
				{
					if (Math.Abs(column[i]) > Math.Abs(column[p]))
					{
						p = i;
					}
				}
				if (p != j)
				{
					for (int k = 0; k < _lu.ColumnCount; k++)
					{
						double t = _lu[p, k];
						_lu.SetValue(p, k, _lu[j, k]);
						_lu.SetValue(j, k, t);
					}
					Swapper.Swap(_pivots, p, j);
					_pivotSign = -_pivotSign;
				}
				if ((j < _lu.RowCount) && (_lu.GetValue(j, j) != 0.0))
				{
					for (int i = j + 1; i < _lu.RowCount; i++)
					{
						_lu.SetValue(i, j, _lu.GetValue(i, j)/_lu.GetValue(j, j));
					}
				}
			}
		}

		public double Determinant()
		{
			double determinant = _pivotSign;
			for (int j = 0; j < _lu.ColumnCount; j++)
			{
				determinant *= _lu.GetValue(j, j);
			}
			return determinant;
		}

		private Matrix GetLowerTriangularFactor()
		{
			Matrix matrix = new Matrix(_lu.RowCount, _lu.ColumnCount);
			for (int i = 0; i < _lu.RowCount; i++)
			{
				for (int j = 0; j < _lu.ColumnCount; j++)
				{
					if (i > j)
					{
						matrix.SetValue(i, j, _lu.GetValue(i, j));
					}
					else if (i == j)
					{
						matrix.SetValue(i, j, 1.0);
					}
					else
					{
						matrix.SetValue(i, j, 0.0);
					}
				}
			}
			return matrix;
		}

		private Matrix GetUpperTriangularFactor()
		{
			Matrix matrix = new Matrix(_lu.RowCount, _lu.ColumnCount);
			for (int i = 0; i < _lu.RowCount; i++)
			{
				for (int j = 0; j < _lu.ColumnCount; j++)
				{
					matrix.SetValue(i, j, i <= j ? _lu.GetValue(i, j) : 0.0);
				}
			}
			return matrix;
		}

		public int Rank()
		{
			int rank = 0;
			for (int j = 0; j < _lu.ColumnCount; j++)
			{
				if (_lu.GetValue(j, j) != 0.0)
					rank++;
			}
			return rank;
		}

		public Matrix Solve(Matrix right)
		{
			Guard.ArgumentNotNull(right, "right");
			if (right.RowCount != _lu.RowCount)
			{
				throw new ArgumentException("MatrixRowCountDiffers");
			}
			if (!NonSingular)
			{
				throw new ArgumentException("MatrixIsSingular");
			}
			return SolveInternal(right);
		}

		private Matrix SolveInternal(Matrix b)
		{
			int nx = b.ColumnCount;
			Matrix subMatrix = b.GetSubMatrix(_pivots, 0, nx - 1);
			for (int k = 0; k < _lu.ColumnCount; k++)
			{
				for (int i = k + 1; i < _lu.ColumnCount; i++)
				{
					for (int j = 0; j < nx; j++)
					{
						subMatrix.SetValue(i, j, subMatrix.GetValue(i, j) - (subMatrix.GetValue(k, j)*_lu.GetValue(i, k)));
					}
				}
			}
			for (int k = _lu.ColumnCount - 1; k >= 0; k--)
			{
				for (int j = 0; j < nx; j++)
				{
					subMatrix.SetValue(k, j, subMatrix.GetValue(k, j)/_lu.GetValue(k, k));
				}
				for (int i = 0; i < k; i++)
				{
					for (int j = 0; j < nx; j++)
					{
						subMatrix.SetValue(i, j, subMatrix.GetValue(i, j) - (subMatrix.GetValue(k, j)*_lu.GetValue(i, k)));
					}
				}
			}
			return subMatrix;
		}

		public Matrix LeftFactorMatrix
		{
			get { return GetLowerTriangularFactor(); }
		}

		public bool NonSingular
		{
			get
			{
				for (int j = 0; j < _lu.ColumnCount; j++)
				{
					if (_lu.GetValue(j, j) == 0.0)
						return false;
				}
				return true;
			}
		}

		public Matrix RightFactorMatrix
		{
			get { return GetUpperTriangularFactor(); }
		}
	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;

namespace Sml.Algebra.Algorithms.Randoms
{
	/// <summary>
	/// Криптографический генератор произвольных чисел ISAAC
	/// </summary>
	/// <remarks>http://burtleburtle.net/bob/rand/isaacafa.html</remarks>
	public class Isaac
	{
		/// <summary>
		/// Логарифм размера массивов результата и памяти
		/// </summary>
		private const int SizeLog = 8;

		/// <summary>
		/// Размера массивов результата и памяти
		/// </summary>
		private const int Size = 1 << SizeLog;

		private const int Mask = (Size - 1) << 2;
		private int _count;

		/// <summary>
		/// Массив результатов
		/// </summary>
		private readonly int[] _results;

		/// <summary>
		/// Начальное состояние
		/// </summary>
		private readonly int[] _memory;

		/// <summary>
		/// Акуммулятор
		/// </summary>
		private int _accume;

		/// <summary>
		/// Последний результат
		/// </summary>
		private int _last;

		private int _counter;



		public Isaac()
		{
			_memory = new int[Size];
			_results = new int[Size];
			Init(false);
		}

		public Isaac(int[] seed)
		{
			_memory = new int[Size];
			_results = new int[Size];
			for (int i = 0; i < seed.Length; ++i)
				_results[i] = seed[i];
			Init(true);
		}

		
		private void Roll()
		{
			int i, j, x, y;

			_last += ++_counter;
			for (i = 0, j = Size / 2; i < Size / 2; )
			{
				x = _memory[i];
				_accume ^= _accume << 13;
				_accume += _memory[j++];
				_memory[i] = y = _memory[(x & Mask) >> 2] + _accume + _last;
				_results[i++] = _last = _memory[((y >> SizeLog) & Mask) >> 2] + x;

				x = _memory[i];
				_accume ^= (int) ((uint) _accume >> 6);
				_accume += _memory[j++];
				_memory[i] = y = _memory[(x & Mask) >> 2] + _accume + _last;
				_results[i++] = _last = _memory[((y >> SizeLog) & Mask) >> 2] + x;

				x = _memory[i];
				_accume ^= _accume << 2;
				_accume += _memory[j++];
				_memory[i] = y = _memory[(x & Mask) >> 2] + _accume + _last;
				_results[i++] = _last = _memory[((y >> SizeLog) & Mask) >> 2] + x;

				x = _memory[i];
				_accume ^= (int) ((uint) _accume >> 16);
				_accume += _memory[j++];
				_memory[i] = y = _memory[(x & Mask) >> 2] + _accume + _last;
				_results[i++] = _last = _memory[((y >> SizeLog) & Mask) >> 2] + x;
			}

			for (j = 0; j < Size / 2; )
			{
				x = _memory[i];
				_accume ^= _accume << 13;
				_accume += _memory[j++];
				_memory[i] = y = _memory[(x & Mask) >> 2] + _accume + _last;
				_results[i++] = _last = _memory[((y >> SizeLog) & Mask) >> 2] + x;

				x = _memory[i];
				_accume ^= (int) ((uint) _accume >> 6);
				_accume += _memory[j++];
				_memory[i] = y = _memory[(x & Mask) >> 2] + _accume + _last;
				_results[i++] = _last = _memory[((y >> SizeLog) & Mask) >> 2] + x;

				x = _memory[i];
				_accume ^= _accume << 2;
				_accume += _memory[j++];
				_memory[i] = y = _memory[(x & Mask) >> 2] + _accume + _last;
				_results[i++] = _last = _memory[((y >> SizeLog) & Mask) >> 2] + x;

				x = _memory[i];
				_accume ^= (int) ((uint) _accume >> 16);
				_accume += _memory[j++];
				_memory[i] = y = _memory[(x & Mask) >> 2] + _accume + _last;
				_results[i++] = _last = _memory[((y >> SizeLog) & Mask) >> 2] + x;
			}
		}

		private void Init(bool flag)
		{
			int i;
			int a, b, c, d, e, f, g, h;
			// Магия
			a = b = c = d = e = f = g = h = unchecked((int)0x9e3779b9);

			for (i = 0; i < 4; ++i)
			{
				a ^= b << 11; d += a; b += c;
				b ^= (int)((uint)c >> 2); e += b; c += d;
				c ^= d << 8; f += c; d += e;
				d ^= (int)((uint)e >> 16); g += d; e += f;
				e ^= f << 10; h += e; f += g;
				f ^= (int)((uint)g >> 4); a += f; g += h;
				g ^= h << 8; b += g; h += a;
				h ^= (int)((uint)a >> 9); c += h; a += b;
			}

			for (i = 0; i < Size; i += 8)
			{              
				if (flag)
				{
					a += _results[i]; b += _results[i + 1]; c += _results[i + 2]; d += _results[i + 3];
					e += _results[i + 4]; f += _results[i + 5]; g += _results[i + 6]; h += _results[i + 7];
				}
				a ^= b << 11; d += a; b += c;
				b ^= (int)((uint)c >> 2); e += b; c += d;
				c ^= d << 8; f += c; d += e;
				d ^= (int)((uint)e >> 16); g += d; e += f;
				e ^= f << 10; h += e; f += g;
				f ^= (int)((uint)g >> 4); a += f; g += h;
				g ^= h << 8; b += g; h += a;
				h ^= (int)((uint)a >> 9); c += h; a += b;
				_memory[i] = a; _memory[i + 1] = b; _memory[i + 2] = c; _memory[i + 3] = d;
				_memory[i + 4] = e; _memory[i + 5] = f; _memory[i + 6] = g; _memory[i + 7] = h;
			}

			if (flag)
			{
				for (i = 0; i < Size; i += 8)
				{
					a += _memory[i]; b += _memory[i + 1]; c += _memory[i + 2]; d += _memory[i + 3];
					e += _memory[i + 4]; f += _memory[i + 5]; g += _memory[i + 6]; h += _memory[i + 7];
					a ^= b << 11; d += a; b += c;
					b ^= (int)((uint)c >> 2); e += b; c += d;
					c ^= d << 8; f += c; d += e;
					d ^= (int)((uint)e >> 16); g += d; e += f;
					e ^= f << 10; h += e; f += g;
					f ^= (int)((uint)g >> 4); a += f; g += h;
					g ^= h << 8; b += g; h += a;
					h ^= (int)((uint)a >> 9); c += h; a += b;
					_memory[i] = a; _memory[i + 1] = b; _memory[i + 2] = c; _memory[i + 3] = d;
					_memory[i + 4] = e; _memory[i + 5] = f; _memory[i + 6] = g; _memory[i + 7] = h;
				}
			}

			Roll();
			_count = Size;
		}

		public int Next()
		{
			if (0 == _count--)
			{
				Roll();
				_count = Size - 1;
			}
			return _results[_count];
		}
	}
}

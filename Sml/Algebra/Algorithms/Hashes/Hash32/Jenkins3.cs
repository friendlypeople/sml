// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;

namespace Sml.Algebra.Algorithms.Hashes.Hash32
{
	internal class Jenkins3 : Hash32AlgorithmBase
	{
		protected override void GenerateHash(byte[] key, int index, int length)
		{
			if (length == 0) return;

			uint a = 0xdeadbeef + (uint)length;
			uint b = a;
			uint c = a;

			int k = index;

			while (length > 12)
			{
				a += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
				b += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
				c += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);

				Mix1(ref a, ref b, ref c);

				length -= 12;
			}

			switch (length)
			{
				case 12:
					a += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					b += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					c += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k] << 24);
					break;
				case 11:
					a += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					b += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					c += (uint)(key[k++] | key[k++] << 8 | key[k] << 16);
					break;
				case 10:
					a += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					b += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					c += (uint)(key[k++] | key[k] << 8);
					break;
				case 9:
					a += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					b += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					c += key[k];
					break;
				case 8:
					a += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					b += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k] << 24);
					break;
				case 7:
					a += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					b += (uint)(key[k++] | key[k++] << 8 | key[k] << 16);
					break;
				case 6:
					a += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					b += (uint)(key[k++] | key[k] << 8);
					break;
				case 5:
					a += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k++] << 24);
					b += key[k];
					break;
				case 4:
					a += (uint)(key[k++] | key[k++] << 8 | key[k++] << 16 | key[k] << 24);
					break;
				case 3:
					a += (uint)(key[k++] | key[k++] << 8 | key[k] << 16);
					break;
				case 2:
					a += (uint)(key[k++] | key[k] << 8);
					break;
				case 1:
					a += key[k];
					break;
			}

			Mix2(ref a, ref b, ref c);
			_hash = c;
		}

		private void Mix1(ref uint a, ref uint b, ref uint c)
		{
			a -= c;
			a ^= (c << 4) | (c >> 28);
			c += b;
			b -= a;
			b ^= (a << 6) | (a >> 26);
			a += c;
			c -= b;
			c ^= (b << 8) | (b >> 24);
			b += a;
			a -= c;
			a ^= (c << 16) | (c >> 16);
			c += b;
			b -= a;
			b ^= (a << 19) | (a >> 13);
			a += c;
			c -= b;
			c ^= (b << 4) | (b >> 28);
			b += a;
		}

		private void Mix2(ref uint a, ref uint b, ref uint c)
		{
			c ^= b;
			c = c - ((b << 14) | (b >> 18));
			a ^= c;
			a = a - ((c << 11) | (c >> 21));
			b ^= a;
			b = b - ((a << 25) | (a >> 7));
			c ^= b;
			c = c - ((b << 16) | (b >> 16));
			a ^= c;
			a = a - ((c << 4) | (c >> 28));
			b ^= a;
			b = b - ((a << 14) | (a >> 18));
			c ^= b;
			c = c - ((b << 24) | (b >> 8));
		}
	}
}

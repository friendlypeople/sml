// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto.MD
{
	internal class MD4 : MDBase
	{
		public MD4() : base(4, 16) { }


		protected override void Setup()
		{
			base.Setup();
			_state[0] = 0x67452301;
			_state[1] = 0xefcdab89;
			_state[2] = 0x98badcfe;
			_state[3] = 0x10325476;
		}


		protected override void TransformBlock(byte[] block, int index)
		{
			uint[] data = Utils.TransformToUints(block, index, 16 * sizeof(uint));


			uint a = _state[0];
			uint b = _state[1];
			uint c = _state[2];
			uint d = _state[3];

			MixStep1(data, ref a, ref b, ref c, ref d);
			MixStep2(data, ref a, ref b, ref c, ref d);
			MixStep3(data, ref a, ref b, ref c, ref d);

			_state[0] += a;
			_state[1] += b;
			_state[2] += c;
			_state[3] += d;
		}

		private void MixStep1(uint[] data, ref uint a, ref uint b, ref uint c, ref uint d)
		{
			a += data[0] + ((b & c) | ((~b) & d));
			a = a << 3 | a >> (32 - 3);
			d += data[1] + ((a & b) | ((~a) & c));
			d = d << 7 | d >> (32 - 7);
			c += data[2] + ((d & a) | ((~d) & b));
			c = c << 11 | c >> (32 - 11);
			b += data[3] + ((c & d) | ((~c) & a));
			b = b << 19 | b >> (32 - 19);
			a += data[4] + ((b & c) | ((~b) & d));
			a = a << 3 | a >> (32 - 3);
			d += data[5] + ((a & b) | ((~a) & c));
			d = d << 7 | d >> (32 - 7);
			c += data[6] + ((d & a) | ((~d) & b));
			c = c << 11 | c >> (32 - 11);
			b += data[7] + ((c & d) | ((~c) & a));
			b = b << 19 | b >> (32 - 19);
			a += data[8] + ((b & c) | ((~b) & d));
			a = a << 3 | a >> (32 - 3);
			d += data[9] + ((a & b) | ((~a) & c));
			d = d << 7 | d >> (32 - 7);
			c += data[10] + ((d & a) | ((~d) & b));
			c = c << 11 | c >> (32 - 11);
			b += data[11] + ((c & d) | ((~c) & a));
			b = b << 19 | b >> (32 - 19);
			a += data[12] + ((b & c) | ((~b) & d));
			a = a << 3 | a >> (32 - 3);
			d += data[13] + ((a & b) | ((~a) & c));
			d = d << 7 | d >> (32 - 7);
			c += data[14] + ((d & a) | ((~d) & b));
			c = c << 11 | c >> (32 - 11);
			b += data[15] + ((c & d) | ((~c) & a));
			b = b << 19 | b >> (32 - 19);
		}

		private void MixStep2(uint[] data, ref uint a, ref uint b, ref uint c, ref uint d)
		{
			a += data[0] + C2 + ((b & (c | d)) | (c & d));
			a = a << 3 | a >> (32 - 3);
			d += data[4] + C2 + ((a & (b | c)) | (b & c));
			d = d << 5 | d >> (32 - 5);
			c += data[8] + C2 + ((d & (a | b)) | (a & b));
			c = c << 9 | c >> (32 - 9);
			b += data[12] + C2 + ((c & (d | a)) | (d & a));
			b = b << 13 | b >> (32 - 13);
			a += data[1] + C2 + ((b & (c | d)) | (c & d));
			a = a << 3 | a >> (32 - 3);
			d += data[5] + C2 + ((a & (b | c)) | (b & c));
			d = d << 5 | d >> (32 - 5);
			c += data[9] + C2 + ((d & (a | b)) | (a & b));
			c = c << 9 | c >> (32 - 9);
			b += data[13] + C2 + ((c & (d | a)) | (d & a));
			b = b << 13 | b >> (32 - 13);
			a += data[2] + C2 + ((b & (c | d)) | (c & d));
			a = a << 3 | a >> (32 - 3);
			d += data[6] + C2 + ((a & (b | c)) | (b & c));
			d = d << 5 | d >> (32 - 5);
			c += data[10] + C2 + ((d & (a | b)) | (a & b));
			c = c << 9 | c >> (32 - 9);
			b += data[14] + C2 + ((c & (d | a)) | (d & a));
			b = b << 13 | b >> (32 - 13);
			a += data[3] + C2 + ((b & (c | d)) | (c & d));
			a = a << 3 | a >> (32 - 3);
			d += data[7] + C2 + ((a & (b | c)) | (b & c));
			d = d << 5 | d >> (32 - 5);
			c += data[11] + C2 + ((d & (a | b)) | (a & b));
			c = c << 9 | c >> (32 - 9);
			b += data[15] + C2 + ((c & (d | a)) | (d & a));
			b = b << 13 | b >> (32 - 13);
		}

		private void MixStep3(uint[] data, ref uint a, ref uint b, ref uint c, ref uint d)
		{
			a += data[0] + C4 + (b ^ c ^ d);
			a = a << 3 | a >> (32 - 3);
			d += data[8] + C4 + (a ^ b ^ c);
			d = d << 9 | d >> (32 - 9);
			c += data[4] + C4 + (d ^ a ^ b);
			c = c << 11 | c >> (32 - 11);
			b += data[12] + C4 + (c ^ d ^ a);
			b = b << 15 | b >> (32 - 15);
			a += data[2] + C4 + (b ^ c ^ d);
			a = a << 3 | a >> (32 - 3);
			d += data[10] + C4 + (a ^ b ^ c);
			d = d << 9 | d >> (32 - 9);
			c += data[6] + C4 + (d ^ a ^ b);
			c = c << 11 | c >> (32 - 11);
			b += data[14] + C4 + (c ^ d ^ a);
			b = b << 15 | b >> (32 - 15);
			a += data[1] + C4 + (b ^ c ^ d);
			a = a << 3 | a >> (32 - 3);
			d += data[9] + C4 + (a ^ b ^ c);
			d = d << 9 | d >> (32 - 9);
			c += data[5] + C4 + (d ^ a ^ b);
			c = c << 11 | c >> (32 - 11);
			b += data[13] + C4 + (c ^ d ^ a);
			b = b << 15 | b >> (32 - 15);
			a += data[3] + C4 + (b ^ c ^ d);
			a = a << 3 | a >> (32 - 3);
			d += data[11] + C4 + (a ^ b ^ c);
			d = d << 9 | d >> (32 - 9);
			c += data[7] + C4 + (d ^ a ^ b);
			c = c << 11 | c >> (32 - 11);
			b += data[15] + C4 + (c ^ d ^ a);
			b = b << 15 | b >> (32 - 15);
		}

		protected override void FinalStep()
		{
			_hash = Utils.TransformToBytes(_state);
		}

		
	}
}

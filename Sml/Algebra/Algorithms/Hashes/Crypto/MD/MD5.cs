// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto.MD
{
	internal class MD5 : MDBase
	{
		public MD5() : base(4, 16) { }

		protected override void Setup()
		{
			base.Setup();

			_state[0] = 0x67452301;
			_state[1] = 0xefcdab89;
			_state[2] = 0x98badcfe;
			_state[3] = 0x10325476;
		}

		protected override void TransformBlock(byte[] block, int index)
		{
			uint[] data = Utils.TransformToUints(block, index, 16 * sizeof(uint));

			uint a = _state[0];
			uint b = _state[1];
			uint c = _state[2];
			uint d = _state[3];

			MixStep1(data,ref a, ref b, ref c, ref d);
			MixStep2(data,ref a, ref b, ref c, ref d);
			MixStep3(data, ref a, ref b, ref c, ref d);
			MixStep4(data, ref a, ref b, ref c, ref d);

			_state[0] += a;
			_state[1] += b;
			_state[2] += c;
			_state[3] += d;
		}

		private static void MixStep4(uint[] data, ref uint a, ref uint b, ref uint c, ref uint d)
		{
			a = data[0] + 0xf4292244 + a + (c ^ (b | ~d));
			a = ((a << 6) | (a >> (32 - 6))) + b;
			d = data[7] + 0x432aff97 + d + (b ^ (a | ~c));
			d = ((d << 10) | (d >> (32 - 10))) + a;
			c = data[14] + 0xab9423a7 + c + (a ^ (d | ~b));
			c = ((c << 15) | (c >> (32 - 15))) + d;
			b = data[5] + 0xfc93a039 + b + (d ^ (c | ~a));
			b = ((b << 21) | (b >> (32 - 21))) + c;
			a = data[12] + 0x655b59c3 + a + (c ^ (b | ~d));
			a = ((a << 6) | (a >> (32 - 6))) + b;
			d = data[3] + 0x8f0ccc92 + d + (b ^ (a | ~c));
			d = ((d << 10) | (d >> (32 - 10))) + a;
			c = data[10] + 0xffeff47d + c + (a ^ (d | ~b));
			c = ((c << 15) | (c >> (32 - 15))) + d;
			b = data[1] + 0x85845dd1 + b + (d ^ (c | ~a));
			b = ((b << 21) | (b >> (32 - 21))) + c;
			a = data[8] + 0x6fa87e4f + a + (c ^ (b | ~d));
			a = ((a << 6) | (a >> (32 - 6))) + b;
			d = data[15] + 0xfe2ce6e0 + d + (b ^ (a | ~c));
			d = ((d << 10) | (d >> (32 - 10))) + a;
			c = data[6] + 0xa3014314 + c + (a ^ (d | ~b));
			c = ((c << 15) | (c >> (32 - 15))) + d;
			b = data[13] + 0x4e0811a1 + b + (d ^ (c | ~a));
			b = ((b << 21) | (b >> (32 - 21))) + c;
			a = data[4] + 0xf7537e82 + a + (c ^ (b | ~d));
			a = ((a << 6) | (a >> (32 - 6))) + b;
			d = data[11] + 0xbd3af235 + d + (b ^ (a | ~c));
			d = ((d << 10) | (d >> (32 - 10))) + a;
			c = data[2] + 0x2ad7d2bb + c + (a ^ (d | ~b));
			c = ((c << 15) | (c >> (32 - 15))) + d;
			b = data[9] + 0xeb86d391 + b + (d ^ (c | ~a));
			b = ((b << 21) | (b >> (32 - 21))) + c;
		}

		private static void MixStep3(uint[] data, ref uint a, ref uint b, ref uint c, ref uint d)
		{
			a = data[5] + 0xfffa3942 + a + (b ^ c ^ d);
			a = ((a << 4) | (a >> (32 - 4))) + b;
			d = data[8] + 0x8771f681 + d + (a ^ b ^ c);
			d = ((d << 11) | (d >> (32 - 11))) + a;
			c = data[11] + 0x6d9d6122 + c + (d ^ a ^ b);
			c = ((c << 16) | (c >> (32 - 16))) + d;
			b = data[14] + 0xfde5380c + b + (c ^ d ^ a);
			b = ((b << 23) | (b >> (32 - 23))) + c;
			a = data[1] + 0xa4beea44 + a + (b ^ c ^ d);
			a = ((a << 4) | (a >> (32 - 4))) + b;
			d = data[4] + 0x4bdecfa9 + d + (a ^ b ^ c);
			d = ((d << 11) | (d >> (32 - 11))) + a;
			c = data[7] + 0xf6bb4b60 + c + (d ^ a ^ b);
			c = ((c << 16) | (c >> (32 - 16))) + d;
			b = data[10] + 0xbebfbc70 + b + (c ^ d ^ a);
			b = ((b << 23) | (b >> (32 - 23))) + c;
			a = data[13] + 0x289b7ec6 + a + (b ^ c ^ d);
			a = ((a << 4) | (a >> (32 - 4))) + b;
			d = data[0] + 0xeaa127fa + d + (a ^ b ^ c);
			d = ((d << 11) | (d >> (32 - 11))) + a;
			c = data[3] + 0xd4ef3085 + c + (d ^ a ^ b);
			c = ((c << 16) | (c >> (32 - 16))) + d;
			b = data[6] + 0x4881d05 + b + (c ^ d ^ a);
			b = ((b << 23) | (b >> (32 - 23))) + c;
			a = data[9] + 0xd9d4d039 + a + (b ^ c ^ d);
			a = ((a << 4) | (a >> (32 - 4))) + b;
			d = data[12] + 0xe6db99e5 + d + (a ^ b ^ c);
			d = ((d << 11) | (d >> (32 - 11))) + a;
			c = data[15] + 0x1fa27cf8 + c + (d ^ a ^ b);
			c = ((c << 16) | (c >> (32 - 16))) + d;
			b = data[2] + 0xc4ac5665 + b + (c ^ d ^ a);
			b = ((b << 23) | (b >> (32 - 23))) + c;
		}

		private static void MixStep2(uint[] data, ref uint a, ref uint b, ref uint c, ref uint d)
		{
			a = data[1] + 0xf61e2562 + a + ((b & d) | (c & ~d));
			a = ((a << 5) | (a >> (32 - 5))) + b;
			d = data[6] + 0xc040b340 + d + ((a & c) | (b & ~c));
			d = ((d << 9) | (d >> (32 - 9))) + a;
			c = data[11] + 0x265e5a51 + c + ((d & b) | (a & ~b));
			c = ((c << 14) | (c >> (32 - 14))) + d;
			b = data[0] + 0xe9b6c7aa + b + ((c & a) | (d & ~a));
			b = ((b << 20) | (b >> (32 - 20))) + c;
			a = data[5] + 0xd62f105d + a + ((b & d) | (c & ~d));
			a = ((a << 5) | (a >> (32 - 5))) + b;
			d = data[10] + 0x2441453 + d + ((a & c) | (b & ~c));
			d = ((d << 9) | (d >> (32 - 9))) + a;
			c = data[15] + 0xd8a1e681 + c + ((d & b) | (a & ~b));
			c = ((c << 14) | (c >> (32 - 14))) + d;
			b = data[4] + 0xe7d3fbc8 + b + ((c & a) | (d & ~a));
			b = ((b << 20) | (b >> (32 - 20))) + c;
			a = data[9] + 0x21e1cde6 + a + ((b & d) | (c & ~d));
			a = ((a << 5) | (a >> (32 - 5))) + b;
			d = data[14] + 0xc33707d6 + d + ((a & c) | (b & ~c));
			d = ((d << 9) | (d >> (32 - 9))) + a;
			c = data[3] + 0xf4d50d87 + c + ((d & b) | (a & ~b));
			c = ((c << 14) | (c >> (32 - 14))) + d;
			b = data[8] + 0x455a14ed + b + ((c & a) | (d & ~a));
			b = ((b << 20) | (b >> (32 - 20))) + c;
			a = data[13] + 0xa9e3e905 + a + ((b & d) | (c & ~d));
			a = ((a << 5) | (a >> (32 - 5))) + b;
			d = data[2] + 0xfcefa3f8 + d + ((a & c) | (b & ~c));
			d = ((d << 9) | (d >> (32 - 9))) + a;
			c = data[7] + 0x676f02d9 + c + ((d & b) | (a & ~b));
			c = ((c << 14) | (c >> (32 - 14))) + d;
			b = data[12] + 0x8d2a4c8a + b + ((c & a) | (d & ~a));
			b = ((b << 20) | (b >> (32 - 20))) + c;
		}

		private static void MixStep1(uint[] data, ref uint a, ref uint b, ref uint c, ref uint d)
		{
			a = data[0] + 0xd76aa478 + a + ((b & c) | (~b & d));
			a = ((a << 7) | (a >> (32 - 7))) + b;
			d = data[1] + 0xe8c7b756 + d + ((a & b) | (~a & c));
			d = ((d << 12) | (d >> (32 - 12))) + a;
			c = data[2] + 0x242070db + c + ((d & a) | (~d & b));
			c = ((c << 17) | (c >> (32 - 17))) + d;
			b = data[3] + 0xc1bdceee + b + ((c & d) | (~c & a));
			b = ((b << 22) | (b >> (32 - 22))) + c;
			a = data[4] + 0xf57c0faf + a + ((b & c) | (~b & d));
			a = ((a << 7) | (a >> (32 - 7))) + b;
			d = data[5] + 0x4787c62a + d + ((a & b) | (~a & c));
			d = ((d << 12) | (d >> (32 - 12))) + a;
			c = data[6] + 0xa8304613 + c + ((d & a) | (~d & b));
			c = ((c << 17) | (c >> (32 - 17))) + d;
			b = data[7] + 0xfd469501 + b + ((c & d) | (~c & a));
			b = ((b << 22) | (b >> (32 - 22))) + c;
			a = data[8] + 0x698098d8 + a + ((b & c) | (~b & d));
			a = ((a << 7) | (a >> (32 - 7))) + b;
			d = data[9] + 0x8b44f7af + d + ((a & b) | (~a & c));
			d = ((d << 12) | (d >> (32 - 12))) + a;
			c = data[10] + 0xffff5bb1 + c + ((d & a) | (~d & b));
			c = ((c << 17) | (c >> (32 - 17))) + d;
			b = data[11] + 0x895cd7be + b + ((c & d) | (~c & a));
			b = ((b << 22) | (b >> (32 - 22))) + c;
			a = data[12] + 0x6b901122 + a + ((b & c) | (~b & d));
			a = ((a << 7) | (a >> (32 - 7))) + b;
			d = data[13] + 0xfd987193 + d + ((a & b) | (~a & c));
			d = ((d << 12) | (d >> (32 - 12))) + a;
			c = data[14] + 0xa679438e + c + ((d & a) | (~d & b));
			c = ((c << 17) | (c >> (32 - 17))) + d;
			b = data[15] + 0x49b40821 + b + ((c & d) | (~c & a));
			b = ((b << 22) | (b >> (32 - 22))) + c;
		}

		protected override void FinalStep()
		{
			_hash = Utils.TransformToBytes(_state);
		}
	}
}

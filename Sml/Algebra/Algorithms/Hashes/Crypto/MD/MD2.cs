// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto.MD
{
	internal class MD2 : CryptoHashAlgorithm
	{

		private readonly byte[] _state		= new byte[16];
		private readonly byte[] _checksum	= new byte[16];

		private static readonly byte[] Spi = new byte[]
		{
			 41,  46,  67, 201, 
			162, 216, 124,   1,  
			61,   54,  84, 161, 
			236, 240,   6,  19,

			 98, 167,   5, 243,
			192, 199, 115, 140, 
			152, 147,  43, 217, 
			188,  76, 130, 202, 
			
			 30, 155,  87,  60, 
			253, 212, 224,  22, 
			103,  66, 111,  24,
			138,  23, 229,  18, 
			
			190,  78, 196, 214, 
			218, 158, 222,  73, 
			160, 251, 245, 142, 
			187,  47, 238, 122, 
			
			169, 104, 121, 145, 
			 21, 178,   7,  63,
			148, 194,  16, 137, 
			 11,  34,  95,  33, 
			
			128, 127,  93, 154, 
			 90, 144,  50,	39, 
			 53,  62, 204, 231, 
			191, 247, 151,   3, 
			
			255,  25,  48, 179, 
			 72, 165, 181, 209, 
			215,  94, 146,  42, 
			172,  86, 170, 198, 
			
			 79, 184,  56, 210,
			150, 164, 125, 182, 
			118, 252, 107, 226, 
			156, 116,   4, 241, 
			
			 69, 157, 112,  89, 
			100, 113, 135,  32, 
			134,  91, 207, 101, 
			230,  45, 168,   2, 
			
			 27,  96,  37, 173, 
			174, 176, 185, 246, 
			 28,  70,  97, 105, 
			 52,  64, 126,  15,
			
			 85,  71, 163,  35, 
			221,  81, 175,  58, 
			195,  92, 249, 206, 
			186, 197, 234,  38, 
			
			 44,  83,  13, 110, 
		   	133,  40, 132,   9, 
			211, 223, 205, 244, 
			 65, 129,  77,  82, 
			
			106, 220,  55, 200, 
			108, 193, 171, 250, 
			 36, 225, 123,   8, 
			 12, 189, 177,  74, 
			
			120, 136, 149, 139, 
			227,  99, 232, 109, 
			233, 203, 213, 254, 
			 59,   0,  29,  57, 
			
			242, 239, 183,  14, 
			102,  88, 208, 228,
			166, 119, 114, 248, 
			235, 117,  75,  10, 
			
			 49,  68,  80, 180, 
			143, 237,  31,  26, 
			219, 153, 141,  51, 
			159,  17, 131,  20
		};

		public MD2() : base(16, 16) { }

		protected override void Setup()
		{
			base.Setup();

			Utils.ZeroMemory(_state);
			Utils.ZeroMemory(_checksum);
		}

		protected override byte[] CreateFinalBlock(byte[] block, int index, int length)
		{
			int padLength = 16 - length;
			byte[] pad = new byte[16];

			Array.Copy(block, index, pad, 0, length);
			for (int i = length; i < 16; i++)
				pad[i] = (byte)padLength;
			return pad;
		}

		protected override void TransformBlock(byte[] block, int index)
		{
			byte[] temp = new byte[48];

			Array.Copy(_state, temp, 16);
			Array.Copy(block, index, temp, 16, 16);

			for (int i = 0; i < 16; i++)
				temp[i + 32] = (byte)(_state[i] ^ block[i + index]);

			uint t = 0;
			for (int i = 0; i < 18; i++)
			{
				for (int j = 0; j < 48; j++)
					t = temp[j] ^= Spi[t];
				t = (byte)(t + i);
			}

			Array.Copy(temp, _state, 16);

			t = _checksum[15];
			for (int i = 0; i < _bufferSize; i++)
			{
				_checksum[i] ^= Spi[block[i + index] ^ t];
				t = _checksum[i];
			}
		}

		protected override void FinalStep()
		{
			TransformBlock(_checksum, 0);
			Array.Copy(_state, _hash, 16);
		}
	}
}

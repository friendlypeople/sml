// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;
using Sml.Algebra.Algorithms.Hashes.Crypto.MD;
using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto.RIPEMD
{
	internal class RIPEMD256 : MDBase
	{
		public RIPEMD256() : base(8, 32) { }


		protected override void Setup()
		{
			base.Setup();
			_state[0] = 0x67452301;
			_state[1] = 0xefcdab89;
			_state[2] = 0x98badcfe;
			_state[3] = 0x10325476;
			_state[4] = 0x76543210;
			_state[5] = 0xFEDCBA98;
			_state[6] = 0x89ABCDEF;
			_state[7] = 0x01234567;

		}

		protected override void TransformBlock(byte[] block, int index)
		{
			uint[] data = Utils.TransformToUints(block, index, 16 * sizeof(uint));

			uint a = _state[0];
			uint b = _state[1];
			uint c = _state[2];
			uint d = _state[3];
			uint aa = _state[4];
			uint bb = _state[5];
			uint cc = _state[6];
			uint dd = _state[7];

			MixStep1(data, ref a, ref b, ref c, ref d);
			MixStep2(data, ref aa, ref bb, ref cc, ref dd);
			MixStep3(data, ref aa, ref b, ref c, ref d);
			MixStep4(data, ref a, ref bb, ref cc, ref dd);
			MixStep5(data, ref aa, ref bb, ref c, ref d);
			MixStep6(data, ref a, ref b, ref cc, ref dd);
			MixStep7(data, ref aa, ref bb, ref cc, ref d);
			MixStep8(data, ref a, ref b, ref c, ref dd);

			_state[0] += aa;
			_state[1] += bb;
			_state[2] += cc;
			_state[3] += dd;
			_state[4] += a;
			_state[5] += b;
			_state[6] += c;
			_state[7] += d;

		}

		private void MixStep1(uint[] data, ref uint a, ref uint b, ref uint c, ref uint d)
		{
			a += data[0] + (b ^ c ^ d);
			a = (a << 11) | (a >> (32 - 11));
			d += data[1] + (a ^ b ^ c);
			d = (d << 14) | (d >> (32 - 14));
			c += data[2] + (d ^ a ^ b);
			c = (c << 15) | (c >> (32 - 15));
			b += data[3] + (c ^ d ^ a);
			b = (b << 12) | (b >> (32 - 12));
			a += data[4] + (b ^ c ^ d);
			a = (a << 5) | (a >> (32 - 5));
			d += data[5] + (a ^ b ^ c);
			d = (d << 8) | (d >> (32 - 8));
			c += data[6] + (d ^ a ^ b);
			c = (c << 7) | (c >> (32 - 7));
			b += data[7] + (c ^ d ^ a);
			b = (b << 9) | (b >> (32 - 9));
			a += data[8] + (b ^ c ^ d);
			a = (a << 11) | (a >> (32 - 11));
			d += data[9] + (a ^ b ^ c);
			d = (d << 13) | (d >> (32 - 13));
			c += data[10] + (d ^ a ^ b);
			c = (c << 14) | (c >> (32 - 14));
			b += data[11] + (c ^ d ^ a);
			b = (b << 15) | (b >> (32 - 15));
			a += data[12] + (b ^ c ^ d);
			a = (a << 6) | (a >> (32 - 6));
			d += data[13] + (a ^ b ^ c);
			d = (d << 7) | (d >> (32 - 7));
			c += data[14] + (d ^ a ^ b);
			c = (c << 9) | (c >> (32 - 9));
			b += data[15] + (c ^ d ^ a);
			b = (b << 8) | (b >> (32 - 8));
		}


		private void MixStep2(uint[] data, ref uint aa, ref uint bb, ref uint cc, ref uint dd)
		{
			aa += data[5] + C1 + ((bb & dd) | (cc & ~dd));
			aa = (aa << 8) | (aa >> (32 - 8));
			dd += data[14] + C1 + ((aa & cc) | (bb & ~cc));
			dd = (dd << 9) | (dd >> (32 - 9));
			cc += data[7] + C1 + ((dd & bb) | (aa & ~bb));
			cc = (cc << 9) | (cc >> (32 - 9));
			bb += data[0] + C1 + ((cc & aa) | (dd & ~aa));
			bb = (bb << 11) | (bb >> (32 - 11));
			aa += data[9] + C1 + ((bb & dd) | (cc & ~dd));
			aa = (aa << 13) | (aa >> (32 - 13));
			dd += data[2] + C1 + ((aa & cc) | (bb & ~cc));
			dd = (dd << 15) | (dd >> (32 - 15));
			cc += data[11] + C1 + ((dd & bb) | (aa & ~bb));
			cc = (cc << 15) | (cc >> (32 - 15));
			bb += data[4] + C1 + ((cc & aa) | (dd & ~aa));
			bb = (bb << 5) | (bb >> (32 - 5));
			aa += data[13] + C1 + ((bb & dd) | (cc & ~dd));
			aa = (aa << 7) | (aa >> (32 - 7));
			dd += data[6] + C1 + ((aa & cc) | (bb & ~cc));
			dd = (dd << 7) | (dd >> (32 - 7));
			cc += data[15] + C1 + ((dd & bb) | (aa & ~bb));
			cc = (cc << 8) | (cc >> (32 - 8));
			bb += data[8] + C1 + ((cc & aa) | (dd & ~aa));
			bb = (bb << 11) | (bb >> (32 - 11));
			aa += data[1] + C1 + ((bb & dd) | (cc & ~dd));
			aa = (aa << 14) | (aa >> (32 - 14));
			dd += data[10] + C1 + ((aa & cc) | (bb & ~cc));
			dd = (dd << 14) | (dd >> (32 - 14));
			cc += data[3] + C1 + ((dd & bb) | (aa & ~bb));
			cc = (cc << 12) | (cc >> (32 - 12));
			bb += data[12] + C1 + ((cc & aa) | (dd & ~aa));
			bb = (bb << 6) | (bb >> (32 - 6));
		}

		private void MixStep3(uint[] data, ref uint aa, ref uint b, ref uint c, ref uint d)
		{
			aa += data[7] + C2 + ((b & c) | (~b & d));
			aa = (aa << 7) | (aa >> (32 - 7));
			d += data[4] + C2 + ((aa & b) | (~aa & c));
			d = (d << 6) | (d >> (32 - 6));
			c += data[13] + C2 + ((d & aa) | (~d & b));
			c = (c << 8) | (c >> (32 - 8));
			b += data[1] + C2 + ((c & d) | (~c & aa));
			b = (b << 13) | (b >> (32 - 13));
			aa += data[10] + C2 + ((b & c) | (~b & d));
			aa = (aa << 11) | (aa >> (32 - 11));
			d += data[6] + C2 + ((aa & b) | (~aa & c));
			d = (d << 9) | (d >> (32 - 9));
			c += data[15] + C2 + ((d & aa) | (~d & b));
			c = (c << 7) | (c >> (32 - 7));
			b += data[3] + C2 + ((c & d) | (~c & aa));
			b = (b << 15) | (b >> (32 - 15));
			aa += data[12] + C2 + ((b & c) | (~b & d));
			aa = (aa << 7) | (aa >> (32 - 7));
			d += data[0] + C2 + ((aa & b) | (~aa & c));
			d = (d << 12) | (d >> (32 - 12));
			c += data[9] + C2 + ((d & aa) | (~d & b));
			c = (c << 15) | (c >> (32 - 15));
			b += data[5] + C2 + ((c & d) | (~c & aa));
			b = (b << 9) | (b >> (32 - 9));
			aa += data[2] + C2 + ((b & c) | (~b & d));
			aa = (aa << 11) | (aa >> (32 - 11));
			d += data[14] + C2 + ((aa & b) | (~aa & c));
			d = (d << 7) | (d >> (32 - 7));
			c += data[11] + C2 + ((d & aa) | (~d & b));
			c = (c << 13) | (c >> (32 - 13));
			b += data[8] + C2 + ((c & d) | (~c & aa));
			b = (b << 12) | (b >> (32 - 12));
		}

		private void MixStep4(uint[] data, ref uint a, ref uint bb, ref uint cc, ref uint dd)
		{
			a += data[6] + C3 + ((bb | ~cc) ^ dd);
			a = (a << 9) | (a >> (32 - 9));
			dd += data[11] + C3 + ((a | ~bb) ^ cc);
			dd = (dd << 13) | (dd >> (32 - 13));
			cc += data[3] + C3 + ((dd | ~a) ^ bb);
			cc = (cc << 15) | (cc >> (32 - 15));
			bb += data[7] + C3 + ((cc | ~dd) ^ a);
			bb = (bb << 7) | (bb >> (32 - 7));
			a += data[0] + C3 + ((bb | ~cc) ^ dd);
			a = (a << 12) | (a >> (32 - 12));
			dd += data[13] + C3 + ((a | ~bb) ^ cc);
			dd = (dd << 8) | (dd >> (32 - 8));
			cc += data[5] + C3 + ((dd | ~a) ^ bb);
			cc = (cc << 9) | (cc >> (32 - 9));
			bb += data[10] + C3 + ((cc | ~dd) ^ a);
			bb = (bb << 11) | (bb >> (32 - 11));
			a += data[14] + C3 + ((bb | ~cc) ^ dd);
			a = (a << 7) | (a >> (32 - 7));
			dd += data[15] + C3 + ((a | ~bb) ^ cc);
			dd = (dd << 7) | (dd >> (32 - 7));
			cc += data[8] + C3 + ((dd | ~a) ^ bb);
			cc = (cc << 12) | (cc >> (32 - 12));
			bb += data[12] + C3 + ((cc | ~dd) ^ a);
			bb = (bb << 7) | (bb >> (32 - 7));
			a += data[4] + C3 + ((bb | ~cc) ^ dd);
			a = (a << 6) | (a >> (32 - 6));
			dd += data[9] + C3 + ((a | ~bb) ^ cc);
			dd = (dd << 15) | (dd >> (32 - 15));
			cc += data[1] + C3 + ((dd | ~a) ^ bb);
			cc = (cc << 13) | (cc >> (32 - 13));
			bb += data[2] + C3 + ((cc | ~dd) ^ a);
			bb = (bb << 11) | (bb >> (32 - 11));
		}

		private void MixStep5(uint[] data, ref uint aa, ref uint bb, ref uint c, ref uint d)
		{
			aa += data[3] + C4 + ((bb | ~c) ^ d);
			aa = (aa << 11) | (aa >> (32 - 11));
			d += data[10] + C4 + ((aa | ~bb) ^ c);
			d = (d << 13) | (d >> (32 - 13));
			c += data[14] + C4 + ((d | ~aa) ^ bb);
			c = (c << 6) | (c >> (32 - 6));
			bb += data[4] + C4 + ((c | ~d) ^ aa);
			bb = (bb << 7) | (bb >> (32 - 7));
			aa += data[9] + C4 + ((bb | ~c) ^ d);
			aa = (aa << 14) | (aa >> (32 - 14));
			d += data[15] + C4 + ((aa | ~bb) ^ c);
			d = (d << 9) | (d >> (32 - 9));
			c += data[8] + C4 + ((d | ~aa) ^ bb);
			c = (c << 13) | (c >> (32 - 13));
			bb += data[1] + C4 + ((c | ~d) ^ aa);
			bb = (bb << 15) | (bb >> (32 - 15));
			aa += data[2] + C4 + ((bb | ~c) ^ d);
			aa = (aa << 14) | (aa >> (32 - 14));
			d += data[7] + C4 + ((aa | ~bb) ^ c);
			d = (d << 8) | (d >> (32 - 8));
			c += data[0] + C4 + ((d | ~aa) ^ bb);
			c = (c << 13) | (c >> (32 - 13));
			bb += data[6] + C4 + ((c | ~d) ^ aa);
			bb = (bb << 6) | (bb >> (32 - 6));
			aa += data[13] + C4 + ((bb | ~c) ^ d);
			aa = (aa << 5) | (aa >> (32 - 5));
			d += data[11] + C4 + ((aa | ~bb) ^ c);
			d = (d << 12) | (d >> (32 - 12));
			c += data[5] + C4 + ((d | ~aa) ^ bb);
			c = (c << 7) | (c >> (32 - 7));
			bb += data[12] + C4 + ((c | ~d) ^ aa);
			bb = (bb << 5) | (bb >> (32 - 5));
		}

		private void MixStep6(uint[] data, ref uint a, ref uint b, ref uint cc, ref uint dd)
		{
			a += data[15] + C5 + ((b & cc) | (~b & dd));
			a = (a << 9) | (a >> (32 - 9));
			dd += data[5] + C5 + ((a & b) | (~a & cc));
			dd = (dd << 7) | (dd >> (32 - 7));
			cc += data[1] + C5 + ((dd & a) | (~dd & b));
			cc = (cc << 15) | (cc >> (32 - 15));
			b += data[3] + C5 + ((cc & dd) | (~cc & a));
			b = (b << 11) | (b >> (32 - 11));
			a += data[7] + C5 + ((b & cc) | (~b & dd));
			a = (a << 8) | (a >> (32 - 8));
			dd += data[14] + C5 + ((a & b) | (~a & cc));
			dd = (dd << 6) | (dd >> (32 - 6));
			cc += data[6] + C5 + ((dd & a) | (~dd & b));
			cc = (cc << 6) | (cc >> (32 - 6));
			b += data[9] + C5 + ((cc & dd) | (~cc & a));
			b = (b << 14) | (b >> (32 - 14));
			a += data[11] + C5 + ((b & cc) | (~b & dd));
			a = (a << 12) | (a >> (32 - 12));
			dd += data[8] + C5 + ((a & b) | (~a & cc));
			dd = (dd << 13) | (dd >> (32 - 13));
			cc += data[12] + C5 + ((dd & a) | (~dd & b));
			cc = (cc << 5) | (cc >> (32 - 5));
			b += data[2] + C5 + ((cc & dd) | (~cc & a));
			b = (b << 14) | (b >> (32 - 14));
			a += data[10] + C5 + ((b & cc) | (~b & dd));
			a = (a << 13) | (a >> (32 - 13));
			dd += data[0] + C5 + ((a & b) | (~a & cc));
			dd = (dd << 13) | (dd >> (32 - 13));
			cc += data[4] + C5 + ((dd & a) | (~dd & b));
			cc = (cc << 7) | (cc >> (32 - 7));
			b += data[13] + C5 + ((cc & dd) | (~cc & a));
			b = (b << 5) | (b >> (32 - 5));
		}

		private void MixStep7(uint[] data, ref uint aa, ref uint bb, ref uint cc, ref uint d)
		{
			aa += data[1] + C6 + ((bb & d) | (cc & ~d));
			aa = (aa << 11) | (aa >> (32 - 11));
			d += data[9] + C6 + ((aa & cc) | (bb & ~cc));
			d = (d << 12) | (d >> (32 - 12));
			cc += data[11] + C6 + ((d & bb) | (aa & ~bb));
			cc = (cc << 14) | (cc >> (32 - 14));
			bb += data[10] + C6 + ((cc & aa) | (d & ~aa));
			bb = (bb << 15) | (bb >> (32 - 15));
			aa += data[0] + C6 + ((bb & d) | (cc & ~d));
			aa = (aa << 14) | (aa >> (32 - 14));
			d += data[8] + C6 + ((aa & cc) | (bb & ~cc));
			d = (d << 15) | (d >> (32 - 15));
			cc += data[12] + C6 + ((d & bb) | (aa & ~bb));
			cc = (cc << 9) | (cc >> (32 - 9));
			bb += data[4] + C6 + ((cc & aa) | (d & ~aa));
			bb = (bb << 8) | (bb >> (32 - 8));
			aa += data[13] + C6 + ((bb & d) | (cc & ~d));
			aa = (aa << 9) | (aa >> (32 - 9));
			d += data[3] + C6 + ((aa & cc) | (bb & ~cc));
			d = (d << 14) | (d >> (32 - 14));
			cc += data[7] + C6 + ((d & bb) | (aa & ~bb));
			cc = (cc << 5) | (cc >> (32 - 5));
			bb += data[15] + C6 + ((cc & aa) | (d & ~aa));
			bb = (bb << 6) | (bb >> (32 - 6));
			aa += data[14] + C6 + ((bb & d) | (cc & ~d));
			aa = (aa << 8) | (aa >> (32 - 8));
			d += data[5] + C6 + ((aa & cc) | (bb & ~cc));
			d = (d << 6) | (d >> (32 - 6));
			cc += data[6] + C6 + ((d & bb) | (aa & ~bb));
			cc = (cc << 5) | (cc >> (32 - 5));
			bb += data[2] + C6 + ((cc & aa) | (d & ~aa));
			bb = (bb << 12) | (bb >> (32 - 12));
		}

		private void MixStep8(uint[] data, ref uint a, ref uint b, ref uint c, ref uint dd)
		{
			a += data[8] + (b ^ c ^ dd);
			a = (a << 15) | (a >> (32 - 15));
			dd += data[6] + (a ^ b ^ c);
			dd = (dd << 5) | (dd >> (32 - 5));
			c += data[4] + (dd ^ a ^ b);
			c = (c << 8) | (c >> (32 - 8));
			b += data[1] + (c ^ dd ^ a);
			b = (b << 11) | (b >> (32 - 11));
			a += data[3] + (b ^ c ^ dd);
			a = (a << 14) | (a >> (32 - 14));
			dd += data[11] + (a ^ b ^ c);
			dd = (dd << 14) | (dd >> (32 - 14));
			c += data[15] + (dd ^ a ^ b);
			c = (c << 6) | (c >> (32 - 6));
			b += data[0] + (c ^ dd ^ a);
			b = (b << 14) | (b >> (32 - 14));
			a += data[5] + (b ^ c ^ dd);
			a = (a << 6) | (a >> (32 - 6));
			dd += data[12] + (a ^ b ^ c);
			dd = (dd << 9) | (dd >> (32 - 9));
			c += data[2] + (dd ^ a ^ b);
			c = (c << 12) | (c >> (32 - 12));
			b += data[13] + (c ^ dd ^ a);
			b = (b << 9) | (b >> (32 - 9));
			a += data[9] + (b ^ c ^ dd);
			a = (a << 12) | (a >> (32 - 12));
			dd += data[7] + (a ^ b ^ c);
			dd = (dd << 5) | (dd >> (32 - 5));
			c += data[10] + (dd ^ a ^ b);
			c = (c << 15) | (c >> (32 - 15));
			b += data[14] + (c ^ dd ^ a);
			b = (b << 8) | (b >> (32 - 8));
		}

		protected override void FinalStep()
		{
			_hash = Utils.TransformToBytes(_state);
		}
	}
}

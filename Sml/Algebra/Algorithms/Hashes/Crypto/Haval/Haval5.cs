// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto.Haval
{
	internal abstract class Haval5 : HavalBase
	{
		protected Haval5(HashSize hashSize) : base(HashRounds.Rounds5, hashSize) { }

		protected override void TransformBlock(byte[] block, int index)
		{
			uint[] temp = Utils.TransformToUints(block, index, _bufferSize);

			uint a = _tempHash[0];
			uint b = _tempHash[1];
			uint c = _tempHash[2];
			uint d = _tempHash[3];
			uint e = _tempHash[4];
			uint f = _tempHash[5];
			uint g = _tempHash[6];
			uint h = _tempHash[7];

			uint t = c & (g ^ b) ^ f & e ^ a & d ^ g;
			h = temp[0] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = b & (f ^ a) ^ e & d ^ h & c ^ f;
			g = temp[1] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = a & (e ^ h) ^ d & c ^ g & b ^ e;
			f = temp[2] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = h & (d ^ g) ^ c & b ^ f & a ^ d;
			e = temp[3] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = g & (c ^ f) ^ b & a ^ e & h ^ c;
			d = temp[4] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = f & (b ^ e) ^ a & h ^ d & g ^ b;
			c = temp[5] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = e & (a ^ d) ^ h & g ^ c & f ^ a;
			b = temp[6] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = d & (h ^ c) ^ g & f ^ b & e ^ h;
			a = temp[7] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = c & (g ^ b) ^ f & e ^ a & d ^ g;
			h = temp[8] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = b & (f ^ a) ^ e & d ^ h & c ^ f;
			g = temp[9] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = a & (e ^ h) ^ d & c ^ g & b ^ e;
			f = temp[10] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = h & (d ^ g) ^ c & b ^ f & a ^ d;
			e = temp[11] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = g & (c ^ f) ^ b & a ^ e & h ^ c;
			d = temp[12] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = f & (b ^ e) ^ a & h ^ d & g ^ b;
			c = temp[13] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = e & (a ^ d) ^ h & g ^ c & f ^ a;
			b = temp[14] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = d & (h ^ c) ^ g & f ^ b & e ^ h;
			a = temp[15] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = c & (g ^ b) ^ f & e ^ a & d ^ g;
			h = temp[16] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = b & (f ^ a) ^ e & d ^ h & c ^ f;
			g = temp[17] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = a & (e ^ h) ^ d & c ^ g & b ^ e;
			f = temp[18] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = h & (d ^ g) ^ c & b ^ f & a ^ d;
			e = temp[19] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = g & (c ^ f) ^ b & a ^ e & h ^ c;
			d = temp[20] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = f & (b ^ e) ^ a & h ^ d & g ^ b;
			c = temp[21] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = e & (a ^ d) ^ h & g ^ c & f ^ a;
			b = temp[22] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = d & (h ^ c) ^ g & f ^ b & e ^ h;
			a = temp[23] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = c & (g ^ b) ^ f & e ^ a & d ^ g;
			h = temp[24] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = b & (f ^ a) ^ e & d ^ h & c ^ f;
			g = temp[25] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = a & (e ^ h) ^ d & c ^ g & b ^ e;
			f = temp[26] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = h & (d ^ g) ^ c & b ^ f & a ^ d;
			e = temp[27] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = g & (c ^ f) ^ b & a ^ e & h ^ c;
			d = temp[28] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = f & (b ^ e) ^ a & h ^ d & g ^ b;
			c = temp[29] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = e & (a ^ d) ^ h & g ^ c & f ^ a;
			b = temp[30] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = d & (h ^ c) ^ g & f ^ b & e ^ h;
			a = temp[31] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (e & ~a ^ b & c ^ g ^ f) ^ b & (e ^ c) ^ a & c ^ f;
			h = temp[5] + 0x452821E6 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (d & ~h ^ a & b ^ f ^ e) ^ a & (d ^ b) ^ h & b ^ e;
			g = temp[14] + 0x38D01377 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (c & ~g ^ h & a ^ e ^ d) ^ h & (c ^ a) ^ g & a ^ d;
			f = temp[26] + 0xBE5466CF + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (b & ~f ^ g & h ^ d ^ c) ^ g & (b ^ h) ^ f & h ^ c;
			e = temp[18] + 0x34E90C6C + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (a & ~e ^ f & g ^ c ^ b) ^ f & (a ^ g) ^ e & g ^ b;
			d = temp[11] + 0xC0AC29B7 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (h & ~d ^ e & f ^ b ^ a) ^ e & (h ^ f) ^ d & f ^ a;
			c = temp[28] + 0xC97C50DD + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (g & ~c ^ d & e ^ a ^ h) ^ d & (g ^ e) ^ c & e ^ h;
			b = temp[7] + 0x3F84D5B5 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (f & ~b ^ c & d ^ h ^ g) ^ c & (f ^ d) ^ b & d ^ g;
			a = temp[16] + 0xB5470917 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (e & ~a ^ b & c ^ g ^ f) ^ b & (e ^ c) ^ a & c ^ f;
			h = temp[0] + 0x9216D5D9 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (d & ~h ^ a & b ^ f ^ e) ^ a & (d ^ b) ^ h & b ^ e;
			g = temp[23] + 0x8979FB1B + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (c & ~g ^ h & a ^ e ^ d) ^ h & (c ^ a) ^ g & a ^ d;
			f = temp[20] + 0xD1310BA6 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (b & ~f ^ g & h ^ d ^ c) ^ g & (b ^ h) ^ f & h ^ c;
			e = temp[22] + 0x98DFB5AC + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (a & ~e ^ f & g ^ c ^ b) ^ f & (a ^ g) ^ e & g ^ b;
			d = temp[1] + 0x2FFD72DB + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (h & ~d ^ e & f ^ b ^ a) ^ e & (h ^ f) ^ d & f ^ a;
			c = temp[10] + 0xD01ADFB7 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (g & ~c ^ d & e ^ a ^ h) ^ d & (g ^ e) ^ c & e ^ h;
			b = temp[4] + 0xB8E1AFED + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (f & ~b ^ c & d ^ h ^ g) ^ c & (f ^ d) ^ b & d ^ g;
			a = temp[8] + 0x6A267E96 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (e & ~a ^ b & c ^ g ^ f) ^ b & (e ^ c) ^ a & c ^ f;
			h = temp[30] + 0xBA7C9045 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (d & ~h ^ a & b ^ f ^ e) ^ a & (d ^ b) ^ h & b ^ e;
			g = temp[3] + 0xF12C7F99 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (c & ~g ^ h & a ^ e ^ d) ^ h & (c ^ a) ^ g & a ^ d;
			f = temp[21] + 0x24A19947 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (b & ~f ^ g & h ^ d ^ c) ^ g & (b ^ h) ^ f & h ^ c;
			e = temp[9] + 0xB3916CF7 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (a & ~e ^ f & g ^ c ^ b) ^ f & (a ^ g) ^ e & g ^ b;
			d = temp[17] + 0x0801F2E2 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (h & ~d ^ e & f ^ b ^ a) ^ e & (h ^ f) ^ d & f ^ a;
			c = temp[24] + 0x858EFC16 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (g & ~c ^ d & e ^ a ^ h) ^ d & (g ^ e) ^ c & e ^ h;
			b = temp[29] + 0x636920D8 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (f & ~b ^ c & d ^ h ^ g) ^ c & (f ^ d) ^ b & d ^ g;
			a = temp[6] + 0x71574E69 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (e & ~a ^ b & c ^ g ^ f) ^ b & (e ^ c) ^ a & c ^ f;
			h = temp[19] + 0xA458FEA3 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (d & ~h ^ a & b ^ f ^ e) ^ a & (d ^ b) ^ h & b ^ e;
			g = temp[12] + 0xF4933D7E + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (c & ~g ^ h & a ^ e ^ d) ^ h & (c ^ a) ^ g & a ^ d;
			f = temp[15] + 0x0D95748F + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (b & ~f ^ g & h ^ d ^ c) ^ g & (b ^ h) ^ f & h ^ c;
			e = temp[13] + 0x728EB658 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (a & ~e ^ f & g ^ c ^ b) ^ f & (a ^ g) ^ e & g ^ b;
			d = temp[2] + 0x718BCD58 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (h & ~d ^ e & f ^ b ^ a) ^ e & (h ^ f) ^ d & f ^ a;
			c = temp[25] + 0x82154AEE + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (g & ~c ^ d & e ^ a ^ h) ^ d & (g ^ e) ^ c & e ^ h;
			b = temp[31] + 0x7B54A41D + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (f & ~b ^ c & d ^ h ^ g) ^ c & (f ^ d) ^ b & d ^ g;
			a = temp[27] + 0xC25A59B5 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = e & (b & d ^ c ^ f) ^ b & a ^ d & g ^ f;
			h = temp[19] + 0x9C30D539 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = d & (a & c ^ b ^ e) ^ a & h ^ c & f ^ e;
			g = temp[9] + 0x2AF26013 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = c & (h & b ^ a ^ d) ^ h & g ^ b & e ^ d;
			f = temp[4] + 0xC5D1B023 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = b & (g & a ^ h ^ c) ^ g & f ^ a & d ^ c;
			e = temp[20] + 0x286085F0 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = a & (f & h ^ g ^ b) ^ f & e ^ h & c ^ b;
			d = temp[28] + 0xCA417918 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = h & (e & g ^ f ^ a) ^ e & d ^ g & b ^ a;
			c = temp[17] + 0xB8DB38EF + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = g & (d & f ^ e ^ h) ^ d & c ^ f & a ^ h;
			b = temp[8] + 0x8E79DCB0 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = f & (c & e ^ d ^ g) ^ c & b ^ e & h ^ g;
			a = temp[22] + 0x603A180E + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = e & (b & d ^ c ^ f) ^ b & a ^ d & g ^ f;
			h = temp[29] + 0x6C9E0E8B + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = d & (a & c ^ b ^ e) ^ a & h ^ c & f ^ e;
			g = temp[14] + 0xB01E8A3E + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = c & (h & b ^ a ^ d) ^ h & g ^ b & e ^ d;
			f = temp[25] + 0xD71577C1 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = b & (g & a ^ h ^ c) ^ g & f ^ a & d ^ c;
			e = temp[12] + 0xBD314B27 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = a & (f & h ^ g ^ b) ^ f & e ^ h & c ^ b;
			d = temp[24] + 0x78AF2FDA + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = h & (e & g ^ f ^ a) ^ e & d ^ g & b ^ a;
			c = temp[30] + 0x55605C60 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = g & (d & f ^ e ^ h) ^ d & c ^ f & a ^ h;
			b = temp[16] + 0xE65525F3 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = f & (c & e ^ d ^ g) ^ c & b ^ e & h ^ g;
			a = temp[26] + 0xAA55AB94 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = e & (b & d ^ c ^ f) ^ b & a ^ d & g ^ f;
			h = temp[31] + 0x57489862 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = d & (a & c ^ b ^ e) ^ a & h ^ c & f ^ e;
			g = temp[15] + 0x63E81440 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = c & (h & b ^ a ^ d) ^ h & g ^ b & e ^ d;
			f = temp[7] + 0x55CA396A + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = b & (g & a ^ h ^ c) ^ g & f ^ a & d ^ c;
			e = temp[3] + 0x2AAB10B6 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = a & (f & h ^ g ^ b) ^ f & e ^ h & c ^ b;
			d = temp[1] + 0xB4CC5C34 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = h & (e & g ^ f ^ a) ^ e & d ^ g & b ^ a;
			c = temp[0] + 0x1141E8CE + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = g & (d & f ^ e ^ h) ^ d & c ^ f & a ^ h;
			b = temp[18] + 0xA15486AF + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = f & (c & e ^ d ^ g) ^ c & b ^ e & h ^ g;
			a = temp[27] + 0x7C72E993 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = e & (b & d ^ c ^ f) ^ b & a ^ d & g ^ f;
			h = temp[13] + 0xB3EE1411 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = d & (a & c ^ b ^ e) ^ a & h ^ c & f ^ e;
			g = temp[6] + 0x636FBC2A + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = c & (h & b ^ a ^ d) ^ h & g ^ b & e ^ d;
			f = temp[21] + 0x2BA9C55D + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = b & (g & a ^ h ^ c) ^ g & f ^ a & d ^ c;
			e = temp[10] + 0x741831F6 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = a & (f & h ^ g ^ b) ^ f & e ^ h & c ^ b;
			d = temp[23] + 0xCE5C3E16 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = h & (e & g ^ f ^ a) ^ e & d ^ g & b ^ a;
			c = temp[11] + 0x9B87931E + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = g & (d & f ^ e ^ h) ^ d & c ^ f & a ^ h;
			b = temp[5] + 0xAFD6BA33 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = f & (c & e ^ d ^ g) ^ c & b ^ e & h ^ g;
			a = temp[2] + 0x6C24CF5C + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (f & ~a ^ c & ~b ^ e ^ b ^ g) ^ c & (e & a ^ f ^ b) ^ a & b ^ g;
			h = temp[24] + 0x7A325381 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (e & ~h ^ b & ~a ^ d ^ a ^ f) ^ b & (d & h ^ e ^ a) ^ h & a ^ f;
			g = temp[4] + 0x28958677 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (d & ~g ^ a & ~h ^ c ^ h ^ e) ^ a & (c & g ^ d ^ h) ^ g & h ^ e;
			f = temp[0] + 0x3B8F4898 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (c & ~f ^ h & ~g ^ b ^ g ^ d) ^ h & (b & f ^ c ^ g) ^ f & g ^ d;
			e = temp[14] + 0x6B4BB9AF + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (b & ~e ^ g & ~f ^ a ^ f ^ c) ^ g & (a & e ^ b ^ f) ^ e & f ^ c;
			d = temp[2] + 0xC4BFE81B + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (a & ~d ^ f & ~e ^ h ^ e ^ b) ^ f & (h & d ^ a ^ e) ^ d & e ^ b;
			c = temp[7] + 0x66282193 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (h & ~c ^ e & ~d ^ g ^ d ^ a) ^ e & (g & c ^ h ^ d) ^ c & d ^ a;
			b = temp[28] + 0x61D809CC + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (g & ~b ^ d & ~c ^ f ^ c ^ h) ^ d & (f & b ^ g ^ c) ^ b & c ^ h;
			a = temp[23] + 0xFB21A991 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (f & ~a ^ c & ~b ^ e ^ b ^ g) ^ c & (e & a ^ f ^ b) ^ a & b ^ g;
			h = temp[26] + 0x487CAC60 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (e & ~h ^ b & ~a ^ d ^ a ^ f) ^ b & (d & h ^ e ^ a) ^ h & a ^ f;
			g = temp[6] + 0x5DEC8032 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (d & ~g ^ a & ~h ^ c ^ h ^ e) ^ a & (c & g ^ d ^ h) ^ g & h ^ e;
			f = temp[30] + 0xEF845D5D + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (c & ~f ^ h & ~g ^ b ^ g ^ d) ^ h & (b & f ^ c ^ g) ^ f & g ^ d;
			e = temp[20] + 0xE98575B1 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (b & ~e ^ g & ~f ^ a ^ f ^ c) ^ g & (a & e ^ b ^ f) ^ e & f ^ c;
			d = temp[18] + 0xDC262302 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (a & ~d ^ f & ~e ^ h ^ e ^ b) ^ f & (h & d ^ a ^ e) ^ d & e ^ b;
			c = temp[25] + 0xEB651B88 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (h & ~c ^ e & ~d ^ g ^ d ^ a) ^ e & (g & c ^ h ^ d) ^ c & d ^ a;
			b = temp[19] + 0x23893E81 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (g & ~b ^ d & ~c ^ f ^ c ^ h) ^ d & (f & b ^ g ^ c) ^ b & c ^ h;
			a = temp[3] + 0xD396ACC5 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (f & ~a ^ c & ~b ^ e ^ b ^ g) ^ c & (e & a ^ f ^ b) ^ a & b ^ g;
			h = temp[22] + 0x0F6D6FF3 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (e & ~h ^ b & ~a ^ d ^ a ^ f) ^ b & (d & h ^ e ^ a) ^ h & a ^ f;
			g = temp[11] + 0x83F44239 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (d & ~g ^ a & ~h ^ c ^ h ^ e) ^ a & (c & g ^ d ^ h) ^ g & h ^ e;
			f = temp[31] + 0x2E0B4482 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (c & ~f ^ h & ~g ^ b ^ g ^ d) ^ h & (b & f ^ c ^ g) ^ f & g ^ d;
			e = temp[21] + 0xA4842004 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (b & ~e ^ g & ~f ^ a ^ f ^ c) ^ g & (a & e ^ b ^ f) ^ e & f ^ c;
			d = temp[8] + 0x69C8F04A + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (a & ~d ^ f & ~e ^ h ^ e ^ b) ^ f & (h & d ^ a ^ e) ^ d & e ^ b;
			c = temp[27] + 0x9E1F9B5E + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (h & ~c ^ e & ~d ^ g ^ d ^ a) ^ e & (g & c ^ h ^ d) ^ c & d ^ a;
			b = temp[12] + 0x21C66842 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (g & ~b ^ d & ~c ^ f ^ c ^ h) ^ d & (f & b ^ g ^ c) ^ b & c ^ h;
			a = temp[9] + 0xF6E96C9A + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (f & ~a ^ c & ~b ^ e ^ b ^ g) ^ c & (e & a ^ f ^ b) ^ a & b ^ g;
			h = temp[1] + 0x670C9C61 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (e & ~h ^ b & ~a ^ d ^ a ^ f) ^ b & (d & h ^ e ^ a) ^ h & a ^ f;
			g = temp[29] + 0xABD388F0 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (d & ~g ^ a & ~h ^ c ^ h ^ e) ^ a & (c & g ^ d ^ h) ^ g & h ^ e;
			f = temp[5] + 0x6A51A0D2 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (c & ~f ^ h & ~g ^ b ^ g ^ d) ^ h & (b & f ^ c ^ g) ^ f & g ^ d;
			e = temp[15] + 0xD8542F68 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (b & ~e ^ g & ~f ^ a ^ f ^ c) ^ g & (a & e ^ b ^ f) ^ e & f ^ c;
			d = temp[17] + 0x960FA728 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (a & ~d ^ f & ~e ^ h ^ e ^ b) ^ f & (h & d ^ a ^ e) ^ d & e ^ b;
			c = temp[10] + 0xAB5133A3 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (h & ~c ^ e & ~d ^ g ^ d ^ a) ^ e & (g & c ^ h ^ d) ^ c & d ^ a;
			b = temp[16] + 0x6EEF0B6C + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (g & ~b ^ d & ~c ^ f ^ c ^ h) ^ d & (f & b ^ g ^ c) ^ b & c ^ h;
			a = temp[13] + 0x137A3BE4 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = b & (d & e & g ^ ~f) ^ d & a ^ e & f ^ g & c;
			h = temp[27] + 0xBA3BF050 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = a & (c & d & f ^ ~e) ^ c & h ^ d & e ^ f & b;
			g = temp[3] + 0x7EFB2A98 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = h & (b & c & e ^ ~d) ^ b & g ^ c & d ^ e & a;
			f = temp[21] + 0xA1F1651D + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = g & (a & b & d ^ ~c) ^ a & f ^ b & c ^ d & h;
			e = temp[26] + 0x39AF0176 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = f & (h & a & c ^ ~b) ^ h & e ^ a & b ^ c & g;
			d = temp[17] + 0x66CA593E + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = e & (g & h & b ^ ~a) ^ g & d ^ h & a ^ b & f;
			c = temp[11] + 0x82430E88 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = d & (f & g & a ^ ~h) ^ f & c ^ g & h ^ a & e;
			b = temp[20] + 0x8CEE8619 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = c & (e & f & h ^ ~g) ^ e & b ^ f & g ^ h & d;
			a = temp[29] + 0x456F9FB4 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = b & (d & e & g ^ ~f) ^ d & a ^ e & f ^ g & c;
			h = temp[19] + 0x7D84A5C3 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = a & (c & d & f ^ ~e) ^ c & h ^ d & e ^ f & b;
			g = temp[0] + 0x3B8B5EBE + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = h & (b & c & e ^ ~d) ^ b & g ^ c & d ^ e & a;
			f = temp[12] + 0xE06F75D8 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = g & (a & b & d ^ ~c) ^ a & f ^ b & c ^ d & h;
			e = temp[7] + 0x85C12073 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = f & (h & a & c ^ ~b) ^ h & e ^ a & b ^ c & g;
			d = temp[13] + 0x401A449F + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = e & (g & h & b ^ ~a) ^ g & d ^ h & a ^ b & f;
			c = temp[8] + 0x56C16AA6 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = d & (f & g & a ^ ~h) ^ f & c ^ g & h ^ a & e;
			b = temp[31] + 0x4ED3AA62 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = c & (e & f & h ^ ~g) ^ e & b ^ f & g ^ h & d;
			a = temp[10] + 0x363F7706 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = b & (d & e & g ^ ~f) ^ d & a ^ e & f ^ g & c;
			h = temp[5] + 0x1BFEDF72 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = a & (c & d & f ^ ~e) ^ c & h ^ d & e ^ f & b;
			g = temp[9] + 0x429B023D + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = h & (b & c & e ^ ~d) ^ b & g ^ c & d ^ e & a;
			f = temp[14] + 0x37D0D724 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = g & (a & b & d ^ ~c) ^ a & f ^ b & c ^ d & h;
			e = temp[30] + 0xD00A1248 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = f & (h & a & c ^ ~b) ^ h & e ^ a & b ^ c & g;
			d = temp[18] + 0xDB0FEAD3 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = e & (g & h & b ^ ~a) ^ g & d ^ h & a ^ b & f;
			c = temp[6] + 0x49F1C09B + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = d & (f & g & a ^ ~h) ^ f & c ^ g & h ^ a & e;
			b = temp[28] + 0x075372C9 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = c & (e & f & h ^ ~g) ^ e & b ^ f & g ^ h & d;
			a = temp[24] + 0x80991B7B + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = b & (d & e & g ^ ~f) ^ d & a ^ e & f ^ g & c;
			h = temp[2] + 0x25D479D8 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = a & (c & d & f ^ ~e) ^ c & h ^ d & e ^ f & b;
			g = temp[23] + 0xF6E8DEF7 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = h & (b & c & e ^ ~d) ^ b & g ^ c & d ^ e & a;
			f = temp[16] + 0xE3FE501A + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = g & (a & b & d ^ ~c) ^ a & f ^ b & c ^ d & h;
			e = temp[22] + 0xB6794C3B + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = f & (h & a & c ^ ~b) ^ h & e ^ a & b ^ c & g;
			d = temp[4] + 0x976CE0BD + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = e & (g & h & b ^ ~a) ^ g & d ^ h & a ^ b & f;
			c = temp[1] + 0x04C006BA + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = d & (f & g & a ^ ~h) ^ f & c ^ g & h ^ a & e;
			b = temp[25] + 0xC1A94FB6 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = c & (e & f & h ^ ~g) ^ e & b ^ f & g ^ h & d;
			a = temp[15] + 0x409F60C4 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			_tempHash[0] += a;
			_tempHash[1] += b;
			_tempHash[2] += c;
			_tempHash[3] += d;
			_tempHash[4] += e;
			_tempHash[5] += f;
			_tempHash[6] += g;
			_tempHash[7] += h;
		}
	}
}

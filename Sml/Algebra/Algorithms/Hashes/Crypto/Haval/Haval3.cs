// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto.Haval
{
	internal abstract class Haval3 : HavalBase
	{
		protected Haval3(HashSize hashSize) : base(HashRounds.Rounds3, hashSize) { }


		protected override void TransformBlock(byte[] block, int index)
		{
			uint[] temp = Utils.TransformToUints(block, index, _bufferSize);

			uint a = _tempHash[0];
			uint b = _tempHash[1];
			uint c = _tempHash[2];
			uint d = _tempHash[3];
			uint e = _tempHash[4];
			uint f = _tempHash[5];
			uint g = _tempHash[6];
			uint h = _tempHash[7];

			uint t = c & (e ^ d) ^ g & a ^ f & b ^ e;
			h = temp[0] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = b & (d ^ c) ^ f & h ^ e & a ^ d;
			g = temp[1] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = a & (c ^ b) ^ e & g ^ d & h ^ c;
			f = temp[2] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = h & (b ^ a) ^ d & f ^ c & g ^ b;
			e = temp[3] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = g & (a ^ h) ^ c & e ^ b & f ^ a;
			d = temp[4] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = f & (h ^ g) ^ b & d ^ a & e ^ h;
			c = temp[5] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = e & (g ^ f) ^ a & c ^ h & d ^ g;
			b = temp[6] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = d & (f ^ e) ^ h & b ^ g & c ^ f;
			a = temp[7] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = c & (e ^ d) ^ g & a ^ f & b ^ e;
			h = temp[8] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = b & (d ^ c) ^ f & h ^ e & a ^ d;
			g = temp[9] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = a & (c ^ b) ^ e & g ^ d & h ^ c;
			f = temp[10] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = h & (b ^ a) ^ d & f ^ c & g ^ b;
			e = temp[11] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = g & (a ^ h) ^ c & e ^ b & f ^ a;
			d = temp[12] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = f & (h ^ g) ^ b & d ^ a & e ^ h;
			c = temp[13] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = e & (g ^ f) ^ a & c ^ h & d ^ g;
			b = temp[14] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = d & (f ^ e) ^ h & b ^ g & c ^ f;
			a = temp[15] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = c & (e ^ d) ^ g & a ^ f & b ^ e;
			h = temp[16] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = b & (d ^ c) ^ f & h ^ e & a ^ d;
			g = temp[17] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = a & (c ^ b) ^ e & g ^ d & h ^ c;
			f = temp[18] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = h & (b ^ a) ^ d & f ^ c & g ^ b;
			e = temp[19] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = g & (a ^ h) ^ c & e ^ b & f ^ a;
			d = temp[20] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = f & (h ^ g) ^ b & d ^ a & e ^ h;
			c = temp[21] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = e & (g ^ f) ^ a & c ^ h & d ^ g;
			b = temp[22] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = d & (f ^ e) ^ h & b ^ g & c ^ f;
			a = temp[23] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = c & (e ^ d) ^ g & a ^ f & b ^ e;
			h = temp[24] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = b & (d ^ c) ^ f & h ^ e & a ^ d;
			g = temp[25] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = a & (c ^ b) ^ e & g ^ d & h ^ c;
			f = temp[26] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = h & (b ^ a) ^ d & f ^ c & g ^ b;
			e = temp[27] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = g & (a ^ h) ^ c & e ^ b & f ^ a;
			d = temp[28] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = f & (h ^ g) ^ b & d ^ a & e ^ h;
			c = temp[29] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = e & (g ^ f) ^ a & c ^ h & d ^ g;
			b = temp[30] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = d & (f ^ e) ^ h & b ^ g & c ^ f;
			a = temp[31] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = f & (d & ~a ^ b & c ^ e ^ g) ^ b & (d ^ c) ^ a & c ^ g;
			h = temp[5] + 0x452821E6 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = e & (c & ~h ^ a & b ^ d ^ f) ^ a & (c ^ b) ^ h & b ^ f;
			g = temp[14] + 0x38D01377 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = d & (b & ~g ^ h & a ^ c ^ e) ^ h & (b ^ a) ^ g & a ^ e;
			f = temp[26] + 0xBE5466CF + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = c & (a & ~f ^ g & h ^ b ^ d) ^ g & (a ^ h) ^ f & h ^ d;
			e = temp[18] + 0x34E90C6C + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = b & (h & ~e ^ f & g ^ a ^ c) ^ f & (h ^ g) ^ e & g ^ c;
			d = temp[11] + 0xC0AC29B7 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = a & (g & ~d ^ e & f ^ h ^ b) ^ e & (g ^ f) ^ d & f ^ b;
			c = temp[28] + 0xC97C50DD + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = h & (f & ~c ^ d & e ^ g ^ a) ^ d & (f ^ e) ^ c & e ^ a;
			b = temp[7] + 0x3F84D5B5 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = g & (e & ~b ^ c & d ^ f ^ h) ^ c & (e ^ d) ^ b & d ^ h;
			a = temp[16] + 0xB5470917 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = f & (d & ~a ^ b & c ^ e ^ g) ^ b & (d ^ c) ^ a & c ^ g;
			h = temp[0] + 0x9216D5D9 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = e & (c & ~h ^ a & b ^ d ^ f) ^ a & (c ^ b) ^ h & b ^ f;
			g = temp[23] + 0x8979FB1B + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = d & (b & ~g ^ h & a ^ c ^ e) ^ h & (b ^ a) ^ g & a ^ e;
			f = temp[20] + 0xD1310BA6 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = c & (a & ~f ^ g & h ^ b ^ d) ^ g & (a ^ h) ^ f & h ^ d;
			e = temp[22] + 0x98DFB5AC + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = b & (h & ~e ^ f & g ^ a ^ c) ^ f & (h ^ g) ^ e & g ^ c;
			d = temp[1] + 0x2FFD72DB + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = a & (g & ~d ^ e & f ^ h ^ b) ^ e & (g ^ f) ^ d & f ^ b;
			c = temp[10] + 0xD01ADFB7 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = h & (f & ~c ^ d & e ^ g ^ a) ^ d & (f ^ e) ^ c & e ^ a;
			b = temp[4] + 0xB8E1AFED + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = g & (e & ~b ^ c & d ^ f ^ h) ^ c & (e ^ d) ^ b & d ^ h;
			a = temp[8] + 0x6A267E96 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = f & (d & ~a ^ b & c ^ e ^ g) ^ b & (d ^ c) ^ a & c ^ g;
			h = temp[30] + 0xBA7C9045 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = e & (c & ~h ^ a & b ^ d ^ f) ^ a & (c ^ b) ^ h & b ^ f;
			g = temp[3] + 0xF12C7F99 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = d & (b & ~g ^ h & a ^ c ^ e) ^ h & (b ^ a) ^ g & a ^ e;
			f = temp[21] + 0x24A19947 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = c & (a & ~f ^ g & h ^ b ^ d) ^ g & (a ^ h) ^ f & h ^ d;
			e = temp[9] + 0xB3916CF7 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = b & (h & ~e ^ f & g ^ a ^ c) ^ f & (h ^ g) ^ e & g ^ c;
			d = temp[17] + 0x0801F2E2 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = a & (g & ~d ^ e & f ^ h ^ b) ^ e & (g ^ f) ^ d & f ^ b;
			c = temp[24] + 0x858EFC16 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = h & (f & ~c ^ d & e ^ g ^ a) ^ d & (f ^ e) ^ c & e ^ a;
			b = temp[29] + 0x636920D8 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = g & (e & ~b ^ c & d ^ f ^ h) ^ c & (e ^ d) ^ b & d ^ h;
			a = temp[6] + 0x71574E69 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = f & (d & ~a ^ b & c ^ e ^ g) ^ b & (d ^ c) ^ a & c ^ g;
			h = temp[19] + 0xA458FEA3 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = e & (c & ~h ^ a & b ^ d ^ f) ^ a & (c ^ b) ^ h & b ^ f;
			g = temp[12] + 0xF4933D7E + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = d & (b & ~g ^ h & a ^ c ^ e) ^ h & (b ^ a) ^ g & a ^ e;
			f = temp[15] + 0x0D95748F + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = c & (a & ~f ^ g & h ^ b ^ d) ^ g & (a ^ h) ^ f & h ^ d;
			e = temp[13] + 0x728EB658 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = b & (h & ~e ^ f & g ^ a ^ c) ^ f & (h ^ g) ^ e & g ^ c;
			d = temp[2] + 0x718BCD58 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = a & (g & ~d ^ e & f ^ h ^ b) ^ e & (g ^ f) ^ d & f ^ b;
			c = temp[25] + 0x82154AEE + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = h & (f & ~c ^ d & e ^ g ^ a) ^ d & (f ^ e) ^ c & e ^ a;
			b = temp[31] + 0x7B54A41D + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = g & (e & ~b ^ c & d ^ f ^ h) ^ c & (e ^ d) ^ b & d ^ h;
			a = temp[27] + 0xC25A59B5 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (f & e ^ g ^ a) ^ f & c ^ e & b ^ a;
			h = temp[19] + 0x9C30D539 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (e & d ^ f ^ h) ^ e & b ^ d & a ^ h;
			g = temp[9] + 0x2AF26013 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (d & c ^ e ^ g) ^ d & a ^ c & h ^ g;
			f = temp[4] + 0xC5D1B023 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (c & b ^ d ^ f) ^ c & h ^ b & g ^ f;
			e = temp[20] + 0x286085F0 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (b & a ^ c ^ e) ^ b & g ^ a & f ^ e;
			d = temp[28] + 0xCA417918 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (a & h ^ b ^ d) ^ a & f ^ h & e ^ d;
			c = temp[17] + 0xB8DB38EF + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (h & g ^ a ^ c) ^ h & e ^ g & d ^ c;
			b = temp[8] + 0x8E79DCB0 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (g & f ^ h ^ b) ^ g & d ^ f & c ^ b;
			a = temp[22] + 0x603A180E + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (f & e ^ g ^ a) ^ f & c ^ e & b ^ a;
			h = temp[29] + 0x6C9E0E8B + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (e & d ^ f ^ h) ^ e & b ^ d & a ^ h;
			g = temp[14] + 0xB01E8A3E + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (d & c ^ e ^ g) ^ d & a ^ c & h ^ g;
			f = temp[25] + 0xD71577C1 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (c & b ^ d ^ f) ^ c & h ^ b & g ^ f;
			e = temp[12] + 0xBD314B27 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (b & a ^ c ^ e) ^ b & g ^ a & f ^ e;
			d = temp[24] + 0x78AF2FDA + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (a & h ^ b ^ d) ^ a & f ^ h & e ^ d;
			c = temp[30] + 0x55605C60 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (h & g ^ a ^ c) ^ h & e ^ g & d ^ c;
			b = temp[16] + 0xE65525F3 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (g & f ^ h ^ b) ^ g & d ^ f & c ^ b;
			a = temp[26] + 0xAA55AB94 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (f & e ^ g ^ a) ^ f & c ^ e & b ^ a;
			h = temp[31] + 0x57489862 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (e & d ^ f ^ h) ^ e & b ^ d & a ^ h;
			g = temp[15] + 0x63E81440 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (d & c ^ e ^ g) ^ d & a ^ c & h ^ g;
			f = temp[7] + 0x55CA396A + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (c & b ^ d ^ f) ^ c & h ^ b & g ^ f;
			e = temp[3] + 0x2AAB10B6 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (b & a ^ c ^ e) ^ b & g ^ a & f ^ e;
			d = temp[1] + 0xB4CC5C34 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (a & h ^ b ^ d) ^ a & f ^ h & e ^ d;
			c = temp[0] + 0x1141E8CE + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (h & g ^ a ^ c) ^ h & e ^ g & d ^ c;
			b = temp[18] + 0xA15486AF + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (g & f ^ h ^ b) ^ g & d ^ f & c ^ b;
			a = temp[27] + 0x7C72E993 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (f & e ^ g ^ a) ^ f & c ^ e & b ^ a;
			h = temp[13] + 0xB3EE1411 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (e & d ^ f ^ h) ^ e & b ^ d & a ^ h;
			g = temp[6] + 0x636FBC2A + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (d & c ^ e ^ g) ^ d & a ^ c & h ^ g;
			f = temp[21] + 0x2BA9C55D + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (c & b ^ d ^ f) ^ c & h ^ b & g ^ f;
			e = temp[10] + 0x741831F6 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (b & a ^ c ^ e) ^ b & g ^ a & f ^ e;
			d = temp[23] + 0xCE5C3E16 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (a & h ^ b ^ d) ^ a & f ^ h & e ^ d;
			c = temp[11] + 0x9B87931E + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (h & g ^ a ^ c) ^ h & e ^ g & d ^ c;
			b = temp[5] + 0xAFD6BA33 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (g & f ^ h ^ b) ^ g & d ^ f & c ^ b;
			a = temp[2] + 0x6C24CF5C + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			_tempHash[0] += a;
			_tempHash[1] += b;
			_tempHash[2] += c;
			_tempHash[3] += d;
			_tempHash[4] += e;
			_tempHash[5] += f;
			_tempHash[6] += g;
			_tempHash[7] += h;
		}

	}
}

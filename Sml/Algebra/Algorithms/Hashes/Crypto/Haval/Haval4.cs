// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;
using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto.Haval
{
	internal abstract class Haval4 : HavalBase
	{
		protected Haval4(HashSize hashSize) : base(HashRounds.Rounds4, hashSize) { }

		protected override void TransformBlock(byte[] block, int index)
		{
			uint[] temp = Utils.TransformToUints(block, index, _bufferSize);

			uint a = _tempHash[0];
			uint b = _tempHash[1];
			uint c = _tempHash[2];
			uint d = _tempHash[3];
			uint e = _tempHash[4];
			uint f = _tempHash[5];
			uint g = _tempHash[6];
			uint h = _tempHash[7];

			uint t = d & (a ^ b) ^ f & g ^ e & c ^ a;
			h = temp[0] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (h ^ a) ^ e & f ^ d & b ^ h;
			g = temp[1] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (g ^ h) ^ d & e ^ c & a ^ g;
			f = temp[2] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (f ^ g) ^ c & d ^ b & h ^ f;
			e = temp[3] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (e ^ f) ^ b & c ^ a & g ^ e;
			d = temp[4] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (d ^ e) ^ a & b ^ h & f ^ d;
			c = temp[5] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (c ^ d) ^ h & a ^ g & e ^ c;
			b = temp[6] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (b ^ c) ^ g & h ^ f & d ^ b;
			a = temp[7] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (a ^ b) ^ f & g ^ e & c ^ a;
			h = temp[8] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (h ^ a) ^ e & f ^ d & b ^ h;
			g = temp[9] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (g ^ h) ^ d & e ^ c & a ^ g;
			f = temp[10] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (f ^ g) ^ c & d ^ b & h ^ f;
			e = temp[11] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (e ^ f) ^ b & c ^ a & g ^ e;
			d = temp[12] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (d ^ e) ^ a & b ^ h & f ^ d;
			c = temp[13] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (c ^ d) ^ h & a ^ g & e ^ c;
			b = temp[14] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (b ^ c) ^ g & h ^ f & d ^ b;
			a = temp[15] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (a ^ b) ^ f & g ^ e & c ^ a;
			h = temp[16] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (h ^ a) ^ e & f ^ d & b ^ h;
			g = temp[17] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (g ^ h) ^ d & e ^ c & a ^ g;
			f = temp[18] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (f ^ g) ^ c & d ^ b & h ^ f;
			e = temp[19] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (e ^ f) ^ b & c ^ a & g ^ e;
			d = temp[20] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (d ^ e) ^ a & b ^ h & f ^ d;
			c = temp[21] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (c ^ d) ^ h & a ^ g & e ^ c;
			b = temp[22] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (b ^ c) ^ g & h ^ f & d ^ b;
			a = temp[23] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = d & (a ^ b) ^ f & g ^ e & c ^ a;
			h = temp[24] + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = c & (h ^ a) ^ e & f ^ d & b ^ h;
			g = temp[25] + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = b & (g ^ h) ^ d & e ^ c & a ^ g;
			f = temp[26] + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = a & (f ^ g) ^ c & d ^ b & h ^ f;
			e = temp[27] + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = h & (e ^ f) ^ b & c ^ a & g ^ e;
			d = temp[28] + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = g & (d ^ e) ^ a & b ^ h & f ^ d;
			c = temp[29] + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = f & (c ^ d) ^ h & a ^ g & e ^ c;
			b = temp[30] + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = e & (b ^ c) ^ g & h ^ f & d ^ b;
			a = temp[31] + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = b & (g & ~a ^ c & f ^ d ^ e) ^ c & (g ^ f) ^ a & f ^ e;
			h = temp[5] + 0x452821E6 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = a & (f & ~h ^ b & e ^ c ^ d) ^ b & (f ^ e) ^ h & e ^ d;
			g = temp[14] + 0x38D01377 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = h & (e & ~g ^ a & d ^ b ^ c) ^ a & (e ^ d) ^ g & d ^ c;
			f = temp[26] + 0xBE5466CF + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = g & (d & ~f ^ h & c ^ a ^ b) ^ h & (d ^ c) ^ f & c ^ b;
			e = temp[18] + 0x34E90C6C + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = f & (c & ~e ^ g & b ^ h ^ a) ^ g & (c ^ b) ^ e & b ^ a;
			d = temp[11] + 0xC0AC29B7 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = e & (b & ~d ^ f & a ^ g ^ h) ^ f & (b ^ a) ^ d & a ^ h;
			c = temp[28] + 0xC97C50DD + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = d & (a & ~c ^ e & h ^ f ^ g) ^ e & (a ^ h) ^ c & h ^ g;
			b = temp[7] + 0x3F84D5B5 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = c & (h & ~b ^ d & g ^ e ^ f) ^ d & (h ^ g) ^ b & g ^ f;
			a = temp[16] + 0xB5470917 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = b & (g & ~a ^ c & f ^ d ^ e) ^ c & (g ^ f) ^ a & f ^ e;
			h = temp[0] + 0x9216D5D9 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = a & (f & ~h ^ b & e ^ c ^ d) ^ b & (f ^ e) ^ h & e ^ d;
			g = temp[23] + 0x8979FB1B + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = h & (e & ~g ^ a & d ^ b ^ c) ^ a & (e ^ d) ^ g & d ^ c;
			f = temp[20] + 0xD1310BA6 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = g & (d & ~f ^ h & c ^ a ^ b) ^ h & (d ^ c) ^ f & c ^ b;
			e = temp[22] + 0x98DFB5AC + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = f & (c & ~e ^ g & b ^ h ^ a) ^ g & (c ^ b) ^ e & b ^ a;
			d = temp[1] + 0x2FFD72DB + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = e & (b & ~d ^ f & a ^ g ^ h) ^ f & (b ^ a) ^ d & a ^ h;
			c = temp[10] + 0xD01ADFB7 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = d & (a & ~c ^ e & h ^ f ^ g) ^ e & (a ^ h) ^ c & h ^ g;
			b = temp[4] + 0xB8E1AFED + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = c & (h & ~b ^ d & g ^ e ^ f) ^ d & (h ^ g) ^ b & g ^ f;
			a = temp[8] + 0x6A267E96 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = b & (g & ~a ^ c & f ^ d ^ e) ^ c & (g ^ f) ^ a & f ^ e;
			h = temp[30] + 0xBA7C9045 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = a & (f & ~h ^ b & e ^ c ^ d) ^ b & (f ^ e) ^ h & e ^ d;
			g = temp[3] + 0xF12C7F99 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = h & (e & ~g ^ a & d ^ b ^ c) ^ a & (e ^ d) ^ g & d ^ c;
			f = temp[21] + 0x24A19947 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = g & (d & ~f ^ h & c ^ a ^ b) ^ h & (d ^ c) ^ f & c ^ b;
			e = temp[9] + 0xB3916CF7 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = f & (c & ~e ^ g & b ^ h ^ a) ^ g & (c ^ b) ^ e & b ^ a;
			d = temp[17] + 0x0801F2E2 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = e & (b & ~d ^ f & a ^ g ^ h) ^ f & (b ^ a) ^ d & a ^ h;
			c = temp[24] + 0x858EFC16 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = d & (a & ~c ^ e & h ^ f ^ g) ^ e & (a ^ h) ^ c & h ^ g;
			b = temp[29] + 0x636920D8 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = c & (h & ~b ^ d & g ^ e ^ f) ^ d & (h ^ g) ^ b & g ^ f;
			a = temp[6] + 0x71574E69 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = b & (g & ~a ^ c & f ^ d ^ e) ^ c & (g ^ f) ^ a & f ^ e;
			h = temp[19] + 0xA458FEA3 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = a & (f & ~h ^ b & e ^ c ^ d) ^ b & (f ^ e) ^ h & e ^ d;
			g = temp[12] + 0xF4933D7E + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = h & (e & ~g ^ a & d ^ b ^ c) ^ a & (e ^ d) ^ g & d ^ c;
			f = temp[15] + 0x0D95748F + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = g & (d & ~f ^ h & c ^ a ^ b) ^ h & (d ^ c) ^ f & c ^ b;
			e = temp[13] + 0x728EB658 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = f & (c & ~e ^ g & b ^ h ^ a) ^ g & (c ^ b) ^ e & b ^ a;
			d = temp[2] + 0x718BCD58 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = e & (b & ~d ^ f & a ^ g ^ h) ^ f & (b ^ a) ^ d & a ^ h;
			c = temp[25] + 0x82154AEE + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = d & (a & ~c ^ e & h ^ f ^ g) ^ e & (a ^ h) ^ c & h ^ g;
			b = temp[31] + 0x7B54A41D + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = c & (h & ~b ^ d & g ^ e ^ f) ^ d & (h ^ g) ^ b & g ^ f;
			a = temp[27] + 0xC25A59B5 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = g & (c & a ^ b ^ f) ^ c & d ^ a & e ^ f;
			h = temp[19] + 0x9C30D539 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = f & (b & h ^ a ^ e) ^ b & c ^ h & d ^ e;
			g = temp[9] + 0x2AF26013 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = e & (a & g ^ h ^ d) ^ a & b ^ g & c ^ d;
			f = temp[4] + 0xC5D1B023 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = d & (h & f ^ g ^ c) ^ h & a ^ f & b ^ c;
			e = temp[20] + 0x286085F0 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = c & (g & e ^ f ^ b) ^ g & h ^ e & a ^ b;
			d = temp[28] + 0xCA417918 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = b & (f & d ^ e ^ a) ^ f & g ^ d & h ^ a;
			c = temp[17] + 0xB8DB38EF + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = a & (e & c ^ d ^ h) ^ e & f ^ c & g ^ h;
			b = temp[8] + 0x8E79DCB0 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = h & (d & b ^ c ^ g) ^ d & e ^ b & f ^ g;
			a = temp[22] + 0x603A180E + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = g & (c & a ^ b ^ f) ^ c & d ^ a & e ^ f;
			h = temp[29] + 0x6C9E0E8B + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = f & (b & h ^ a ^ e) ^ b & c ^ h & d ^ e;
			g = temp[14] + 0xB01E8A3E + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = e & (a & g ^ h ^ d) ^ a & b ^ g & c ^ d;
			f = temp[25] + 0xD71577C1 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = d & (h & f ^ g ^ c) ^ h & a ^ f & b ^ c;
			e = temp[12] + 0xBD314B27 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = c & (g & e ^ f ^ b) ^ g & h ^ e & a ^ b;
			d = temp[24] + 0x78AF2FDA + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = b & (f & d ^ e ^ a) ^ f & g ^ d & h ^ a;
			c = temp[30] + 0x55605C60 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = a & (e & c ^ d ^ h) ^ e & f ^ c & g ^ h;
			b = temp[16] + 0xE65525F3 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = h & (d & b ^ c ^ g) ^ d & e ^ b & f ^ g;
			a = temp[26] + 0xAA55AB94 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = g & (c & a ^ b ^ f) ^ c & d ^ a & e ^ f;
			h = temp[31] + 0x57489862 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = f & (b & h ^ a ^ e) ^ b & c ^ h & d ^ e;
			g = temp[15] + 0x63E81440 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = e & (a & g ^ h ^ d) ^ a & b ^ g & c ^ d;
			f = temp[7] + 0x55CA396A + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = d & (h & f ^ g ^ c) ^ h & a ^ f & b ^ c;
			e = temp[3] + 0x2AAB10B6 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = c & (g & e ^ f ^ b) ^ g & h ^ e & a ^ b;
			d = temp[1] + 0xB4CC5C34 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = b & (f & d ^ e ^ a) ^ f & g ^ d & h ^ a;
			c = temp[0] + 0x1141E8CE + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = a & (e & c ^ d ^ h) ^ e & f ^ c & g ^ h;
			b = temp[18] + 0xA15486AF + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = h & (d & b ^ c ^ g) ^ d & e ^ b & f ^ g;
			a = temp[27] + 0x7C72E993 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = g & (c & a ^ b ^ f) ^ c & d ^ a & e ^ f;
			h = temp[13] + 0xB3EE1411 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = f & (b & h ^ a ^ e) ^ b & c ^ h & d ^ e;
			g = temp[6] + 0x636FBC2A + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = e & (a & g ^ h ^ d) ^ a & b ^ g & c ^ d;
			f = temp[21] + 0x2BA9C55D + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = d & (h & f ^ g ^ c) ^ h & a ^ f & b ^ c;
			e = temp[10] + 0x741831F6 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = c & (g & e ^ f ^ b) ^ g & h ^ e & a ^ b;
			d = temp[23] + 0xCE5C3E16 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = b & (f & d ^ e ^ a) ^ f & g ^ d & h ^ a;
			c = temp[11] + 0x9B87931E + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = a & (e & c ^ d ^ h) ^ e & f ^ c & g ^ h;
			b = temp[5] + 0xAFD6BA33 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = h & (d & b ^ c ^ g) ^ d & e ^ b & f ^ g;
			a = temp[2] + 0x6C24CF5C + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = a & (e & ~c ^ f & ~g ^ b ^ g ^ d) ^ f & (b & c ^ e ^ g) ^ c & g ^ d;
			h = temp[24] + 0x7A325381 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = h & (d & ~b ^ e & ~f ^ a ^ f ^ c) ^ e & (a & b ^ d ^ f) ^ b & f ^ c;
			g = temp[4] + 0x28958677 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = g & (c & ~a ^ d & ~e ^ h ^ e ^ b) ^ d & (h & a ^ c ^ e) ^ a & e ^ b;
			f = temp[0] + 0x3B8F4898 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = f & (b & ~h ^ c & ~d ^ g ^ d ^ a) ^ c & (g & h ^ b ^ d) ^ h & d ^ a;
			e = temp[14] + 0x6B4BB9AF + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = e & (a & ~g ^ b & ~c ^ f ^ c ^ h) ^ b & (f & g ^ a ^ c) ^ g & c ^ h;
			d = temp[2] + 0xC4BFE81B + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = d & (h & ~f ^ a & ~b ^ e ^ b ^ g) ^ a & (e & f ^ h ^ b) ^ f & b ^ g;
			c = temp[7] + 0x66282193 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = c & (g & ~e ^ h & ~a ^ d ^ a ^ f) ^ h & (d & e ^ g ^ a) ^ e & a ^ f;
			b = temp[28] + 0x61D809CC + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = b & (f & ~d ^ g & ~h ^ c ^ h ^ e) ^ g & (c & d ^ f ^ h) ^ d & h ^ e;
			a = temp[23] + 0xFB21A991 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = a & (e & ~c ^ f & ~g ^ b ^ g ^ d) ^ f & (b & c ^ e ^ g) ^ c & g ^ d;
			h = temp[26] + 0x487CAC60 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = h & (d & ~b ^ e & ~f ^ a ^ f ^ c) ^ e & (a & b ^ d ^ f) ^ b & f ^ c;
			g = temp[6] + 0x5DEC8032 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = g & (c & ~a ^ d & ~e ^ h ^ e ^ b) ^ d & (h & a ^ c ^ e) ^ a & e ^ b;
			f = temp[30] + 0xEF845D5D + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = f & (b & ~h ^ c & ~d ^ g ^ d ^ a) ^ c & (g & h ^ b ^ d) ^ h & d ^ a;
			e = temp[20] + 0xE98575B1 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = e & (a & ~g ^ b & ~c ^ f ^ c ^ h) ^ b & (f & g ^ a ^ c) ^ g & c ^ h;
			d = temp[18] + 0xDC262302 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = d & (h & ~f ^ a & ~b ^ e ^ b ^ g) ^ a & (e & f ^ h ^ b) ^ f & b ^ g;
			c = temp[25] + 0xEB651B88 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = c & (g & ~e ^ h & ~a ^ d ^ a ^ f) ^ h & (d & e ^ g ^ a) ^ e & a ^ f;
			b = temp[19] + 0x23893E81 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = b & (f & ~d ^ g & ~h ^ c ^ h ^ e) ^ g & (c & d ^ f ^ h) ^ d & h ^ e;
			a = temp[3] + 0xD396ACC5 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = a & (e & ~c ^ f & ~g ^ b ^ g ^ d) ^ f & (b & c ^ e ^ g) ^ c & g ^ d;
			h = temp[22] + 0x0F6D6FF3 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = h & (d & ~b ^ e & ~f ^ a ^ f ^ c) ^ e & (a & b ^ d ^ f) ^ b & f ^ c;
			g = temp[11] + 0x83F44239 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = g & (c & ~a ^ d & ~e ^ h ^ e ^ b) ^ d & (h & a ^ c ^ e) ^ a & e ^ b;
			f = temp[31] + 0x2E0B4482 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = f & (b & ~h ^ c & ~d ^ g ^ d ^ a) ^ c & (g & h ^ b ^ d) ^ h & d ^ a;
			e = temp[21] + 0xA4842004 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = e & (a & ~g ^ b & ~c ^ f ^ c ^ h) ^ b & (f & g ^ a ^ c) ^ g & c ^ h;
			d = temp[8] + 0x69C8F04A + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = d & (h & ~f ^ a & ~b ^ e ^ b ^ g) ^ a & (e & f ^ h ^ b) ^ f & b ^ g;
			c = temp[27] + 0x9E1F9B5E + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = c & (g & ~e ^ h & ~a ^ d ^ a ^ f) ^ h & (d & e ^ g ^ a) ^ e & a ^ f;
			b = temp[12] + 0x21C66842 + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = b & (f & ~d ^ g & ~h ^ c ^ h ^ e) ^ g & (c & d ^ f ^ h) ^ d & h ^ e;
			a = temp[9] + 0xF6E96C9A + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			t = a & (e & ~c ^ f & ~g ^ b ^ g ^ d) ^ f & (b & c ^ e ^ g) ^ c & g ^ d;
			h = temp[1] + 0x670C9C61 + (t >> 7 | t << 25) + (h >> 11 | h << 21);

			t = h & (d & ~b ^ e & ~f ^ a ^ f ^ c) ^ e & (a & b ^ d ^ f) ^ b & f ^ c;
			g = temp[29] + 0xABD388F0 + (t >> 7 | t << 25) + (g >> 11 | g << 21);

			t = g & (c & ~a ^ d & ~e ^ h ^ e ^ b) ^ d & (h & a ^ c ^ e) ^ a & e ^ b;
			f = temp[5] + 0x6A51A0D2 + (t >> 7 | t << 25) + (f >> 11 | f << 21);

			t = f & (b & ~h ^ c & ~d ^ g ^ d ^ a) ^ c & (g & h ^ b ^ d) ^ h & d ^ a;
			e = temp[15] + 0xD8542F68 + (t >> 7 | t << 25) + (e >> 11 | e << 21);

			t = e & (a & ~g ^ b & ~c ^ f ^ c ^ h) ^ b & (f & g ^ a ^ c) ^ g & c ^ h;
			d = temp[17] + 0x960FA728 + (t >> 7 | t << 25) + (d >> 11 | d << 21);

			t = d & (h & ~f ^ a & ~b ^ e ^ b ^ g) ^ a & (e & f ^ h ^ b) ^ f & b ^ g;
			c = temp[10] + 0xAB5133A3 + (t >> 7 | t << 25) + (c >> 11 | c << 21);

			t = c & (g & ~e ^ h & ~a ^ d ^ a ^ f) ^ h & (d & e ^ g ^ a) ^ e & a ^ f;
			b = temp[16] + 0x6EEF0B6C + (t >> 7 | t << 25) + (b >> 11 | b << 21);

			t = b & (f & ~d ^ g & ~h ^ c ^ h ^ e) ^ g & (c & d ^ f ^ h) ^ d & h ^ e;
			a = temp[13] + 0x137A3BE4 + (t >> 7 | t << 25) + (a >> 11 | a << 21);

			_tempHash[0] += a;
			_tempHash[1] += b;
			_tempHash[2] += c;
			_tempHash[3] += d;
			_tempHash[4] += e;
			_tempHash[5] += f;
			_tempHash[6] += g;
			_tempHash[7] += h;
		}
	}
}

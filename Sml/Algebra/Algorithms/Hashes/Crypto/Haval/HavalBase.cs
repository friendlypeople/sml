// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto.Haval
{
	internal abstract class HavalBase : CryptoHashAlgorithm
	{
		private readonly	int		_rounds;
		protected readonly uint[]	_tempHash = new uint[8];

		private const int HAVAL_VERSION = 1;

		protected HavalBase(HashRounds rounds, HashSize hashSize)
			: base(128, (int)hashSize)
		{
			_rounds = (int)rounds;
		}

		protected override void Setup()
		{
			base.Setup();

			_tempHash[0] = 0x243F6A88;
			_tempHash[1] = 0x85A308D3;
			_tempHash[2] = 0x13198A2E;
			_tempHash[3] = 0x03707344;
			_tempHash[4] = 0xA4093822;
			_tempHash[5] = 0x299F31D0;
			_tempHash[6] = 0x082EFA98;
			_tempHash[7] = 0xEC4E6C89;
		}

		private void TailorDigestBits()
		{
			uint t;

			switch (_hashSize)
			{
				case 16:
					t = (_tempHash[7] & 0x000000FF) | (_tempHash[6] & 0xFF000000) | (_tempHash[5] & 0x00FF0000) | (_tempHash[4] & 0x0000FF00);
					_tempHash[0] += t >> 8 | t << 24;
					t = (_tempHash[7] & 0x0000FF00) | (_tempHash[6] & 0x000000FF) | (_tempHash[5] & 0xFF000000) | (_tempHash[4] & 0x00FF0000);
					_tempHash[1] += t >> 16 | t << 16;
					t = (_tempHash[7] & 0x00FF0000) | (_tempHash[6] & 0x0000FF00) | (_tempHash[5] & 0x000000FF) | (_tempHash[4] & 0xFF000000);
					_tempHash[2] += t >> 24 | t << 8;
					t = (_tempHash[7] & 0xFF000000) | (_tempHash[6] & 0x00FF0000) | (_tempHash[5] & 0x0000FF00) | (_tempHash[4] & 0x000000FF);
					_tempHash[3] += t;
					break;
				case 20:
					t = _tempHash[7] & 0x3F | (uint)(_tempHash[6] & (0x7F << 25)) | _tempHash[5] & (0x3F << 19);
					_tempHash[0] += t >> 19 | t << 13;
					t = _tempHash[7] & (0x3F << 6) | _tempHash[6] & 0x3F | (uint)(_tempHash[5] & (0x7F << 25));
					_tempHash[1] += t >> 25 | t << 7;
					t = (_tempHash[7] & (0x7F << 12)) | (_tempHash[6] & (0x3F << 6)) | (_tempHash[5] & 0x3F);
					_tempHash[2] += t;
					t = (_tempHash[7] & (0x3F << 19)) | (_tempHash[6] & (0x7F << 12)) | (_tempHash[5] & (0x3F << 6));
					_tempHash[3] += (t >> 6);
					t = (_tempHash[7] & ((uint)0x7F << 25)) | _tempHash[6] & (0x3F << 19) | _tempHash[5] & (0x7F << 12);
					_tempHash[4] += (t >> 12);
					break;
				case 24:
					t = _tempHash[7] & 0x1F | (uint)(_tempHash[6] & (0x3F << 26));
					_tempHash[0] += t >> 26 | t << 6;
					t = (_tempHash[7] & (0x1F << 5)) | (_tempHash[6] & 0x1F);
					_tempHash[1] += t;
					t = (_tempHash[7] & (0x3F << 10)) | (_tempHash[6] & (0x1F << 5));
					_tempHash[2] += (t >> 5);
					t = (_tempHash[7] & (0x1F << 16)) | (_tempHash[6] & (0x3F << 10));
					_tempHash[3] += (t >> 10);
					t = (_tempHash[7] & (0x1F << 21)) | (_tempHash[6] & (0x1F << 16));
					_tempHash[4] += (t >> 16);
					t = (uint)(_tempHash[7] & (0x3F << 26)) | _tempHash[6] & (0x1F << 21);
					_tempHash[5] += (t >> 21);
					break;
				case 28:
					_tempHash[0] += ((_tempHash[7] >> 27) & 0x1F);
					_tempHash[1] += ((_tempHash[7] >> 22) & 0x1F);
					_tempHash[2] += ((_tempHash[7] >> 18) & 0x0F);
					_tempHash[3] += ((_tempHash[7] >> 13) & 0x1F);
					_tempHash[4] += ((_tempHash[7] >> 9) & 0x0F);
					_tempHash[5] += ((_tempHash[7] >> 4) & 0x1F);
					_tempHash[6] += (_tempHash[7] & 0x0F);
					break;
			}
		}

		protected override void FinalStep()
		{
			TailorDigestBits();
			_hash = Utils.TransformToBytes(_tempHash, 0, _hashSize / 4);
		}

		protected override byte[] CreateFinalBlock(byte[] block, int index, int length)
		{
			ulong bits = _processedBytes * 8;
			int bufLength = (length < 118) ? 128 : 256;
			byte[] pad = new byte[bufLength];

			int pos = bufLength - 10;

			Array.Copy(block, index, pad, 0, length);
			pad[length] = 0x01;
			pad[pos++] = (byte)((_rounds << 3) | (HAVAL_VERSION & 0x07));
			pad[pos++] = (byte)(_hashSize << 1);

			Utils.WriteUlongToByteArray(bits, pad, pos);
			return pad;
		}
	}
}

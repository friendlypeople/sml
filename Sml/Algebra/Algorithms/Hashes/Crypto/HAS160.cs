// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;
using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto
{
	internal class HAS160 : CryptoHashAlgorithm
	{
		private static readonly int[] SRot = new[]
        {
            5, 11,  7, 15,  6, 13,  8, 14,  7, 12,  9, 11,  8, 15,  6, 12,  9, 14,  5, 13
        };

		private static readonly int[] STor = new[] 
        {
            27, 21, 25, 17, 26, 19, 24, 18, 25, 20, 23, 21, 24, 17, 26, 20, 23, 18, 27, 19
        };

		private static readonly int[] SIndex = new[]
        {
            18,  0,  1,  2,  3, 19,  4,  5, 6,  7, 16,  8,  9, 10, 11, 17, 12, 13, 14, 15,
            18,  3,  6,  9, 12, 19, 15,  2, 5,  8, 16, 11, 14,  1,  4, 17,  7, 10, 13,  0,
            18, 12,  5, 14,  7, 19,  0,  9, 2, 11, 16,  4, 13,  6, 15, 17,  8,  1, 10,  3,
            18,  7,  2, 13,  8, 19,  3, 14, 9,  4, 16, 15, 10,  5,  0, 17, 11,  6,  1, 12
        };

		private readonly uint[] _tempHash = new uint[5];

		public HAS160() : base(64, 20) { }

		protected override void Setup()
		{
			base.Setup();

			_tempHash[0] = 0x67452301;
			_tempHash[1] = 0xEFCDAB89;
			_tempHash[2] = 0x98BADCFE;
			_tempHash[3] = 0x10325476;
			_tempHash[4] = 0xC3D2E1F0;

		}

		protected override byte[] CreateFinalBlock(byte[] block, int index, int length)
		{
			ulong bits = _processedBytes * 8;
			int bufLength = (length < 56) ? 64 : 128;

			byte[] pad = new byte[bufLength];
			Array.Copy(block, index, pad, 0, length);
			pad[length] = 0x80;
			Utils.WriteUlongToByteArray(bits, pad, bufLength - 8);

			return pad;
		}

		protected override void TransformBlock(byte[] block, int index)
		{
			uint a = _tempHash[0];
			uint b = _tempHash[1];
			uint c = _tempHash[2];
			uint d = _tempHash[3];
			uint e = _tempHash[4];

			uint[] data = new uint[20];
			Utils.TransformToUints(block, data, index, _bufferSize);

			data[16] = data[0] ^ data[1] ^ data[2] ^ data[3];
			data[17] = data[4] ^ data[5] ^ data[6] ^ data[7];
			data[18] = data[8] ^ data[9] ^ data[10] ^ data[11];
			data[19] = data[12] ^ data[13] ^ data[14] ^ data[15];

			for (int r = 0; r < 20; r++)
			{
				uint T = data[SIndex[r]] + (a << SRot[r] | a >> STor[r]) + ((b & c) | (~b & d)) + e;
				e = d;
				d = c;
				c = b << 10 | b >> 22;
				b = a;
				a = T;
			}

			data[16] = data[3] ^ data[6] ^ data[9] ^ data[12];
			data[17] = data[2] ^ data[5] ^ data[8] ^ data[15];
			data[18] = data[1] ^ data[4] ^ data[11] ^ data[14];
			data[19] = data[0] ^ data[7] ^ data[10] ^ data[13];

			for (int r = 20; r < 40; r++)
			{
				uint T = data[SIndex[r]] + 0x5A827999 + (a << SRot[r - 20] | a >> STor[r - 20]) + (b ^ c ^ d) + e;
				e = d;
				d = c;
				c = b << 17 | b >> 15;
				b = a;
				a = T;
			}

			data[16] = data[5] ^ data[7] ^ data[12] ^ data[14];
			data[17] = data[0] ^ data[2] ^ data[9] ^ data[11];
			data[18] = data[4] ^ data[6] ^ data[13] ^ data[15];
			data[19] = data[1] ^ data[3] ^ data[8] ^ data[10];

			for (int r = 40; r < 60; r++)
			{
				uint T = data[SIndex[r]] + 0x6ED9EBA1 + (a << SRot[r - 40] | a >> STor[r - 40]) + (c ^ (b | ~d)) + e;
				e = d;
				d = c;
				c = b << 25 | b >> 7;
				b = a;
				a = T;
			}

			data[16] = data[2] ^ data[7] ^ data[8] ^ data[13];
			data[17] = data[3] ^ data[4] ^ data[9] ^ data[14];
			data[18] = data[0] ^ data[5] ^ data[10] ^ data[15];
			data[19] = data[1] ^ data[6] ^ data[11] ^ data[12];

			for (int r = 60; r < 80; r++)
			{
				uint T = data[SIndex[r]] + 0x8F1BBCDC + (a << SRot[r - 60] | a >> STor[r - 60]) + (b ^ c ^ d) + e;
				e = d;
				d = c;
				c = b << 30 | b >> 2;
				b = a;
				a = T;
			}

			_tempHash[0] += a;
			_tempHash[1] += b;
			_tempHash[2] += c;
			_tempHash[3] += d;
			_tempHash[4] += e;
		}

		protected override void FinalStep()
		{
			_hash = Utils.TransformToBytes(_tempHash);
		}
	}
}

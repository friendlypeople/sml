// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;
using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto
{
	internal class Whirlpool : CryptoHashAlgorithm
	{
		public Whirlpool() : base(64, 64) { }

		private const int	ROUNDS					= 10;
		private const uint	REDUCTION_POLYNOMIAL	= 0x011D;

		private static readonly uint[] SSbox =
        {
            0x18, 0x23, 0xc6, 0xe8, 0x87, 0xb8, 0x01, 0x4f, 0x36, 0xa6, 0xd2, 0xf5, 0x79, 0x6f, 0x91, 0x52,
            0x60, 0xbc, 0x9b, 0x8e, 0xa3, 0x0c, 0x7b, 0x35, 0x1d, 0xe0, 0xd7, 0xc2, 0x2e, 0x4b, 0xfe, 0x57,
            0x15, 0x77, 0x37, 0xe5, 0x9f, 0xf0, 0x4a, 0xda, 0x58, 0xc9, 0x29, 0x0a, 0xb1, 0xa0, 0x6b, 0x85,
            0xbd, 0x5d, 0x10, 0xf4, 0xcb, 0x3e, 0x05, 0x67, 0xe4, 0x27, 0x41, 0x8b, 0xa7, 0x7d, 0x95, 0xd8,
            0xfb, 0xee, 0x7c, 0x66, 0xdd, 0x17, 0x47, 0x9e, 0xca, 0x2d, 0xbf, 0x07, 0xad, 0x5a, 0x83, 0x33,
            0x63, 0x02, 0xaa, 0x71, 0xc8, 0x19, 0x49, 0xd9, 0xf2, 0xe3, 0x5b, 0x88, 0x9a, 0x26, 0x32, 0xb0,
            0xe9, 0x0f, 0xd5, 0x80, 0xbe, 0xcd, 0x34, 0x48, 0xff, 0x7a, 0x90, 0x5f, 0x20, 0x68, 0x1a, 0xae,
            0xb4, 0x54, 0x93, 0x22, 0x64, 0xf1, 0x73, 0x12, 0x40, 0x08, 0xc3, 0xec, 0xdb, 0xa1, 0x8d, 0x3d,
            0x97, 0x00, 0xcf, 0x2b, 0x76, 0x82, 0xd6, 0x1b, 0xb5, 0xaf, 0x6a, 0x50, 0x45, 0xf3, 0x30, 0xef,
            0x3f, 0x55, 0xa2, 0xea, 0x65, 0xba, 0x2f, 0xc0, 0xde, 0x1c, 0xfd, 0x4d, 0x92, 0x75, 0x06, 0x8a,
            0xb2, 0xe6, 0x0e, 0x1f, 0x62, 0xd4, 0xa8, 0x96, 0xf9, 0xc5, 0x25, 0x59, 0x84, 0x72, 0x39, 0x4c,
            0x5e, 0x78, 0x38, 0x8c, 0xd1, 0xa5, 0xe2, 0x61, 0xb3, 0x21, 0x9c, 0x1e, 0x43, 0xc7, 0xfc, 0x04,
            0x51, 0x99, 0x6d, 0x0d, 0xfa, 0xdf, 0x7e, 0x24, 0x3b, 0xab, 0xce, 0x11, 0x8f, 0x4e, 0xb7, 0xeb,
            0x3c, 0x81, 0x94, 0xf7, 0xb9, 0x13, 0x2c, 0xd3, 0xe7, 0x6e, 0xc4, 0x03, 0x56, 0x44, 0x7f, 0xa9,
            0x2a, 0xbb, 0xc1, 0x53, 0xdc, 0x0b, 0x9d, 0x6c, 0x31, 0x74, 0xf6, 0x46, 0xac, 0x89, 0x14, 0xe1,
            0x16, 0x3a, 0x69, 0x09, 0x70, 0xb6, 0xd0, 0xed, 0xcc, 0x42, 0x98, 0xa4, 0x28, 0x5c, 0xf8, 0x86
        };

		private static readonly ulong[] Sc0 = new ulong[256];
		private static readonly ulong[] Sc1 = new ulong[256];
		private static readonly ulong[] Sc2 = new ulong[256];
		private static readonly ulong[] Sc3 = new ulong[256];
		private static readonly ulong[] Sc4 = new ulong[256];
		private static readonly ulong[] Sc5 = new ulong[256];
		private static readonly ulong[] Sc6 = new ulong[256];
		private static readonly ulong[] Sc7 = new ulong[256];

		private static readonly ulong[] Src = new ulong[ROUNDS + 1];

		private readonly ulong[] _tempHash = new ulong[8];

		static Whirlpool()
		{
			for (int i = 0; i < 256; i++)
			{
				uint v1 = SSbox[i];
				uint v2 = MaskWithReductionPolynomial(v1 << 1);
				uint v4 = MaskWithReductionPolynomial(v2 << 1);
				uint v5 = v4 ^ v1;
				uint v8 = MaskWithReductionPolynomial(v4 << 1);
				uint v9 = v8 ^ v1;

				Sc0[i] = PackIntoUlong(v1, v1, v4, v1, v8, v5, v2, v9);
				Sc1[i] = PackIntoUlong(v9, v1, v1, v4, v1, v8, v5, v2);
				Sc2[i] = PackIntoUlong(v2, v9, v1, v1, v4, v1, v8, v5);
				Sc3[i] = PackIntoUlong(v5, v2, v9, v1, v1, v4, v1, v8);
				Sc4[i] = PackIntoUlong(v8, v5, v2, v9, v1, v1, v4, v1);
				Sc5[i] = PackIntoUlong(v1, v8, v5, v2, v9, v1, v1, v4);
				Sc6[i] = PackIntoUlong(v4, v1, v8, v5, v2, v9, v1, v1);
				Sc7[i] = PackIntoUlong(v1, v4, v1, v8, v5, v2, v9, v1);
			}

			Src[0] = 0;

			for (int r = 1; r <= ROUNDS; r++)
			{
				int i = 8 * (r - 1);
				Src[r] = (Sc0[i] & 0xff00000000000000) ^
						  (Sc1[i + 1] & 0x00ff000000000000) ^
						  (Sc2[i + 2] & 0x0000ff0000000000) ^
						  (Sc3[i + 3] & 0x000000ff00000000) ^
						  (Sc4[i + 4] & 0x00000000ff000000) ^
						  (Sc5[i + 5] & 0x0000000000ff0000) ^
						  (Sc6[i + 6] & 0x000000000000ff00) ^
						  (Sc7[i + 7] & 0x00000000000000ff);
			}

		}

		private static ulong PackIntoUlong(uint b7, uint b6, uint b5, uint b4, uint b3, uint b2, uint b1, uint b0)
		{
			return ((ulong)b7 << 56) ^
				   ((ulong)b6 << 48) ^
				   ((ulong)b5 << 40) ^
				   ((ulong)b4 << 32) ^
				   ((ulong)b3 << 24) ^
				   ((ulong)b2 << 16) ^
				   ((ulong)b1 << 8) ^
				   b0;
		}

		private static uint MaskWithReductionPolynomial(uint input)
		{
			if (input >= 0x100)
				input ^= REDUCTION_POLYNOMIAL;
			return input;
		}


		protected override void Setup()
		{
			base.Setup();
			Utils.ZeroMemory(_tempHash);
		}

		protected override byte[] CreateFinalBlock(byte[] block, int index, int length)
		{
			ulong bits = _processedBytes * 8;
			int bufLength = (length > 31) ? 128 : 64;

			byte[] pad = new byte[bufLength];
			Array.Copy(block, index, pad, 0, length);
			pad[length] = 0x80;
			Utils.WriteUlongToByteArraySwapOrder(bits, pad, bufLength - 8);

			return pad;
		}

		protected override void TransformBlock(byte[] block, int index)
		{
			ulong[] k = new ulong[8];
			ulong[] m = new ulong[8];

			ulong[] temp = new ulong[8];
			ulong[] data = new ulong[8];

			Utils.TransformToUlongsSwapOrder(block, index, _bufferSize, data, 0);

			for (int i = 0; i < 8; i++)
			{
				k[i] = _tempHash[i];
				temp[i] = data[i] ^ k[i];
			}

			for (int round = 1; round <= ROUNDS; round++)
			{
				for (int i = 0; i < 8; i++)
				{
					m[i] = 0;
					m[i] ^= Sc0[(byte)(k[(i - 0) & 7] >> 56)];
					m[i] ^= Sc1[(byte)(k[(i - 1) & 7] >> 48)];
					m[i] ^= Sc2[(byte)(k[(i - 2) & 7] >> 40)];
					m[i] ^= Sc3[(byte)(k[(i - 3) & 7] >> 32)];
					m[i] ^= Sc4[(byte)(k[(i - 4) & 7] >> 24)];
					m[i] ^= Sc5[(byte)(k[(i - 5) & 7] >> 16)];
					m[i] ^= Sc6[(byte)(k[(i - 6) & 7] >> 8)];
					m[i] ^= Sc7[(byte)(k[(i - 7) & 7])];
				}

				Array.Copy(m, 0, k, 0, k.Length);

				k[0] ^= Src[round];

				for (int i = 0; i < 8; i++)
				{
					m[i] = k[i];

					m[i] ^= Sc0[(byte)(temp[(i - 0) & 7] >> 56)];
					m[i] ^= Sc1[(byte)(temp[(i - 1) & 7] >> 48)];
					m[i] ^= Sc2[(byte)(temp[(i - 2) & 7] >> 40)];
					m[i] ^= Sc3[(byte)(temp[(i - 3) & 7] >> 32)];
					m[i] ^= Sc4[(byte)(temp[(i - 4) & 7] >> 24)];
					m[i] ^= Sc5[(byte)(temp[(i - 5) & 7] >> 16)];
					m[i] ^= Sc6[(byte)(temp[(i - 6) & 7] >> 8)];
					m[i] ^= Sc7[(byte)(temp[(i - 7) & 7])];
				}
				Array.Copy(m, 0, temp, 0, temp.Length);
			}

			for (int i = 0; i < 8; i++)
				_tempHash[i] ^= temp[i] ^ data[i];
		}

		protected override void FinalStep()
		{
			_hash = Utils.TransformToBytesSwapOrder(_tempHash);
		}
	}
}

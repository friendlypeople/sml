// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto.Tiger
{
	internal abstract class TigerBase : CryptoHashAlgorithm
	{
		private readonly	byte	_padData;
		private readonly	int		_rounds;
		protected readonly	ulong[] _tempHash = new ulong[3];

		protected TigerBase(HashRounds rounds, byte padData)
			: base(64, 24)
		{
			_padData = padData;
			_rounds = (int)rounds;
		}

		protected override void Setup()
		{
			base.Setup();

			_tempHash[0] = 0x0123456789ABCDEF;
			_tempHash[1] = 0xFEDCBA9876543210;
			_tempHash[2] = 0xF096A5B4C3B2E187;
		}

		protected override byte[] CreateFinalBlock(byte[] block, int index, int length)
		{
			ulong bits = _processedBytes * 8;
			int bufLength = (length < 56) ? 64 : 128;


			byte[] pad = new byte[bufLength];
			Array.Copy(block, index, pad, 0, length);
			pad[length] = _padData;

			Utils.WriteUlongToByteArray(bits, pad, bufLength - 8);
			return pad;
		}

		protected override void TransformBlock(byte[] block, int index)
		{
			ulong a = _tempHash[0];
			ulong b = _tempHash[1];
			ulong c = _tempHash[2];

			ulong data0 = Utils.ConvertBytesToUlong(block, index + 8 * 0);
			ulong data1 = Utils.ConvertBytesToUlong(block, index + 8 * 1);
			ulong data2 = Utils.ConvertBytesToUlong(block, index + 8 * 2);
			ulong data3 = Utils.ConvertBytesToUlong(block, index + 8 * 3);
			ulong data4 = Utils.ConvertBytesToUlong(block, index + 8 * 4);
			ulong data5 = Utils.ConvertBytesToUlong(block, index + 8 * 5);
			ulong data6 = Utils.ConvertBytesToUlong(block, index + 8 * 6);
			ulong data7 = Utils.ConvertBytesToUlong(block, index + 8 * 7);

			

			c ^= data0;
			a -= TigerConst.T1[(byte)c] ^ TigerConst.T2[(byte)(c >> 16)] ^ TigerConst.T3[(byte)(c >> 32)] ^ TigerConst.T4[(byte)(c >> 48)];
			b += TigerConst.T4[(byte)(c >> 8) & 0xff] ^ TigerConst.T3[(byte)(c >> 24)] ^ TigerConst.T2[(byte)(c >> 40)] ^ TigerConst.T1[(byte)(c >> 56)];
			b *= 5;

			a ^= data1;
			b -= TigerConst.T1[(byte)a] ^ TigerConst.T2[(byte)(a >> 16)] ^ TigerConst.T3[(byte)(a >> 32)] ^ TigerConst.T4[(byte)(a >> 48)];
			c += TigerConst.T4[(byte)(a >> 8)] ^ TigerConst.T3[(byte)(a >> 24)] ^ TigerConst.T2[(byte)(a >> 40)] ^ TigerConst.T1[(byte)(a >> 56)];
			c *= 5;

			b ^= data2;
			c -= TigerConst.T1[(byte)b] ^ TigerConst.T2[(byte)(b >> 16)] ^ TigerConst.T3[(byte)(b >> 32)] ^ TigerConst.T4[(byte)(b >> 48)];
			a += TigerConst.T4[(byte)(b >> 8)] ^ TigerConst.T3[(byte)(b >> 24)] ^ TigerConst.T2[(byte)(b >> 40)] ^ TigerConst.T1[(byte)(b >> 56)];
			a *= 5;

			c ^= data3;
			a -= TigerConst.T1[(byte)c] ^ TigerConst.T2[(byte)(c >> 16)] ^ TigerConst.T3[(byte)(c >> 32)] ^ TigerConst.T4[(byte)(c >> 48)];
			b += TigerConst.T4[(byte)(c >> 8) & 0xff] ^ TigerConst.T3[(byte)(c >> 24)] ^ TigerConst.T2[(byte)(c >> 40)] ^ TigerConst.T1[(byte)(c >> 56)];
			b *= 5;

			a ^= data4;
			b -= TigerConst.T1[(byte)a] ^ TigerConst.T2[(byte)(a >> 16)] ^ TigerConst.T3[(byte)(a >> 32)] ^ TigerConst.T4[(byte)(a >> 48)];
			c += TigerConst.T4[(byte)(a >> 8)] ^ TigerConst.T3[(byte)(a >> 24)] ^ TigerConst.T2[(byte)(a >> 40)] ^ TigerConst.T1[(byte)(a >> 56)];
			c *= 5;

			b ^= data5;
			c -= TigerConst.T1[(byte)b] ^ TigerConst.T2[(byte)(b >> 16)] ^ TigerConst.T3[(byte)(b >> 32)] ^ TigerConst.T4[(byte)(b >> 48)];
			a += TigerConst.T4[(byte)(b >> 8)] ^ TigerConst.T3[(byte)(b >> 24)] ^ TigerConst.T2[(byte)(b >> 40)] ^ TigerConst.T1[(byte)(b >> 56)];
			a *= 5;

			c ^= data6;
			a -= TigerConst.T1[(byte)c] ^ TigerConst.T2[(byte)(c >> 16)] ^ TigerConst.T3[(byte)(c >> 32)] ^ TigerConst.T4[(byte)(c >> 48)];
			b += TigerConst.T4[(byte)(c >> 8) & 0xff] ^ TigerConst.T3[(byte)(c >> 24)] ^ TigerConst.T2[(byte)(c >> 40)] ^ TigerConst.T1[(byte)(c >> 56)];
			b *= 5;

			a ^= data7;
			b -= TigerConst.T1[(byte)a] ^ TigerConst.T2[(byte)(a >> 16)] ^ TigerConst.T3[(byte)(a >> 32)] ^ TigerConst.T4[(byte)(a >> 48)];
			c += TigerConst.T4[(byte)(a >> 8)] ^ TigerConst.T3[(byte)(a >> 24)] ^ TigerConst.T2[(byte)(a >> 40)] ^ TigerConst.T1[(byte)(a >> 56)];
			c *= 5;

			data0 -= data7 ^ TigerConst.C1;
			data1 ^= data0;
			data2 += data1;
			data3 -= data2 ^ (~data1 << 19);
			data4 ^= data3;
			data5 += data4;
			data6 -= data5 ^ (~data4 >> 23);
			data7 ^= data6;
			data0 += data7;
			data1 -= data0 ^ (~data7 << 19);
			data2 ^= data1;
			data3 += data2;
			data4 -= data3 ^ (~data2 >> 23);
			data5 ^= data4;
			data6 += data5;
			data7 -= data6 ^ TigerConst.C2;

			b ^= data0;
			c -= TigerConst.T1[(byte)b] ^ TigerConst.T2[(byte)(b >> 16)] ^ TigerConst.T3[(byte)(b >> 32)] ^ TigerConst.T4[(byte)(b >> 48)];
			a += TigerConst.T4[(byte)(b >> 8)] ^ TigerConst.T3[(byte)(b >> 24)] ^ TigerConst.T2[(byte)(b >> 40)] ^ TigerConst.T1[(byte)(b >> 56)];
			a *= 7;

			c ^= data1;
			a -= TigerConst.T1[(byte)c] ^ TigerConst.T2[(byte)(c >> 16)] ^ TigerConst.T3[(byte)(c >> 32)] ^ TigerConst.T4[(byte)(c >> 48)];
			b += TigerConst.T4[(byte)(c >> 8) & 0xff] ^ TigerConst.T3[(byte)(c >> 24)] ^ TigerConst.T2[(byte)(c >> 40)] ^ TigerConst.T1[(byte)(c >> 56)];
			b *= 7;

			a ^= data2;
			b -= TigerConst.T1[(byte)a] ^ TigerConst.T2[(byte)(a >> 16)] ^ TigerConst.T3[(byte)(a >> 32)] ^ TigerConst.T4[(byte)(a >> 48)];
			c += TigerConst.T4[(byte)(a >> 8)] ^ TigerConst.T3[(byte)(a >> 24)] ^ TigerConst.T2[(byte)(a >> 40)] ^ TigerConst.T1[(byte)(a >> 56)];
			c *= 7;

			b ^= data3;
			c -= TigerConst.T1[(byte)b] ^ TigerConst.T2[(byte)(b >> 16)] ^ TigerConst.T3[(byte)(b >> 32)] ^ TigerConst.T4[(byte)(b >> 48)];
			a += TigerConst.T4[(byte)(b >> 8)] ^ TigerConst.T3[(byte)(b >> 24)] ^ TigerConst.T2[(byte)(b >> 40)] ^ TigerConst.T1[(byte)(b >> 56)];
			a *= 7;

			c ^= data4;
			a -= TigerConst.T1[(byte)c] ^ TigerConst.T2[(byte)(c >> 16)] ^ TigerConst.T3[(byte)(c >> 32)] ^ TigerConst.T4[(byte)(c >> 48)];
			b += TigerConst.T4[(byte)(c >> 8) & 0xff] ^ TigerConst.T3[(byte)(c >> 24)] ^ TigerConst.T2[(byte)(c >> 40)] ^ TigerConst.T1[(byte)(c >> 56)];
			b *= 7;

			a ^= data5;
			b -= TigerConst.T1[(byte)a] ^ TigerConst.T2[(byte)(a >> 16)] ^ TigerConst.T3[(byte)(a >> 32)] ^ TigerConst.T4[(byte)(a >> 48)];
			c += TigerConst.T4[(byte)(a >> 8)] ^ TigerConst.T3[(byte)(a >> 24)] ^ TigerConst.T2[(byte)(a >> 40)] ^ TigerConst.T1[(byte)(a >> 56)];
			c *= 7;

			b ^= data6;
			c -= TigerConst.T1[(byte)b] ^ TigerConst.T2[(byte)(b >> 16)] ^ TigerConst.T3[(byte)(b >> 32)] ^ TigerConst.T4[(byte)(b >> 48)];
			a += TigerConst.T4[(byte)(b >> 8)] ^ TigerConst.T3[(byte)(b >> 24)] ^ TigerConst.T2[(byte)(b >> 40)] ^ TigerConst.T1[(byte)(b >> 56)];
			a *= 7;

			c ^= data7;
			a -= TigerConst.T1[(byte)c] ^ TigerConst.T2[(byte)(c >> 16)] ^ TigerConst.T3[(byte)(c >> 32)] ^ TigerConst.T4[(byte)(c >> 48)];
			b += TigerConst.T4[(byte)(c >> 8) & 0xff] ^ TigerConst.T3[(byte)(c >> 24)] ^ TigerConst.T2[(byte)(c >> 40)] ^ TigerConst.T1[(byte)(c >> 56)];
			b *= 7;

			data0 -= data7 ^ TigerConst.C1;
			data1 ^= data0;
			data2 += data1;
			data3 -= data2 ^ (~data1 << 19);
			data4 ^= data3;
			data5 += data4;
			data6 -= data5 ^ (~data4 >> 23);
			data7 ^= data6;
			data0 += data7;
			data1 -= data0 ^ (~data7 << 19);
			data2 ^= data1;
			data3 += data2;
			data4 -= data3 ^ (~data2 >> 23);
			data5 ^= data4;
			data6 += data5;
			data7 -= data6 ^ TigerConst.C2;

			a ^= data0;
			b -= TigerConst.T1[(byte)a] ^ TigerConst.T2[(byte)(a >> 16)] ^ TigerConst.T3[(byte)(a >> 32)] ^ TigerConst.T4[(byte)(a >> 48)];
			c += TigerConst.T4[(byte)(a >> 8)] ^ TigerConst.T3[(byte)(a >> 24)] ^ TigerConst.T2[(byte)(a >> 40)] ^ TigerConst.T1[(byte)(a >> 56)];
			c *= 9;

			b ^= data1;
			c -= TigerConst.T1[(byte)b] ^ TigerConst.T2[(byte)(b >> 16)] ^ TigerConst.T3[(byte)(b >> 32)] ^ TigerConst.T4[(byte)(b >> 48)];
			a += TigerConst.T4[(byte)(b >> 8)] ^ TigerConst.T3[(byte)(b >> 24)] ^ TigerConst.T2[(byte)(b >> 40)] ^ TigerConst.T1[(byte)(b >> 56)];
			a *= 9;

			c ^= data2;
			a -= TigerConst.T1[(byte)c] ^ TigerConst.T2[(byte)(c >> 16)] ^ TigerConst.T3[(byte)(c >> 32)] ^ TigerConst.T4[(byte)(c >> 48)];
			b += TigerConst.T4[(byte)(c >> 8) & 0xff] ^ TigerConst.T3[(byte)(c >> 24)] ^ TigerConst.T2[(byte)(c >> 40)] ^ TigerConst.T1[(byte)(c >> 56)];
			b *= 9;

			a ^= data3;
			b -= TigerConst.T1[(byte)a] ^ TigerConst.T2[(byte)(a >> 16)] ^ TigerConst.T3[(byte)(a >> 32)] ^ TigerConst.T4[(byte)(a >> 48)];
			c += TigerConst.T4[(byte)(a >> 8)] ^ TigerConst.T3[(byte)(a >> 24)] ^ TigerConst.T2[(byte)(a >> 40)] ^ TigerConst.T1[(byte)(a >> 56)];
			c *= 9;

			b ^= data4;
			c -= TigerConst.T1[(byte)b] ^ TigerConst.T2[(byte)(b >> 16)] ^ TigerConst.T3[(byte)(b >> 32)] ^ TigerConst.T4[(byte)(b >> 48)];
			a += TigerConst.T4[(byte)(b >> 8)] ^ TigerConst.T3[(byte)(b >> 24)] ^ TigerConst.T2[(byte)(b >> 40)] ^ TigerConst.T1[(byte)(b >> 56)];
			a *= 9;

			c ^= data5;
			a -= TigerConst.T1[(byte)c] ^ TigerConst.T2[(byte)(c >> 16)] ^ TigerConst.T3[(byte)(c >> 32)] ^ TigerConst.T4[(byte)(c >> 48)];
			b += TigerConst.T4[(byte)(c >> 8) & 0xff] ^ TigerConst.T3[(byte)(c >> 24)] ^ TigerConst.T2[(byte)(c >> 40)] ^ TigerConst.T1[(byte)(c >> 56)];
			b *= 9;

			a ^= data6;
			b -= TigerConst.T1[(byte)a] ^ TigerConst.T2[(byte)(a >> 16)] ^ TigerConst.T3[(byte)(a >> 32)] ^ TigerConst.T4[(byte)(a >> 48)];
			c += TigerConst.T4[(byte)(a >> 8)] ^ TigerConst.T3[(byte)(a >> 24)] ^ TigerConst.T2[(byte)(a >> 40)] ^ TigerConst.T1[(byte)(a >> 56)];
			c *= 9;

			b ^= data7;
			c -= TigerConst.T1[(byte)b] ^ TigerConst.T2[(byte)(b >> 16)] ^ TigerConst.T3[(byte)(b >> 32)] ^ TigerConst.T4[(byte)(b >> 48)];
			a += TigerConst.T4[(byte)(b >> 8)] ^ TigerConst.T3[(byte)(b >> 24)] ^ TigerConst.T2[(byte)(b >> 40)] ^ TigerConst.T1[(byte)(b >> 56)];
			a *= 9;

			for (int rounds = 3; rounds < _rounds; rounds++)
			{
				data0 -= data7 ^ TigerConst.C1;
				data1 ^= data0;
				data2 += data1;
				data3 -= data2 ^ (~data1 << 19);
				data4 ^= data3;
				data5 += data4;
				data6 -= data5 ^ (~data4 >> 23);
				data7 ^= data6;
				data0 += data7;
				data1 -= data0 ^ (~data7 << 19);
				data2 ^= data1;
				data3 += data2;
				data4 -= data3 ^ (~data2 >> 23);
				data5 ^= data4;
				data6 += data5;
				data7 -= data6 ^ TigerConst.C2;

				c ^= data0;
				a -= TigerConst.T1[(byte)c] ^ TigerConst.T2[(byte)(c >> 16)] ^ TigerConst.T3[(byte)(c >> 32)] ^ TigerConst.T4[(byte)(c >> 48)];
				b += TigerConst.T4[(byte)(c >> 8) & 0xff] ^ TigerConst.T3[(byte)(c >> 24)] ^ TigerConst.T2[(byte)(c >> 40)] ^ TigerConst.T1[(byte)(c >> 56)];
				b *= 9;

				a ^= data1;
				b -= TigerConst.T1[(byte)a] ^ TigerConst.T2[(byte)(a >> 16)] ^ TigerConst.T3[(byte)(a >> 32)] ^ TigerConst.T4[(byte)(a >> 48)];
				c += TigerConst.T4[(byte)(a >> 8)] ^ TigerConst.T3[(byte)(a >> 24)] ^ TigerConst.T2[(byte)(a >> 40)] ^ TigerConst.T1[(byte)(a >> 56)];
				c *= 9;

				b ^= data2;
				c -= TigerConst.T1[(byte)b] ^ TigerConst.T2[(byte)(b >> 16)] ^ TigerConst.T3[(byte)(b >> 32)] ^ TigerConst.T4[(byte)(b >> 48)];
				a += TigerConst.T4[(byte)(b >> 8)] ^ TigerConst.T3[(byte)(b >> 24)] ^ TigerConst.T2[(byte)(b >> 40)] ^ TigerConst.T1[(byte)(b >> 56)];
				a *= 9;

				c ^= data3;
				a -= TigerConst.T1[(byte)c] ^ TigerConst.T2[(byte)(c >> 16)] ^ TigerConst.T3[(byte)(c >> 32)] ^ TigerConst.T4[(byte)(c >> 48)];
				b += TigerConst.T4[(byte)(c >> 8) & 0xff] ^ TigerConst.T3[(byte)(c >> 24)] ^ TigerConst.T2[(byte)(c >> 40)] ^ TigerConst.T1[(byte)(c >> 56)];
				b *= 9;

				a ^= data4;
				b -= TigerConst.T1[(byte)a] ^ TigerConst.T2[(byte)(a >> 16)] ^ TigerConst.T3[(byte)(a >> 32)] ^ TigerConst.T4[(byte)(a >> 48)];
				c += TigerConst.T4[(byte)(a >> 8)] ^ TigerConst.T3[(byte)(a >> 24)] ^ TigerConst.T2[(byte)(a >> 40)] ^ TigerConst.T1[(byte)(a >> 56)];
				c *= 9;

				b ^= data5;
				c -= TigerConst.T1[(byte)b] ^ TigerConst.T2[(byte)(b >> 16)] ^ TigerConst.T3[(byte)(b >> 32)] ^ TigerConst.T4[(byte)(b >> 48)];
				a += TigerConst.T4[(byte)(b >> 8)] ^ TigerConst.T3[(byte)(b >> 24)] ^ TigerConst.T2[(byte)(b >> 40)] ^ TigerConst.T1[(byte)(b >> 56)];
				a *= 9;

				c ^= data6;
				a -= TigerConst.T1[(byte)c] ^ TigerConst.T2[(byte)(c >> 16)] ^ TigerConst.T3[(byte)(c >> 32)] ^ TigerConst.T4[(byte)(c >> 48)];
				b += TigerConst.T4[(byte)(c >> 8) & 0xff] ^ TigerConst.T3[(byte)(c >> 24)] ^ TigerConst.T2[(byte)(c >> 40)] ^ TigerConst.T1[(byte)(c >> 56)];
				b *= 9;

				a ^= data7;
				b -= TigerConst.T1[(byte)a] ^ TigerConst.T2[(byte)(a >> 16)] ^ TigerConst.T3[(byte)(a >> 32)] ^ TigerConst.T4[(byte)(a >> 48)];
				c += TigerConst.T4[(byte)(a >> 8)] ^ TigerConst.T3[(byte)(a >> 24)] ^ TigerConst.T2[(byte)(a >> 40)] ^ TigerConst.T1[(byte)(a >> 56)];
				c *= 9;

				ulong tempA = a;
				a = c;
				c = b;
				b = tempA;
			}

			_tempHash[0] ^= a;
			_tempHash[1] = b - _tempHash[1];
			_tempHash[2] += c;
		}


	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;
using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto.SHA
{
	internal class SHA1 : CryptoHashAlgorithm
	{
		private const uint C1 = 0x5A827999;
		private const uint C2 = 0x6ED9EBA1;
		private const uint C3 = 0x8F1BBCDC;
		private const uint C4 = 0xCA62C1D6;

		private readonly uint[] _state = new uint[5];

		public SHA1() : base(64, 20) { }

		protected override void Setup()
		{
			base.Setup();

			_state[0] = 0x67452301;
			_state[1] = 0xEFCDAB89;
			_state[2] = 0x98BADCFE;
			_state[3] = 0x10325476;
			_state[4] = 0xC3D2E1F0;
		}

		protected override byte[] CreateFinalBlock(byte[] block, int index, int length)
		{
			ulong bits = _processedBytes * 8;
			int bufLength = (length < 56) ? 64 : 128;

			byte[] pad = new byte[bufLength];
			Array.Copy(block, index, pad, 0, length);
			pad[length] = 0x80;
			Utils.WriteUlongToByteArraySwapOrder(bits, pad, bufLength - 8);

			return pad;
		}

		protected override void TransformBlock(byte[] block, int index)
		{
			uint[] data = new uint[80];

			Utils.TransformToUintsSwapOrder(block, index, _bufferSize, data, 0);

			for (int i = 16; i < 80; i++)
			{
				uint T = data[i - 3] ^ data[i - 8] ^ data[i - 14] ^ data[i - 16];
				data[i] = ((T << 1) | (T >> 31));
			}

			uint a = _state[0];
			uint b = _state[1];
			uint c = _state[2];
			uint d = _state[3];
			uint e = _state[4];
			uint r = 0;

			uint T1 = data[r++] + C1 + ((a << 5) | (a >> (32 - 5))) + ((b & c) | (~b & d)) + e;
			uint X7 = ((b << 30) | (b >> (32 - 30)));
			uint T2 = data[r++] + C1 + ((T1 << 5) | (T1 >> (32 - 5))) + ((a & X7) | (~a & c)) + d;
			uint X8 = ((a << 30) | (a >> (32 - 30)));
			uint T3 = data[r++] + C1 + ((T2 << 5) | (T2 >> (32 - 5))) + ((T1 & X8) | (~T1 & X7)) + c;
			uint X1 = ((T1 << 30) | (T1 >> (32 - 30)));
			uint T4 = data[r++] + C1 + ((T3 << 5) | (T3 >> (32 - 5))) + ((T2 & X1) | (~T2 & X8)) + X7;
			uint X2 = ((T2 << 30) | (T2 >> (32 - 30)));
			uint T5 = data[r++] + C1 + ((T4 << 5) | (T4 >> (32 - 5))) + ((T3 & X2) | (~T3 & X1)) + X8;
			uint X3 = ((T3 << 30) | (T3 >> (32 - 30)));
			uint T6 = data[r++] + C1 + ((T5 << 5) | (T5 >> (32 - 5))) + ((T4 & X3) | (~T4 & X2)) + X1;
			uint X4 = ((T4 << 30) | (T4 >> (32 - 30)));
			uint T7 = data[r++] + C1 + ((T6 << 5) | (T6 >> (32 - 5))) + ((T5 & X4) | (~T5 & X3)) + X2;
			uint X5 = ((T5 << 30) | (T5 >> (32 - 30)));
			uint T8 = data[r++] + C1 + ((T7 << 5) | (T7 >> (32 - 5))) + ((T6 & X5) | (~T6 & X4)) + X3;
			uint X6 = ((T6 << 30) | (T6 >> (32 - 30)));
			T1 = data[r++] + C1 + ((T8 << 5) | (T8 >> (32 - 5))) + ((T7 & X6) | (~T7 & X5)) + X4;
			X7 = ((T7 << 30) | (T7 >> (32 - 30)));
			T2 = data[r++] + C1 + ((T1 << 5) | (T1 >> (32 - 5))) + ((T8 & X7) | (~T8 & X6)) + X5;
			X8 = ((T8 << 30) | (T8 >> (32 - 30)));
			T3 = data[r++] + C1 + ((T2 << 5) | (T2 >> (32 - 5))) + ((T1 & X8) | (~T1 & X7)) + X6;
			X1 = ((T1 << 30) | (T1 >> (32 - 30)));
			T4 = data[r++] + C1 + ((T3 << 5) | (T3 >> (32 - 5))) + ((T2 & X1) | (~T2 & X8)) + X7;
			X2 = ((T2 << 30) | (T2 >> (32 - 30)));
			T5 = data[r++] + C1 + ((T4 << 5) | (T4 >> (32 - 5))) + ((T3 & X2) | (~T3 & X1)) + X8;
			X3 = ((T3 << 30) | (T3 >> (32 - 30)));
			T6 = data[r++] + C1 + ((T5 << 5) | (T5 >> (32 - 5))) + ((T4 & X3) | (~T4 & X2)) + X1;
			X4 = ((T4 << 30) | (T4 >> (32 - 30)));
			T7 = data[r++] + C1 + ((T6 << 5) | (T6 >> (32 - 5))) + ((T5 & X4) | (~T5 & X3)) + X2;
			X5 = ((T5 << 30) | (T5 >> (32 - 30)));
			T8 = data[r++] + C1 + ((T7 << 5) | (T7 >> (32 - 5))) + ((T6 & X5) | (~T6 & X4)) + X3;
			X6 = ((T6 << 30) | (T6 >> (32 - 30)));
			T1 = data[r++] + C1 + ((T8 << 5) | (T8 >> (32 - 5))) + ((T7 & X6) | (~T7 & X5)) + X4;
			X7 = ((T7 << 30) | (T7 >> (32 - 30)));
			T2 = data[r++] + C1 + ((T1 << 5) | (T1 >> (32 - 5))) + ((T8 & X7) | (~T8 & X6)) + X5;
			X8 = ((T8 << 30) | (T8 >> (32 - 30)));
			T3 = data[r++] + C1 + ((T2 << 5) | (T2 >> (32 - 5))) + ((T1 & X8) | (~T1 & X7)) + X6;
			X1 = ((T1 << 30) | (T1 >> (32 - 30)));
			T4 = data[r++] + C1 + ((T3 << 5) | (T3 >> (32 - 5))) + ((T2 & X1) | (~T2 & X8)) + X7;
			X2 = ((T2 << 30) | (T2 >> (32 - 30)));
			T5 = data[r++] + C2 + ((T4 << 5) | (T4 >> (32 - 5))) + (T3 ^ X2 ^ X1) + X8;
			X3 = ((T3 << 30) | (T3 >> (32 - 30)));
			T6 = data[r++] + C2 + ((T5 << 5) | (T5 >> (32 - 5))) + (T4 ^ X3 ^ X2) + X1;
			X4 = ((T4 << 30) | (T4 >> (32 - 30)));
			T7 = data[r++] + C2 + ((T6 << 5) | (T6 >> (32 - 5))) + (T5 ^ X4 ^ X3) + X2;
			X5 = ((T5 << 30) | (T5 >> (32 - 30)));
			T8 = data[r++] + C2 + ((T7 << 5) | (T7 >> (32 - 5))) + (T6 ^ X5 ^ X4) + X3;
			X6 = ((T6 << 30) | (T6 >> (32 - 30)));
			T1 = data[r++] + C2 + ((T8 << 5) | (T8 >> (32 - 5))) + (T7 ^ X6 ^ X5) + X4;
			X7 = ((T7 << 30) | (T7 >> (32 - 30)));
			T2 = data[r++] + C2 + ((T1 << 5) | (T1 >> (32 - 5))) + (T8 ^ X7 ^ X6) + X5;
			X8 = ((T8 << 30) | (T8 >> (32 - 30)));
			T3 = data[r++] + C2 + ((T2 << 5) | (T2 >> (32 - 5))) + (T1 ^ X8 ^ X7) + X6;
			X1 = ((T1 << 30) | (T1 >> (32 - 30)));
			T4 = data[r++] + C2 + ((T3 << 5) | (T3 >> (32 - 5))) + (T2 ^ X1 ^ X8) + X7;
			X2 = ((T2 << 30) | (T2 >> (32 - 30)));
			T5 = data[r++] + C2 + ((T4 << 5) | (T4 >> (32 - 5))) + (T3 ^ X2 ^ X1) + X8;
			X3 = ((T3 << 30) | (T3 >> (32 - 30)));
			T6 = data[r++] + C2 + ((T5 << 5) | (T5 >> (32 - 5))) + (T4 ^ X3 ^ X2) + X1;
			X4 = ((T4 << 30) | (T4 >> (32 - 30)));
			T7 = data[r++] + C2 + ((T6 << 5) | (T6 >> (32 - 5))) + (T5 ^ X4 ^ X3) + X2;
			X5 = ((T5 << 30) | (T5 >> (32 - 30)));
			T8 = data[r++] + C2 + ((T7 << 5) | (T7 >> (32 - 5))) + (T6 ^ X5 ^ X4) + X3;
			X6 = ((T6 << 30) | (T6 >> (32 - 30)));
			T1 = data[r++] + C2 + ((T8 << 5) | (T8 >> (32 - 5))) + (T7 ^ X6 ^ X5) + X4;
			X7 = ((T7 << 30) | (T7 >> (32 - 30)));
			T2 = data[r++] + C2 + ((T1 << 5) | (T1 >> (32 - 5))) + (T8 ^ X7 ^ X6) + X5;
			X8 = ((T8 << 30) | (T8 >> (32 - 30)));
			T3 = data[r++] + C2 + ((T2 << 5) | (T2 >> (32 - 5))) + (T1 ^ X8 ^ X7) + X6;
			X1 = ((T1 << 30) | (T1 >> (32 - 30)));
			T4 = data[r++] + C2 + ((T3 << 5) | (T3 >> (32 - 5))) + (T2 ^ X1 ^ X8) + X7;
			X2 = ((T2 << 30) | (T2 >> (32 - 30)));
			T5 = data[r++] + C2 + ((T4 << 5) | (T4 >> (32 - 5))) + (T3 ^ X2 ^ X1) + X8;
			X3 = ((T3 << 30) | (T3 >> (32 - 30)));
			T6 = data[r++] + C2 + ((T5 << 5) | (T5 >> (32 - 5))) + (T4 ^ X3 ^ X2) + X1;
			X4 = ((T4 << 30) | (T4 >> (32 - 30)));
			T7 = data[r++] + C2 + ((T6 << 5) | (T6 >> (32 - 5))) + (T5 ^ X4 ^ X3) + X2;
			X5 = ((T5 << 30) | (T5 >> (32 - 30)));
			T8 = data[r++] + C2 + ((T7 << 5) | (T7 >> (32 - 5))) + (T6 ^ X5 ^ X4) + X3;
			X6 = ((T6 << 30) | (T6 >> (32 - 30)));
			T1 = data[r++] + C3 + ((T8 << 5) | (T8 >> (32 - 5))) + ((T7 & X6) | (T7 & X5) | (X6 & X5)) + X4;
			X7 = ((T7 << 30) | (T7 >> (32 - 30)));
			T2 = data[r++] + C3 + ((T1 << 5) | (T1 >> (32 - 5))) + ((T8 & X7) | (T8 & X6) | (X7 & X6)) + X5;
			X8 = ((T8 << 30) | (T8 >> (32 - 30)));
			T3 = data[r++] + C3 + ((T2 << 5) | (T2 >> (32 - 5))) + ((T1 & X8) | (T1 & X7) | (X8 & X7)) + X6;
			X1 = ((T1 << 30) | (T1 >> (32 - 30)));
			T4 = data[r++] + C3 + ((T3 << 5) | (T3 >> (32 - 5))) + ((T2 & X1) | (T2 & X8) | (X1 & X8)) + X7;
			X2 = ((T2 << 30) | (T2 >> (32 - 30)));
			T5 = data[r++] + C3 + ((T4 << 5) | (T4 >> (32 - 5))) + ((T3 & X2) | (T3 & X1) | (X2 & X1)) + X8;
			X3 = ((T3 << 30) | (T3 >> (32 - 30)));
			T6 = data[r++] + C3 + ((T5 << 5) | (T5 >> (32 - 5))) + ((T4 & X3) | (T4 & X2) | (X3 & X2)) + X1;
			X4 = ((T4 << 30) | (T4 >> (32 - 30)));
			T7 = data[r++] + C3 + ((T6 << 5) | (T6 >> (32 - 5))) + ((T5 & X4) | (T5 & X3) | (X4 & X3)) + X2;
			X5 = ((T5 << 30) | (T5 >> (32 - 30)));
			T8 = data[r++] + C3 + ((T7 << 5) | (T7 >> (32 - 5))) + ((T6 & X5) | (T6 & X4) | (X5 & X4)) + X3;
			X6 = ((T6 << 30) | (T6 >> (32 - 30)));
			T1 = data[r++] + C3 + ((T8 << 5) | (T8 >> (32 - 5))) + ((T7 & X6) | (T7 & X5) | (X6 & X5)) + X4;
			X7 = ((T7 << 30) | (T7 >> (32 - 30)));
			T2 = data[r++] + C3 + ((T1 << 5) | (T1 >> (32 - 5))) + ((T8 & X7) | (T8 & X6) | (X7 & X6)) + X5;
			X8 = ((T8 << 30) | (T8 >> (32 - 30)));
			T3 = data[r++] + C3 + ((T2 << 5) | (T2 >> (32 - 5))) + ((T1 & X8) | (T1 & X7) | (X8 & X7)) + X6;
			X1 = ((T1 << 30) | (T1 >> (32 - 30)));
			T4 = data[r++] + C3 + ((T3 << 5) | (T3 >> (32 - 5))) + ((T2 & X1) | (T2 & X8) | (X1 & X8)) + X7;
			X2 = ((T2 << 30) | (T2 >> (32 - 30)));
			T5 = data[r++] + C3 + ((T4 << 5) | (T4 >> (32 - 5))) + ((T3 & X2) | (T3 & X1) | (X2 & X1)) + X8;
			X3 = ((T3 << 30) | (T3 >> (32 - 30)));
			T6 = data[r++] + C3 + ((T5 << 5) | (T5 >> (32 - 5))) + ((T4 & X3) | (T4 & X2) | (X3 & X2)) + X1;
			X4 = ((T4 << 30) | (T4 >> (32 - 30)));
			T7 = data[r++] + C3 + ((T6 << 5) | (T6 >> (32 - 5))) + ((T5 & X4) | (T5 & X3) | (X4 & X3)) + X2;
			X5 = ((T5 << 30) | (T5 >> (32 - 30)));
			T8 = data[r++] + C3 + ((T7 << 5) | (T7 >> (32 - 5))) + ((T6 & X5) | (T6 & X4) | (X5 & X4)) + X3;
			X6 = ((T6 << 30) | (T6 >> (32 - 30)));
			T1 = data[r++] + C3 + ((T8 << 5) | (T8 >> (32 - 5))) + ((T7 & X6) | (T7 & X5) | (X6 & X5)) + X4;
			X7 = ((T7 << 30) | (T7 >> (32 - 30)));
			T2 = data[r++] + C3 + ((T1 << 5) | (T1 >> (32 - 5))) + ((T8 & X7) | (T8 & X6) | (X7 & X6)) + X5;
			X8 = ((T8 << 30) | (T8 >> (32 - 30)));
			T3 = data[r++] + C3 + ((T2 << 5) | (T2 >> (32 - 5))) + ((T1 & X8) | (T1 & X7) | (X8 & X7)) + X6;
			X1 = ((T1 << 30) | (T1 >> (32 - 30)));
			T4 = data[r++] + C3 + ((T3 << 5) | (T3 >> (32 - 5))) + ((T2 & X1) | (T2 & X8) | (X1 & X8)) + X7;
			X2 = ((T2 << 30) | (T2 >> (32 - 30)));
			T5 = data[r++] + C4 + ((T4 << 5) | (T4 >> (32 - 5))) + (T3 ^ X2 ^ X1) + X8;
			X3 = ((T3 << 30) | (T3 >> (32 - 30)));
			T6 = data[r++] + C4 + ((T5 << 5) | (T5 >> (32 - 5))) + (T4 ^ X3 ^ X2) + X1;
			X4 = ((T4 << 30) | (T4 >> (32 - 30)));
			T7 = data[r++] + C4 + ((T6 << 5) | (T6 >> (32 - 5))) + (T5 ^ X4 ^ X3) + X2;
			X5 = ((T5 << 30) | (T5 >> (32 - 30)));
			T8 = data[r++] + C4 + ((T7 << 5) | (T7 >> (32 - 5))) + (T6 ^ X5 ^ X4) + X3;
			X6 = ((T6 << 30) | (T6 >> (32 - 30)));
			T1 = data[r++] + C4 + ((T8 << 5) | (T8 >> (32 - 5))) + (T7 ^ X6 ^ X5) + X4;
			X7 = ((T7 << 30) | (T7 >> (32 - 30)));
			T2 = data[r++] + C4 + ((T1 << 5) | (T1 >> (32 - 5))) + (T8 ^ X7 ^ X6) + X5;
			X8 = ((T8 << 30) | (T8 >> (32 - 30)));
			T3 = data[r++] + C4 + ((T2 << 5) | (T2 >> (32 - 5))) + (T1 ^ X8 ^ X7) + X6;
			X1 = ((T1 << 30) | (T1 >> (32 - 30)));
			T4 = data[r++] + C4 + ((T3 << 5) | (T3 >> (32 - 5))) + (T2 ^ X1 ^ X8) + X7;
			X2 = ((T2 << 30) | (T2 >> (32 - 30)));
			T5 = data[r++] + C4 + ((T4 << 5) | (T4 >> (32 - 5))) + (T3 ^ X2 ^ X1) + X8;
			X3 = ((T3 << 30) | (T3 >> (32 - 30)));
			T6 = data[r++] + C4 + ((T5 << 5) | (T5 >> (32 - 5))) + (T4 ^ X3 ^ X2) + X1;
			X4 = ((T4 << 30) | (T4 >> (32 - 30)));
			T7 = data[r++] + C4 + ((T6 << 5) | (T6 >> (32 - 5))) + (T5 ^ X4 ^ X3) + X2;
			X5 = ((T5 << 30) | (T5 >> (32 - 30)));
			T8 = data[r++] + C4 + ((T7 << 5) | (T7 >> (32 - 5))) + (T6 ^ X5 ^ X4) + X3;
			X6 = ((T6 << 30) | (T6 >> (32 - 30)));
			T1 = data[r++] + C4 + ((T8 << 5) | (T8 >> (32 - 5))) + (T7 ^ X6 ^ X5) + X4;
			X7 = ((T7 << 30) | (T7 >> (32 - 30)));
			T2 = data[r++] + C4 + ((T1 << 5) | (T1 >> (32 - 5))) + (T8 ^ X7 ^ X6) + X5;
			X8 = ((T8 << 30) | (T8 >> (32 - 30)));
			T3 = data[r++] + C4 + ((T2 << 5) | (T2 >> (32 - 5))) + (T1 ^ X8 ^ X7) + X6;
			X1 = ((T1 << 30) | (T1 >> (32 - 30)));
			T4 = data[r++] + C4 + ((T3 << 5) | (T3 >> (32 - 5))) + (T2 ^ X1 ^ X8) + X7;
			X2 = ((T2 << 30) | (T2 >> (32 - 30)));
			T5 = data[r++] + C4 + ((T4 << 5) | (T4 >> (32 - 5))) + (T3 ^ X2 ^ X1) + X8;
			X3 = ((T3 << 30) | (T3 >> (32 - 30)));
			T6 = data[r++] + C4 + ((T5 << 5) | (T5 >> (32 - 5))) + (T4 ^ X3 ^ X2) + X1;
			X4 = ((T4 << 30) | (T4 >> (32 - 30)));
			T7 = data[r++] + C4 + ((T6 << 5) | (T6 >> (32 - 5))) + (T5 ^ X4 ^ X3) + X2;
			X5 = ((T5 << 30) | (T5 >> (32 - 30)));

			_state[0] += data[r++] + C4 + ((T7 << 5) | (T7 >> (32 - 5))) + (T6 ^ X5 ^ X4) + X3;
			_state[1] += T7;
			_state[2] += ((T6 << 30) | (T6 >> (32 - 30)));
			_state[3] += X5;
			_state[4] += X4;
		}

		protected override void FinalStep()
		{
			_hash = Utils.TransformToBytesSwapOrder(_state);
		}
	}
}

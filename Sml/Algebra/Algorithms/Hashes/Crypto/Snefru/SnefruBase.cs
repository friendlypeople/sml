// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;
using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Crypto.Snefru
{
	internal abstract class SnefruBase : CryptoHashAlgorithm
	{
		private readonly int	_rounds;
		private readonly uint[] _state;

	

		protected SnefruBase(HashRounds rounds, HashSize hashSize) : base(64 - (int)hashSize, (int)hashSize)
		{
			_rounds = (int)rounds;
			_state = new uint[_hashSize / 4];
		}

		protected override void Setup()
		{
			base.Setup();
			Utils.ZeroMemory(_state);
		}

		protected override void FinalStep()
		{
			_hash = Utils.TransformToBytesSwapOrder(_state);
		}

		protected override byte[] CreateFinalBlock(byte[] block, int index, int length)
		{
			ulong bits = _processedBytes * 8;
			int bufferSize = 2 * _bufferSize;

			byte[] pad = new byte[bufferSize];
			Array.Copy(block, index, pad, 0, length);

			Utils.WriteUlongToByteArraySwapOrder(bits, pad, bufferSize - 8);
			return pad;
		}

		protected override void TransformBlock(byte[] block, int index)
		{
			uint[] work = new uint[16];
			Array.Copy(_state, 0, work, 0, _state.Length);

			Utils.TransformToUintsSwapOrder(block, index, _bufferSize, work, _state.Length);

			for (int i = 0; i < _rounds; i++)
			{
				uint[] sbox0 = SnefruConst.SBoxes[i * 2];
				uint[] sbox1 = SnefruConst.SBoxes[i * 2 + 1];

				for (int j = 0; j < 4; j++)
				{
					work[15] ^= sbox0[(byte)work[0]];
					work[1] ^= sbox0[(byte)work[0]];
					work[0] ^= sbox0[(byte)work[1]];
					work[2] ^= sbox0[(byte)work[1]];
					work[1] ^= sbox1[(byte)work[2]];
					work[3] ^= sbox1[(byte)work[2]];
					work[2] ^= sbox1[(byte)work[3]];
					work[4] ^= sbox1[(byte)work[3]];
					work[3] ^= sbox0[(byte)work[4]];
					work[5] ^= sbox0[(byte)work[4]];
					work[4] ^= sbox0[(byte)work[5]];
					work[6] ^= sbox0[(byte)work[5]];
					work[5] ^= sbox1[(byte)work[6]];
					work[7] ^= sbox1[(byte)work[6]];
					work[6] ^= sbox1[(byte)work[7]];
					work[8] ^= sbox1[(byte)work[7]];
					work[7] ^= sbox0[(byte)work[8]];
					work[9] ^= sbox0[(byte)work[8]];
					work[8] ^= sbox0[(byte)work[9]];
					work[10] ^= sbox0[(byte)work[9]];
					work[9] ^= sbox1[(byte)work[10]];
					work[11] ^= sbox1[(byte)work[10]];
					work[10] ^= sbox1[(byte)work[11]];
					work[12] ^= sbox1[(byte)work[11]];
					work[11] ^= sbox0[(byte)work[12]];
					work[13] ^= sbox0[(byte)work[12]];
					work[12] ^= sbox0[(byte)work[13]];
					work[14] ^= sbox0[(byte)work[13]];
					work[13] ^= sbox1[(byte)work[14]];
					work[15] ^= sbox1[(byte)work[14]];
					work[14] ^= sbox1[(byte)work[15]];
					work[0] ^= sbox1[(byte)work[15]];

					int shift = SnefruConst.Shifts[j];

					for (int k = 0; k < work.Length; k++)
						work[k] = (work[k] >> shift) | (work[k] << (32 - shift));
				}
			}

			_state[0] ^= work[15];
			_state[1] ^= work[14];
			_state[2] ^= work[13];
			_state[3] ^= work[12];

			if (_hashSize == 32)
			{
				_state[4] ^= work[11];
				_state[5] ^= work[10];
				_state[6] ^= work[9];
				_state[7] ^= work[8];
			}
		}
	}
}

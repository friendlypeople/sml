// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Sml.Algebra.Algorithms.Hashes.Checksum;
using Sml.Algebra.Algorithms.Hashes.Crypto;
using Sml.Algebra.Algorithms.Hashes.Crypto.Haval;
using Sml.Algebra.Algorithms.Hashes.Crypto.MD;
using Sml.Algebra.Algorithms.Hashes.Crypto.RIPEMD;
using Sml.Algebra.Algorithms.Hashes.Crypto.SHA;
using Sml.Algebra.Algorithms.Hashes.Crypto.Snefru;
using Sml.Algebra.Algorithms.Hashes.Crypto.Tiger;
using Sml.Algebra.Algorithms.Hashes.Hash32;
using Sml.Algebra.Algorithms.Hashes.Hash64;

namespace Sml.Algebra.Algorithms.Hashes
{
	public static class HashFactory
	{
		public static class Hash32
		{
			public static IHashAlgorithm<int> CreateMurmur2()
			{
				return new Murmur2();
			}

			public static IHashAlgorithm<int> CreateAP()
			{
				return new AP();
			}

			public static IHashAlgorithm<int> CreateBernstein()
			{
				return new Bernstein();
			}

			public static IHashAlgorithm<int> CreateBkdr()
			{
				return new Bkdr();
			}

			public static IHashAlgorithm<int> CreateDEC()
			{
				return new DEC();
			}

			public static IHashAlgorithm<int> CreateDJB()
			{
				return new DJB();
			}

			public static IHashAlgorithm<int> CreateElf()
			{
				return new Elf();
			}

			public static IHashAlgorithm<int> CreateFnv()
			{
				return new Fnv();
			}

			public static IHashAlgorithm<int> CreateJenkins3()
			{
				return new Jenkins3();
			}

			public static IHashAlgorithm<int> CreateJs()
			{
				return new Js();
			}

			public static IHashAlgorithm<int> CreateModifiedBernstein()
			{
				return new ModifiedBernstein();
			}

			public static IHashAlgorithm<int> CreateModifiedFnv()
			{
				return new ModifiedFnv();
			}

			public static IHashAlgorithm<int> CreateOneAtTime()
			{
				return new OneAtTime();
			}

			public static IHashAlgorithm<int> CreatePjw()
			{
				return new Pjw();
			}

			public static IHashAlgorithm<int> CreateRotating()
			{
				return new Rotating();
			}

			public static IHashAlgorithm<int> CreateRs()
			{
				return new Rs();
			}

			public static IHashAlgorithm<int> CreateSdbm()
			{
				return new Sdbm();
			}

			public static IHashAlgorithm<int> CreateShiftAddXor()
			{
				return new ShiftAddXor();
			}

			public static IHashAlgorithm<int> CreateShiftAddXor2()
			{
				return new ShiftAddXor2();
			}

			public static IHashAlgorithm<int> CreateSuperFast()
			{
				return new SuperFast();
			}

			public static IHashAlgorithm<int> CreateXor()
			{
				return new Xor();
			}
		}

		public static class Hash64
		{

			public static IHashAlgorithm<long> CreateFnv()
			{
				return new Fnv64();
			}

			public static IHashAlgorithm<long> CreateModifiedFnv()
			{
				return new ModifiedFnv64();
			}

			public static IHashAlgorithm<long> CreateMurmur2()
			{
				return new Murmur2_64();
			}
		}

		public static class Crypto
		{
			public static IHashAlgorithm<byte[]> CreateGost()
			{
				return new Gost();
			}

			public static IHashAlgorithm<byte[]> CreateMD5()
			{
				return new MD5();
			}

			public static IHashAlgorithm<byte[]> CreateMD4()
			{
				return new MD4();
			}

			public static IHashAlgorithm<byte[]> CreateMD2()
			{
				return new MD2();
			}

			public static IHashAlgorithm<byte[]> CreateHaval3_128()
			{
				return new Haval3_128();
			}

			public static IHashAlgorithm<byte[]> CreateHaval3_160()
			{
				return new Haval3_160();
			}

			public static IHashAlgorithm<byte[]> CreateHaval3_192()
			{
				return new Haval3_192();
			}

			public static IHashAlgorithm<byte[]> CreateHaval3_224()
			{
				return new Haval3_224();
			}

			public static IHashAlgorithm<byte[]> CreateHaval3_256()
			{
				return new Haval3_256();
			}


			public static IHashAlgorithm<byte[]> CreateHaval4_128()
			{
				return new Haval4_128();
			}

			public static IHashAlgorithm<byte[]> CreateHaval4_160()
			{
				return new Haval4_160();
			}

			public static IHashAlgorithm<byte[]> CreateHaval4_192()
			{
				return new Haval4_192();
			}

			public static IHashAlgorithm<byte[]> CreateHaval4_224()
			{
				return new Haval4_224();
			}

			public static IHashAlgorithm<byte[]> CreateHaval4_256()
			{
				return new Haval4_256();
			}

			public static IHashAlgorithm<byte[]> CreateHaval5_128()
			{
				return new Haval5_128();
			}

			public static IHashAlgorithm<byte[]> CreateHaval5_160()
			{
				return new Haval5_160();
			}

			public static IHashAlgorithm<byte[]> CreateHaval5_192()
			{
				return new Haval5_192();
			}

			public static IHashAlgorithm<byte[]> CreateHaval5_224()
			{
				return new Haval5_224();
			}

			public static IHashAlgorithm<byte[]> CreateHaval5_256()
			{
				return new Haval5_256();
			}

			public static IHashAlgorithm<byte[]> CreateHAS160()
			{
				return new HAS160();
			}

			public static IHashAlgorithm<byte[]> CreateRIPEMD128()
			{
				return new RIPEMD128();
			}

			public static IHashAlgorithm<byte[]> CreateRIPEMD160()
			{
				return new RIPEMD160();
			}

			public static IHashAlgorithm<byte[]> CreateRIPEMD256()
			{
				return new RIPEMD256();
			}

			public static IHashAlgorithm<byte[]> CreateRIPEMD320()
			{
				return new RIPEMD320();
			}

			public static IHashAlgorithm<byte[]> CreateSnefru4_128()
			{
				return new Snefru4_128();
			}

			public static IHashAlgorithm<byte[]> CreateSnefru4_256()
			{
				return new Snefru4_256();
			}

			public static IHashAlgorithm<byte[]> CreateSnefru8_128()
			{
				return new Snefru8_128();
			}

			public static IHashAlgorithm<byte[]> CreateSnefru8_256()
			{
				return new Snefru8_256();
			}

			public static IHashAlgorithm<byte[]> CreateTiger2()
			{
				return new Tiger2();
			}

			public static IHashAlgorithm<byte[]> CreateTiger3()
			{
				return new Tiger3();
			}

			public static IHashAlgorithm<byte[]> CreateTiger4()
			{
				return new Tiger4();
			}

			public static IHashAlgorithm<byte[]> CreateWhirlpool()
			{
				return new Whirlpool();
			}

			public static IHashAlgorithm<byte[]> CreateSHA1()
			{
				return new SHA1();
			}

			public static IHashAlgorithm<byte[]> CreateSHA224()
			{
				return new SHA224();
			}

			public static IHashAlgorithm<byte[]> CreateSHA256()
			{
				return new SHA256();
			}

			public static IHashAlgorithm<byte[]> CreateSHA384()
			{
				return new SHA384();
			}

			public static IHashAlgorithm<byte[]> CreateSHA512()
			{
				return new SHA512();
			}

			
		}

		public static class Checksum
		{
			public static IHashAlgorithm<int> CreateAdler32()
			{
				return new Adler32();
			}

			public static IHashAlgorithm<int> CreateCRC32_Castagnoli()
			{
				return new CRC32(CRC32Kinds.Castagnoli);
			}

			public static IHashAlgorithm<int> CreateCRC32_Q()
			{
				return new CRC32(CRC32Kinds.CRC_32Q);
			}

			public static IHashAlgorithm<int> CreateCRC32_IEEE802_3()
			{
				return new CRC32(CRC32Kinds.IEEE_802_3);
			}

			public static IHashAlgorithm<int> CreateCRC32_Koopman()
			{
				return new CRC32(CRC32Kinds.Koopman);
			}

			public static IHashAlgorithm<long> CreateCRC64_ISO()
			{
				return new CRC64(CRC64Kinds.ISO);
			}

			public static IHashAlgorithm<long> CreateCRC64_ECMA_182()
			{
				return new CRC64(CRC64Kinds.ECMA_182);
			}




		}

	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;
using Sml.Common;

namespace Sml.Algebra.Algorithms.Hashes.Hash64
{
	// TODO перенести в Crypto, так как возвращаемое значение не ulong, а byte[16]
	/// <summary>
	/// Основной алгоритм взят из http://blog.teamleadnet.com/2012/08/murmurhash3-ultra-fast-hash-algorithm.html
	/// </summary>
	internal class Murmur3_64 : Hash64AlgorithmBase
	{
		public static int READ_SIZE = 16;
		private const ulong C1 = 0x87c37b91114253d5L;
		private const ulong C2 = 0x4cf5ad432745937fL;

		protected const uint Seed = 0xc58f1a7b;
		private ulong _h1;
		private ulong _h2;

		private void MixBody(ulong k1, ulong k2)
		{
			_h1 ^= MixKey1(k1);

			_h1 = RotateLeft(_h1, 27);
			_h1 += _h2;
			_h1 = _h1 * 5 + 0x52dce729;

			_h2 ^= MixKey2(k2);

			_h2 = RotateLeft(_h2, 31);
			_h2 += _h1;
			_h2 = _h2 * 5 + 0x38495ab5;
		}

		private static ulong MixKey1(ulong k1)
		{
			k1 *= C1;
			k1 = RotateLeft(k1, 31);
			k1 *= C2;
			return k1;
		}

		private static ulong MixKey2(ulong k2)
		{
			k2 *= C2;
			k2 = RotateLeft(k2, 33);
			k2 *= C1;
			return k2;
		}

		private static ulong MixFinal(ulong k)
		{
			// avalanche bits

			k ^= k >> 33;
			k *= 0xff51afd7ed558ccdL;
			k ^= k >> 33;
			k *= 0xc4ceb9fe1a85ec53L;
			k ^= k >> 33;
			return k;
		}

		public static ulong RotateLeft(ulong original, int bits)
		{
			return (original << bits) | (original >> (64 - bits));
		}

		public static ulong RotateRight(ulong original, int bits)
		{
			return (original >> bits) | (original << (64 - bits));
		}

		unsafe public static ulong GetUInt64(byte[] bb, int pos)
		{
			fixed (byte* pbyte = &bb[pos])
				return *((ulong*) pbyte);
		}

		protected override void GenerateHash(byte[] key, int index, int length)
		{
			_h1 = Seed;
			int pos = index;
			int remaining = length;

			for (; remaining >= READ_SIZE; remaining -= READ_SIZE)
			{
				ulong k1 = GetUInt64(key, pos);
				pos += 8;

				ulong k2 = GetUInt64(key, pos);
				pos += 8;
				MixBody(k1, k2);
			}
			if (remaining > 0)
			{
				ulong k1 = 0;
				ulong k2 = 0;

				switch (remaining)
				{
					case 15:
						k2 ^= (ulong)key[pos + 14] << 48; // fall through
						goto case 14;
					case 14:
						k2 ^= (ulong)key[pos + 13] << 40; // fall through
						goto case 13;
					case 13:
						k2 ^= (ulong)key[pos + 12] << 32; // fall through
						goto case 12;
					case 12:
						k2 ^= (ulong)key[pos + 11] << 24; // fall through
						goto case 11;
					case 11:
						k2 ^= (ulong)key[pos + 10] << 16; // fall through
						goto case 10;
					case 10:
						k2 ^= (ulong)key[pos + 9] << 8; // fall through
						goto case 9;
					case 9:
						k2 ^= key[pos + 8]; // fall through
						goto case 8;
					case 8:
						k1 ^= GetUInt64(key, pos);
						break;
					case 7:
						k1 ^= (ulong)key[pos + 6] << 48; // fall through
						goto case 6;
					case 6:
						k1 ^= (ulong)key[pos + 5] << 40; // fall through
						goto case 5;
					case 5:
						k1 ^= (ulong)key[pos + 4] << 32; // fall through
						goto case 4;
					case 4:
						k1 ^= (ulong)key[pos + 3] << 24; // fall through
						goto case 3;
					case 3:
						k1 ^= (ulong)key[pos + 2] << 16; // fall through
						goto case 2;
					case 2:
						k1 ^= (ulong)key[pos + 1] << 8; // fall through
						goto case 1;
					case 1:
						k1 ^= key[pos]; // fall through
						break;
					default:
						throw new Exception("Something went wrong with remaining bytes calculation.");
				}
				_h1 ^= MixKey1(k1);
				_h2 ^= MixKey2(k2);
			}

			_h1 ^= (uint)length;
			_h2 ^= (uint)length;

			_h1 += _h2;
			_h2 += _h1;

			_h1 = MixFinal(_h1);
			_h2 = MixFinal(_h2);

			_h1 += _h2;
			_h2 += _h1;

			//Utils.W
			//_hash
			//var hash = new byte[READ_SIZE];

			//Array.Copy(BitConverter.GetBytes(h1), 0, hash, 0, 8);
			//Array.Copy(BitConverter.GetBytes(h2), 0, hash, 8, 8);

			//return hash;

		}
	}
}

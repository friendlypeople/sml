// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 




namespace Sml.Algebra.Algorithms.Hashes.Hash64
{
	internal class Murmur2_64 : Hash64AlgorithmBase
	{
		protected const ulong M = 0xc6a4a7935bd1e995;
		protected const int R = 47;
		protected const uint Seed = 0xc58f1a7b;

		protected override void GenerateHash(byte[] key, int index, int length)
		{
			if (length == 0) return;

			_hash = Seed ^ (ulong)length;
			int k = 0;


			while (length >= 8)
			{
				ulong temp = key[k++] | (ulong)key[k++] << 8 | (ulong)key[k++] << 16 |
						  (ulong)key[k++] << 24 | (ulong)key[k++] << 32 | (ulong)key[k++] << 40 |
						  (ulong)key[k++] << 48 | (ulong)key[k++] << 56;

				temp *= M;
				temp ^= temp >> R;
				temp *= M;

				_hash ^= temp;
				_hash *= M;
				length -= 8;
			}

			switch (length)
			{
				case 7:
					_hash ^= (ulong)key[k++] << 48 | (ulong)key[k++] << 40 | (ulong)key[k++] << 32 |
						 (ulong)key[k++] << 24 | (ulong)key[k++] << 16 | (ulong)key[k++] << 8 |
						 key[k];
					_hash *= M;
					break;
				case 6:
					_hash ^= (ulong)key[k++] << 40 | (ulong)key[k++] << 32 | (ulong)key[k++] << 24 |
						 (ulong)key[k++] << 16 | (ulong)key[k++] << 8 | key[k];
					_hash *= M;
					break;
				case 5:
					_hash ^= (ulong)key[k++] << 32 | (ulong)key[k++] << 24 | (ulong)key[k++] << 16 |
						 (ulong)key[k++] << 8 | key[k];
					_hash *= M;
					break;
				case 4:
					_hash ^= (ulong)key[k++] << 24 | (ulong)key[k++] << 16 | (ulong)key[k++] << 8 |
						 key[k];
					_hash *= M;
					break;
				case 3:
					_hash ^= (ulong)key[k++] << 16 | (ulong)key[k++] << 8 | key[k];
					_hash *= M;
					break;
				case 2:
					_hash ^= (ulong)key[k++] << 8 | key[k];
					_hash *= M;
					break;
				case 1:
					_hash ^= key[k];
					_hash *= M;
					break;
			}

			_hash ^= _hash >> R;
			_hash *= M;
			_hash ^= _hash >> R;

		}
	}
}

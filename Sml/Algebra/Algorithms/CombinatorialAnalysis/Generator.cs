// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Sml.Common;
using Sml.DataStructures.Data;

namespace Sml.Algebra.Algorithms.CombinatorialAnalysis
{
	public sealed class Generator<T>
	{
		private readonly IComparer<T> _equalityComparer;

		public Generator() : this(Comparer<T>.Default) { }

		public Generator(IComparer<T> equalityComparer)
		{
			_equalityComparer = equalityComparer;
		}

		private T[] SortCollection(T[] startPermution)
		{
			T[] sortedPermution = new T[startPermution.Length];
			Array.Copy(startPermution, sortedPermution, startPermution.Length);
			Array.Sort(sortedPermution, _equalityComparer);
			return sortedPermution;
		}


		public CombinatorialCollection<T> GeneratePermutations(T[] startPermution)
		{
			T[] sortedPermution = SortCollection(startPermution);
			int N = sortedPermution.Length;
			CombinatorialCollection<T> col = new CombinatorialCollection<T>();

			Dictionary<int, T> dataTable = new Dictionary<int, T>();
			List<int> indexList = new List<int>();

			for (int i = 0; i < N; i++)
			{
				indexList.Add(i);
				dataTable[i + 1] = sortedPermution[i];
			}
			indexList.Add(N);
			dataTable[0] = default(T);

			while (indexList[0] == 0)
			{
				T[] ar = new T[N];
				for (int i = 1; i <= N; i++)
					ar[i - 1] = dataTable[indexList[i]];
				col.Add(ar);

				int j = N;
				while (_equalityComparer.Compare(dataTable[indexList[j - 1]], dataTable[indexList[j]]) == 1)
					j--;

				int k = N;
				while (_equalityComparer.Compare(dataTable[indexList[j - 1]], dataTable[indexList[k]]) == 1)
					k--;
				Swapper.Swap(indexList, j - 1, k);

				int[] r = new int[N + 1];

				for (int i = j; i <= N; i++)
					r[i] = indexList[N - (i - j)];

				for (int i = j; i <= N; i++)
					indexList[i] = r[i];
			}
			return col;
		}

		public CombinatorialCollection<T> GenerateCombinations(T[] startPermution, int m)
		{
			T[] sortedPermution = SortCollection(startPermution);
			int N = sortedPermution.Length;
			CombinatorialCollection<T> col = new CombinatorialCollection<T>();

			Dictionary<int, T> dataTable = new Dictionary<int, T>();
			List<int> indexList = new List<int>();

			for (int i = 0; i < N; i++)
			{
				indexList.Add(i);
				dataTable[i + 1] = sortedPermution[i];
			}
			indexList.Add(N);

			int j;
			do
			{
				T[] ar = new T[m];
				for (int i = 1; i <= m; i++)
					ar[i - 1] = dataTable[indexList[i]];
				col.Add(ar);
				j = m;

				while (j > 0 && indexList[j] >= N + j - m)
					j--;
				if (j == 0) continue;

				indexList[j] += 1;
				for (int i = j + 1; i <= m; i++)
					indexList[i] = indexList[i - 1] + 1;

			} while (j != 0);
			return col;
		}

		public CombinatorialCollection<T> GenerateAllocations(T[] startPermution, int m)
		{
			CombinatorialCollection<T> col = new CombinatorialCollection<T>();

			foreach (T[] item in GenerateCombinations(startPermution, m))
			{
				CombinatorialCollection<T> collection = GeneratePermutations(item);
				col.AddRange(collection);
			}
			return col;
		}

		public CombinatorialCollection<T> GenerateAllocationWithRepetitions(T[] startPermution, int m)
		{
			int N = startPermution.Length;
			CombinatorialCollection<T> col = new CombinatorialCollection<T>();

			Dictionary<int, T> dataTable = new Dictionary<int, T>();
			List<int> indexList = new List<int>();

			for (int i = 0; i < N; i++)
				dataTable[i] = startPermution[i];

			indexList.AddRange(new int[m + 1]);

			while (indexList[m] == 0)
			{
				int i = 0;
				indexList[0]++;
				while (indexList[i] == N && indexList[m] == 0)
				{
					indexList[i] = 0;
					indexList[i + 1]++;
					i++;
				}
				T[] ar = new T[m];
				for (int j = m - 1; j >= 0; j--)
					ar[j] = dataTable[indexList[j]];
				col.Add(ar);
			}
			return col;
		}

		public CombinatorialCollection<T> GeneratePermutationWithRepetitions(T[] startPermution, int m)
		{

			SmlPair<T, int>[] mAr = new SmlPair<T,int>[startPermution.Length];
			for (int i = 0; i < startPermution.Length; i++)
			{
				mAr[i] = new SmlPair<T, int>(startPermution[i], m);
			}
			return GeneratePermutationWithRepetitions(mAr);
		}

		public CombinatorialCollection<T> GeneratePermutationWithRepetitions(SmlPair<T,int>[] data)
		{
			List<T> temp = new List<T>();
			for (int i = 0; i < data.Length; i++)
				for (int j = 0; j < data[i].Second; j++)
					temp.Add(data[i].First);

			T[] sortedPermution = SortCollection(temp.ToArray());
			int N = sortedPermution.Length;
			CombinatorialCollection<T> col = new CombinatorialCollection<T>();

			Dictionary<int, T> dataTable = new Dictionary<int, T>();
			List<int> indexList = new List<int>();

			for (int i = 0; i < N; i++)
			{
				indexList.Add(i);
				dataTable[i + 1] = sortedPermution[i];
			}
			indexList.Add(N);
			dataTable[0] = default(T);

			while (indexList[0] == 0)
			{
				T[] ar = new T[N];
				for (int i = 1; i <= N; i++)
					ar[i - 1] = dataTable[indexList[i]];
				col.Add(ar);

				int j = N;
				
				int compare = _equalityComparer.Compare(dataTable[indexList[j - 1]], dataTable[indexList[j]]);
				while (compare == 1 || compare == 0)
				{
					j--;
					compare = _equalityComparer.Compare(dataTable[indexList[j - 1]], dataTable[indexList[j]]);
				}

				int k = N;

				compare = _equalityComparer.Compare(dataTable[indexList[j - 1]], dataTable[indexList[k]]);
				while (compare == 1 || compare == 0)
				{
					k--;
					compare = _equalityComparer.Compare(dataTable[indexList[j - 1]], dataTable[indexList[k]]);
				} 
					
				Swapper.Swap(indexList, j - 1, k);

				int[] r = new int[N + 1];

				for (int i = j; i <= N; i++)
					r[i] = indexList[N - (i - j)];

				for (int i = j; i <= N; i++)
					indexList[i] = r[i];
			}
			return col;
		}

		public CombinatorialCollection<T> GenerateCombinationWithRepetitions(T[] startPermution, int m)
		{
			T[] sortedPermution = SortCollection(startPermution);
			int N = sortedPermution.Length;
			int K = N + m - 1;
			CombinatorialCollection<T> col = new CombinatorialCollection<T>();

			Generator<int> gen = new Generator<int>();

			int[] array = new int[K];
			for (int i = 0; i < K; i++)
				array[i] = i+1;

			CombinatorialCollection<int> collection = gen.GenerateCombinations(array, N - 1);
			foreach (int[] ints in collection)
			{
				T[] ar = new T[m];
				int k = 0;
				int i = 1;
				int zeroPos = K - ints[N - i - 1];
				for (int temp = 0; temp < K; temp++)
				{
					if (temp != zeroPos)
					{
						ar[k] = sortedPermution[i - 1];
						k++;
					}
					else
					{
						i++;
						if(i < N)
							zeroPos = K - ints[N - i - 1];
					}
				}
				col.Add(ar);
			}
			return col;
		}

	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Sml.Algebra.Data;
using Sml.Common;

namespace Sml.Algebra.Algorithms.CombinatorialAnalysis
{
	public static class Calculator
	{

		/// <summary>
		/// Метод расчета количества перестановок из n
		/// </summary>
		/// <param name="n">Общее количество элементов</param>
		/// <returns>Количество перестановок из n</returns>
		public static BigInteger Permutation(int n)
		{
			return Factorial.PoorMans(n);
		}

		/// <summary>
		/// Метод расчета количества перестановок с повторениями из n элементов по m
		/// </summary>
		/// <param name="m">Массив количеств вхождений эелементов в перестановку</param>
		/// <param name="n">Длина собранной последовательности</param>
		/// <returns>
		/// Количество перестановок с повторениями
		/// </returns>
		public static BigInteger PermutationWithRepetition(int[] m, out int n)
		{
			Guard.ArgumentIsPositive(m.Length, "Длина массива");
			int max = int.MinValue;
			n = 0;
			if (m.Length == 1)
			{
				n = m[0];
				return 1;
			}

			for (int i = 0; i < m.Length; i++)
			{
				max = Math.Max(max, m[i]);
				n += m[i];
			}
			
			int k = n - max;
			BigInteger A = 1;
			for (int i = n; i > k; i--)
				A *= i;

			BigInteger B = 1;
			bool f = true;
			for (int i = 0; i < m.Length; i++)
			{
				if (m[i] == max && f)
				{
					f = false;
					continue;
				}
				B *= Factorial.PoorMans(m[i]);
			}
			return A / B;
		}

		/// <summary>
		/// Метод расчета количества размещений из n элементов по m
		/// </summary>
		/// <param name="n">Общее количество элементов</param>
		/// <param name="m">Размер выборки</param>
		/// <returns>Количество размещений из n элементов по m</returns>
		public static BigInteger Allocation(int n, int m)
		{
			Guard.ArgumentIsPositive(n, "n");
			Guard.ArgumentIsPositive(m, "m");
			if (m > n) throw new ArgumentException("n должно быть больше или равно m");
			if (m == n) return Factorial.PoorMans(n);
			int k = n - m;

			BigInteger A = 1;
			for (int i = n; i > k; i--)
				A *= i;
			return A;
		}


		/// <summary>
		/// Метод расчета количества сочетаний с повторениями из n элементов по m
		/// </summary>
		/// <param name="n">Общее количество элементов</param>
		/// <param name="m">Размер выборки</param>
		/// <returns>Количество сочетаний с повторениями из n элементов по m</returns>
		public static BigInteger CombinationWithRepetition(int n, int m)
		{
			return Combination(m + n - 1, m);
		}

		/// <summary>
		/// Метод расчета количества размещений с повторениями из n элементов по m
		/// </summary>
		/// <param name="n">Общее количество элементов</param>
		/// <param name="m">Размер выборки</param>
		/// <returns>Количество размещений с повторениями из n элементов по m</returns>
		public static BigInteger AllocationWithRepetition(int n, int m)
		{
			BigInteger res = n;
			for (int i = 1; i < m; i++)
				res *= n;
			return res;
		}


		/// <summary>
		/// Метод расчета количества сочетаний из n элементов по m
		/// </summary>
		/// <param name="n">Общее количество элементов</param>
		/// <param name="m">Размер выборки</param>
		/// <returns>Количество сочетаний из n элементов по m</returns>
		public static BigInteger Combination(int n, int m)
		{
			Guard.ArgumentIsPositive(n, "n");
			Guard.ArgumentIsPositive(m, "m");
			if (m > n) throw new ArgumentException("n должно быть больше или равно m");
			if (m == n) return 1;
			int k = n - m;

			int min, max;
			SmlMath.MinMax(k, m, out min, out max);

			BigInteger A = 1;
			for (int i = n; i > max; i--)
				A *= i;

			return A / Factorial.PoorMans(min);
		}
	}
}

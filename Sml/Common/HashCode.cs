// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 




using System.Collections.Generic;
using Sml.Algebra.Algorithms;
using Sml.Algebra.Algorithms.Hashes;

namespace Sml.Common
{
	/// <summary>
	/// Предоставляет методы для расчета и объединения хэш-кодов
	/// </summary>
	public static class HashCode
	{
		/// <summary>
		/// Возвращает объеденный хэш код основанный на переданных хэш кодах
		/// </summary>
		/// <param name="key">Список хэшей</param>
		/// <returns>Объеденный хэш код. </returns>
		public static int GetHashCode(params int[] key)
		{
			IHashAlgorithm<int> hash = HashFactory.Hash32.CreateShiftAddXor2();
			return hash.Generate(key);
		}

		///// <summary>
		///// Возвращает объеденный хэш код основанный на переданных хэш кодах
		///// </summary>
		///// <param name="hash1">Первый хэш</param>
		///// <param name="hash2">Второй хэш</param>
		///// <returns>Объеденный хэш код.</returns>
		//public static int GetHashCode(int hash1, int hash2)
		//{
		//    return ((hash1 << 5) + hash1) ^ hash2;
		//}

		///// <summary>
		///// Возвращает объеденный хэш код основанный на переданных хэш кодах
		///// </summary>
		///// <param name="hash1">Первый хэш</param>
		///// <param name="hash2">Второй хэш</param>
		///// <param name="hash3">Третий хэш</param>
		///// <returns>Объеденный хэш код.</returns>
		//public static int GetHashCode(int hash1, int hash2, int hash3)
		//{
		//    return GetHashCode(GetHashCode(hash1, hash2), hash3);
		//}

		///// <summary>
		///// Возвращает объеденный хэш код основанный на переданных хэш кодах
		///// </summary>
		///// <param name="hash1">Первый хэш</param>
		///// <param name="hash2">Второй хэш</param>
		///// <param name="hash3">Третий хэш</param>
		///// <param name="hash4">Четвертый хэш</param>
		///// <returns>Объеденный хэш код.</returns>
		//public static int GetHashCode(int hash1, int hash2, int hash3, int hash4)
		//{
		//    return GetHashCode(GetHashCode(hash1, hash2, hash3), hash4);
		//}

		/// <summary>
		/// Получить хэш на основе компаратора, корректно отрабатываем null
		/// </summary>
		/// <param name="item">Объект, с которого снимаем хэш</param>
		/// <param name="comparer">Компаратор для получения хэша.</param>
		/// <returns>Полученный хэш код</returns>
		public static int GetHashCode<T>(T item, IEqualityComparer<T> comparer)
		{
			if (ReferenceEquals(item, null))
			//if (item == null)
				return int.MinValue;
			return comparer.GetHashCode(item);
		}
	}
}

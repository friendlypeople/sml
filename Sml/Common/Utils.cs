// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Sml.Common
{
	public static class Utils
	{
		public unsafe static byte[] TransformToBytes(int[] key)
		{
			const int size = sizeof(int);
			byte[] buffer = new byte[size * key.Length];

			fixed (byte* pB = &buffer[0])
			{
				byte* pointer = pB;
				for (int i = 0; i < key.Length; i++)
				{
					*((int*)pointer) = key[i];
					pointer += size;
				}
			}
			return buffer;
		}

		public unsafe static int[] TransformToInts(byte[] key)
		{
			const int size = sizeof(int);
			int newSize = key.Length / size;
			if (key.Length % size != 0) newSize++;

			int[] buffer = new int[newSize];
			fixed (byte* pB = &key[0])
			{
				byte* pointer = pB;
				for (int i = 0; i < newSize; i++)
				{
					buffer[i] = *((int*)pointer);
					pointer += size;
				}
			}
			return buffer;
		}

		public static byte[] TransformToBytes(uint[] key)
		{
			return TransformToBytes(key, 0, key.Length);
		}

		public static byte[] TransformToBytes(uint[] key, int index, int length)
		{
			Guard.CheckIndexAndLength(key, index, length);

			const int size = sizeof(uint);
			byte[] buffer = new byte[size * length];
			for (int i = index; i < index + length; i++)
			{
				byte[] bytes = BitConverter.GetBytes(key[i]);
				Array.Copy(bytes, 0, buffer, size * i, size);
			}
			return buffer;
		}

		public static byte[] TransformToBytes(ulong[] key)
		{
			return TransformToBytes(key, 0, key.Length);
		}

		public static byte[] TransformToBytes(ulong[] key, int index, int length)
		{
			Guard.CheckIndexAndLength(key, index, length);

			const int size = sizeof(ulong);
			byte[] buffer = new byte[size * length];
			for (int i = index; i < index + length; i++)
			{
				byte[] bytes = BitConverter.GetBytes(key[i]);
				Array.Copy(bytes, 0, buffer, size * i, size);
			}
			return buffer;
		}

		internal static void TransformToUintsSwapOrder(byte[] src, int index, int length, uint[] result, int indexOut)
		{
			for (int i = indexOut; length > 0; length -= 4)
			{
				result[i++] =
					((uint)src[index++] << 24) |
					((uint)src[index++] << 16) |
					((uint)src[index++] << 8) |
					src[index++];
			}
		}

		internal static void TransformToUlongsSwapOrder(byte[] src, int index, int length, ulong[] result, int indexOut)
		{
			for (int i = indexOut; length > 0; length -= 8)
			{
				result[i++] =
					((ulong)src[index++] << 56) |
					((ulong)src[index++] << 48) |
					((ulong)src[index++] << 40) |
					((ulong)src[index++] << 32) |
					((ulong)src[index++] << 24) |
					((ulong)src[index++] << 16) |
					((ulong)src[index++] << 8) |
					src[index++];
			}
		}

		public static byte[] TransformToBytesSwapOrder(uint[] key)
		{
			return TransformToBytesSwapOrder(key, 0, key.Length);
		}

		public static byte[] TransformToBytesSwapOrder(uint[] key, int index, int length)
		{
			byte[] result = new byte[length * 4];

			for (int j = 0; length > 0; length--, index++)
			{
				result[j++] = (byte)(key[index] >> 24);
				result[j++] = (byte)(key[index] >> 16);
				result[j++] = (byte)(key[index] >> 8);
				result[j++] = (byte)key[index];
			}

			return result;
		}


		public static byte[] TransformToBytesSwapOrder(ulong[] key)
		{
			return TransformToBytesSwapOrder(key, 0, key.Length);
		}

		public static byte[] TransformToBytesSwapOrder(ulong[] key, int index, int length)
		{
			byte[] result = new byte[length * 8];

			for (int j = 0; length > 0; length--, index++)
			{
				result[j++] = (byte)(key[index] >> 56);
				result[j++] = (byte)(key[index] >> 48);
				result[j++] = (byte)(key[index] >> 40);
				result[j++] = (byte)(key[index] >> 32);
				result[j++] = (byte)(key[index] >> 24);
				result[j++] = (byte)(key[index] >> 16);
				result[j++] = (byte)(key[index] >> 8);
				result[j++] = (byte)key[index];
			}

			return result;
		}


		public static byte[] TransformToBytes(string key)
		{
			List<byte> buffer = new List<byte>();
			foreach (char c in key)
			{
				buffer.AddRange(BitConverter.GetBytes(c));
			}
			return buffer.ToArray();
		}

		public static uint[] TransformToUints(byte[] key)
		{
			return TransformToUints(key, 0, key.Length);
		}

		public static uint[] TransformToUints(byte[] key, int index, int length)
		{
			const int size = sizeof(uint);
			int newSize = length / size;

			Debug.Assert((length % size) == 0);

			uint[] buffer = new uint[newSize];

			for (int i = 0; i < newSize; i++)
				buffer[i] = BitConverter.ToUInt32(key, index + i * size);

			return buffer;
		}

		public static ulong[] TransformToUlongs(byte[] key, int index, int length)
		{
			const int size = sizeof(ulong);
			int newSize = length / size;

			Debug.Assert((length % size) == 0);

			ulong[] buffer = new ulong[newSize];

			for (int i = 0; i < newSize; i++)
				buffer[i] = BitConverter.ToUInt64(key, index + i * size);

			return buffer;
		}



		internal static void TransformToUints(byte[] key, uint[] dst, int index, int length)
		{
			const int size = sizeof(uint);
			int newSize = length / size;

			Debug.Assert((length % size) == 0);

			for (int i = 0; i < newSize; i++)
				dst[i] = BitConverter.ToUInt32(key, index + i * size);
		}

		

		public static string TransformToHexString(byte[] key)
		{
			return TransformToHexString(key, false);
		}

		public static string TransformToHexString(byte[] key, bool isGrouped)
		{
			string hex = BitConverter.ToString(key).ToUpper();

			if (isGrouped)
			{
				string[] ar = BitConverter.ToString(key).ToUpper().Split(new[] { '-' });
				hex = "";

				for (int i = 0; i < ar.Length / 4; i++)
				{
					if (i != 0)
						hex += "-";
					hex += ar[i * 4] + ar[i * 4 + 1] + ar[i * 4 + 2] + ar[i * 4 + 3];
				}
			}
			else
				hex = hex.Replace("-", "");

			return hex;
		}

		internal static unsafe void WriteUlongToByteArray(ulong data, byte[] src, int index)
		{
			Debug.Assert(index + 8 <= src.Length);
			fixed (byte* pByte = &src[index])
				*((ulong*) pByte) = data;
		}

		internal static void ZeroMemory(Array array)
		{
			Array.Clear(array, 0, array.Length);
		}

		internal static void Memset<T>(T[] array, T defValue)
		{
			for (int i = 0; i < array.Length; i++)
				array[i] = defValue;
		}

		internal static void Memset<T>(IList<T> array, T defValue)
		{
			for (int i = 0; i < array.Count; i++)
				array[i] = defValue;
		}

		

		public static uint ConvertBytesToUint(byte[] data, int index)
		{
			Debug.Assert(index >= 0);
			Debug.Assert(index + 4 <= data.Length);

			return data[index++] | ((uint)data[index++] << 8) |
				   ((uint)data[index++] << 16) | ((uint)data[index] << 24);
		}

		public static ulong ConvertBytesToUlong(byte[] data, int index)
		{
			Debug.Assert(index >= 0);
			Debug.Assert(index + 8 <= data.Length);

			return data[index++] |
				   ((ulong)data[index++] << 8) |
				   ((ulong)data[index++] << 16) |
				   ((ulong)data[index++] << 24) |
				   ((ulong)data[index++] << 32) |
				   ((ulong)data[index++] << 40) |
				   ((ulong)data[index++] << 48) |
				   ((ulong)data[index] << 56);
		}

		internal static void WriteUlongToByteArraySwapOrder(ulong data, byte[] src, int index)
		{
			Debug.Assert(index + 8 <= src.Length);

			src[index++] = (byte)(data >> 56);
			src[index++] = (byte)(data >> 48);
			src[index++] = (byte)(data >> 40);
			src[index++] = (byte)(data >> 32);
			src[index++] = (byte)(data >> 24);
			src[index++] = (byte)(data >> 16);
			src[index++] = (byte)(data >> 8);
			src[index] = (byte)data;
		}
	}
}

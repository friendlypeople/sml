// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using Sml.Common;
using Sml.Common.Visitors;
using Sml.Graphs.Data.Trees;

namespace Sml.DataStructures.Data.Queues
{
	[Serializable]
	public class PriorityQueue<T> : IQueue<T>, ICollection<T>, IVisitable<T>
	{
		private int _count;
		private int _defaultPriority;
		private readonly PriorityQueueType _queueType;
		private readonly RedBlackTreeList<int, T> _tree;

		public PriorityQueue(PriorityQueueType queueType)
		{
			if ((queueType != PriorityQueueType.Minimum) && (queueType != PriorityQueueType.Maximum))
			{
				throw new ArgumentOutOfRangeException("queueType");
			}
			_queueType = queueType;
			_tree = new RedBlackTreeList<int, T>();
		}

		public void Add(T item)
		{
			Add(item, _defaultPriority);
		}

		public void Add(T item, int priority)
		{
			AddItem(item, priority);
		}

		protected virtual void AddItem(T item, int priority)
		{
			LinkedList<T> list;
			if (_tree.TryGetValue(priority, out list))
			{
				list.AddLast(item);
			}
			else
			{
				list = new LinkedList<T>();
				list.AddLast(item);
				_tree.Add(priority, list);
			}
			Count++;
		}

		public void AddPriorityGroup(IList<T> items, int priority)
		{
			Guard.ArgumentNotNull(items, "items");
			AddPriorityGroupItem(items, priority);
		}

		protected virtual void AddPriorityGroupItem(IList<T> items, int priority)
		{
			LinkedList<T> list;
			if (_tree.TryGetValue(priority, out list))
			{
				for (int i = 0; i < items.Count; i++)
				{
					list.AddLast(items[i]);
				}
			}
			else
			{
				list = new LinkedList<T>(items);
				_tree.Add(priority, list);
			}
		}

		private void CheckTreeNotEmpty()
		{
			if (_tree.Count == 0)
			{
				throw new InvalidOperationException("The Priority Queue is empty.");
			}
		}

		public void Clear()
		{
			ClearItems();
		}

		protected virtual void ClearItems()
		{
			_tree.Clear();
			Count = 0;
		}

		public bool Contains(T item)
		{
			return _tree.ContainsValue(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
			if ((array.Length - arrayIndex) < Count)
			{
				throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");
			}
			foreach (KeyValuePair<int, LinkedList<T>> pair in _tree)
			{
				foreach (T local in pair.Value)
				{
					array.SetValue(local, arrayIndex++);
				}
			}
		}

		public void DecreaseItemPriority(int by)
		{
			_tree.ManipulateKeys(delegate(int oldKey)
			{
                return oldKey - by;
			});
		}

		public T Dequeue()
		{
			int num;
			return Dequeue(out num);
		}

		public T Dequeue(out int priority)
		{
			return DequeueItem(out priority);
		}

		protected virtual T DequeueItem(out int priority)
		{
			SmlPair<int, LinkedList<T>> nextItem = GetNextItem();
			T local = nextItem.Second.First.Value;
			nextItem.Second.RemoveFirst();
			int key = nextItem.First;
			if (nextItem.Second.Count == 0)
			{
				_tree.Remove(nextItem.First);
			}
			Count--;
			priority = key;
			return local;
		}

		public void Enqueue(T item)
		{
			Add(item);
		}

		public void Enqueue(T item, int priority)
		{
			Add(item, priority);
		}

		public IEnumerator<T> GetEnumerator()
		{
			return _tree.GetValueEnumerator();
		}

		public IEnumerator<SmlPair<int, T>> GetKeyEnumerator()
		{
			return _tree.GetKeyEnumerator();
		}

		private SmlPair<int, LinkedList<T>> GetNextItem()
		{
			CheckTreeNotEmpty();
			if (_queueType == PriorityQueueType.Maximum)
			{
				return _tree.MaximumAssociation;
			}
			return _tree.MinimumAssociation;
		}

		public IList<T> GetPriorityGroup(int priority)
		{
			LinkedList<T> list;
			if (_tree.TryGetValue(priority, out list))
			{
				return new List<T>(list);
			}
			return new List<T>();
		}

		public void IncreaseItemPriority(int by)
		{
			_tree.ManipulateKeys(delegate(int oldKey) {
			                                               	return oldKey + by;
			});
		}

		public T Peek()
		{
			return GetNextItem().Second.First.Value;
		}

		public bool Remove(T item)
		{
			int num;
			return Remove(item, out num);
		}

		public bool Remove(T item, out int priority)
		{
			return RemoveItem(item, out priority);
		}

		protected virtual bool RemoveItem(T item, out int priority)
		{
			bool flag = _tree.Remove(item, out priority);
			if (flag)
			{
				Count--;
			}
			return flag;
		}

		protected virtual bool RemoveItems(int priority)
		{
			LinkedList<T> list;
			if (_tree.TryGetValue(priority, out list))
			{
				_tree.Remove(priority);
				Count -= list.Count;
				return true;
			}
			return false;
		}

		public bool RemovePriorityGroup(int priority)
		{
			return RemoveItems(priority);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public int Count
		{
			get { return _count; }
			private set { _count = value; }
		}

		public int DefaultPriority
		{
			get { return _defaultPriority; }
			set { _defaultPriority = value; }
		}

		public bool IsEmpty
		{
			get { return (Count == 0); }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public void Accept(IVisitor<T> visitor)
		{
			_tree.Accept(visitor);
		}

	}
}

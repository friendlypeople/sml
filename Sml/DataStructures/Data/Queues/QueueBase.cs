// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Sml.DataStructures.Data.Queues
{
	[Serializable, DebuggerDisplay("Count = {Count}"), ComVisible(false)]
	public class QueueBase<T> : ICollection, IQueue<T>, ICollection<T>
	{
		private readonly Queue<T> _innerQueue;

		public QueueBase()
		{
			_innerQueue = new Queue<T>();
		}

		public QueueBase(IEnumerable<T> collection)
		{
			_innerQueue = new Queue<T>(collection);
		}

		public QueueBase(int capacity)
		{
			_innerQueue = new Queue<T>(capacity);
		}

		public void Add(T item)
		{
			EnqueueItem(item);
		}

		public void Clear()
		{
			ClearItems();
		}

		protected virtual void ClearItems()
		{
			_innerQueue.Clear();
		}

		public bool Contains(T item)
		{
			return _innerQueue.Contains(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			_innerQueue.CopyTo(array, arrayIndex);
		}

		public T Dequeue()
		{
			return DequeueItem();
		}

		protected virtual T DequeueItem()
		{
			return _innerQueue.Dequeue();
		}

		public void Enqueue(T item)
		{
			EnqueueItem(item);
		}

		protected virtual void EnqueueItem(T item)
		{
			_innerQueue.Enqueue(item);
		}

		public Queue<T>.Enumerator GetEnumerator()
		{
			return _innerQueue.GetEnumerator();
		}

		public T Peek()
		{
			return _innerQueue.Peek();
		}

		bool ICollection<T>.Remove(T item)
		{
			throw new NotSupportedException();
		}

		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return _innerQueue.GetEnumerator();
		}

		void ICollection.CopyTo(Array array, int index)
		{
			_innerQueue.CopyTo((T[])array, index);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _innerQueue.GetEnumerator();
		}

		public T[] ToArray()
		{
			return _innerQueue.ToArray();
		}

		public void TrimExcess()
		{
			_innerQueue.TrimExcess();
		}

		public int Count
		{
			get { return _innerQueue.Count; }
		}

		public bool IsEmpty
		{
			get { return (Count == 0); }
		}

		public bool IsFull
		{ 
			get { return false; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		bool ICollection.IsSynchronized
		{
			get { return ((ICollection) _innerQueue).IsSynchronized; }
		}

		object ICollection.SyncRoot
		{
			get { return ((ICollection) _innerQueue).SyncRoot; }
		}
	}


}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using Sml.Common;
using Sml.Common.Visitors;

namespace Sml.DataStructures.Data.Queues
{
	[Serializable]
	public class CircularQueue<T> : IQueue<T>, ICollection<T>, IVisitable<T>
	{
		private readonly int _capacity;
		private readonly LinkedList<T> _data;

		public CircularQueue(int capacity)
		{
			_data = new LinkedList<T>();
			if (capacity < 1)
				throw new ArgumentException("CapacityCanNotBeLessThanOne", "capacity");

			_capacity = capacity;
		}

		public void Clear()
		{
			ClearItems();
		}

		protected virtual void ClearItems()
		{
			_data.Clear();
		}

		public bool Contains(T item)
		{
			return (!IsEmpty && _data.Contains(item));
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
			if (!IsEmpty)
			{
				_data.CopyTo(array, arrayIndex);
			}
		}

		public T Dequeue()
		{
			if (IsEmpty)
				throw new InvalidOperationException("QueueIsEmpty");
			return DequeueItem();
		}

		protected virtual T DequeueItem()
		{
			T local = _data.First.Value;
			_data.RemoveFirst();
			return local;
		}

		public void Enqueue(T item)
		{
			EnqueueItem(item);
		}

		protected virtual void EnqueueItem(T item)
		{
			if (_data.Count == _capacity)
				_data.RemoveFirst();
			_data.AddLast(item);
		}

		public IEnumerator<T> GetEnumerator()
		{
			return _data.GetEnumerator();
		}

		public T Peek()
		{
			if (IsEmpty)
				throw new InvalidOperationException("QueueIsEmpty");

			return _data.First.Value;
		}

		public bool Remove(T item)
		{
			return RemoveItem(item);
		}

		protected virtual bool RemoveItem(T item)
		{
			return _data.Remove(item);
		}

		void ICollection<T>.Add(T item)
		{
			Enqueue(item);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public int Capacity
		{
			get { return _capacity; }
		}

		public int Count
		{
			get { return _data.Count; }
		}

		public bool IsEmpty
		{
			get { return (_data.Count == 0); }
		}

		public bool IsFull
		{
			get { return (_data.Count == _capacity); }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		#region Implementation of IVisitable<T>

		public void Accept(IVisitor<T> visitor)
		{
			AcceptVisitor.Accept(_data, visitor);
		}

		#endregion
	}
}

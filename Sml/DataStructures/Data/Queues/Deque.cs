// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using Sml.Common;
using Sml.Common.Visitors;

namespace Sml.DataStructures.Data.Queues
{
	[Serializable]
	public class Deque<T> : IDeque<T>, ICollection<T>, IVisitable<T>
	{
		private readonly LinkedList<T> _list;

		public Deque()
		{
			_list = new LinkedList<T>();
		}

		public Deque(IEnumerable<T> collection)
		{
			_list = new LinkedList<T>(collection);
		}

		public void Accept(IVisitor<T> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			AcceptVisitor.Accept(_list, visitor);
		}

		public void Clear()
		{
			ClearItems();
		}

		protected virtual void ClearItems()
		{
			_list.Clear();
		}

		public bool Contains(T item)
		{
			return _list.Contains(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			_list.CopyTo(array, arrayIndex);
		}

		public T DequeueHead()
		{
			if (_list.Count == 0)
			{
				throw new InvalidOperationException("DequeIsEmpty");
			}
			return DequeueHeadItem();
		}

		protected virtual T DequeueHeadItem()
		{
			T local = _list.First.Value;
			_list.RemoveFirst();
			return local;
		}

		public T DequeueTail()
		{
			if (_list.Count == 0)
			{
				throw new InvalidOperationException("DequeIsEmpty");
			}
			return DequeueTailItem();
		}

		protected virtual T DequeueTailItem()
		{
			T local = _list.Last.Value;
			_list.RemoveLast();
			return local;
		}

		public void EnqueueHead(T item)
		{
			EnqueueHeadItem(item);
		}

		protected virtual void EnqueueHeadItem(T item)
		{
			_list.AddFirst(item);
		}

		public void EnqueueTail(T item)
		{
			EnqueueTailItem(item);
		}

		protected virtual void EnqueueTailItem(T item)
		{
			_list.AddLast(item);
		}

		public IEnumerator<T> GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		void ICollection<T>.Add(T item)
		{
			throw new NotSupportedException();
		}

		bool ICollection<T>.Remove(T item)
		{
			throw new NotSupportedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public int Count
		{
			get { return _list.Count; }
		}

		public T Head
		{
			get
			{
				if (_list.Count == 0)
				{
					throw new InvalidOperationException("DequeIsEmpty");
				}
				return _list.First.Value;
			}
		}

		public bool IsEmpty
		{
			get { return (Count == 0); }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public T Tail
		{
			get
			{
				if (_list.Count == 0)
					throw new InvalidOperationException("DequeIsEmpty");
				return _list.Last.Value;
			}
		}
	}
}

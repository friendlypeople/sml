// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Sml.Common;
using Sml.Geometry.Data;

namespace Sml.DataStructures.Data.Collections
{
	//[DebuggerDisplay("{this.ToString()}")]
	public class BitVector : IEnumerable<bool>, ICloneable
	{
		private readonly List<byte> _data = new List<byte>();
		private int					_bitSize;

		public BitVector() { }

		public BitVector(int bitSize) : this(bitSize, false) { }

		public BitVector(int bitSize, bool defaultValue)
		{
			SetAllImpl(bitSize, defaultValue);
		}

		public BitVector(bool[] values)
		{
			Add(values);
		}

		public BitVector(BitArray bitArray)
		{
			foreach (bool item in bitArray)
				Add(item);
		}

		public BitVector(IEnumerable<bool> values)
		{
			foreach (bool item in values)
				Add(item);
		}

		public BitVector(BitVector vector)
		{
			CloneImpl(vector, this);
		}

		public BitVector(IEnumerable<int> values)
		{
			foreach (int item in values)
				Add(item);
		}

		private void SetAllImpl(int bitSize, bool defaultValue)
		{
			int byteSize = (bitSize >> 3) + 1;
			byte val = (byte)(defaultValue ? 0xFF : 0);

			for (int i = 0; i < byteSize; i++)
				_data.Add(val);
			_bitSize = bitSize;
		}

		private void PushByte(byte b)
		{
			int bitPosInLastByte = _bitSize & 0x7;
			byte addByte = b;
			if (bitPosInLastByte != 0)
			{
				int index = (_bitSize >> 3);
				byte leftPart = (byte)(b >> bitPosInLastByte);
				_data[index] |= leftPart;
				addByte = (byte)(b ^ (leftPart << bitPosInLastByte));
			}
			_data.Add(addByte);
			_bitSize += 8;
		}

		private static void CloneImpl(BitVector src, BitVector dst)
		{
			dst._data.Clear();
			dst._data.AddRange(src._data);
			dst._bitSize = src._bitSize;
		}

		public bool this[int index]
		{
			get { return Get(index); }
			set { Set(index, value); }
		}

		public bool Get(int index)
		{
			Guard.CheckIndexAndLength(index, Count);

			int valueRenamed = _data[index >> 3] & 0xff;
			return ((valueRenamed >> (index & 0x7)) & 1) == 1;
		}

		public void Set(int index, bool value)
		{
			Guard.CheckIndexAndLength(index, Count);
			int index1 = (index >> 3);
			
			if (value)
				_data[index1] |= (byte) (1 << (index & 0x7));
			else
				_data[index1] &= (byte) ~(1 << (index & 0x7));
		}

		public void SetAll(bool value)
		{
			SetAllImpl(_bitSize, value);
		}

		public int Count
		{
			get { return _bitSize; }
		}

		private byte ToBit(bool item)
		{
			return (byte) (item ? 1 : 0);
		}

		public void Add(bool item)
		{
			int bitPosInLastByte = _bitSize & 0x7;
			if (bitPosInLastByte == 0)
				_data.Add(0);

			_data[_bitSize >> 3] |= (byte)(ToBit(item) << bitPosInLastByte);
			_bitSize++;
		}

		public void Add(IEnumerable<bool> items)
		{
			foreach (bool item in items)
				Add(item);
		}

		public void Add(params bool[] items)
		{
			Add((IEnumerable<bool>)items);
		}

		public void Add(long item)
		{
			while (item != 0)
			{
				PushByte((byte)(item & 0xFF));
				item >>= 8;
			}
		}

		public void Add(ulong item)
		{
			while (item != 0)
			{
				PushByte((byte)(item & 0xFF));
				item >>= 8;
			}
		}

		public BitVector And(BitVector vector)
		{
			BitVector bv = new BitVector(this);
			int bitSize = Math.Min(bv._bitSize, vector._bitSize);
			for (int i = 0; i < bitSize; i++)
			{
				bool res = bv[i] & vector[i];
				bv.Set(i, res);				
			}
			return bv;

		}

		public BitVector And(BitArray array)
		{
			BitVector bv = new BitVector(this);
			int bitSize = Math.Min(bv._bitSize, array.Count);
			for (int i = 0; i < bitSize; i++)
				bv[i] &= array[i];
			return bv;
		}

		public BitVector Or(BitVector vector)
		{
			BitVector bv = new BitVector(this);
			int bitSize = Math.Min(bv._bitSize, vector._bitSize);
			for (int i = 0; i < bitSize; i++)
				bv[i] |= vector[i];
			return bv;
		}

		public BitVector Or(BitArray array)
		{
			BitVector bv = new BitVector(this);
			int bitSize = Math.Min(bv._bitSize, array.Count);
			for (int i = 0; i < bitSize; i++)
				bv[i] |= array[i];
			return bv;
		}

		public BitVector Xor(BitVector vector)
		{
			BitVector bv = new BitVector(this);
			int bitSize = Math.Min(bv._bitSize, vector._bitSize);
			for (int i = 0; i < bitSize; i++)
				bv[i] ^= vector[i];
			return bv;

		}

		public BitVector Xor(BitArray array)
		{
			BitVector bv = new BitVector(this);
			int bitSize = Math.Min(bv._bitSize, array.Count);
			for (int i = 0; i < bitSize; i++)
				bv[i] ^= array[i];
			return bv;
		}

		public BitVector Not()
		{
			BitVector bv = new BitVector();
			for (int i = 0; i < _bitSize; i++)
			{
				bool items = !this[i];
				bv.Add(items);
			}
			return bv;
		}

		public static BitVector operator &(BitVector v1, BitVector v2)
		{
			return v1.And(v2);
		}

		public static BitVector operator &(BitVector v1, BitArray v2)
		{
			return v1.And(v2);
		}

		public static BitVector operator |(BitVector v1, BitVector v2)
		{
			return v1.Or(v2);
		}

		public static BitVector operator |(BitVector v1, BitArray v2)
		{
			return v1.Or(v2);
		}

		public static BitVector operator ^(BitVector v1, BitVector v2)
		{
			return v1.Xor(v2);
		}

		public static BitVector operator ^(BitVector v1, BitArray v2)
		{
			return v1.Xor(v2);
		}

		public static BitVector operator !(BitVector v1)
		{
			return v1.Not();
		}

		public static bool operator ==(BitVector v1, BitArray v2)
		{
			return !ReferenceEquals(null, v1) && v1.Equals(v2);
		}

		public static bool operator !=(BitVector v1, BitArray v2)
		{
			return !(v1 == v2);
		}


		public override string ToString()
		{
			string value = string.Empty;
			for (int index = 0; index < Count; index++)
				value += Get(index) ? '1' : '0';
			return value;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public BitVector Clone()
		{
			return new BitVector(this);
		}

		public BitArray ToBitArray()
		{
			return new BitArray(_data.ToArray());
		}

		public IEnumerator<bool> GetEnumerator()
		{
			for (int i = 0; i < _bitSize; i++)
				yield return this[i];
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public override bool Equals(object obj)
		{
			return obj.GetType() == typeof(SmlPoint2D) && Equals((BitVector)obj);
		}

		public bool Equals(BitVector other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;

			if (_bitSize != other._bitSize) return false;
			for (int i = 0; i < _bitSize; i++)
			{
				if (_data[i] != other._data[i])
					return false;
			}
			return true;
		}

		public override int GetHashCode()
		{
			return _data.GetHashCode();
		}
	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using Sml.Common;
using Sml.Common.Visitors;

namespace Sml.DataStructures.Data.Collections
{
	[Serializable]
	public class Bag<T> : IBag<T>, IEquatable<Bag<T>>
	{
		private int _count;

		private readonly Dictionary<T, int> _data;

		public Bag()
		{
			_data = new Dictionary<T, int>();
		}

		private Bag(IDictionary<T, int> dictionary)
		{
			_data = new Dictionary<T, int>(dictionary);
			foreach (KeyValuePair<T, int> pair in _data)
			{
				Count += pair.Value;
			}
		}

		public Bag(IEqualityComparer<T> comparer)
		{
			Guard.ArgumentNotNull(comparer, "comparer");
			_data = new Dictionary<T, int>(comparer);
		}

		public Bag(int capacity)
		{
			_data = new Dictionary<T, int>(capacity);
		}

		public Bag(int capacity, IEqualityComparer<T> comparer)
		{
			Guard.ArgumentNotNull(comparer, "comparer");
			_data = new Dictionary<T, int>(capacity, comparer);
		}

		public void Accept(IVisitor<KeyValuePair<T, int>> visitor)
		{
			AcceptVisitor.Accept(_data, visitor);
		}

		public void Accept(IVisitor<T> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			foreach (T local in this)
			{
				if (visitor.HasCompleted)
				{
					break;
				}
				visitor.Visit(local);
			}
		}

		public void Add(T item)
		{
			AddItem(item, 1);
		}

		public void Add(T item, int amount)
		{
			if (amount <= 0)
			{
				throw new ArgumentOutOfRangeException("amount", "OnlyAddPositiveAmount");
			}
			AddItem(item, amount);
		}

		protected virtual void AddItem(T item, int amount)
		{
			int num;
			if (_data.TryGetValue(item, out num))
			{
				_data[item] = num + amount;
			}
			else
			{
				_data.Add(item, amount);
			}
			Count += amount;
		}

		public void Clear()
		{
			ClearItems();
		}

		protected virtual void ClearItems()
		{
			_data.Clear();
			Count = 0;
		}

		public bool Contains(T item)
		{
			return _data.ContainsKey(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
			if ((array.Length - arrayIndex) < Count)
			{
				throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");
			}
			int num = arrayIndex;
			foreach (KeyValuePair<T, int> pair in _data)
			{
				int num2 = pair.Value;
				T key = pair.Key;
				for (int i = 0; i < num2; i++)
				{
					array.SetValue(key, num++);
				}
			}
		}

		public bool Equals(Bag<T> other)
		{
			if (other == null)
			{
				return false;
			}
			if (Count != other.Count)
			{
				return false;
			}
			foreach (KeyValuePair<T, int> pair in _data)
			{
				if (!other.Contains(pair.Key))
				{
					return false;
				}
				if (other[pair.Key] != pair.Value)
				{
					return false;
				}
			}
			return true;
		}

		public IEnumerator<KeyValuePair<T, int>> GetCountEnumerator()
		{
			return _data.GetEnumerator();
		}

		public IEnumerator<T> GetEnumerator()
		{
			foreach (KeyValuePair<T, int> iteratorVariable0 in _data)
			{
				yield return iteratorVariable0.Key;
			}
		}

		public Bag<T> Intersection(Bag<T> bag)
		{
			return IntersectionInternal(bag);
		}

		private Bag<T> IntersectionInternal(IBag<T> bag)
		{
			Guard.ArgumentNotNull(bag, "bag");
			Bag<T> bag2 = new Bag<T>();
			using (IEnumerator<KeyValuePair<T, int>> enumerator = bag.GetCountEnumerator())
			{
				while (enumerator.MoveNext())
				{
					int num;
					KeyValuePair<T, int> current = enumerator.Current;
					if (_data.TryGetValue(current.Key, out num))
					{
						bag2.Add(current.Key, Math.Min(current.Value, num));
					}
				}
			}
			return bag2;
		}

		IBag<T> IBag<T>.Intersection(IBag<T> bag)
		{
			return IntersectionInternal(bag);
		}

		IBag<T> IBag<T>.Subtract(IBag<T> bag)
		{
			return SubtractInternal(bag);
		}

		IBag<T> IBag<T>.Union(IBag<T> bag)
		{
			return Union((Bag<T>) bag);
		}

		public static Bag<T> operator +(Bag<T> left, Bag<T> right)
		{
			return left.Union(right);
		}

		public static Bag<T> operator *(Bag<T> left, Bag<T> right)
		{
			return left.Intersection(right);
		}

		public static Bag<T> operator -(Bag<T> left, Bag<T> right)
		{
			return left.Subtract(right);
		}

		public bool Remove(T item)
		{
			int num;
			if (_data.TryGetValue(item, out num))
			{
				RemoveItem(item, 1, num);
				return true;
			}
			return false;
		}

		public bool Remove(T item, int maximum)
		{
			int num;
			if (maximum <= 0)
			{
				throw new ArgumentOutOfRangeException("maximum");
			}
			if (_data.TryGetValue(item, out num))
			{
				RemoveItem(item, maximum, num);
				return true;
			}
			return false;
		}

		public bool RemoveAll(T item)
		{
			int num;
			if (_data.TryGetValue(item, out num))
			{
				RemoveItem(item, num, num);
				return true;
			}
			return false;
		}

		protected virtual void RemoveItem(T item, int maximum, int itemCount)
		{
			if (maximum >= itemCount)
			{
				Count -= itemCount;
				_data.Remove(item);
			}
			else
			{
				Count -= maximum;
				_data[item] = itemCount - maximum;
			}
		}

		public Bag<T> Subtract(Bag<T> bag)
		{
			return SubtractInternal(bag);
		}

		private Bag<T> SubtractInternal(IBag<T> bag)
		{
			Guard.ArgumentNotNull(bag, "bag");
			Bag<T> bag2 = new Bag<T>(_data);
			using (IEnumerator<KeyValuePair<T, int>> enumerator = bag.GetCountEnumerator())
			{
				while (enumerator.MoveNext())
				{
					int num;
					KeyValuePair<T, int> current = enumerator.Current;
					if (bag2._data.TryGetValue(current.Key, out num))
					{
						if ((num - current.Value) <= 0)
						{
							bag2.RemoveAll(current.Key);
						}
						else
						{
							bag2.Remove(current.Key, current.Value);
						}
					}
				}
			}
			return bag2;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public Bag<T> Union(Bag<T> bag)
		{
			return UnionInternal(bag);
		}

		private Bag<T> UnionInternal(IBag<T> bag)
		{
			Guard.ArgumentNotNull(bag, "bag");
			Bag<T> bag2 = new Bag<T>();
			foreach (KeyValuePair<T, int> pair in _data)
			{
				bag2.Add(pair.Key, pair.Value);
			}
			using (IEnumerator<KeyValuePair<T, int>> enumerator = bag.GetCountEnumerator())
			{
				while (enumerator.MoveNext())
				{
					KeyValuePair<T, int> current = enumerator.Current;
					bag2.Add(current.Key, current.Value);
				}
			}
			return bag2;
		}

		public int Count
		{
			get { return _count; }
			private set { _count = value; }
		}

		public bool IsEmpty
		{
			get { return (Count == 0); }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public int this[T item]
		{
			get
			{
				int num;
				if (_data.TryGetValue(item, out num))
				{
					return num;
				}
				return 0;
			}
		}
	}

}

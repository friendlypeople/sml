// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using Sml.Common;
using Sml.Common.Visitors;

namespace Sml.DataStructures.Data.Collections
{
	public class Set<T> : ISet, ICollection<T>, IVisitable<T>, IEquatable<Set<T>>
	{
		private readonly IEqualityComparer<T>	_comparer;
		private Dictionary<int, T>				_setTable = new Dictionary<int, T>();

		/// <summary>
		/// Initializes a new instance of the <see cref="Set&lt;T&gt;"/> class.
		/// </summary>
		public Set() : this(EqualityComparer<T>.Default) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="Set&lt;T&gt;"/> class.
		/// </summary>
		/// <param name="equalityComparer">The equality comparer.</param>
		public Set(IEqualityComparer<T> equalityComparer)
		{
			Guard.ArgumentNotNull(equalityComparer, "equalityComparer");
			_comparer = equalityComparer;
		}


		/// <summary>
		/// Initializes a new instance of the <see cref="Set&lt;T&gt;"/> class.
		/// </summary>
		/// <param name="collection">The collection.</param>
		public Set(IEnumerable<T> collection) :
			this(collection, EqualityComparer<T>.Default)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Set&lt;T&gt;"/> class.
		/// </summary>
		/// <param name="collection">The collection.</param>
		/// <param name="equalityComparer">The equality comparer.</param>
		public Set(IEnumerable<T> collection, IEqualityComparer<T> equalityComparer)
			: this(equalityComparer)
		{
			Add(collection);
		}

		private void CheckConsistentComparison(Set<T> otherSet)
		{
			if (otherSet == null)
				throw new ArgumentNullException("otherSet");

			if (!Equals(_comparer, otherSet._comparer))
				throw new InvalidOperationException("InconsistentComparisons");
		}

		
		public IEqualityComparer<T> Comparer
		{
			get { return _comparer; }
		}

		public int Count
		{
			get { return _setTable.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool Contains(T item)
		{
			int hash = HashCode.GetHashCode(item, _comparer);
			return _setTable.ContainsKey(hash);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
			if ((array.Length - arrayIndex) < Count)
				throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");

			foreach (T num in this)
				array.SetValue(num, arrayIndex++);
		}

		public bool TryGetItem(T item, out T foundItem)
		{
			int hash = HashCode.GetHashCode(item, _comparer);
			return _setTable.TryGetValue(hash, out foundItem);
		}

		public void Add(T item)
		{
			int hash = HashCode.GetHashCode(item, _comparer);
			_setTable[hash] = item;
		}

		public void Add(IEnumerable<T> collection)
		{
			Guard.ArgumentNotNull(collection, "collection");
			if (ReferenceEquals(collection, this)) return;

			foreach (T item in collection)
				Add(item);
		}

		public bool Remove(T item)
		{
			int hash = HashCode.GetHashCode(item, _comparer);
			if (!_setTable.ContainsKey(hash))
				return false;
			_setTable.Remove(hash);
			return true;
		}

		public int Remove(IEnumerable<T> collection)
		{
			Guard.ArgumentNotNull(collection, "collection");

			int count = 0;

			if (collection == this)
			{
				count = _setTable.Count;
				Clear();
				return count;
			}
			foreach (T item in collection)
			{
				if (Remove(item))
					++count;
			}

			return count;
		}

		public void Clear()
		{
			_setTable = new Dictionary<int, T>();
		}

		public Set<T> Intersection(Set<T> set)
		{
			Guard.ArgumentNotNull(set, "set");
			CheckConsistentComparison(set);

			Set<T> smaller	= set;
			Set<T> larger	= this;

			if (set.Count > Count)
			{
				smaller = this;
				larger = set;
			}

			Set<T> result = new Set<T>(_comparer);
			foreach (T item in smaller)
			{
				if (larger.Contains(item))
					result.Add(item);
			}
			return result; 
		}


		public Set<T> Union(Set<T> set)
		{
			CheckConsistentComparison(set);
			Set<T> smaller	= set;
			Set<T> larger	= this;
			if (set.Count > Count)
			{
				smaller = this;
				larger = set;
			}

			Set<T> result = new Set<T>(_comparer);
			result.Add(larger);
			result.Add(smaller);
			return result;
		}

		public Set<T> Subtract(Set<T> set)
		{
			CheckConsistentComparison(set);
			Set<T> smaller = set;
			Set<T> larger = this;
			if (set.Count > Count)
			{
				smaller = this;
				larger = set;
			}

			Set<T> result = new Set<T>(_comparer);
			foreach (T item in smaller)
			{
				if (!larger.Contains(item))
					result.Add(item);
			}
			return result;
		}

		

		public bool IsSupersetOf(Set<T> otherSet)
		{
			CheckConsistentComparison(otherSet);

			if (otherSet.Count > Count)
				return false;

			foreach (T item in otherSet)
			{
				if (!Contains(item))
					return false;
			}

			return true;
		}

		public bool IsProperSupersetOf(Set<T> otherSet)
		{
			CheckConsistentComparison(otherSet);

			if (otherSet.Count >= Count)
				return false;     // Can't be a proper superset of a bigger or equal set

			return IsSupersetOf(otherSet);
		}

		public bool IsProperSubsetOf(Set<T> otherSet)
		{
			return otherSet.IsProperSupersetOf(this);
		}

		public bool IsSubsetOf(Set<T> otherSet)
		{
			return otherSet.IsSupersetOf(this);
		}

		bool ISet.IsProperSubsetOf(ISet other)
		{
			return IsProperSupersetOf((Set<T>)other);
		}

		public bool IsProperSupersetOf(ISet other)
		{
			return IsProperSubsetOf((Set<T>)other);
		}

		bool ISet.IsSubsetOf(ISet other)
		{
			return IsSubsetOf((Set<T>)other); 
		}

		bool ISet.IsSupersetOf(ISet other)
		{
			return IsSupersetOf((Set<T>)other);
		}

		ISet ISet.Intersection(ISet other)
		{
			return Intersection((Set<T>)other);
		}

		ISet ISet.Subtract(ISet other)
		{
			return Subtract((Set<T>)other);
		}

		ISet ISet.Union(ISet other)
		{
			return Union((Set<T>)other);
		}

		#region Implementation of IEnumerable

		public IEnumerator<T> GetEnumerator()
		{
			return _setTable.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion

		public void Accept(IVisitor<T> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			foreach (T num in this)
			{
				if (visitor.HasCompleted)
					break;
				visitor.Visit(num);
			}
		}

		public bool IsEmpty
		{
			get { return Count == 0; }
		}

		#region Implementation of IEquatable<Set<T>>

		public bool Equals(Set<T> other)
		{
			CheckConsistentComparison(other);
			if (other.Count != Count)
				return false;

			foreach (T item in other)
				if (!Contains(item))
					return false;
			return true;
		}

		#endregion
	}
}

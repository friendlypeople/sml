// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Sml.Common;
using Sml.Common.Visitors;

namespace Sml.DataStructures.Data.Collections
{
	[Serializable]
	public class PascalSet : ICollection<int>, IVisitable<int>, ISet, IEquatable<PascalSet>
	{
		private int					_count;
		private readonly BitArray	_data;
		private readonly int		_lowerBound;
		private readonly int		_upperBound;

		public PascalSet(int upperBound) : this(0, upperBound) { }

		public PascalSet(int upperBound, int[] initialValues) : this(0, upperBound, initialValues) { }

		public PascalSet(int lowerBound, int upperBound)
		{
			if (lowerBound < 0)
			{
				throw new ArgumentException("LowerBoundLargerThanZero", "lowerBound");
			}
			if (upperBound < lowerBound)
			{
				throw new ArgumentException("UpperBoundMustBeLargerThanLowerBound", "upperBound");
			}
			_lowerBound = lowerBound;
			_upperBound = upperBound;
			_data = new BitArray((upperBound - lowerBound) + 1, false);
		}

		private PascalSet(BitArray initialData, int lowerBound, int upperBound)
		{
			_upperBound = upperBound;
			_lowerBound = lowerBound;
			_data = initialData;
			for (int i = 0; i < _data.Length; i++)
			{
				if (_data[i])
				{
					Count++;
				}
			}
		}

		public PascalSet(int lowerBound, int upperBound, int[] initialValues) : this(lowerBound, upperBound)
		{
			Guard.ArgumentNotNull(initialValues, "initialValues");
		    for (int i = 0; i < initialValues.Length; i++)
		        Add(initialValues[i]);
		}

		public void Accept(IVisitor<int> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			foreach (int num in this)
			{
			    if (visitor.HasCompleted)
			        break;

			    visitor.Visit(num);
			}
		}

		public void Add(int item)
		{
			CheckValidIndex(item);
			int offSet = GetOffSet(item);
		    if (!_data[offSet])
		        AddItem(item, offSet);
		}

		protected virtual void AddItem(int item, int offset)
		{
			Count++;
			_data[offset] = true;
		}

		private void CheckIfUniverseTheSame(PascalSet set)
		{
		    if (!IsUniverseTheSame(set))
		        throw new ArgumentException("UniverseNotTheSame", "set");
		}

		private void CheckValidIndex(int index)
		{
		    if (!IsIndexValid(index))
		        throw new ArgumentException("ItemNotInUniverse", "index");
		}

		public void Clear()
		{
			ClearItems();
		}

		protected virtual void ClearItems()
		{
			_data.SetAll(false);
			Count = 0;
		}

		public bool Contains(int item)
		{
			CheckValidIndex(item);
			return _data[item];
		}

		public void CopyTo(int[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
		    if ((array.Length - arrayIndex) < Count)
		        throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");
		    foreach (int num in this)
		        array.SetValue(num, arrayIndex++);
		}

		public bool Equals(PascalSet other)
		{
		    if ((other == null) || !IsUniverseTheSame(other))
		        return false;
		    for (int i = 0; i < _data.Length; i++)
			{
			    if (_data[i] != other._data[i])
			        return false;
			}
			return true;
		}

		public IEnumerator<int> GetEnumerator()
		{
			for (int i = 0; i < _data.Count; i++)
			{
			    if (_data[i])
			        yield return (i + _lowerBound);
			}
		}

		private int GetOffSet(int item)
		{
			return (item - _lowerBound);
		}

		public PascalSet Intersection(PascalSet set)
		{
			Guard.ArgumentNotNull(set, "set");
			CheckIfUniverseTheSame(set);
			return new PascalSet(_data.And(set._data), _lowerBound, _upperBound);
		}

		public PascalSet Inverse()
		{
			return new PascalSet(_data.Not(), _lowerBound, _upperBound);
		}

		private bool IsIndexValid(int index)
		{
			return ((index >= _lowerBound) && (index <= _upperBound));
		}

		public bool IsProperSubsetOf(PascalSet set)
		{
			Guard.ArgumentNotNull(set, "set");
			CheckIfUniverseTheSame(set);
			return (IsSubsetOf(set) && !set.IsSubsetOf(this));
		}

		public bool IsProperSupersetOf(PascalSet set)
		{
			Guard.ArgumentNotNull(set, "set");
			CheckIfUniverseTheSame(set);
			return (IsSupersetOf(set) && !set.IsSupersetOf(this));
		}

		public bool IsSubsetOf(PascalSet set)
		{
			Guard.ArgumentNotNull(set, "set");
			CheckIfUniverseTheSame(set);
			for (int i = 0; i < _data.Length; i++)
			{
			    if (_data[i] && !set._data[i])
			        return false;
			}
			return true;
		}

		public bool IsSupersetOf(PascalSet set)
		{
			Guard.ArgumentNotNull(set, "set");
			CheckIfUniverseTheSame(set);
			for (int i = 0; i < _data.Length; i++)
			{
			    if (set._data[i] && !_data[i])
			        return false;
			}
			return true;
		}

		private bool IsUniverseTheSame(PascalSet set)
		{
			return ((set._lowerBound == _lowerBound) && (set._upperBound == _upperBound));
		}

		ISet ISet.Intersection(ISet set)
		{
			return Intersection((PascalSet) set);
		}

		bool ISet.IsProperSubsetOf(ISet set)
		{
			return IsProperSubsetOf((PascalSet) set);
		}

		bool ISet.IsProperSupersetOf(ISet set)
		{
			return IsProperSupersetOf((PascalSet) set);
		}

		bool ISet.IsSubsetOf(ISet set)
		{
			return IsSubsetOf((PascalSet) set);
		}

		bool ISet.IsSupersetOf(ISet set)
		{
			return IsSupersetOf((PascalSet) set);
		}

		ISet ISet.Subtract(ISet set)
		{
			return Subtract((PascalSet) set);
		}

		ISet ISet.Union(ISet set)
		{
			return Union((PascalSet) set);
		}

		public static PascalSet operator +(PascalSet left, PascalSet right)
		{
			Guard.ArgumentNotNull(left, "left");
			return left.Union(right);
		}

		public static bool operator >(PascalSet left, PascalSet right)
		{
			Guard.ArgumentNotNull(left, "left");
			return left.IsProperSupersetOf(right);
		}

		public static bool operator >=(PascalSet left, PascalSet right)
		{
			Guard.ArgumentNotNull(left, "left");
			return left.IsSupersetOf(right);
		}

		public static bool operator <(PascalSet left, PascalSet right)
		{
			Guard.ArgumentNotNull(left, "left");
			return left.IsProperSubsetOf(right);
		}

		public static bool operator <=(PascalSet left, PascalSet right)
		{
			Guard.ArgumentNotNull(left, "left");
			return left.IsSubsetOf(right);
		}

		public static PascalSet op_LogicalNot(PascalSet set)
		{
			Guard.ArgumentNotNull(set, "set");
			return set.Inverse();
		}

		public static PascalSet operator *(PascalSet left, PascalSet right)
		{
			Guard.ArgumentNotNull(left, "left");
			return left.Intersection(right);
		}

		public static PascalSet operator -(PascalSet left, PascalSet right)
		{
			Guard.ArgumentNotNull(left, "left");
			return left.Subtract(right);
		}

		public bool Remove(int item)
		{
			CheckValidIndex(item);
			int offSet = GetOffSet(item);
			if (_data[offSet])
			{
				RemoveItem(item, offSet);
				return true;
			}
			return false;
		}

		protected virtual void RemoveItem(int item, int offset)
		{
			Count--;
			_data[offset] = false;
		}

		public PascalSet Subtract(PascalSet set)
		{
			Guard.ArgumentNotNull(set, "set");
			CheckIfUniverseTheSame(set);
			return new PascalSet(_data.Xor(set._data), _lowerBound, _upperBound);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public PascalSet Union(PascalSet set)
		{
			Guard.ArgumentNotNull(set, "set");
			CheckIfUniverseTheSame(set);
			return new PascalSet(_data.Or(set._data), _lowerBound, _upperBound);
		}

		public int Capacity
		{
			get { return ((_upperBound - _lowerBound) + 1); }
		}

		public int Count
		{
			get { return _count; }
			private set { _count = value; }
		}

		public bool IsEmpty
		{
			get { return (Count == 0); }
		}

		public bool IsFull
		{
			get { return (Capacity == Count); }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool this[int item]
		{
			get
			{
				CheckValidIndex(item);
				return _data[GetOffSet(item)];
			}
		}

		public int LowerBound
		{
			get { return _lowerBound; }
		}

		public int UpperBound
		{
			get { return _upperBound; }
		}
	}
}

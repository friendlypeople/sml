// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;

namespace Sml.DataStructures.Data.Collections
{
	/// <summary>
	/// Система непересекающихся множеств на основе дерева
	/// </summary>
	public class ForestDisjointSet<T>
	{
		private readonly Dictionary<T, Element> _elements = new Dictionary<T, Element>();

		private int _setCount;

		public ForestDisjointSet() { }

		public ForestDisjointSet(int elementCapacity)
		{
			_elements = new Dictionary<T, Element>(elementCapacity);
		}

		public int ElementCount
		{
			get { return _elements.Count; }
		}

		public int SetCount
		{
			get { return _setCount; }
		}

		private Element Find(Element element)
		{
			Element root = FindNoCompression(element);
			CompressPath(element, root);
			return root;
		}

		private static void CompressPath(Element element, Element root)
		{
			Element parent = element;
			while (parent != root)
			{
				Element temp = parent;
				parent = parent.Parent;
				temp.Parent = root;
			}
		}

		private Element FindNoCompression(Element element)
		{
			Element parent = element;
			while (parent.Parent != null)
				parent = parent.Parent;
			return parent;
		}

		private bool Union(Element left, Element right)
		{
			if (left != right)
			{
				Element lElem = Find(left);
				Element rElem = Find(right);

				if (lElem.Rank > rElem.Rank)
				{
					rElem.Parent = lElem;
					_setCount--;
					return true;
				}
				if (lElem.Rank < rElem.Rank)
				{
					lElem.Parent = rElem;
					_setCount--;
					return true;
				}
				if (lElem != rElem)
				{
					rElem.Parent = lElem;
					lElem.Rank++;
					_setCount--;
					return true;
				}
			}
			return false;
		}

		public T FindSet(T value)
		{
			return Find(_elements[value]).Value;
		}

		/// <summary>
		/// Внести в структуру новый элемент, создав для него множество из самого себя.
		/// </summary>
		/// <param name="value">Новый элемент</param>
		public void MakeSet(T value)
		{
			Element element = new Element(value);
			_elements.Add(value, element);
			_setCount++;
		}


		public bool AreInSameSet(T left, T right)
		{
			return FindSet(left).Equals(FindSet(right));
		}

		public bool Union(T left, T right)
		{
			return Union(_elements[left], _elements[right]);
		}

		public bool Contains(T value)
		{
			return _elements.ContainsKey(value);
		}

		private class Element
		{
			public Element Parent;
			public int Rank;
			public readonly T Value;

			public Element(T value)
			{
				Value = value;
			}
		}
	}
}

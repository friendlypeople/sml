// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using Sml.Common;
using Sml.Common.Visitors;
using Sml.DataStructures.Algorithms.Comparers;

namespace Sml.DataStructures.Data.Collections
{
	[Serializable]
	public class Heap<T> : IHeap<T>, ICollection<T>, IVisitable<T>
	{
		private readonly IComparer<T> _comparerToUse;
		private readonly List<T> _data;
		private readonly HeapType _thisType;

		public Heap(HeapType type) : this(type, Comparer<T>.Default) { }

		public Heap(HeapType type, IComparer<T> comparer)
		{
			Guard.ArgumentNotNull(comparer, "comparer");
			if ((type != HeapType.Minimum) && (type != HeapType.Maximum))
			{
				throw new ArgumentOutOfRangeException("type");
			}
			_thisType = type;
			List<T> list = new List<T>();
			list.Add(default(T));
			_data = list;
			_comparerToUse = (type == HeapType.Minimum) ? comparer : new ReverseComparer<T>(comparer);
		}

		public Heap(HeapType type, Comparison<T> comparer) : this(type, new ComparisonComparer<T>(comparer)) { }

		public Heap(HeapType type, int capacity) : this(type, capacity, Comparer<T>.Default) { }

		public Heap(HeapType type, int capacity, IComparer<T> comparer)
		{
			Guard.ArgumentNotNull(comparer, "comparer");
			if ((type != HeapType.Minimum) && (type != HeapType.Maximum))
			{
				throw new ArgumentOutOfRangeException("type");
			}
			_thisType = type;
			List<T> list = new List<T>(capacity);
			list.Add(default(T));
			_data = list;
			_comparerToUse = (type == HeapType.Minimum) ? comparer : new ReverseComparer<T>(comparer);
		}

		public Heap(HeapType type, int capacity, Comparison<T> comparer)
			: this(type, capacity, new ComparisonComparer<T>(comparer))
		{
		}

		public void Accept(IVisitor<T> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			for (int i = 1; i < _data.Count; i++)
			{
				if (visitor.HasCompleted)
				{
					return;
				}
				visitor.Visit(_data[i]);
			}
		}

		public void Add(T item)
		{
			AddItem(item);
		}

		protected virtual void AddItem(T item)
		{
			_data.Add(default(T));
			int num = _data.Count - 1;
			while ((num > 1) && (_comparerToUse.Compare(_data[num/2], item) > 0))
			{
				_data[num] = _data[num/2];
				num /= 2;
			}
			_data[num] = item;
		}

		public void Clear()
		{
			ClearItems();
		}

		protected virtual void ClearItems()
		{
			_data.RemoveRange(1, _data.Count - 1);
		}

		public bool Contains(T item)
		{
			return _data.Contains(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
			if ((array.Length - arrayIndex) < Count)
			{
				throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");
			}
			for (int i = 1; i < _data.Count; i++)
			{
				array[arrayIndex++] = _data[i];
			}
		}

		public IEnumerator<T> GetEnumerator()
		{
			int iteratorVariable0 = 1;
			while (true)
			{
				if (iteratorVariable0 >= _data.Count)
				{
					yield break;
				}
				yield return _data[iteratorVariable0];
				iteratorVariable0++;
			}
		}

		public T RemoveRoot()
		{
			if (Count == 0)
				throw new InvalidOperationException("HeapIsEmpty");
			T item = _data[1];
			RemoveRootItem(item);
			return item;
		}

		protected virtual void RemoveRootItem(T item)
		{
			T x = _data[Count];
			_data.RemoveAt(Count);
			if (Count > 0)
			{
				int num = 1;
				while ((num*2) < _data.Count)
				{
					int num2 = num*2;
				    if (((num2 + 1) < _data.Count) && (_comparerToUse.Compare(_data[num2 + 1], _data[num2]) < 0))
				        num2++;
				    if (_comparerToUse.Compare(x, _data[num2]) <= 0)
				        break;

				    _data[num] = _data[num2];
					num = num2;
				}
				_data[num] = x;
			}
		}

		bool ICollection<T>.Remove(T item)
		{
			throw new NotSupportedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public int Count
		{
			get { return (_data.Count - 1); }
		}

		public bool IsEmpty
		{
			get { return (Count == 0); }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public T Root
		{
			get
			{
				if (Count == 0)
					throw new InvalidOperationException("HeapIsEmpty");
				return _data[1];
			}
		}

		public HeapType Type
		{
			get { return _thisType; }
		}
	}
}

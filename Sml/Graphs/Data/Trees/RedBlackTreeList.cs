// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Sml.Common;
using Sml.Common.Visitors;
using Sml.DataStructures.Data;

namespace Sml.Graphs.Data.Trees
{
	[Serializable]
	internal class RedBlackTreeList<TKey, TValue> : RedBlackTree<TKey, LinkedList<TValue>>, IVisitable<TValue>
	{
		public void Accept(IVisitor<TValue> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			TraverseItems(delegate(TKey key, LinkedList<TValue> list)
			              	{
			              		foreach (TValue local in list)
			              		{
			              			if (visitor.HasCompleted)
			              				return true;
			              			visitor.Visit(local);
			              		}
			              		return false;
			              	});
		}

		public bool ContainsValue(TValue value)
		{
			return TraverseItems(delegate(TKey key, LinkedList<TValue> list)
			{
			   return list.Contains(value);
			});
		}

		public IEnumerator<SmlPair<TKey, TValue>> GetKeyEnumerator()
		{
			Stack<BinaryTree<SmlPair<TKey, LinkedList<TValue>>>> iteratorVariable0 =
				new Stack<BinaryTree<SmlPair<TKey, LinkedList<TValue>>>>();
			if (Tree != null)
			{
				iteratorVariable0.Push(Tree);
			}
			while (iteratorVariable0.Count > 0)
			{
				BinaryTree<SmlPair<TKey, LinkedList<TValue>>> iteratorVariable1 = iteratorVariable0.Pop();
				LinkedList<TValue> iteratorVariable2 = iteratorVariable1.Data.Second;
				foreach (TValue iteratorVariable3 in iteratorVariable2)
				{
					yield return new SmlPair<TKey, TValue>(iteratorVariable1.Data.First, iteratorVariable3);
				}
				if (iteratorVariable1.Left != null)
				{
					iteratorVariable0.Push(iteratorVariable1.Left);
				}
				if (iteratorVariable1.Right != null)
				{
					iteratorVariable0.Push(iteratorVariable1.Right);
				}
			}
		}

		public IEnumerator<TValue> GetValueEnumerator()
		{
			Stack<BinaryTree<SmlPair<TKey, LinkedList<TValue>>>> iteratorVariable0 =
				new Stack<BinaryTree<SmlPair<TKey, LinkedList<TValue>>>>();
			if (Tree != null)
			{
				iteratorVariable0.Push(Tree);
			}
			while (iteratorVariable0.Count > 0)
			{
				BinaryTree<SmlPair<TKey, LinkedList<TValue>>> iteratorVariable1 = iteratorVariable0.Pop();
				LinkedList<TValue> iteratorVariable2 = iteratorVariable1.Data.Second;
				foreach (TValue iteratorVariable3 in iteratorVariable2)
				{
					yield return iteratorVariable3;
				}
				if (iteratorVariable1.Left != null)
				{
					iteratorVariable0.Push(iteratorVariable1.Left);
				}
				if (iteratorVariable1.Right != null)
				{
					iteratorVariable0.Push(iteratorVariable1.Right);
				}
			}
		}

		public bool Remove(TValue value, out TKey key)
		{
			TKey foundKey = default(TKey);
			bool flag = TraverseItems(
				delegate(TKey itemKey, LinkedList<TValue> list)
					{
						if (!list.Remove(value))
							return false;
						if (list.Count == 0)
							Remove(itemKey);
						foundKey = itemKey;
						return true;
					});
			key = foundKey;
			return flag;
		}

		private bool TraverseItems(NodeAction shouldStop)
		{
			Stack<BinaryTree<SmlPair<TKey, LinkedList<TValue>>>> stack =
				new Stack<BinaryTree<SmlPair<TKey, LinkedList<TValue>>>>();
			if (Tree != null)
			{
				stack.Push(Tree);
			}
			while (stack.Count > 0)
			{
				BinaryTree<SmlPair<TKey, LinkedList<TValue>>> tree = stack.Pop();
				if (shouldStop(tree.Data.First, tree.Data.Second))
				{
					return true;
				}
				if (tree.Left != null)
				{
					stack.Push(tree.Left);
				}
				if (tree.Right != null)
				{
					stack.Push(tree.Right);
				}
			}
			return false;
		}

		private delegate bool NodeAction(TKey key, LinkedList<TValue> values);
	}
}

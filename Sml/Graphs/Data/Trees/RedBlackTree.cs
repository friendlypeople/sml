// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;

namespace Sml.Graphs.Data.Trees
{
	[Serializable]
	public class RedBlackTree<TKey, TValue> : BinarySearchTreeBase<TKey, TValue>
	{
		public RedBlackTree() { }

		public RedBlackTree(IComparer<TKey> comparer) : base(comparer) { }

		public RedBlackTree(Comparison<TKey> comparison) : base(comparison) { }

		protected override void AddItem(TKey key, TValue value)
		{
			if (Equals(key, null))
			{
				throw new ArgumentNullException("key");
			}
			RedBlackTreeNode<TKey, TValue> tree = (RedBlackTreeNode<TKey, TValue>) Tree;
			RedBlackTreeNode<TKey, TValue> node2 = InsertNode(tree, key, value);
			node2.Color = NodeColor.Black;
			Tree = node2;
		}

		private static RedBlackTreeNode<TKey, TValue> DoubleRotation(RedBlackTreeNode<TKey, TValue> node, bool direction)
		{
			node[!direction] = SingleRotation(node[!direction], !direction);
			return SingleRotation(node, direction);
		}

		private RedBlackTreeNode<TKey, TValue> InsertNode(RedBlackTreeNode<TKey, TValue> node, TKey key, TValue value)
		{
			if (node == null)
			{
				node = new RedBlackTreeNode<TKey, TValue>(key, value);
				return node;
			}
			if (Comparer.Compare(key, node.Key) == 0)
			{
				throw new ArgumentException("ItemAlreadyInTree");
			}
			bool flag = Comparer.Compare(node.Key, key) < 0;
			node[flag] = InsertNode(node[flag], key, value);
			if (IsRed(node[flag]))
			{
				if (IsRed(node[!flag]))
				{
					node.Color = NodeColor.Red;
					node.Left.Color = NodeColor.Black;
					node.Right.Color = NodeColor.Black;
					return node;
				}
				if (IsRed(node[flag][flag]))
				{
					node = SingleRotation(node, !flag);
					return node;
				}
				if (IsRed(node[flag][!flag]))
				{
					node = DoubleRotation(node, !flag);
				}
			}
			return node;
		}

		private static bool IsBlack(RedBlackTreeNode<TKey, TValue> node)
		{
			if (node != null)
			{
				return (node.Color == NodeColor.Black);
			}
			return true;
		}

		private static bool IsRed(RedBlackTreeNode<TKey, TValue> node)
		{
			return ((node != null) && (node.Color == NodeColor.Red));
		}

		protected override bool RemoveItem(TKey key)
		{
			if (Tree != null)
			{
				RedBlackTreeNode<TKey, TValue> node = new RedBlackTreeNode<TKey, TValue>(default(TKey), default(TValue));
				RedBlackTreeNode<TKey, TValue> node2 = node;
				node.Right = (RedBlackTreeNode<TKey, TValue>) Tree;
				RedBlackTreeNode<TKey, TValue> node3 = null;
				RedBlackTreeNode<TKey, TValue> node4 = null;
				bool direction = true;
				while (node2[direction] != null)
				{
					bool flag2 = direction;
					RedBlackTreeNode<TKey, TValue> node5 = node3;
					node3 = node2;
					node2 = node2[direction];
					int num = Comparer.Compare(node2.Key, key);
					if (num == 0)
					{
						node4 = node2;
					}
					direction = num < 0;
					if (IsBlack(node2) && IsBlack(node2[direction]))
					{
						if (IsRed(node2[!direction]))
						{
							node3 = node3[flag2] = SingleRotation(node2, direction);
						}
						else if (IsBlack(node2[direction]))
						{
							RedBlackTreeNode<TKey, TValue> node6 = node3[!flag2];
							if (node6 != null)
							{
								if (IsBlack(node6.Left) && IsBlack(node6.Right))
								{
									node3.Color = NodeColor.Black;
									node6.Color = NodeColor.Red;
									node2.Color = NodeColor.Red;
									continue;
								}
								bool flag3 = node5.Right == node3;
								if (IsRed(node6[flag2]))
								{
									node5[flag3] = DoubleRotation(node3, flag2);
								}
								else if (IsRed(node6[!flag2]))
								{
									node5[flag3] = SingleRotation(node3, flag2);
								}
								node2.Color = node5[flag3].Color = NodeColor.Red;
								node5[flag3].Left.Color = NodeColor.Black;
								node5[flag3].Right.Color = NodeColor.Black;
							}
						}
					}
				}
				if (node4 != null)
				{
					node4.Key = node2.Key;
					node4.Value = node2.Value;
					node3[node3.Right == node2] = node2[node2.Left == null];
				}
				Tree = node.Right;
				if (Tree != null)
				{
					((RedBlackTreeNode<TKey, TValue>) Tree).Color = NodeColor.Black;
				}
				if (node4 != null)
				{
					return true;
				}
			}
			return false;
		}

		private static RedBlackTreeNode<TKey, TValue> SingleRotation(RedBlackTreeNode<TKey, TValue> node, bool direction)
		{
			RedBlackTreeNode<TKey, TValue> node2 = node[!direction];
			node[!direction] = node2[direction];
			node2[direction] = node;
			node.Color = NodeColor.Red;
			node2.Color = NodeColor.Black;
			return node2;
		}
	}
}

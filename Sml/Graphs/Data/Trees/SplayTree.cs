// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Sml.DataStructures.Data;

namespace Sml.Graphs.Data.Trees
{
	[Serializable]
	public class SplayTree<TKey, TValue> : BinarySearchTreeBase<TKey, TValue>
	{
		public SplayTree() { }

		public SplayTree(IComparer<TKey> comparer) : base(comparer) { }

		public SplayTree(Comparison<TKey> comparison) : base(comparison) { }

		protected override void AddItem(TKey key, TValue value)
		{
			SmlPair<TKey, TValue> data = new SmlPair<TKey, TValue>(key, value);
			if (Tree == null)
			{
				Tree = new BinaryTree<SmlPair<TKey, TValue>>(data);
			}
			else
			{
				Splay(key);
				int num = Comparer.Compare(key, Tree.Data.First);
				if (num == 0)
				{
					throw new ArgumentException("Already in the tree.", "key");
				}
				BinaryTree<SmlPair<TKey, TValue>> tree = new BinaryTree<SmlPair<TKey, TValue>>(data);
				if (num < 0)
				{
					tree.Left = Tree.Left;
					tree.Right = Tree;
					Tree.Left = null;
				}
				else
				{
					tree.Right = Tree.Right;
					tree.Left = Tree;
					Tree.Right = null;
				}
				Tree = tree;
			}
		}

		protected override BinaryTree<SmlPair<TKey, TValue>> FindNode(TKey key)
		{
			BinaryTree<SmlPair<TKey, TValue>> tree = base.FindNode(key);
			if (tree != null)
			{
				Splay(key);
			}
			return tree;
		}

		protected override bool RemoveItem(TKey key)
		{
			if (Tree == null)
			{
				return false;
			}
			Splay(key);
			if (Comparer.Compare(key, Tree.Data.First) != 0)
			{
				return false;
			}
			if (Tree.Left == null)
			{
				Tree = Tree.Right;
			}
			else
			{
				BinaryTree<SmlPair<TKey, TValue>> right = Tree.Right;
				Tree = Tree.Left;
				Splay(key);
				Tree.Right = right;
			}
			return true;
		}

		private void Splay(TKey key)
		{
			BinaryTree<SmlPair<TKey, TValue>> tree = new BinaryTree<SmlPair<TKey, TValue>>(null, null, null, false);
			BinaryTree<SmlPair<TKey, TValue>> tree2 = tree;
			BinaryTree<SmlPair<TKey, TValue>> tree3 = tree;
			BinaryTree<SmlPair<TKey, TValue>> left = Tree;
			while (true)
			{
				if (PreSplay(key, ref tree3, ref left)) break;
				if ((Comparer.Compare(key, left.Data.First) > 0) && (left.Right != null))
				{
					if (Comparer.Compare(key, left.Right.Data.First) > 0)
					{
						BinaryTree<SmlPair<TKey, TValue>> right = left.Right;
						left.Right = right.Left;
						right.Left = left;
						left = right;
						if (left.Right == null)
							break;
					}
					tree2.Right = left;
					tree2 = left;
					left = left.Right;
				}
			}
			tree2.Right = left.Left;
			tree3.Left = left.Right;
			left.Left = tree.Right;
			left.Right = tree.Left;
			Tree = left;
		}

		private bool PreSplay(TKey key, ref BinaryTree<SmlPair<TKey, TValue>> tree3, ref BinaryTree<SmlPair<TKey, TValue>> left)
		{
			while (Comparer.Compare(key, left.Data.First) < 0)
			{
				if (left.Left == null)
					return true;

				if (Comparer.Compare(key, left.Left.Data.First) < 0)
				{
					BinaryTree<SmlPair<TKey, TValue>> right = left.Left;
					left.Left = right.Right;
					right.Right = left;
					left = right;
					if (left.Left == null)
						return true;
				}
				tree3.Left = left;
				tree3 = left;
				left = left.Left;
			}
			return false;
		}

		public override KeyValuePair<TKey, TValue> Maximum
		{
			get
			{
				KeyValuePair<TKey, TValue> maximum = base.Maximum;
				Splay(maximum.Key);
				return maximum;
			}
		}

		public override KeyValuePair<TKey, TValue> Minimum
		{
			get
			{
				KeyValuePair<TKey, TValue> minimum = base.Minimum;
				Splay(minimum.Key);
				return minimum;
			}
		}
	}
}

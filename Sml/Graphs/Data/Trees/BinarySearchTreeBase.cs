// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sml.Common;
using Sml.Common.Visitors;
using Sml.DataStructures.Algorithms.Comparers;
using Sml.DataStructures.Data;

namespace Sml.Graphs.Data.Trees
{
	[Serializable]
	public abstract class BinarySearchTreeBase<TKey, TValue> : ISearchTree<TKey, TValue>
	{
		internal delegate TKey KeyManipulator(TKey oldKey);

		private int _count;
		private readonly IComparer<TKey> _comparer;
        private BinaryTree<SmlPair<TKey, TValue>> _tree;

		protected BinarySearchTreeBase()
		{
			_comparer = Comparer<TKey>.Default;
		}

		protected BinarySearchTreeBase(IComparer<TKey> comparer)
		{
			Guard.ArgumentNotNull(comparer, "comparer");
			_comparer = comparer;
		}

		protected BinarySearchTreeBase(Comparison<TKey> comparison)
		{
			Guard.ArgumentNotNull(comparison, "comparison");
			_comparer = new ComparisonComparer<TKey>(comparison);
		}

		public void Add(KeyValuePair<TKey, TValue> item)
		{
			Add(item.Key, item.Value);
		}

		public void Add(TKey key, TValue value)
		{
			AddItem(key, value);
			Count++;
		}

		protected abstract void AddItem(TKey key, TValue value);

		public void Clear()
		{
			ClearItems();
		}

		protected virtual void ClearItems()
		{
			_tree = null;
			Count = 0;
		}

		public bool Contains(KeyValuePair<TKey, TValue> item)
		{
            BinaryTree<SmlPair<TKey, TValue>> tree = FindNode(item.Key);
			return ((tree != null) && item.Value.Equals(tree.Data.Second));
		}

		public bool ContainsKey(TKey key)
		{
			return (FindNode(key) != null);
		}

		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
			if ((array.Length - arrayIndex) < Count)
			{
				throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");
			}
            foreach (SmlPair<TKey, TValue> association in _tree)
			{
				array[arrayIndex++] = association.ToKeyValuePair();
			}
		}

		public void DepthFirstTraversal(OrderedVisitor<KeyValuePair<TKey, TValue>> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			VisitNode(_tree, visitor);
		}


        protected BinaryTree<SmlPair<TKey, TValue>> FindMaximumNode()
		{
			return FindMaximumNode(_tree);
		}

        protected static BinaryTree<SmlPair<TKey, TValue>> FindMaximumNode(BinaryTree<SmlPair<TKey, TValue>> startNode)
		{
            BinaryTree<SmlPair<TKey, TValue>> right = startNode;
			while (right.Right != null)
			{
				right = right.Right;
			}
			return right;
		}

        protected BinaryTree<SmlPair<TKey, TValue>> FindMinimumNode()
		{
			return FindMinimumNode(_tree);
		}

		protected static BinaryTree<SmlPair<TKey, TValue>> FindMinimumNode(BinaryTree<SmlPair<TKey, TValue>> startNode)
		{
			BinaryTree<SmlPair<TKey, TValue>> left = startNode;
			while (left.Left != null)
			{
				left = left.Left;
			}
			return left;
		}

		protected virtual BinaryTree<SmlPair<TKey, TValue>> FindNode(TKey key)
		{
			if (_tree != null)
			{
				int num;
				for (BinaryTree<SmlPair<TKey, TValue>> tree = _tree; tree != null; tree = (num < 0) ? tree.Left : tree.Right)
				{
					num = _comparer.Compare(key, tree.Data.First);
					if (num == 0)
					{
						return tree;
					}
				}
			}
			return null;
		}

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			if (_tree != null)
			{
				Stack<BinaryTree<SmlPair<TKey, TValue>>> stack = new Stack<BinaryTree<SmlPair<TKey, TValue>>>();
				stack.Push(_tree);


				while (stack.Count > 0)
				{
					BinaryTree<SmlPair<TKey, TValue>> iteratorVariable1 = stack.Pop();
					yield return iteratorVariable1.Data.ToKeyValuePair();
					if (iteratorVariable1.Left != null)
						stack.Push(iteratorVariable1.Left);
					if (iteratorVariable1.Right != null)
						stack.Push(iteratorVariable1.Right);
				}
			}
		}

		public IEnumerator<KeyValuePair<TKey, TValue>> GetSortedEnumerator()
		{
			if (_tree == null)
			{
				goto Label_00CA;
			}
			TrackingVisitor<SmlPair<TKey, TValue>> visitor = new TrackingVisitor<SmlPair<TKey, TValue>>();
			InOrderVisitor<SmlPair<TKey, TValue>> orderedVisitor = new InOrderVisitor<SmlPair<TKey, TValue>>(visitor);
			_tree.DepthFirstTraversal(orderedVisitor);
			IList<SmlPair<TKey, TValue>> trackingList = visitor.TrackingList;
			int iteratorVariable3 = 0;
			Label_PostSwitchInIterator:
			if (iteratorVariable3 < trackingList.Count)
			{
				yield return trackingList[iteratorVariable3].ToKeyValuePair();
				iteratorVariable3++;
				goto Label_PostSwitchInIterator;
			}
			Label_00CA:
			;
		}

		internal void ManipulateKeys(KeyManipulator manipulator)
		{
			if (_tree != null)
			{
				Stack<BinaryTree<SmlPair<TKey, TValue>>> stack = new Stack<BinaryTree<SmlPair<TKey, TValue>>>();
				stack.Push(_tree);
				while (stack.Count > 0)
				{
					BinaryTree<SmlPair<TKey, TValue>> tree = stack.Pop();
					tree.Data.First = manipulator(tree.Data.First);
					if (tree.Left != null)
						stack.Push(tree.Left);
					if (tree.Right != null)
						stack.Push(tree.Right);
				}
			}
		}

		public bool Remove(KeyValuePair<TKey, TValue> item)
		{
			return Remove(item.Key);
		}

		public bool Remove(TKey key)
		{
			bool flag = RemoveItem(key);
			if (flag)
				Count--;
			return flag;
		}

		protected abstract bool RemoveItem(TKey key);

		IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
		{
			return GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			BinaryTree<SmlPair<TKey, TValue>> tree = FindNode(key);
			if (tree == null)
			{
				value = default(TValue);
				return false;
			}
			value = tree.Data.Second;
			return true;
		}

		private static void VisitNode(BinaryTree<SmlPair<TKey, TValue>> node,
		                              OrderedVisitor<KeyValuePair<TKey, TValue>> visitor)
		{
			if (node != null)
			{
				KeyValuePair<TKey, TValue> pair = node.Data.ToKeyValuePair();
				visitor.VisitPreOrder(pair);
				VisitNode(node.Left, visitor);
				visitor.VisitInOrder(pair);
				VisitNode(node.Right, visitor);
				visitor.VisitPostOrder(pair);
			}
		}

		public IComparer<TKey> Comparer
		{
			get { return _comparer; }
		}

		public int Count
		{
			get { return _count; }
			private set { _count = value; }
		}

		public bool IsEmpty
		{
			get { return (_count == 0); }
		}

		public bool IsFixedSize
		{
			get { return false; }
		}

		public bool IsFull
		{
			get { return false; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public TValue this[TKey key]
		{
			get
			{
				BinaryTree<SmlPair<TKey, TValue>> tree = FindNode(key);
				if (tree == null)
					throw new KeyNotFoundException("key");
				return tree.Data.Second;
			}
			set
			{
				BinaryTree<SmlPair<TKey, TValue>> tree = FindNode(key);
				if (tree == null)
					throw new KeyNotFoundException("key");
				tree.Data.Second = value;
			}
		}

		public ICollection<TKey> Keys
		{
			get
			{
				KeyTrackingVisitor<TKey, TValue> visitor = new KeyTrackingVisitor<TKey, TValue>();
				InOrderVisitor<KeyValuePair<TKey, TValue>> visitor2 = new InOrderVisitor<KeyValuePair<TKey, TValue>>(visitor);
				DepthFirstTraversal(visitor2);
				return new ReadOnlyCollection<TKey>(visitor.TrackingList);
			}
		}

		public virtual KeyValuePair<TKey, TValue> Maximum
		{
			get { return MaximumAssociation.ToKeyValuePair(); }
		}

		internal SmlPair<TKey, TValue> MaximumAssociation
		{
			get
			{
				if (Count == 0)
					throw new InvalidOperationException("SearchTreeIsEmpty");
				return FindMaximumNode().Data;
			}
		}

		public virtual KeyValuePair<TKey, TValue> Minimum
		{
			get { return MinimumAssociation.ToKeyValuePair(); }
		}

		internal SmlPair<TKey, TValue> MinimumAssociation
		{
			get
			{
				if (Count == 0)
					throw new InvalidOperationException("SearchTreeIsEmpty");
				return FindMinimumNode().Data;
			}
		}

		protected BinaryTree<SmlPair<TKey, TValue>> Tree
		{
			get { return _tree; }
			set { _tree = value; }
		}

		public ICollection<TValue> Values
		{
			get
			{
				ValueTrackingVisitor<TKey, TValue> visitor = new ValueTrackingVisitor<TKey, TValue>();
				InOrderVisitor<KeyValuePair<TKey, TValue>> visitor2 = new InOrderVisitor<KeyValuePair<TKey, TValue>>(visitor);
				DepthFirstTraversal(visitor2);
				return new ReadOnlyCollection<TValue>(visitor.TrackingList);
			}
		}
	}
}

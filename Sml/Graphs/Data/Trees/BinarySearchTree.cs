// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Sml.DataStructures.Data;

namespace Sml.Graphs.Data.Trees
{
	[Serializable]
	public class BinarySearchTree<TKey, TValue> : BinarySearchTreeBase<TKey, TValue>
	{
		public BinarySearchTree() { }

		public BinarySearchTree(IComparer<TKey> comparer) : base(comparer) { }

		public BinarySearchTree(Comparison<TKey> comparison) : base(comparison) { }

		protected override void AddItem(TKey key, TValue value)
		{
			SmlPair<TKey, TValue> data = new SmlPair<TKey, TValue>(key, value);
			if (Tree == null)
			{
				Tree = new BinaryTree<SmlPair<TKey, TValue>>(data);
				return;
			}
			BinaryTree<SmlPair<TKey, TValue>> left = Tree;

			while (true)
			{
				int num = Comparer.Compare(data.First, left.Data.First);
				if (num == 0)
					throw new ArgumentException("ItemAlreadyInTree", "key");

				if (num < 0)
				{
					if (left.Left == null)
					{
						left.Left = new BinaryTree<SmlPair<TKey, TValue>>(data);
						return;
					}
					left = left.Left;
					continue;
				}
				if (left.Right == null)
				{
					left.Right = new BinaryTree<SmlPair<TKey, TValue>>(data);
					return;
				}
				left = left.Right;
			}
		}

		private static BinaryTree<SmlPair<TKey, TValue>> FindMaximumNode(BinaryTree<SmlPair<TKey, TValue>> startNode,
																			 out BinaryTree<SmlPair<TKey, TValue>> parent)
		{
			BinaryTree<SmlPair<TKey, TValue>> right = startNode;
			parent = null;
			while (right.Right != null)
			{
				parent = right;
				right = right.Right;
			}
			return right;
		}

		private BinaryTree<SmlPair<TKey, TValue>> FindNode(TKey key, out BinaryTree<SmlPair<TKey, TValue>> parent)
		{
			BinaryTree<SmlPair<TKey, TValue>> left = Tree;
			parent = null;
			while (left != null)
			{
				int num = Comparer.Compare(key, left.Data.First);
				if (num == 0)
				{
					return left;
				}
				if (num < 0)
				{
					parent = left;
					left = left.Left;
				}
				else
				{
					parent = left;
					left = left.Right;
				}
			}
			return null;
		}

		protected override bool RemoveItem(TKey key)
		{
			BinaryTree<SmlPair<TKey, TValue>> tree;
			BinaryTree<SmlPair<TKey, TValue>> tree2 = FindNode(key, out tree);
			if (tree2 == null)
			{
				return false;
			}
			if (tree2.Degree == 2)
			{
				BinaryTree<SmlPair<TKey, TValue>> tree3;
				BinaryTree<SmlPair<TKey, TValue>> tree4 = FindMaximumNode(tree2.Left, out tree3);
				tree = tree3 ?? tree2;
				tree2.Data = tree4.Data;
				tree2 = tree4;
			}
			BinaryTree<SmlPair<TKey, TValue>> tree5 = tree2.Left ?? tree2.Right;
			if (tree2 == Tree)
			{
				Tree = tree5;
			}
			else if (tree2 == tree.Left)
			{
				tree.Left = tree5;
			}
			else
			{
				tree.Right = tree5;
			}
			return true;
		}
	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sml.Common;
using Sml.Common.Visitors;
using Sml.DataStructures.Algorithms.Sorters;
using Sml.DataStructures.Data.Queues;

namespace Sml.Graphs.Data.Trees
{
	[Serializable]
	public class Tree<T> : ICollection<T>, IVisitable<T>, ITree<T>, ISortable<Tree<T>>
	{
		private readonly List<Tree<T>>	_childNodes;
		private T						_data;
		private Tree<T>					_parent;

		public Tree(T data)
		{
			_childNodes = new List<Tree<T>>();
			Guard.ArgumentNotNull(data, "data");
			_data = data;
		}
    
		public void Accept(IVisitor<T> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			foreach (T local in this)
			{
				if (visitor.HasCompleted)
				{
					break;
				}
				visitor.Visit(local);
			}
		}

		public void Add(Tree<T> child)
		{
			InsertItem(Count, child);
		}

		public void Add(T item)
		{
			Tree<T> tree = new Tree<T>(item);
			InsertItem(Count, tree);
		}

		public void BreadthFirstTraversal(IVisitor<T> visitor)
		{
			VisitableQueue<Tree<T>> queue = new VisitableQueue<Tree<T>>();
			queue.Enqueue(this);
			while (!queue.IsEmpty)
			{
				Tree<T> tree = queue.Dequeue();
				visitor.Visit(tree.Data);
				for (int i = 0; i < tree.Degree; i++)
				{
					Tree<T> child = tree.GetChild(i);
					if (child != null)
					{
						queue.Enqueue(child);
					}
				}
			}
		}

		public virtual void Clear()
		{
			_childNodes.Clear();
		}

		public bool Contains(T item)
		{
			foreach (T local in this)
			{
				if (item.Equals(local))
				{
					return true;
				}
			}
			return false;
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
			foreach (T local in this)
			{
				if (arrayIndex >= array.Length)
				{
					throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");
				}
				array[arrayIndex++] = local;
			}
		}

		public void DepthFirstTraversal(OrderedVisitor<T> orderedVisitor)
		{
			Guard.ArgumentNotNull(orderedVisitor, "orderedVisitor");
			if (!orderedVisitor.HasCompleted)
			{
				orderedVisitor.VisitPreOrder(Data);
				for (int i = 0; i < Degree; i++)
				{
					if (GetChild(i) != null)
					{
						GetChild(i).DepthFirstTraversal(orderedVisitor);
					}
				}
				orderedVisitor.VisitPostOrder(Data);
			}
		}

		private int FindMaximumChildHeight()
		{
			int num = 0;
			for (int i = 0; i < Degree; i++)
			{
				int height = GetChild(i).Height;
				if (height > num)
				{
					num = height;
				}
			}
			return num;
		}

		public Tree<T> FindNode(Predicate<T> condition)
		{
			Guard.ArgumentNotNull(condition, "condition");
			if (condition(Data))
				return this;

			for (int i = 0; i < Degree; i++)
			{
				Tree<T> tree = _childNodes[i].FindNode(condition);
				if (tree != null)
					return tree;
			}
			return null;
		}

		public Tree<T> GetChild(int index)
		{
			return _childNodes[index];
		}

		public IEnumerator<T> GetEnumerator()
		{
			Stack<Tree<T>> stack = new Stack<Tree<T>>();
			stack.Push(this);
   
			while (stack.Count > 0)
			{
				Tree<T> iteratorVariable1 = stack.Pop();
        	
				if (iteratorVariable1 == null) continue;
				yield return iteratorVariable1.Data;

				for (int j = 0; j < iteratorVariable1.Degree; j++)
					stack.Push(iteratorVariable1.GetChild(j));
			}
		}

		public IList<Tree<T>> GetPath()
		{
			return GetPath(x => x);
		}

		public IList<TOutput> GetPath<TOutput>(Converter<Tree<T>, TOutput> converter)
		{
			if (converter == null)
			{
				throw new ArgumentNullException("converter");
			}
			return GetPath(converter, false);
		}

		protected IList<TOutput> GetPath<TOutput>(Converter<Tree<T>, TOutput> converter, bool includeThis)
		{
			List<TOutput> list = new List<TOutput>();
			if (includeThis)
			{
				list.Add(converter(this));
			}
			for (Tree<T> tree = Parent; tree != null; tree = tree.Parent)
			{
				list.Add(converter(tree));
			}
			list.Reverse();
			return list;
		}

		protected virtual void InsertItem(int index, Tree<T> item)
		{
			if (item._parent != null)
				item._parent.Remove(item);

			if (_childNodes.Contains(item)) return;

			_childNodes.Add(item);
			item._parent = this;
		}

		void ITree<T>.Add(ITree<T> child)
		{
			Add((Tree<T>) child);
		}

		ITree<T> ITree<T>.FindNode(Predicate<T> condition)
		{
			return FindNode(condition);
		}

		ITree<T> ITree<T>.GetChild(int index)
		{
			return GetChild(index);
		}

		bool ITree<T>.Remove(ITree<T> child)
		{
			return Remove((Tree<T>) child);
		}

		void ISortable<Tree<T>>.Sort(ISorter<Tree<T>> sorter)
		{
			throw new NotSupportedException();
		}

		public bool Remove(Tree<T> child)
		{
			int index = _childNodes.IndexOf(child);
			if (index > -1)
			{
				RemoveItem(index, child);
				return true;
			}
			return false;
		}

		public bool Remove(T item)
		{
			return RemoveItem(item);
		}

		public void RemoveAt(int index)
		{
			RemoveItem(index, _childNodes[index]);
		}

		protected virtual bool RemoveItem(T item)
		{
			for (int i = 0; i < _childNodes.Count; i++)
			{
				if (_childNodes[i].Data.Equals(item))
				{
					_childNodes[i]._parent = null;
					_childNodes.RemoveAt(i);
					return true;
				}
			}
			return false;
		}

		protected virtual void RemoveItem(int index, Tree<T> item)
		{
			item._parent = null;
			_childNodes.RemoveAt(index);
		}

		public void Sort(IComparisonSorter<Tree<T>> sorter, IComparer<Tree<T>> comparer)
		{
			Guard.ArgumentNotNull(sorter, "sorter");
			Guard.ArgumentNotNull(comparer, "comparer");
			sorter.Sort(_childNodes, comparer);
		}

		public void Sort(IComparisonSorter<Tree<T>> sorter, Comparison<Tree<T>> comparison)
		{
			Guard.ArgumentNotNull(sorter, "sorter");
			Guard.ArgumentNotNull(comparison, "comparison");
			sorter.Sort(_childNodes, comparison);
		}

		public void Sort(ISorter<Tree<T>> sorter, SortOrder order)
		{
			Guard.ArgumentNotNull(sorter, "sorter");
			sorter.Sort(_childNodes, order);
		}

		public void SortAllDescendants(IComparisonSorter<Tree<T>> sorter, IComparer<Tree<T>> comparer)
		{
			Guard.ArgumentNotNull(sorter, "sorter");
			Guard.ArgumentNotNull(comparer, "comparer");
			sorter.Sort(_childNodes, comparer);
			for (int i = 0; i < _childNodes.Count; i++)
			{
				_childNodes[i].SortAllDescendants(sorter, comparer);
			}
		}

		public void SortAllDescendants(IComparisonSorter<Tree<T>> sorter, Comparison<Tree<T>> comparison)
		{
			Guard.ArgumentNotNull(sorter, "sorter");
			Guard.ArgumentNotNull(comparison, "comparison");
			sorter.Sort(_childNodes, comparison);
			for (int i = 0; i < _childNodes.Count; i++)
			{
				_childNodes[i].SortAllDescendants(sorter, comparison);
			}
		}

		public void SortAllDescendants(ISorter<Tree<T>> sorter, SortOrder order)
		{
			Guard.ArgumentNotNull(sorter, "sorter");
			sorter.Sort(_childNodes, order);
			for (int i = 0; i < _childNodes.Count; i++)
			{
				_childNodes[i].SortAllDescendants(sorter, order);
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public override string ToString()
		{
			return Data.ToString();
		}
		public IList<Tree<T>> Ancestors
		{
			get { return GetPath(x => x); }
		}

		public IList<Tree<T>> ChildNodes
		{
			get { return new ReadOnlyCollection<Tree<T>>(_childNodes); }
		}

		public int Count
		{
			get { return _childNodes.Count; }
		}

		public T Data
		{
			get { return _data; }
			set 
			{
				Guard.ArgumentNotNull(value, "value");
				_data = value;
			}
		}

		public int Degree
		{
			get { return _childNodes.Count; }
		}

		public int Height
		{
			get
			{
				if (Degree == 0)
					return 0;
				return (1 + FindMaximumChildHeight());
			}
		}

		public bool IsEmpty
		{
			get { return (Count == 0); }
		}

		public virtual bool IsLeafNode
		{
			get { return (Degree == 0); }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public Tree<T> this[int index]
		{
			get { return GetChild(index); }
		}

		ITree<T> ITree<T>.Parent
		{
			get { return _parent; }
		}

		public Tree<T> Parent
		{
			get { return _parent; }
			set
			{
				Guard.ArgumentNotNull(value, "value");
				value.Add(this);
			}
		}
	}
}
 


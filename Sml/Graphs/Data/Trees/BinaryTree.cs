// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using Sml.Common;
using Sml.Common.Visitors;
using Sml.DataStructures.Data.Queues;

namespace Sml.Graphs.Data.Trees
{
	[Serializable]
	public class BinaryTree<T> : ICollection<T>, IVisitable<T>, ITree<T>
	{
		private BinaryTree<T> _parent;
		private T _data;
		private BinaryTree<T> _leftSubtree;
		private BinaryTree<T> _rightSubtree;

		public BinaryTree(T data) : this(data, null, null) { }

		public BinaryTree(T data, T left, T right) : 
			this(data, new BinaryTree<T>(left), new BinaryTree<T>(right)) { }

		public BinaryTree(T data, BinaryTree<T> left, BinaryTree<T> right) :
			this(data, left, right, true) { }

		internal BinaryTree(T data, BinaryTree<T> left, BinaryTree<T> right, bool validateData)
		{
			if (validateData)
				Guard.ArgumentNotNull(data, "data");

			_leftSubtree = left;
			if (left != null)
				left.Parent = this;

			_rightSubtree = right;
			if (right != null)
				right.Parent = this;

			_data = data;
		}

		public void Add(BinaryTree<T> subtree)
		{
			Guard.ArgumentNotNull(subtree, "subtree");
			AddItem(subtree);
		}

		public void Add(T item)
		{
			AddItem(new BinaryTree<T>(item));
		}

		protected virtual void AddItem(BinaryTree<T> subtree)
		{
			if (_leftSubtree == null)
			{
				if (subtree.Parent != null)
					subtree.Parent.Remove(subtree);

				_leftSubtree = subtree;
				subtree.Parent = this;
			}
			else
			{
				if (_rightSubtree != null)
					throw new InvalidOperationException("This binary tree is full.");

				if (subtree.Parent != null)
					subtree.Parent.Remove(subtree);

				_rightSubtree = subtree;
				subtree.Parent = this;
			}
		}

		public virtual void BreadthFirstTraversal(IVisitor<T> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");

			VisitableQueue<BinaryTree<T>> queue = new VisitableQueue<BinaryTree<T>>();
			queue.Enqueue(this);
			while (!queue.IsEmpty)
			{
				if (visitor.HasCompleted)
					return;
				BinaryTree<T> tree = queue.Dequeue();
				visitor.Visit(tree.Data);
				for (int i = 0; i < tree.Degree; i++)
				{
					BinaryTree<T> child = tree.GetChild(i);
					if (child != null)
					{
						queue.Enqueue(child);
					}
				}
			}
		}

		public virtual void Clear()
		{
			if (_leftSubtree != null)
			{
				_leftSubtree.Parent = null;
				_leftSubtree = null;
			}
			if (_rightSubtree != null)
			{
				_rightSubtree.Parent = null;
				_rightSubtree = null;
			}
		}

		public bool Contains(T item)
		{
			foreach (T local in this)
			{
				if (item.Equals(local))
				{
					return true;
				}
			}
			return false;
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
			foreach (T local in this)
			{
				if (arrayIndex >= array.Length)
				{
					throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");
				}
				array[arrayIndex++] = local;
			}
		}

		public virtual void DepthFirstTraversal(OrderedVisitor<T> orderedVisitor)
		{
			Guard.ArgumentNotNull(orderedVisitor, "orderedVisitor");
			if (!orderedVisitor.HasCompleted)
			{
				orderedVisitor.VisitPreOrder(Data);
				if (_leftSubtree != null)
				{
					_leftSubtree.DepthFirstTraversal(orderedVisitor);
				}
				orderedVisitor.VisitInOrder(_data);
				if (_rightSubtree != null)
				{
					_rightSubtree.DepthFirstTraversal(orderedVisitor);
				}
				orderedVisitor.VisitPostOrder(Data);
			}
		}

		protected virtual int FindMaximumChildHeight()
		{
			int height = 0;
			int num2 = 0;
			if (_leftSubtree != null)
			{
				height = _leftSubtree.Height;
			}
			if (_rightSubtree != null)
			{
				num2 = _rightSubtree.Height;
			}
			if (height > num2)
			{
				return height;
			}
			return num2;
		}

		public BinaryTree<T> FindNode(Predicate<T> condition)
		{
			Guard.ArgumentNotNull(condition, "condition");
			if (condition(Data))
			{
				return this;
			}
			if (_leftSubtree != null)
			{
				BinaryTree<T> tree = _leftSubtree.FindNode(condition);
				if (tree != null)
				{
					return tree;
				}
			}
			if (_rightSubtree != null)
			{
				BinaryTree<T> tree2 = _rightSubtree.FindNode(condition);
				if (tree2 != null)
				{
					return tree2;
				}
			}
			return null;
		}

		public BinaryTree<T> GetChild(int index)
		{
			switch (index)
			{
				case 0:
					return _leftSubtree;

				case 1:
					return _rightSubtree;
			}
			throw new ArgumentOutOfRangeException("index");
		}

		public IEnumerator<T> GetEnumerator()
		{
			Stack<BinaryTree<T>> iteratorVariable0 = new Stack<BinaryTree<T>>();
			iteratorVariable0.Push(this);
			while (true)
			{
				if (iteratorVariable0.Count <= 0)
				{
					yield break;
				}
				BinaryTree<T> iteratorVariable1 = iteratorVariable0.Pop();
				yield return iteratorVariable1.Data;
				if (iteratorVariable1._leftSubtree != null)
				{
					iteratorVariable0.Push(iteratorVariable1._leftSubtree);
				}
				if (iteratorVariable1._rightSubtree != null)
				{
					iteratorVariable0.Push(iteratorVariable1._rightSubtree);
				}
			}
		}

		void ITree<T>.Add(ITree<T> child)
		{
			AddItem((BinaryTree<T>) child);
		}

		ITree<T> ITree<T>.FindNode(Predicate<T> condition)
		{
			return FindNode(condition);
		}

		ITree<T> ITree<T>.GetChild(int index)
		{
			return GetChild(index);
		}

		bool ITree<T>.Remove(ITree<T> child)
		{
			return Remove((BinaryTree<T>) child);
		}

		public bool Remove(BinaryTree<T> child)
		{
			if ((_leftSubtree != null) && (_leftSubtree == child))
			{
				RemoveLeft();
				return true;
			}
			if ((_rightSubtree != null) && (_rightSubtree == child))
			{
				RemoveRight();
				return true;
			}
			return false;
		}

		public bool Remove(T item)
		{
			if ((_leftSubtree != null) && _leftSubtree._data.Equals(item))
			{
				RemoveLeft();
				return true;
			}
			if ((_rightSubtree != null) && _rightSubtree._data.Equals(item))
			{
				RemoveRight();
				return true;
			}
			return false;
		}

		public virtual void RemoveLeft()
		{
			if (_leftSubtree != null)
			{
				_leftSubtree.Parent = null;
				_leftSubtree = null;
			}
		}

		public virtual void RemoveRight()
		{
			if (_rightSubtree != null)
			{
				_rightSubtree.Parent = null;
				_rightSubtree = null;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void Accept(IVisitor<T> visitor)
		{
			
		}

		public override string ToString()
		{
			return _data.ToString();
		}

		public int Count
		{
			get
			{
				int num = 0;
				if (_leftSubtree != null) num++;
				if (_rightSubtree != null) num++;
				return num;
			}
		}

		public virtual T Data
		{
			get { return _data; }
			set
			{
				Guard.ArgumentNotNull(value, "data");
				_data = value;
			}
		}

		public int Degree
		{
			get { return Count; }
		}

		public virtual int Height
		{
			get { return Degree == 0 ? 0 : 1 + FindMaximumChildHeight(); }
		}

		public bool IsEmpty
		{
			get { return (Count == 0); }
		}

		public bool IsFull
		{
			get { return ((_leftSubtree != null) && (_rightSubtree != null)); }
		}

		public virtual bool IsLeafNode
		{
			get { return (Degree == 0); }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public BinaryTree<T> this[int index]
		{
			get { return GetChild(index); }
		}

		public virtual BinaryTree<T> Left
		{
			get { return _leftSubtree; }
			set
			{
				if (_leftSubtree != null)
					RemoveLeft();
				if (value != null)
				{
					if (value.Parent != null)
						value.Parent.Remove(value);

					value.Parent = this;
				}
				_leftSubtree = value;
			}
		}

		ITree<T> ITree<T>.Parent
		{
			get { return Parent; }
		}

		public BinaryTree<T> Parent
		{
			get { return _parent; }
			private set { _parent = value; }
		}

		public virtual BinaryTree<T> Right
		{
			get { return _rightSubtree; }
			set
			{
				if (_rightSubtree != null)
					RemoveRight();

				if (value != null)
				{
					if (value.Parent != null)
						value.Parent.Remove(value);
					value.Parent = this;
				}
				_rightSubtree = value;
			}
		}
	}
}

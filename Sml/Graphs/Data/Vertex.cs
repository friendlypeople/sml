// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sml.Geometry.Data;

namespace Sml.Graphs.Data
{
	public class Vertex<T> : SmlTaggedObject
	{
		private T _data;
		private double _weight;
		
		private readonly List<Edge<T>> _outpuEdges =  new List<Edge<T>>();
		private readonly List<Edge<T>> _inputEdges = new List<Edge<T>>();

		public Vertex(T data)
		{
			_data = data;
		}

		public Vertex(T data, double weight)
		{
			_data = data;
			_weight = weight;
		}

		internal void AddEdge(Edge<T> edge)
		{
			if (edge.IsDirected)
			{
				if (edge.From == this)
					_outpuEdges.Add(edge);
			}
			else
				_outpuEdges.Add(edge);
			_inputEdges.Add(edge);
		}

		public Edge<T> GetOutputEdgeTo(Vertex<T> toVertex)
		{
			foreach (Edge<T> outpuEdge in _outpuEdges)
			{
				if (outpuEdge.IsDirected)
				{
					if (outpuEdge.To == toVertex)
						return outpuEdge;
				}
				else if ((outpuEdge.From == toVertex) || (outpuEdge.To == toVertex))
					return outpuEdge;
			}
			return null;
		}

        // Этот метод не используется.
		public Edge<T> GetInputEdgeFrom(Vertex<T> toVertex)
		{
			foreach (Edge<T> inputEdge in _inputEdges)
			{
				if ((inputEdge.To == toVertex) || (inputEdge.From == toVertex))
					return inputEdge;
			}
			return null;
		}

		public bool HasOutputEdgeTo(Vertex<T> toVertex)
		{
			return GetOutputEdgeTo(toVertex) != null;
		}

		public bool HasInputEdgeFrom(Vertex<T> fromVertex)
		{
			foreach (Edge<T> inputEdge in _inputEdges)
			{
				if ((inputEdge.From == fromVertex) || (inputEdge.To == fromVertex))
					return true;
			}
			return false;
		}

 


		internal void RemoveEdge(Edge<T> edge)
		{
			RemoveEdgeFromVertex(edge);
		}

		private void RemoveEdgeFromVertex(Edge<T> edge)
		{
			_inputEdges.Remove(edge);
			if (edge.IsDirected)
			{
				if (edge.From == this)
					_outpuEdges.Remove(edge);
			}
			else
				_outpuEdges.Remove(edge);
		}

		public T Data
		{
			get { return _data; }
			set { _data = value; }
		}

		public int Degree
		{
			get { return _outpuEdges.Count; }
		}

		public IList<Edge<T>> OutputEdges
		{
			get { return new ReadOnlyCollection<Edge<T>>(_outpuEdges); }
		}

		public IList<Edge<T>> InputEdges
		{
			get { return new ReadOnlyCollection<Edge<T>>(_inputEdges); }
		}

		public int InputEdgeCount
		{
			get { return (_inputEdges.Count - _outpuEdges.Count); }
		}

		public double Weight
		{
			get { return _weight; }
			set { _weight = value; }
		}
	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using Sml.Common;
using Sml.Common.Visitors;
using Sml.DataStructures.Data.Queues;

namespace Sml.Graphs.Data
{
	public class Graph<T> : ICollection<T>, IVisitable<T>
	{
		private readonly Dictionary<Edge<T>, object>	_edges		= new Dictionary<Edge<T>, object>();
		private readonly Dictionary<Vertex<T>, object>	_vertices	= new Dictionary<Vertex<T>, object>();
		private readonly bool _isDirected;

		public Graph() { }
        
		public Graph(bool isDirected)
		{
			_isDirected = isDirected;
		}

	    public void AddEdge(Edge<T> edge)
		{
			CheckEdgeNotNull(edge);
			if (edge.IsDirected != _isDirected)
				throw new ArgumentException("MismatchedEdgeType", "edge");

			if (!_vertices.ContainsKey(edge.From) || !_vertices.ContainsKey(edge.To))
				throw new ArgumentException("VertexCouldNotBeFound", "edge");

			if (edge.From.HasOutputEdgeTo(edge.To))
				throw new ArgumentException("EdgeAlreadyExists", "edge");

			_edges.Add(edge, null);
			AddEdgeToVertices(edge);
		}

		public Edge<T> AddEdge(Vertex<T> from, Vertex<T> to)
		{
			return AddEdge(from, to, 0);
		}

		public Edge<T> AddEdge(Vertex<T> from, Vertex<T> to, double weight)
		{
			Edge<T> edge = new Edge<T>(from, to, weight, _isDirected);
			AddEdge(edge);
			return edge;
		}

		private static void CheckEdgeNotNull(Edge<T> edge)
		{
			Guard.ArgumentNotNull(edge, "edge");
		}

		private static void AddEdgeToVertices(Edge<T> edge)
		{
			edge.From.AddEdge(edge);
			edge.To.AddEdge(edge);
		}

		public Vertex<T> AddVertex(T item)
		{
			Vertex<T> key = new Vertex<T>(item);
			_vertices.Add(key, null);
			return key;
		}

		public void AddVertex(Vertex<T> vertex)
		{
			Guard.ArgumentNotNull(vertex, "vertex");
			if (_vertices.ContainsKey(vertex))
				throw new ArgumentException("VertexAlreadyExists", "vertex");

			_vertices.Add(vertex, null);
		}

		/// <summary>
		/// Обход вершин дерева в ширину.
		/// </summary>
		/// <param name="visitor">Посититель</param>
		/// <param name="startVertex">Начальный узел</param>
		/// <remarks>
		/// Метод просмотра вершин дерева, при котором каждый уровень полностью
		/// подвергается анализу до перехода к следующему уровню)
		/// </remarks>
		public void BreadthFirstTraversal(IVisitor<Vertex<T>> visitor, Vertex<T> startVertex)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			Guard.ArgumentNotNull(startVertex, "startVertex");

			List<Vertex<T>> list = new List<Vertex<T>>(_vertices.Count);
			VisitableQueue<Vertex<T>> queue = new VisitableQueue<Vertex<T>>();
			queue.Enqueue(startVertex);
			list.Add(startVertex);
			while (!queue.IsEmpty && !visitor.HasCompleted)
			{
				Vertex<T> vertex = queue.Dequeue();
				visitor.Visit(vertex);
				IList<Edge<T>> emanatingEdges = vertex.OutputEdges;
				for (int i = 0; i < emanatingEdges.Count; i++)
				{
					Vertex<T> partnerVertex = emanatingEdges[i].GetPartnerVertex(vertex);
					if (!list.Contains(partnerVertex))
					{
						queue.Enqueue(partnerVertex);
						list.Add(partnerVertex);
					}
				}
			}
		}

		public bool ContainsEdge(Edge<T> edge)
		{
			return _edges.ContainsKey(edge);
		}

		public bool ContainsEdge(T fromValue, T toValue)
		{
			if (_isDirected)
			{
				foreach (Edge<T> edge in _edges.Keys)
				{
					if (edge.From.Data.Equals(fromValue) && edge.To.Data.Equals(toValue))
						return true;
				}
			}
			else
			{
				foreach (Edge<T> edge2 in _edges.Keys)
				{
					if ((edge2.From.Data.Equals(fromValue) && edge2.To.Data.Equals(toValue)) ||
					    (edge2.From.Data.Equals(toValue) && edge2.To.Data.Equals(fromValue)))
						return true;
				}
			}
			return false;
		}

		public bool ContainsEdge(Vertex<T> from, Vertex<T> to)
		{
			if (_isDirected)
				return from.HasOutputEdgeTo(to);

			return from.HasInputEdgeFrom(to);
		}

		public bool ContainsVertex(T item)
		{
			foreach (Vertex<T> vertex in _vertices.Keys)
			{
				if (vertex.Data.Equals(item))
					return true;
			}
			return false;
		}

		public bool ContainsVertex(Vertex<T> vertex)
		{
			return _vertices.ContainsKey(vertex);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Guard.ArgumentNotNull(array, "array");
			if ((array.Length - arrayIndex) < _vertices.Count)
				throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");

			int index = arrayIndex;
			foreach (Vertex<T> vertex in _vertices.Keys)
			{
				array.SetValue(vertex.Data, index);
				index++;
			}
		}

		/// <summary>
		/// Обход в глубину
		/// </summary>
		/// <param name="visitor">Посититель</param>
		/// <param name="startVertex">Стартовый индекс.</param>
		public void DepthFirstTraversal(OrderedVisitor<Vertex<T>> visitor, Vertex<T> startVertex)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			Guard.ArgumentNotNull(startVertex, "startVertex");
			List<Vertex<T>> visitedVertices = new List<Vertex<T>>(_vertices.Count);
			DepthFirstTraversal(visitor, startVertex, ref visitedVertices);
		}

		private static void DepthFirstTraversal(OrderedVisitor<Vertex<T>> visitor, Vertex<T> startVertex, ref List<Vertex<T>> visitedVertices)
		{
			if (!visitor.HasCompleted)
			{
				visitedVertices.Add(startVertex);
				visitor.VisitPreOrder(startVertex);
				IList<Edge<T>> emanatingEdges = startVertex.OutputEdges;
				for (int i = 0; i < emanatingEdges.Count; i++)
				{
					Vertex<T> partnerVertex = emanatingEdges[i].GetPartnerVertex(startVertex);
					if (!visitedVertices.Contains(partnerVertex))
					{
						DepthFirstTraversal(visitor, partnerVertex, ref visitedVertices);
					}
				}
				visitor.VisitPostOrder(startVertex);
			}
		}

		public IList<Vertex<T>> FindVertices(Predicate<T> predicate)
		{
			Guard.ArgumentNotNull(predicate, "predicate");
			List<Vertex<T>> list = new List<Vertex<T>>();
			foreach (Vertex<T> vertex in _vertices.Keys)
			{
				if (predicate(vertex.Data))
					list.Add(vertex);
			}
			return list;
		}

		private Vertex<T> GetAnyVertex()
		{
			using (Dictionary<Vertex<T>, object>.KeyCollection.Enumerator enumerator = _vertices.Keys.GetEnumerator())
			{
				while (enumerator.MoveNext())
					return enumerator.Current;
			}
			return null;
		}

		public Edge<T> GetEdge(Vertex<T> from, Vertex<T> to)
		{
			return from.GetOutputEdgeTo(to);
		}

		public Edge<T> GetEdge(T fromVertexValue, T toVertexValue)
		{
			foreach (Vertex<T> vertex in _vertices.Keys)
			{
				if (vertex.Data.Equals(fromVertexValue))
				{
					for (int i = 0; i < vertex.OutputEdges.Count; i++)
					{
						Edge<T> edge = vertex.OutputEdges[i];
						if (edge.GetPartnerVertex(vertex).Data.Equals(toVertexValue))
							return edge;
					}
				}
			}
			return null;
		}

		public IEnumerator<T> GetEnumerator()
		{
			List<Vertex<T>> iteratorVariable0 = new List<Vertex<T>>(_vertices.Count);
			iteratorVariable0.AddRange(_vertices.Keys);
			int iteratorVariable1 = 0;
			while (true)
			{
				if (iteratorVariable1 >= iteratorVariable0.Count)
				{
					yield break;
				}
				Vertex<T> iteratorVariable2 = iteratorVariable0[iteratorVariable1];
				yield return iteratorVariable2.Data;
				iteratorVariable1++;
			}
		}

		public Vertex<T> GetVertex(T vertexValue)
		{
			foreach (Vertex<T> vertex in _vertices.Keys)
			{
				if (vertex.Data.Equals(vertexValue))
					return vertex;
			}
			return null;
		}

		public bool IsCyclic()
		{
			DummyVisitor<Vertex<T>> visitor = new DummyVisitor<Vertex<T>>();
			return (TopologicalSortTraversalInternal(visitor) < _vertices.Count);
		}

		public bool IsStronglyConnected()
		{
			if (_isDirected)
				throw new InvalidOperationException("UndirectedGraphStrongConnectedness");

			if (_vertices.Count == 0)
				throw new InvalidOperationException("GraphIsEmpty");

			CountingVisitor<Vertex<T>> visitor = new CountingVisitor<Vertex<T>>();
			foreach (Vertex<T> vertex in _vertices.Keys)
			{
				BreadthFirstTraversal(visitor, vertex);
				if (visitor.Count != _vertices.Count)
				{
					return false;
				}
				visitor.ResetCount();
			}
			return true;
		}

		public bool IsWeaklyConnected()
		{
			if (_vertices.Count == 0)
			{
				throw new InvalidOperationException("GraphIsEmpty");
			}
			CountingVisitor<Vertex<T>> visitor = new CountingVisitor<Vertex<T>>();
			BreadthFirstTraversal(visitor, GetAnyVertex());
			return (visitor.Count == _vertices.Count);
		}

		public bool RemoveEdge(Edge<T> edge)
		{
			CheckEdgeNotNull(edge);
			if (!_edges.Remove(edge))
			{
				return false;
			}
			edge.From.RemoveEdge(edge);
			edge.To.RemoveEdge(edge);
			return true;
		}

		public bool RemoveEdge(Vertex<T> from, Vertex<T> to)
		{
			Guard.ArgumentNotNull(from, "from");
			Guard.ArgumentNotNull(to, "to");

			if (_isDirected)
			{
				foreach (Edge<T> edge in _edges.Keys)
				{
					if ((edge.From == from) && (edge.To == to))
					{
						RemoveEdge(edge);
						return true;
					}
				}
			}
			else
			{
				foreach (Edge<T> edge in _edges.Keys)
				{
					if (((edge.From != from) || (edge.To != to)) && ((edge.From != to) || (edge.To != from)))
						continue;
					RemoveEdge(edge);
					return true;
				}
			}
			return false;
		}

		public bool RemoveVertex(Vertex<T> vertex)
		{
			if (!_vertices.Remove(vertex))
			{
				return false;
			}
			IList<Edge<T>> incidentEdges = vertex.InputEdges;
			while (incidentEdges.Count > 0)
			{
				RemoveEdge(incidentEdges[0]);
			}
			return true;
		}

		public bool RemoveVertex(T item)
		{
			foreach (Vertex<T> vertex in _vertices.Keys)
			{
				if (vertex.Data.Equals(item))
				{
					RemoveVertex(vertex);
					return true;
				}
			}
			return false;
		}

		public IList<Vertex<T>> TopologicalSort()
		{
			TrackingVisitor<Vertex<T>> visitor = new TrackingVisitor<Vertex<T>>();
			TopologicalSortTraversal(visitor);
			return visitor.TrackingList;
		}

		public void TopologicalSortTraversal(IVisitor<Vertex<T>> visitor)
		{
			if (TopologicalSortTraversalInternal(visitor) < _vertices.Count)
			{
				throw new InvalidOperationException("GraphHasCycles");
			}
		}

		private int TopologicalSortTraversalInternal(IVisitor<Vertex<T>> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			if (!_isDirected)
				throw new ArgumentException("OperationOnlyValidForDirectedGraph");

			int num = 0;
			if (!IsEmpty)
			{
				Dictionary<Vertex<T>, int> dictionary = new Dictionary<Vertex<T>, int>(_vertices.Count);
				Queue<Vertex<T>> queue = new Queue<Vertex<T>>();
				foreach (Vertex<T> vertex in Vertices)
				{
					int incomingEdgeCount = vertex.InputEdgeCount;
					dictionary.Add(vertex, incomingEdgeCount);
					if (incomingEdgeCount == 0)
						queue.Enqueue(vertex);
				}
				if (queue.Count <= 0)
					return num;

				while ((queue.Count > 0) && !visitor.HasCompleted)
				{
					Vertex<T> key = queue.Dequeue();
					dictionary.Remove(key);
					visitor.Visit(key);
					num++;
					foreach (Edge<T> edge in key.OutputEdges)
					{
						Dictionary<Vertex<T>, int> dictionary2 = dictionary;
						Vertex<T> toVertex = edge.To;
						Vertex<T> vertex = toVertex;
						(dictionary2)[vertex] = dictionary2[vertex] - 1;
						if (dictionary[toVertex] == 0)
							queue.Enqueue(toVertex);
					}
				}
			}
			return num;
		}

		//public ObjectMatrix<int> GetReachabilityMatrix()
		//{

		//}

		public ICollection<Edge<T>> Edges
		{
			get { return _edges.Keys; }
		}

		public bool IsDirected
		{
			get { return _isDirected; }
		}


		public ICollection<Vertex<T>> Vertices
		{
			get { return _vertices.Keys; }
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#region Implementation of ICollection<T>

		void ICollection<T>.Add(T item)
		{
			AddVertex(new Vertex<T>(item));
		}

		public void Clear()
		{
			_vertices.Clear();
			_edges.Clear();
		}

		bool ICollection<T>.Contains(T item)
		{
			return ContainsVertex(item);
		}

		bool ICollection<T>.Remove(T item)
		{
			return RemoveVertex(item);
		}

		public int Count
		{
			get { return _vertices.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		#endregion

		public void Accept(IVisitor<T> visitor)
		{
			Guard.ArgumentNotNull(visitor, "visitor");
			foreach (T local in this)
			{
				if (visitor.HasCompleted)
					break;
				visitor.Visit(local);
			}
		}

		public bool IsEmpty
		{
			get { return (_vertices.Count == 0); }

		}
	}
}

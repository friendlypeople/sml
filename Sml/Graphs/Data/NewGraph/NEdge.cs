// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Diagnostics;

namespace Sml.Graphs.Data.NewGraph
{
	[DebuggerDisplay("{_from}-->{_to} Weight = {_weight}")]
	public class NEdge<TVertex> : INEdge<TVertex>, ICloneable
	{
		private readonly TVertex	_from;
		private readonly TVertex	_to;
		private double				_weight;

		public NEdge(TVertex from, TVertex to, double weight)
		{
			_from = from;
			_to = to;
			_weight = weight;
		}

		public NEdge(TVertex from, TVertex to) : this(from, to, 0) { }


		public TVertex From
		{
			get { return _from; }
		}

		public TVertex To
		{
			get { return _to; }
		}

		public double Weight
		{
			get { return _weight; }
			set { _weight = value; }
		}

		public virtual INEdge<TVertex> Clone()
		{
			NEdge<TVertex> newEdge = new NEdge<TVertex>(From, To, Weight);
			return newEdge;
		}

		public TVertex GetPartnerVertex(TVertex vertex)
		{
			if (vertex.Equals(_from))
				return _to;
			if (vertex.Equals(_to))
				return _from;
			throw new ArgumentException("VertexNotPartOfEdge", "vertex");
		}

		object ICloneable.Clone()
		{
			return Clone();
		}
	}

	public class NTaggedEdge<TVertex, TTag> : NEdge<TVertex>
	{
		private TTag _tag;

        public NTaggedEdge(TVertex from, TVertex to, double weight, TTag tag) : base(from, to, weight)
        {
            _tag = tag;
        }

		public NTaggedEdge(TVertex from, TVertex to, double weight) : base(from, to, weight) { }

		public NTaggedEdge(TVertex from, TVertex to) : base(from, to) { }

		public TTag Tag
		{
			get { return _tag; }
			set { _tag = value; }
		}

		public override INEdge<TVertex> Clone()
		{
			NTaggedEdge<TVertex, TTag> newEdge = new NTaggedEdge<TVertex, TTag>(From, To, Weight);
			newEdge.Tag = Tag;
			return newEdge;
		}
	}
}

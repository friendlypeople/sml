// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;
using Sml.Common;
using Sml.DataStructures.Algorithms.Comparers;
using Sml.DataStructures.Data;
using Sml.DataStructures.Data.Collections;

namespace Sml.Graphs.Data.NewGraph
{
	public class NGraphAlgorithms
	{
		private class VertexWrapper<TVertex>
		{
			public readonly TVertex Vertex;

			public VertexWrapper(TVertex vertex)
			{
				Vertex = vertex;
			}
		}

		public static NGraph<TVertex, TEdge> DijkstrasAlgorithm<TVertex, TEdge>(NGraph<TVertex, TEdge> weightedGraph, TVertex fromVertex) where TEdge : NEdge<TVertex>
		{
			Guard.ArgumentNotNull(weightedGraph, "weightedGraph");
			Guard.ArgumentNotNull(fromVertex, "fromVertex");

			if (!weightedGraph.ContainsVertex(fromVertex))
				throw new ArgumentException("VertexCouldNotBeFound", "fromVertex");

			NGraph<TVertex, TEdge> dijkstrasGraph = new NGraph<TVertex, TEdge>(weightedGraph.IsDirected);

			Heap<SmlPair<double, TVertex>> heap
				= new Heap<SmlPair<double, TVertex>>(HeapType.Minimum, new PairKeyComparer<double, TVertex>());

			Dictionary<TVertex, NVertexInfo<TEdge>> vertexStatus = new Dictionary<TVertex, NVertexInfo<TEdge>>();
			
			foreach (TVertex vertex in weightedGraph.Vertices)
				vertexStatus.Add(vertex, new NVertexInfo<TEdge>(double.MaxValue, null, false));

			vertexStatus[fromVertex].Distance = 0.0;
			heap.Add(new SmlPair<double, TVertex>(0.0, fromVertex));

			dijkstrasGraph.AddVertex(fromVertex);

			while (heap.Count > 0)
			{
				SmlPair<double, TVertex> pair = heap.RemoveRoot();
				NVertexInfo<TEdge> vertexInfo = vertexStatus[pair.Second];
				if (!vertexInfo.IsFinalised)
				{
					IEnumerable<TEdge> emanatingEdges = weightedGraph.OutEdges(pair.Second);
					vertexStatus[pair.Second].IsFinalised = true;

					if (vertexInfo.EdgeFollowed != null)
						dijkstrasGraph.AddVerticesAndEdge((TEdge)vertexInfo.EdgeFollowed.Clone());

					foreach (TEdge edge in emanatingEdges)
					{

						TVertex partnerVertex = edge.GetPartnerVertex(pair.Second);
						double key = vertexInfo.Distance + edge.Weight;
						NVertexInfo<TEdge> info = vertexStatus[partnerVertex];
						if (key < info.Distance)
						{
							info.EdgeFollowed = edge;
							info.Distance = key;
							heap.Add(new SmlPair<double, TVertex>(key, partnerVertex));
						}
					}
				}
			}
			return dijkstrasGraph;
		}

		public static NGraph<TVertex, TEdge> PrimsAlgorithm<TVertex, TEdge>(NGraph<TVertex, TEdge> weightedGraph, TVertex fromVertex) where TEdge : NEdge<TVertex>
		{
			Guard.ArgumentNotNull(weightedGraph, "weightedGraph");
			Guard.ArgumentNotNull(fromVertex, "fromVertex");

			if (!weightedGraph.ContainsVertex(fromVertex))
				throw new ArgumentException("VertexCouldNotBeFound", "fromVertex");

			NGraph<TVertex, TEdge> primsGraph = new NGraph<TVertex, TEdge>(weightedGraph.IsDirected);

			Heap<SmlPair<double, TVertex>> heap = 
				new Heap<SmlPair<double, TVertex>>(HeapType.Minimum, new PairKeyComparer<double, TVertex>());

			Dictionary<TVertex, NVertexInfo<TEdge>> vertexStatus = new Dictionary<TVertex, NVertexInfo<TEdge>>();

			foreach (TVertex vertex in weightedGraph.Vertices)
				vertexStatus.Add(vertex, new NVertexInfo<TEdge>(double.MaxValue, null, false));

			vertexStatus[fromVertex].Distance = 0.0;
			heap.Add(new SmlPair<double, TVertex>(0.0, fromVertex));
			primsGraph.AddVertex(fromVertex);

			while (heap.Count > 0)
			{
				SmlPair<double, TVertex> association = heap.RemoveRoot();
				vertexStatus[association.Second].IsFinalised = true;

				NVertexInfo<TEdge> vertexInfo = vertexStatus[association.Second];
				if (vertexInfo.EdgeFollowed != null)
					primsGraph.AddVerticesAndEdge((TEdge)vertexInfo.EdgeFollowed.Clone());

				//IEnumerable<TEdge> incidentEdges = weightedGraph.InEdges(association.Second);
                IEnumerable<TEdge> incidentEdges = weightedGraph.IncidentEdges(association.Second);
				foreach (TEdge edge in incidentEdges)
				{
					TVertex partnerVertex = edge.GetPartnerVertex(association.Second);
					NVertexInfo<TEdge> info = vertexStatus[partnerVertex];
					if (!info.IsFinalised && (edge.Weight < info.Distance))
					{
						info.EdgeFollowed = edge;
						info.Distance = edge.Weight;
						heap.Add(new SmlPair<double, TVertex>(edge.Weight, partnerVertex));
					}
				}
			}

			return primsGraph;
		}

		public static NGraph<TVertex, TEdge> KruskalsAlgorithm<TVertex, TEdge>(NGraph<TVertex, TEdge> weightedGraph) where TEdge : NEdge<TVertex>
		{
			Guard.ArgumentNotNull(weightedGraph, "weightedGraph");
			int num = weightedGraph.VertexCount - 1;

			Dictionary<TVertex, VertexWrapper<TVertex>> dic = new Dictionary<TVertex, VertexWrapper<TVertex>>();

			Heap<SmlPair<double, TEdge>> heap 
				= new Heap<SmlPair<double, TEdge>>(HeapType.Minimum, new PairKeyComparer<double, TEdge>());

			NGraph<TVertex, TEdge> graph = new NGraph<TVertex, TEdge>(false);

			foreach (TVertex vertex in weightedGraph.Vertices)
			{
				graph.AddVertex(vertex);
				dic.Add(vertex, null);
			}

			foreach (TEdge edge in weightedGraph.Edges)
				heap.Add(new SmlPair<double, TEdge>(edge.Weight, edge));

			while ((heap.Count > 0) && (num > 0))
			{
				TEdge edge = heap.RemoveRoot().Second;

				TVertex fromVertex = edge.From;
				TVertex toVertex = edge.To;

				while (dic[fromVertex] != null)
					fromVertex = dic[fromVertex].Vertex;

				while (dic[toVertex] != null)
					toVertex = dic[toVertex].Vertex;

				if (!fromVertex.Equals(toVertex))
				{
					dic[fromVertex] = new VertexWrapper<TVertex>(edge.To);
					num--;
					graph.AddEdge(edge);
				}
			}
			return graph;
		}

	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Sml.Common;
using Sml.Common.Visitors;
using Sml.DataStructures.Data.Collections;
using Sml.DataStructures.Data.Queues;

namespace Sml.Graphs.Data.NewGraph
{
	public delegate bool VertexPredicate<TVertex>(TVertex v);
	public delegate bool EdgePredicate<TVertex, TEdge>(TEdge e) where TEdge : INEdge<TVertex>;

	public delegate TEdge EdgeCreator<TVertex, TEdge>(TVertex source, TVertex target) where TEdge : INEdge<TVertex>;

    [DebuggerDisplay("Edges = {_edgeCount} Vertexes = {VertexCount}")]
    public class NGraph<TVertex, TEdge> : ICollection<TVertex>, IVisitable<TVertex> where TEdge : INEdge<TVertex>
    {
        private readonly bool _isDirected;
        private int _edgeCount;
        private int _edgeCapacity;


        private readonly NVertexEdgeCollection<TVertex, TEdge> _incidentsEdges = new NVertexEdgeCollection<TVertex, TEdge>();
        private readonly NVertexEdgeCollection<TVertex, TEdge> _outputEdges;


        public NGraph() { }

        public NGraph(int capacity)
            : this(false, capacity)
        {
            _edgeCapacity = capacity;
        }

        public NGraph(bool isDirected)
            : this(isDirected, 0)
        {
            _isDirected = isDirected;
            if (_isDirected)
                _outputEdges = new NVertexEdgeCollection<TVertex, TEdge>();
        }

        public NGraph(bool isDirected, int capacity)
        {
            _edgeCapacity = capacity;
            _isDirected = isDirected;
            if (capacity > 0)
            {
                if (_isDirected)
                    _outputEdges = new NVertexEdgeCollection<TVertex, TEdge>(capacity);
                _incidentsEdges = new NVertexEdgeCollection<TVertex, TEdge>(capacity);
            }
        }

        private NGraph(NVertexEdgeCollection<TVertex, TEdge> vertexIncidnetEdges, NVertexEdgeCollection<TVertex, TEdge> vertexOutEdges, int edgeCount, int edgeCapacity, bool isDirected)
        {
            _isDirected = isDirected;
            _incidentsEdges = vertexIncidnetEdges;
            _outputEdges = vertexOutEdges;
            _edgeCount = edgeCount;
            _edgeCapacity = edgeCapacity;
        }

        public NGraph<TVertex, TEdge> Clone()
        {
            if (_isDirected)
                return new NGraph<TVertex, TEdge>(_incidentsEdges.Clone(), _outputEdges.Clone(), _edgeCount, _edgeCapacity, _isDirected);
            return new NGraph<TVertex, TEdge>(_incidentsEdges.Clone(), null, _edgeCount, _edgeCapacity, _isDirected);
        }

        #region Properties

        public int EdgeCapacity
        {
            get { return _edgeCapacity; }
            set { _edgeCapacity = value; }
        }

        public IEnumerable<TVertex> Vertices
        {
            get
            {
                return _incidentsEdges.Keys;
            }
        }

        public int VertexCount
        {
            get { return _incidentsEdges.Count; }
        }

        public static Type VertexType
        {
            get { return typeof(TVertex); }
        }

        public bool IsVerticesEmpty
        {
            get { return (_incidentsEdges.Count == 0); }
        }

        public bool IsEdgesEmpty
        {
            get { return (_edgeCount == 0); }
        }

        public bool IsDirected
        {
            get { return _isDirected; }
        }

        public static Type EdgeType
        {
            get { return typeof(TEdge); }
        }

        public int EdgeCount
        {
            get { return _edgeCount; }
        }

        /// <summary>
        /// Получить все ребра графа
        /// </summary>
        public IEnumerable<TEdge> Edges
        {
            get
            {
                if (_isDirected)
                {
                    foreach (NEdgeCollection<TVertex, TEdge> iteri in _outputEdges.Values)
                    {
                        foreach (TEdge iterj in iteri)
                        {
                            yield return iterj;
                        }
                    }
                }
                else
                {
                    Set<TEdge> set = new Set<TEdge>();
                    foreach (NEdgeCollection<TVertex, TEdge> iteri in _incidentsEdges.Values)
                    {
                        foreach (TEdge iterj in iteri)
                        {
                            if (!set.Contains(iterj))
                            {
                                set.Add(iterj);
                                yield return iterj;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Simple methods //+++

        public bool IsIncidentEdgesEmpty(TVertex v)
        {
            return (Degree(v) == 0);
        }

        public bool IsOutEdgesEmpty(TVertex v)
        {
            return (OutDegree(v) == 0);
        }

        public bool IsInEdgesEmpty(TVertex v)
        {
            return (InDegree(v) == 0);
        }

        /// <summary>
        /// Степень вершины
        /// </summary>
        /// <param name="v">вершина</param>
        /// <returns></returns>
        public int Degree(TVertex v)
        {
            return _incidentsEdges[v].Count;
        }

        public int InDegree(TVertex v)
        {
            if (_isDirected)
                return _incidentsEdges[v].Count - _outputEdges[v].Count;
            return _incidentsEdges[v].Count;
        }

        public int OutDegree(TVertex v)
        {
            if (_isDirected)
                return _outputEdges[v].Count;
            return _incidentsEdges[v].Count;
        }

        #endregion

        #region Contains Edge/Vertex //+++

        /// <summary>
        /// Содержит ли граф вершину
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public bool ContainsVertex(TVertex v)
        {
            return _incidentsEdges.ContainsKey(v);
        }

        /// <summary>
        /// Содержит ли граф ребро
        /// </summary>
        /// <param name="edge">ребро</param>
        /// <returns>Содержит</returns>
        public bool ContainsEdge(TEdge edge)
        {
            return ContainsEdge(edge.From, edge.To);
        }

        /// <summary>
        /// Содержит ли граф ребро из одной заданной вершины в другую
        /// </summary>
        /// <param name="from">От</param>
        /// <param name="to">К</param>
        /// <returns>Содержит</returns>
        public bool ContainsEdge(TVertex from, TVertex to)
        {
            if (_incidentsEdges.ContainsKey(from))
            {
                foreach (TEdge local in OutEdges(from))
                {
                    TVertex partner = local.GetPartnerVertex(from);
                    if (partner.Equals(to))
                        return true;
                }
            }
            return false;
        }

        #endregion

        #region Implementation of IVisitableCollection<TVertex> //+++

        void ICollection<TVertex>.Add(TVertex item)
        {
            AddVertex(item);
        }

        public void Clear()
        {
            _incidentsEdges.Clear();
            if (_isDirected)
                _outputEdges.Clear();
            _edgeCount = 0;
        }

        bool ICollection<TVertex>.Contains(TVertex item)
        {
            return ContainsVertex(item);
        }

        public void CopyTo(TVertex[] array, int arrayIndex)
        {
            Guard.ArgumentNotNull(array, "array");
            if ((array.Length - arrayIndex) < VertexCount)
                throw new ArgumentException("NotEnoughSpaceInTargetArray", "array");

            int index = arrayIndex;
            foreach (TVertex vertex in Vertices)
            {
                array.SetValue(vertex, index);
                index++;
            }
        }

        bool ICollection<TVertex>.Remove(TVertex item)
        {
            return RemoveVertex(item);
        }

        int ICollection<TVertex>.Count
        {
            get { return VertexCount; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        #endregion

        #region Getter methods //+++

        public IEnumerable<TEdge> InEdges(TVertex v)
        {
            if (_isDirected)
            {
                NEdgeCollection<TVertex, TEdge> incList = _incidentsEdges[v];
                NEdgeCollection<TVertex, TEdge> outList = _outputEdges[v];
                foreach (TEdge edge in incList)
                {
                    if (!outList.Contains(edge))
                        yield return edge;
                }
            }
            else
            {
                foreach (TEdge edge in _incidentsEdges[v])
                    yield return edge;
            }
        }

        public TEdge InEdge(TVertex v, int index)
        {
            if (_isDirected)
            {
                NEdgeCollection<TVertex, TEdge> incList = _incidentsEdges[v];
                NEdgeCollection<TVertex, TEdge> outList = _outputEdges[v];
                int curIndex = 0;
                foreach (TEdge edge in incList)
                {
                    if (!outList.Contains(edge))
                    {
                        if (curIndex == index)
                            return edge;
                        curIndex++;
                    }
                }
                throw new IndexOutOfRangeException();
            }
            return _incidentsEdges[v][index];
        }

        public IEnumerable<TEdge> OutEdges(TVertex v)
        {
            if (_isDirected)
                return _outputEdges[v];
            return _incidentsEdges[v];
        }

        public TEdge OutEdge(TVertex v, int index)
        {
            if (_isDirected)
                return _outputEdges[v][index];
            return _incidentsEdges[v][index];
        }

        public IEnumerable<TEdge> IncidentEdges(TVertex v)
        {
            return _incidentsEdges[v];
        }

        public TEdge IncidentEdge(TVertex v, int index)
        {
            return _incidentsEdges[v][index];
        }

        #endregion

        #region Add/Remove Edges //+++

        public virtual bool AddEdge(TEdge edge)
        {
            if (ContainsEdge(edge.From, edge.To))
                return false;
            _incidentsEdges[edge.From].Add(edge);
            _incidentsEdges[edge.To].Add(edge);
            if (_isDirected)
                _outputEdges[edge.From].Add(edge);
            _edgeCount++;
            return true;
        }

        public TEdge AddEdge(TVertex from, TVertex to, EdgeCreator<TVertex, TEdge> creator)
        {
            TEdge edge = creator(from, to);
            AddEdge(edge);
            return edge;
        }

        public virtual int AddEdgeRange(IEnumerable<TEdge> edges)
        {
            int num = 0;
            foreach (TEdge local in edges)
            {
                if (AddEdge(local))
                    num++;
            }
            return num;
        }

        public int AddEdgeRange(params TEdge[] edges)
        {
            return AddEdgeRange((IEnumerable<TEdge>)edges);
        }

        /// <summary>
        /// Удалить все ребра вершины
        /// </summary>
        /// <param name="vertex">Вершина</param>
        public void ClearEdges(TVertex vertex)
        {
            NEdgeCollection<TVertex, TEdge> list = _incidentsEdges[vertex];
            if (_isDirected)
            {
                NEdgeCollection<TVertex, TEdge> ocol = _outputEdges[vertex];
                foreach (TEdge edge in ocol)
                {
                    _incidentsEdges[edge.From].Remove(edge);
                    _incidentsEdges[edge.To].Remove(edge);
                }
                _edgeCount -= ocol.Count;
                ocol.Clear();
                foreach (TEdge edge in list)
                {
                    _incidentsEdges[edge.From].Remove(edge);
                    _outputEdges[edge.From].Remove(edge);
                }
            }
            else
            {
                foreach (TEdge edge in list)
                {
                    TVertex partner = edge.GetPartnerVertex(vertex);
                    _incidentsEdges[partner].Remove(edge);
                }
            }
            _edgeCount -= list.Count;
            list.Clear();
        }

        /// <summary>
        /// Удалить все выходящие ребра вершины
        /// </summary>
        /// <param name="vertex">Вершина</param>
        public void ClearOutEdges(TVertex vertex)
        {
            if (_isDirected)
            {
                NEdgeCollection<TVertex, TEdge> list = _outputEdges[vertex];
                foreach (TEdge local in list)
                {
                    _incidentsEdges[local.To].Remove(local);
                    _incidentsEdges[local.From].Remove(local);
                }
                _edgeCount -= list.Count;
                list.Clear();
                return;
            }
            ClearEdges(vertex);
        }

        /// <summary>
        /// Удалить все входящие ребра вершины
        /// </summary>
        /// <param name="vertex">Вершина</param>
        public void ClearInEdges(TVertex vertex)
        {
            if (_isDirected)
            {
                NEdgeCollection<TVertex, TEdge> list = _incidentsEdges[vertex];
                List<TEdge> toRemove = new List<TEdge>();
                foreach (TEdge edge in list)
                {
                    if (edge.To.Equals(vertex))
                    {
                        toRemove.Add(edge);
                        _outputEdges[edge.From].Remove(edge);
                        _incidentsEdges[edge.From].Remove(edge);
                    }
                }
                foreach (TEdge edge in toRemove)
                    list.Remove(edge);
                _edgeCount -= toRemove.Count;
                return;
            }
            ClearEdges(vertex);
        }

        public virtual bool RemoveEdge(TEdge edge)
        {
            if (!ContainsEdge(edge))
                return false;

            _incidentsEdges[edge.From].Remove(edge);
            _incidentsEdges[edge.To].Remove(edge);
            if (_isDirected)
                _outputEdges[edge.From].Remove(edge);
            _edgeCount--;
            return true;
        }

        public int RemoveEdgeIf(EdgePredicate<TVertex, TEdge> predicate)
        {
            return RemoveEdgeIf(predicate, Edges);
        }

        public int RemoveInEdgeIf(TVertex v, EdgePredicate<TVertex, TEdge> predicate)
        {
            return RemoveEdgeIf(predicate, InEdges(v));
        }

        public int RemoveOutEdgeIf(TVertex v, EdgePredicate<TVertex, TEdge> predicate)
        {
            return RemoveEdgeIf(predicate, OutEdges(v));
        }

        private int RemoveEdgeIf(EdgePredicate<TVertex, TEdge> predicate, IEnumerable<TEdge> src)
        {
            int removedEdgesCount = 0;
            List<TEdge> edges = new List<TEdge>(src);
            foreach (TEdge edge in edges)
            {
                if (predicate(edge))
                {
                    if (RemoveEdge(edge))
                        removedEdgesCount++;
                }
            }
            return removedEdgesCount;
        }

        #endregion

        #region Add/Remove Vertex //+++

        public virtual bool AddVertex(TVertex v)
        {
            if (ContainsVertex(v))
                return false;

            if (EdgeCapacity > 0)
            {
                _incidentsEdges.Add(v, new NEdgeCollection<TVertex, TEdge>(EdgeCapacity));
                if (_isDirected)
                    _outputEdges.Add(v, new NEdgeCollection<TVertex, TEdge>(EdgeCapacity));
            }
            else
            {
                _incidentsEdges.Add(v, new NEdgeCollection<TVertex, TEdge>());
                if (_isDirected)
                    _outputEdges.Add(v, new NEdgeCollection<TVertex, TEdge>());
            }
            return true;
        }

        public virtual int AddVertexRange(IEnumerable<TVertex> vertices)
        {
            int num = 0;
            foreach (TVertex local in vertices)
            {
                if (AddVertex(local))
                    num++;
            }
            return num;
        }

        public int AddVertexRange(params TVertex[] vertices)
        {
            return AddVertexRange((IEnumerable<TVertex>)vertices);
        }

        public virtual bool RemoveVertex(TVertex vertex)
        {
            if (!ContainsVertex(vertex))
                return false;

            ClearEdges(vertex);
            _incidentsEdges.Remove(vertex);
            if (_isDirected)
                _outputEdges.Remove(vertex);
            return true;
        }


        public int RemoveVertexIf(VertexPredicate<TVertex> predicate)
        {
            int removedVertexesCount = 0;
            List<TVertex> vertices = new List<TVertex>(Vertices);
            foreach (TVertex vert in vertices)
            {
                if (predicate(vert))
                {
                    if(RemoveVertex(vert))
                        removedVertexesCount++;
                }
            }
            return removedVertexesCount;
        }

        #endregion

        public virtual bool AddVerticesAndEdge(TEdge e)
        {
            AddVertex(e.From);
            AddVertex(e.To);
            return AddEdge(e);
        }

        public int AddVerticesAndEdgeRange(IEnumerable<TEdge> edges)
        {
            int num = 0;
            foreach (TEdge edge in edges)
                if (AddVerticesAndEdge(edge)) num++;

            return num;
        }

        public int AddVerticesAndEdgeRange(params TEdge[] edges)
        {
            return AddVerticesAndEdgeRange((IEnumerable<TEdge>)edges);
        }

        public void MergeVertex(TVertex vertex, EdgeCreator<TVertex, TEdge> edgeFactory)
        {
            NEdgeCollection<TVertex, TEdge> inci = _incidentsEdges[vertex];
            if (_isDirected)
            {
                NEdgeCollection<TVertex, TEdge> ocol = _outputEdges[vertex];
                foreach (TEdge iEdge in inci)
                {
                    if (ocol.Contains(iEdge))
                        continue;
                    foreach (TEdge oEdge in ocol)
                    {
                        AddEdge(edgeFactory(iEdge.From, oEdge.To));
                    }
                }
            }
            else
            {
                for (int i = 0; i < inci.Count; i++)
                {
                    TVertex fromVertex = inci[i].GetPartnerVertex(vertex);
                    for (int j = i + 1; j < inci.Count; j++)
                    {
                        TVertex toVertex = inci[j].GetPartnerVertex(vertex);
                        AddEdge(edgeFactory(fromVertex, toVertex));
                    }
                }
            }
            RemoveVertex(vertex);
        }

        public int MergeVertexIf(VertexPredicate<TVertex> vertexPredicate, EdgeCreator<TVertex, TEdge> edgeFactory)
        {
            int mergedVertexesCount = 0;
            foreach (TVertex vertex in Vertices)
            {
                if (vertexPredicate(vertex))
                {
                    MergeVertex(vertex, edgeFactory);
                    mergedVertexesCount++;
                }
            }
            return mergedVertexesCount;
        }

        public void TrimEdgeExcess()
        {
            if (_isDirected)
            {
                foreach (NEdgeCollection<TVertex, TEdge> list in _outputEdges.Values)
                    list.TrimExcess();
            }

            foreach (NEdgeCollection<TVertex, TEdge> list2 in _incidentsEdges.Values)
                list2.TrimExcess();
        }

        /// <summary>
        /// Является ли граф цикличным
        /// </summary>
        /// <returns></returns>
        public bool IsCyclic()
        {
            if (!_isDirected)
                throw new InvalidOperationException("OperationOnlyValidForDirectedGraph");
            DummyVisitor<TVertex> visitor = new DummyVisitor<TVertex>();
            return (TopologicalSortTraversalInternal(visitor) < VertexCount);
        }

        /// <summary>
        /// Является ли направленный граф сильносвязанным.
        /// </summary>
        /// <returns></returns>
        public bool IsStronglyConnected()
        {
            if (!_isDirected)
                throw new InvalidOperationException("UndirectedGraphStrongConnectedness");

            if (VertexCount == 0) return false;

            CountingVisitor<TVertex> visitor = new CountingVisitor<TVertex>();
            foreach (TVertex vertex in _outputEdges.Keys)
            {
                BreadthFirstTraversal(visitor, vertex);
                if (visitor.Count != VertexCount)
                {
                    return false;
                }
                visitor.ResetCount();
            }
            return true;
        }

        /// <summary>
        /// Является ли граф слабосвязанным
        /// </summary>
        public bool IsWeaklyConnected()
        {
            if (VertexCount == 0) return false;

            CountingVisitor<TVertex> visitor = new CountingVisitor<TVertex>();
            BreadthFirstTraversal(visitor, GetAnyVertex());
            return (visitor.Count == VertexCount);
        }

        /// <summary>
        /// Получить произвольную вершину
        /// </summary>
        /// <returns></returns>
        public TVertex GetAnyVertex()
        {
            foreach (TVertex vertex in _outputEdges.Keys)
                return vertex;
            return default(TVertex);
        }

        /// <summary>
        /// Обход вершин дерева в ширину.
        /// </summary>
        /// <param name="visitor">Посититель</param>
        /// <param name="startVertex">Начальный узел</param>
        /// <remarks>
        /// Метод просмотра вершин дерева, при котором каждый уровень полностью
        /// подвергается анализу до перехода к следующему уровню)
        /// </remarks>
        public void BreadthFirstTraversal(IVisitor<TVertex> visitor, TVertex startVertex)
        {
            Guard.ArgumentNotNull(visitor, "visitor");
            Guard.ArgumentNotNull(startVertex, "startVertex");

            // Уже посещенные вершины
            List<TVertex> list = new List<TVertex>(VertexCount);

            VisitableQueue<TVertex> queue = new VisitableQueue<TVertex>();
            queue.Enqueue(startVertex);
            list.Add(startVertex);
            while (!queue.IsEmpty && !visitor.HasCompleted)
            {
                TVertex vertex = queue.Dequeue();
                visitor.Visit(vertex);
                IEnumerable<TEdge> outCol = OutEdges(vertex);
                foreach (TEdge edge in outCol)
                {
                    TVertex partnerVertex = edge.GetPartnerVertex(vertex);
                    if (!list.Contains(partnerVertex))
                    {
                        queue.Enqueue(partnerVertex);
                        list.Add(partnerVertex);
                    }
                }
            }
        }

        /// <summary>
        /// Обход в глубину
        /// </summary>
        /// <param name="visitor">Посититель</param>
        /// <param name="startVertex">Стартовый индекс.</param>
        public void DepthFirstTraversal(OrderedVisitor<TVertex> visitor, TVertex startVertex)
        {
            Guard.ArgumentNotNull(visitor, "visitor");
            Guard.ArgumentNotNull(startVertex, "startVertex");
            List<TVertex> visitedVertices = new List<TVertex>(VertexCount);
            DepthFirstTraversal(visitor, startVertex, ref visitedVertices);
        }

        private void DepthFirstTraversal(OrderedVisitor<TVertex> visitor, TVertex startVertex, ref List<TVertex> visitedVertices)
        {
            if (!visitor.HasCompleted)
            {
                visitedVertices.Add(startVertex);
                visitor.VisitPreOrder(startVertex);

                IEnumerable<TEdge> outCol = OutEdges(startVertex);
                foreach (TEdge edge in outCol)
                {
                    TVertex partnerVertex = edge.GetPartnerVertex(startVertex);
                    if (!visitedVertices.Contains(partnerVertex))
                    {
                        DepthFirstTraversal(visitor, partnerVertex, ref visitedVertices);
                    }
                }
                visitor.VisitPostOrder(startVertex);
            }
        }

        public IList<TVertex> TopologicalSort()
        {
            TrackingVisitor<TVertex> visitor = new TrackingVisitor<TVertex>();
            TopologicalSortTraversal(visitor);
            return visitor.TrackingList;
        }

        public void TopologicalSortTraversal(IVisitor<TVertex> visitor)
        {
            if (!_isDirected)
                throw new InvalidOperationException("OperationOnlyValidForDirectedGraph");
            if (TopologicalSortTraversalInternal(visitor) < VertexCount)
            {
                throw new InvalidOperationException("GraphHasCycles");
            }
        }

        private int TopologicalSortTraversalInternal(IVisitor<TVertex> visitor)
        {
            Guard.ArgumentNotNull(visitor, "visitor");
            //if (!_isDirected)
            //    throw new ArgumentException("OperationOnlyValidForDirectedGraph");

            int num = 0;
            if (!IsEdgesEmpty)
            {
                Dictionary<TVertex, int> dictionary = new Dictionary<TVertex, int>(VertexCount);
                Queue<TVertex> queue = new Queue<TVertex>();
                foreach (TVertex vertex in Vertices)
                {
                    int incomingEdgeCount = InDegree(vertex);
                    dictionary.Add(vertex, incomingEdgeCount);
                    if (incomingEdgeCount == 0)
                        queue.Enqueue(vertex);
                }
                if (queue.Count <= 0)
                    return num;

                while ((queue.Count > 0) && !visitor.HasCompleted)
                {
                    TVertex key = queue.Dequeue();
                    dictionary.Remove(key);
                    visitor.Visit(key);
                    num++;
                    foreach (TEdge edge in _outputEdges[key])
                    {
                        Dictionary<TVertex, int> dict = dictionary;
                        TVertex toVertex = edge.To;
                        TVertex vertex = toVertex;
                        (dict)[vertex] = dict[vertex] - 1;
                        if (dictionary[toVertex] == 0)
                            queue.Enqueue(toVertex);
                    }
                }
            }
            return num;
        }

        #region Implementation of IEnumerable

        public IEnumerator<TVertex> GetEnumerator()
        {
            return Vertices.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Implementation of IVisitable<TVertex>

        public void Accept(IVisitor<TVertex> visitor)
        {
            Guard.ArgumentNotNull(visitor, "visitor");
            foreach (TVertex local in this)
            {
                if (visitor.HasCompleted) break;
                visitor.Visit(local);
            }
        }

        #endregion

    }
	
}

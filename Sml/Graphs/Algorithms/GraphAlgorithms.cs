// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Sml.Common;
using Sml.DataStructures.Algorithms.Comparers;
using Sml.DataStructures.Data;
using Sml.DataStructures.Data.Collections;
using Sml.Graphs.Data;

namespace Sml.Graphs.Algorithms
{
	public static class GraphAlgorithms
	{
		private static Graph<T> BuildGraphDijkstra<T>(Graph<T> weightedGraph, Vertex<T> fromVertex,
		                                              ICollection<KeyValuePair<Vertex<T>, VertexInfo<T>>> vertexStatus)
		{
			Graph<T> graph = new Graph<T>(weightedGraph.IsDirected);
			Dictionary<Vertex<T>, Vertex<T>> dictionary = new Dictionary<Vertex<T>, Vertex<T>>(vertexStatus.Count);

			foreach (KeyValuePair<Vertex<T>, VertexInfo<T>> pair in vertexStatus)
			{
				Vertex<T> vertex = new Vertex<T>(pair.Key.Data, pair.Value.Distance);
				dictionary.Add(pair.Key, vertex);
				graph.AddVertex(vertex);
			}

			foreach (KeyValuePair<Vertex<T>, VertexInfo<T>> pair2 in vertexStatus)
			{
				VertexInfo<T> info = pair2.Value;
			    if ((info.EdgeFollowed != null) && (pair2.Key != fromVertex))
			        graph.AddEdge(dictionary[info.EdgeFollowed.GetPartnerVertex(pair2.Key)], dictionary[pair2.Key],
			                      info.EdgeFollowed.Weight);
			}

			return graph;
		}

		private static Graph<T> BuildGraphPrim<T>(Graph<T> weightedGraph,
		                                          ICollection<KeyValuePair<Vertex<T>, VertexInfo<T>>> vertexStatus)
		{
			Graph<T> graph = new Graph<T>(weightedGraph.IsDirected);
			Dictionary<Vertex<T>, Vertex<T>> dictionary = new Dictionary<Vertex<T>, Vertex<T>>(vertexStatus.Count);

			foreach (KeyValuePair<Vertex<T>, VertexInfo<T>> pair in vertexStatus)
			{
				Vertex<T> vertex = new Vertex<T>(pair.Key.Data, pair.Key.Weight);
				dictionary.Add(pair.Key, vertex);
				graph.AddVertex(vertex);
			}

			foreach (KeyValuePair<Vertex<T>, VertexInfo<T>> pair2 in vertexStatus)
			{
				VertexInfo<T> info = pair2.Value;
			    if (info.EdgeFollowed != null)
			        graph.AddEdge(dictionary[info.EdgeFollowed.GetPartnerVertex(pair2.Key)], dictionary[pair2.Key], info.Distance);
			}

			return graph;
		}

		public static Graph<T> DijkstrasAlgorithm<T>(Graph<T> weightedGraph, Vertex<T> fromVertex)
		{
			Guard.ArgumentNotNull(weightedGraph, "weightedGraph");
			Guard.ArgumentNotNull(fromVertex, "fromVertex");
		    if (!weightedGraph.ContainsVertex(fromVertex))
		        throw new ArgumentException("VertexCouldNotBeFound", "fromVertex");

		    Heap<SmlPair<double, Vertex<T>>> heap = new Heap<SmlPair<double, Vertex<T>>>(HeapType.Minimum,
			                                                                                     new PairKeyComparer
			                                                                                     	<double, Vertex<T>>());
			Dictionary<Vertex<T>, VertexInfo<T>> vertexStatus = new Dictionary<Vertex<T>, VertexInfo<T>>();
		    foreach (Vertex<T> vertex in weightedGraph.Vertices)
		        vertexStatus.Add(vertex, new VertexInfo<T>(double.MaxValue, null, false));

		    vertexStatus[fromVertex].Distance = 0.0;
			heap.Add(new SmlPair<double, Vertex<T>>(0.0, fromVertex));
			while (heap.Count > 0)
			{
				SmlPair<double, Vertex<T>> pair = heap.RemoveRoot();
				VertexInfo<T> info = vertexStatus[pair.Second];
				if (!info.IsFinalised)
				{
					IList<Edge<T>> emanatingEdges = pair.Second.OutputEdges;
					vertexStatus[pair.Second].IsFinalised = true;
					for (int i = 0; i < emanatingEdges.Count; i++)
					{
						Vertex<T> partnerVertex = emanatingEdges[i].GetPartnerVertex(pair.Second);
						double key = info.Distance + emanatingEdges[i].Weight;
						VertexInfo<T> info2 = vertexStatus[partnerVertex];
						if (key < info2.Distance)
						{
							info2.EdgeFollowed = emanatingEdges[i];
							info2.Distance = key;
							heap.Add(new SmlPair<double, Vertex<T>>(key, partnerVertex));
						}
					}
				}
			}
			return BuildGraphDijkstra(weightedGraph, fromVertex, vertexStatus);
		}

		public static Graph<T> KruskalsAlgorithm<T>(Graph<T> weightedGraph)
		{
			Guard.ArgumentNotNull(weightedGraph, "weightedGraph");
			int num = weightedGraph.Vertices.Count - 1;
			Dictionary<Vertex<T>, Vertex<T>> dictionary = new Dictionary<Vertex<T>, Vertex<T>>();
			Dictionary<Vertex<T>, Vertex<T>> dictionary2 = new Dictionary<Vertex<T>, Vertex<T>>();
			Heap<SmlPair<double, Edge<T>>> heap = new Heap<SmlPair<double, Edge<T>>>(HeapType.Minimum,
			                                                                                 new PairKeyComparer
			                                                                                 	<double, Edge<T>>());
			Graph<T> graph = new Graph<T>(false);
			foreach (Vertex<T> vertex in weightedGraph.Vertices)
			{
				Vertex<T> vertex2 = new Vertex<T>(vertex.Data);
				dictionary2.Add(vertex, vertex2);
				graph.AddVertex(vertex2);
				dictionary.Add(vertex, null);
			}

		    foreach (Edge<T> edge in weightedGraph.Edges)
		        heap.Add(new SmlPair<double, Edge<T>>(edge.Weight, edge));

		    while ((heap.Count > 0) && (num > 0))
			{
				Edge<T> edge2 = heap.RemoveRoot().Second;
				Vertex<T> fromVertex = edge2.From;
				Vertex<T> toVertex = edge2.To;

			    while (dictionary[fromVertex] != null)
			        fromVertex = dictionary[fromVertex];

			    while (dictionary[toVertex] != null)
			        toVertex = dictionary[toVertex];

			    if (fromVertex != toVertex)
				{
					dictionary[fromVertex] = edge2.To;
					num--;
					graph.AddEdge(dictionary2[edge2.From], dictionary2[edge2.To], edge2.Weight);
				}
			}
			return graph;
		}

		public static Graph<T> PrimsAlgorithm<T>(Graph<T> weightedGraph, Vertex<T> fromVertex)
		{
			Guard.ArgumentNotNull(weightedGraph, "weightedGraph");
			Guard.ArgumentNotNull(fromVertex, "fromVertex");

		    if (!weightedGraph.ContainsVertex(fromVertex))
		        throw new ArgumentException("VertexCouldNotBeFound", "fromVertex");

		    Heap<SmlPair<double, Vertex<T>>> heap = new Heap<SmlPair<double, Vertex<T>>>(HeapType.Minimum,
			                                                                                     new PairKeyComparer
			                                                                                     	<double, Vertex<T>>());
			Dictionary<Vertex<T>, VertexInfo<T>> vertexStatus = new Dictionary<Vertex<T>, VertexInfo<T>>();
		    foreach (Vertex<T> vertex in weightedGraph.Vertices)
		        vertexStatus.Add(vertex, new VertexInfo<T>(double.MaxValue, null, false));
            
		    vertexStatus[fromVertex].Distance = 0.0;
			heap.Add(new SmlPair<double, Vertex<T>>(0.0, fromVertex));

			while (heap.Count > 0)
			{
				SmlPair<double, Vertex<T>> association = heap.RemoveRoot();
				IList<Edge<T>> incidentEdges = association.Second.InputEdges;
				vertexStatus[association.Second].IsFinalised = true;
				for (int i = 0; i < incidentEdges.Count; i++) // Можно переделать в foreach
				{
					Edge<T> edge = incidentEdges[i];
					Vertex<T> partnerVertex = edge.GetPartnerVertex(association.Second);
					VertexInfo<T> info = vertexStatus[partnerVertex];
					if (!info.IsFinalised && (edge.Weight < info.Distance))
					{
						info.EdgeFollowed = edge;
						info.Distance = edge.Weight;
						heap.Add(new SmlPair<double, Vertex<T>>(edge.Weight, partnerVertex));
					}
				}
			}

			return BuildGraphPrim(weightedGraph, vertexStatus);
		}
	}
}

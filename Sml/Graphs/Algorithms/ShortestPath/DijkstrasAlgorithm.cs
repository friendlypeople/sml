// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Sml.Common;
using Sml.DataStructures.Algorithms.Comparers;
using Sml.DataStructures.Data;
using Sml.DataStructures.Data.Collections;
using Sml.Graphs.Data;
using Sml.Graphs.Data.NewGraph;

namespace Sml.Graphs.Algorithms.ShortestPath
{
    public class DijkstrasAlgorithm
    {
        public static NGraph<TVertex, TEdge> GetPath<TVertex, TEdge>(NGraph<TVertex, TEdge> weightedGraph, TVertex fromVertex, TVertex toVertex) where TEdge : NEdge<TVertex>
        {
            Guard.ArgumentNotNull(weightedGraph, "weightedGraph");
            Guard.ArgumentNotNull(fromVertex, "fromVertex");

            if (!weightedGraph.ContainsVertex(fromVertex))
                throw new ArgumentException("VertexCouldNotBeFound", "fromVertex");

            Heap<SmlPair<double, TVertex>> heap = new Heap<SmlPair<double, TVertex>>(HeapType.Minimum, new PairKeyComparer<double, TVertex>());

            Dictionary<TVertex, NVertexInfo<TEdge>> vertexStatus = new Dictionary<TVertex, NVertexInfo<TEdge>>();

            vertexStatus.Add(fromVertex, new NVertexInfo<TEdge>(0.0, null, false));
            heap.Add(new SmlPair<double, TVertex>(0.0, fromVertex));

            while (heap.Count > 0)
            {
                SmlPair<double, TVertex> pair = heap.RemoveRoot();
                NVertexInfo<TEdge> vertexInfo = vertexStatus[pair.Second];
                IEnumerable<TEdge> emanatingEdges = weightedGraph.OutEdges(pair.Second);

                foreach (TEdge edge in emanatingEdges)
                {
                    TVertex partnerVertex = edge.GetPartnerVertex(pair.Second);
                    NVertexInfo<TEdge> info;
                    if (!vertexStatus.TryGetValue(partnerVertex, out info))
                    {
                        info = new NVertexInfo<TEdge>(double.MaxValue, null, false);
                        vertexStatus.Add(partnerVertex, info);
                    }
                    double key = vertexInfo.Distance + edge.Weight;
                    if (key < info.Distance)
                    {
                        info.Distance = key;
                        info.EdgeFollowed = edge;
                        heap.Add(new SmlPair<double, TVertex>(key, partnerVertex));
                    }
                }
            }

            // Если не нашли путь
            if (!vertexStatus.ContainsKey(toVertex))
                return null;

            // Строим граф на основании словаря кратчайших путей(ребер)
            NGraph<TVertex, TEdge> dijkstrasGraph = new NGraph<TVertex, TEdge>(weightedGraph.IsDirected);
            TVertex currentVertex = toVertex;
            while (!currentVertex.Equals(fromVertex))
            {
                TEdge edge = vertexStatus[currentVertex].EdgeFollowed;
                dijkstrasGraph.AddVerticesAndEdge(edge);
                currentVertex = edge.GetPartnerVertex(currentVertex);
            }
            return dijkstrasGraph;
        }

        public static double GetWeight<TVertex, TEdge>(NGraph<TVertex, TEdge> weightedGraph, TVertex fromVertex, TVertex toVertex) where TEdge : NEdge<TVertex>
        {
            Guard.ArgumentNotNull(weightedGraph, "weightedGraph");
            Guard.ArgumentNotNull(fromVertex, "fromVertex");

            if (!weightedGraph.ContainsVertex(fromVertex))
                throw new ArgumentException("VertexCouldNotBeFound", "fromVertex");
            if (fromVertex.Equals(toVertex)) return 0.0;

            Heap<SmlPair<double, TVertex>> heap = new Heap<SmlPair<double, TVertex>>(HeapType.Minimum, new PairKeyComparer<double, TVertex>());

            Dictionary<TVertex, NVertexInfo<TEdge>> vertexStatus = new Dictionary<TVertex, NVertexInfo<TEdge>>();

            vertexStatus.Add(fromVertex, new NVertexInfo<TEdge>(0.0, null, false));
            heap.Add(new SmlPair<double, TVertex>(0.0, fromVertex));

            while (heap.Count > 0)
            {
                SmlPair<double, TVertex> pair = heap.RemoveRoot();
                NVertexInfo<TEdge> vertexInfo = vertexStatus[pair.Second];
                IEnumerable<TEdge> emanatingEdges = weightedGraph.OutEdges(pair.Second);

                foreach (TEdge edge in emanatingEdges)
                {
                    TVertex partnerVertex = edge.GetPartnerVertex(pair.Second);
                    NVertexInfo<TEdge> info;
                    if (!vertexStatus.TryGetValue(partnerVertex, out info))
                    {
                        info = new NVertexInfo<TEdge>(double.MaxValue, null, false);
                        vertexStatus.Add(partnerVertex, info);
                    }
                    double key = vertexInfo.Distance + edge.Weight;
                    if (key < info.Distance)
                    {
                        info.Distance = key;
                        info.EdgeFollowed = edge;
                        heap.Add(new SmlPair<double, TVertex>(key, partnerVertex));
                    }
                }
            }

            // Если не нашли путь
            if (!vertexStatus.ContainsKey(toVertex))
                return double.PositiveInfinity;
            return vertexStatus[toVertex].Distance;
        }
    }
}

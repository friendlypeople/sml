// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System.Collections.Generic;
using Sml.Geometry.Data;

namespace Sml.Geometry.Algorithms.Comparers
{
	/// <summary>
	/// Сравнивает точки по X
	/// </summary>
    public class SimplePointsXComparator : IComparer<SmlPoint2D>
	{
        public int Compare(SmlPoint2D x, SmlPoint2D y)
		{
			return x.X.CompareTo(y.X);
		}
	}

	/// <summary>
	/// Сравнивает точки по Y
	/// </summary>
    public class SimplePointsYComparator : IComparer<SmlPoint2D>
	{
        public int Compare(SmlPoint2D x, SmlPoint2D y)
		{
			return x.Y.CompareTo(y.Y);
		}
	}

	/// <summary>
	/// Сортирует точки по Y c заданной точностью
	/// </summary>
    public class PointsYComparator : IComparer<SmlPoint2D>
	{
		/// <summary>
		/// Точность компаратора
		/// </summary>
		private readonly double _prec;

		/// <summary>
		/// Конструктор компаратора
		/// </summary>
		/// <param name="prec">Точность компаратора</param>
		public PointsYComparator(double prec)
		{
			_prec = prec;
		}

        public int Compare(SmlPoint2D p1, SmlPoint2D p2)
		{
			double p1Y = p1.Y;
			double p2Y = p2.Y;
			bool equalY = p1Y - _prec <= p2Y && p2Y <= p1Y + _prec;
			if (equalY)
				return 0;
			return p2Y < p1Y - _prec ? 1 : -1;
		}
	}

	/// <summary>
	/// Сортирует точки по X c заданной точностью
	/// </summary>
    public class PointsXComparator : IComparer<SmlPoint2D>
	{
		/// <summary>
		/// Точность компаратора
		/// </summary>
		private readonly double _prec;

		/// <summary>
		/// Конструктор компаратора
		/// </summary>
		/// <param name="prec">Точность компаратора</param>
		public PointsXComparator(double prec)
		{
			_prec = prec;
		}

        public int Compare(SmlPoint2D p1, SmlPoint2D p2)
		{
			double p1X = p1.X;
			double p2X = p2.X;
			bool equalX = p1X - _prec <= p2X && p2X <= p1X + _prec;
			if (equalX)
				return 0;
			return p2X < p1X - _prec ? 1 : -1;
		}
	}

	
}

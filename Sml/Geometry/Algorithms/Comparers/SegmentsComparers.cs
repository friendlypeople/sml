// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System.Collections.Generic;
using Sml.Geometry.Data;

namespace Sml.Geometry.Algorithms.Comparers
{
	/// <summary>
	/// Компаратор горизонтальных отрезков в порядке убывания их правых точек
	/// </summary>
	public class HorizontalSegmentsComparator : IComparer<SmlSegment2D>
	{
		/// <summary>
		/// Точность компаратора
		/// </summary>
		private readonly double _prec;

		/// <summary>
		/// Конструктор компаратора
		/// </summary>
		/// <param name="prec">Точность компаратора</param>
		public HorizontalSegmentsComparator(double prec)
		{
			_prec = prec;
		}

        public int Compare(SmlSegment2D s1, SmlSegment2D s2)
		{
			double right1 = Right(s1, _prec).X;
			double right2 = Right(s2, _prec).X;
			if (right1 - _prec <= right2 && right2 <= right1 + _prec)
				return 0;
			return right1 - _prec > right2 ? -1 : 1;
		}

		/// <summary>
		/// Метод определения правой точки отрезка
		/// </summary>
		/// <param name="s">Исследуемый отрезок</param>
		/// <param name="prec">Точность определения</param>
		/// <returns>Правая точка</returns>
        public static SmlPoint2D Right(SmlSegment2D s, double prec)
		{
			return s.StartPoint.X < s.EndPoint.X - prec
                    ? new SmlPoint2D(s.EndPoint.X, s.EndPoint.Y)
                    : new SmlPoint2D(s.StartPoint.X, s.StartPoint.Y);
		}

		/// <summary>
		/// Метод определения левой точки отрезка
		/// </summary>
		/// <param name="s">Исследуемый отрезок</param>
		/// <param name="prec">Точность определения</param>
		/// <returns>Левая точка</returns>
        public static SmlPoint2D Left(SmlSegment2D s, double prec)
		{
			return s.StartPoint.X < s.EndPoint.X - prec
                    ? new SmlPoint2D(s.StartPoint.X, s.StartPoint.Y)
                    : new SmlPoint2D(s.EndPoint.X, s.EndPoint.Y);
		}
	}

	/// <summary>
	/// Компаратор сортировки вертикальных отрезков в порядке возрастания их нижних точек
	/// </summary>
    public class VerticalSegmentsComparator : IComparer<SmlSegment2D>
	{
		/// <summary>
		/// Точность компаратора
		/// </summary>
		private readonly double _prec;

		/// <summary>
		/// Конструктор компаратороа
		/// </summary>
		/// <param name="prec">Точность компаратора</param>
		public VerticalSegmentsComparator(double prec)
		{
			_prec = prec;
		}


        public int Compare(SmlSegment2D s1, SmlSegment2D s2)
		{
			double down1 = Down(s1, _prec).Y;
			double down2 = Down(s2, _prec).Y;
			if (down1 - _prec <= down2 && down2 <= down1 + _prec)
				return 0;
			return down1 - _prec > down2 ? 1 : -1;
		}

		/// <summary>
		/// Метод определения нижней точки отрезка
		/// </summary>
		/// <param name="s">Исследуемый отрезок</param>
		/// <param name="prec">Точнось определения</param>
		/// <returns>Нижняя точка</returns>
        public static SmlPoint2D Down(SmlSegment2D s, double prec)
		{
            return s.StartPoint.Y < s.EndPoint.Y - prec ? new SmlPoint2D(s.StartPoint.X, s.StartPoint.Y) : new SmlPoint2D(s.EndPoint.X, s.EndPoint.Y);
		}

		/// <summary>
		/// Метод определения верхней точки отрезка
		/// </summary>
		/// <param name="s">Исследуемый отрезок</param>
		/// <param name="prec">Точнось определения</param>
		/// <returns>Верхняя точка</returns>
        public static SmlPoint2D Up(SmlSegment2D s, double prec)
		{
			return s.StartPoint.Y < s.EndPoint.Y - prec
                    ? new SmlPoint2D(s.EndPoint.X, s.EndPoint.Y)
                    : new SmlPoint2D(s.StartPoint.X, s.StartPoint.Y);
		}

	}
}

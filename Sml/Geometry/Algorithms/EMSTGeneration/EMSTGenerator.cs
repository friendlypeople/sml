// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Sml.Graphs.Data;

namespace Sml.Geometry.Algorithms.EMSTGeneration
{
    /// <summary>
    /// Генератор ЕМОД(Евклидова минимального остовного дерева)
    /// EMST(Euclidean Minimum Spanning Tree)
    /// </summary>
    public class EMSTGenerator
    {
        private Graph<int> _deloneyGraph;

        /// <summary>
        /// Вершины в объединенной вершине
        /// </summary>
        private Dictionary<int, UnionVertex> _unionVertexTable = new Dictionary<int, UnionVertex>();

        private Dictionary<Vertex<int>,Vertex<int>>  _vertexTable = new Dictionary<Vertex<int>, Vertex<int>>();

        public EMSTGenerator(Graph<int> deloneyGraph)
        {
            _deloneyGraph = deloneyGraph;
        }

        public Graph<int> GenerateEMST()
        {
            Graph<int> emst = new Graph<int>();
            InitializeEMST(emst);

            int currentStage = 1;

            int newUnionNumber = _unionVertexTable.Count;
            for (int i = 0; _unionVertexTable.Count != 1; i++)
            {
                while (!_unionVertexTable.ContainsKey(i))
                    i++;

                UnionVertex unionVertex1 = _unionVertexTable[i];
                if (unionVertex1.Stage == currentStage)
                {
                   // foreach (UnionVertex unionVertex in _unionVertexTable.Values)
                    //    unionVertex.ClearUnionVertex();
                    ClearGraph();
                    currentStage++;
                }

                Vertex<int> minVertex1 = null;
                Edge<int> minEdge = null;
                double minWeigth = double.MaxValue;

                foreach (Vertex<int> realVertex in unionVertex1.Vertexes)
                    foreach (Edge<int> outputEdge in realVertex.OutputEdges)
                        if (minWeigth > outputEdge.Weight)
                        {
                            minWeigth = outputEdge.Weight;
                            minEdge = outputEdge;
                            minVertex1 = realVertex;
                        }

                emst.AddEdge(_vertexTable[minEdge.From], _vertexTable[minEdge.To]);

                Vertex<int> minVertex2 = minEdge.GetPartnerVertex(minVertex1);
                int unionVertexIndex = (int) minVertex2.Tag;

                UnionVertex unionVertex2 = _unionVertexTable[unionVertexIndex];
                
                UnionOfTheVertexes(unionVertex1, unionVertex2, newUnionNumber);
                newUnionNumber++;
            }

            return emst;
        }

        private void InitializeEMST(Graph<int> emst)
        {
            foreach (Vertex<int> vertex in _deloneyGraph.Vertices)
            {
                Vertex<int> emstVertex = new Vertex<int>(vertex.Data);
                emst.AddVertex(emstVertex);
                _vertexTable.Add(vertex, emstVertex);

                vertex.Tag = vertex.Data;
                _unionVertexTable.Add(vertex.Data, new UnionVertex(vertex.Data, vertex));
            }
        }

        /// <summary>
        /// Объединение вершин.
        /// </summary>
        /// <param name="unionVertex1">Объединенная вершина 1.</param>
        /// <param name="unionVertex2">Объединенная вершина 2.</param>
        /// <param name="newUnionNumber">Номер новой объединенной вершины.</param>
        private void UnionOfTheVertexes(UnionVertex unionVertex1, UnionVertex unionVertex2, int newUnionNumber)
        {
            List<Vertex<int>> vertices1 = _unionVertexTable[unionVertex1.Number].Vertexes;
            List<Vertex<int>> vertices2 = _unionVertexTable[unionVertex2.Number].Vertexes;
            
            _unionVertexTable.Remove(unionVertex1.Number);
            _unionVertexTable.Remove(unionVertex2.Number);

            List<Vertex<int>> newVertices = new List<Vertex<int>>(vertices1);
            newVertices.AddRange(vertices2);

            foreach (Vertex<int> vertex in newVertices)
                vertex.Tag = newUnionNumber;

            int minStage = Math.Min(unionVertex1.Stage, unionVertex2.Stage);
            UnionVertex newUnionVertex = new UnionVertex(newUnionNumber, minStage + 1, newVertices);
            _unionVertexTable.Add(newUnionNumber, newUnionVertex);
        }

        /// <summary>
        /// Операция отчистки.
        /// </summary>
        private void ClearGraph()
        {
            List<Edge<int>> edgesToRemove = new List<Edge<int>>();

            foreach (UnionVertex unionVertex in _unionVertexTable.Values)
                for (int i = unionVertex.Vertexes.Count - 1; i >= 0; i--)
                {
                    Vertex<int> realVertex = unionVertex.Vertexes[i];
                    if (realVertex.Degree == 0)
                    {
                        unionVertex.Vertexes.Remove(realVertex);
                        continue;
                    }

                    foreach (Edge<int> edge in realVertex.OutputEdges)
                    {
                        Vertex<int> oppositeVertex = edge.GetPartnerVertex(realVertex);
                        if (unionVertex.Vertexes.Contains(oppositeVertex))
                            edgesToRemove.Add(edge);
                    }
                }

            foreach (Edge<int> edge in edgesToRemove)
                _deloneyGraph.RemoveEdge(edge);
        }
    }
}

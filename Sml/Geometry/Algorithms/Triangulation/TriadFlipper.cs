// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;
using Sml.Geometry.Data;

namespace Sml.Geometry.Algorithms.Triangulation
{
	internal class TriadFlipper
	{
		/// <summary>
		/// Треугольники триангуляции
		/// </summary>
		private readonly List<Triad> _triads;
		private readonly List<SmlSPoint2D> _points;

		public TriadFlipper(List<Triad> triads, List<SmlSPoint2D> points)
		{
			_triads = triads;
			_points = points;
		}

		public void DoWork(double fraction)
		{
			// Теперь нам нужно преобразовать треугольники не удовлетворяющие критериям Делоне
			int triadsCount = _triads.Count;
			bool[] idsA = new bool[triadsCount];
			bool[] idsB = new bool[triadsCount];

			// Мы поддерживаем "Список" треугольников, которые мы хотим перевернуть, чтобы 
			// распространиять любые последующие изменения
			// Когда количество поварачиваемых треугольников велико, лучше всего использовать массив (bool[])
			// Когда количество поварачиваемых треугольников мало, лучше всего использовать множество(Set<> )
			// Мы переключаемся между этими режимами, когда количество перевернутых треугольников уменьшается
			//
			// Окончание итерационного цикла включено для предотвращения вырожденных случаев "колебаний" и 
			// случаев, когда алгоритм не в состоянии остановиться.
			int flipped = FlipTriangles(_triads, idsA);

			int iterations = 1;
			while (flipped > (int)(fraction * triadsCount) && iterations < 1000)
			{
				// нечетные итерации
				if ((iterations & 1) == 1)
					flipped = FlipTriangles(_triads, idsA, idsB);
				else
					flipped = FlipTriangles(_triads, idsB, idsA);

				iterations++;
			}

			Set<int> idSetA = new Set<int>(), idSetB = new Set<int>();
			flipped = FlipTriangles(_triads,
				((iterations & 1) == 1) ? idsA : idsB, idSetA);

			iterations = 1;
			while (flipped > 0 && iterations < 2000)
			{
				if ((iterations & 1) == 1)
					flipped = FlipTriangles(_triads, idSetA, idSetB);
				else
					flipped = FlipTriangles(_triads, idSetB, idSetA);

				iterations++;
			}
		}

		/// <summary>
		/// Перевернуть(Flip) треугольники, несоответствующие триангуляции Делоне
		/// и записать в массив idsFlipped true для треугольников, которые необходимо переворчивать
		/// </summary>
		private int FlipTriangles(List<Triad> triads, bool[] idsFlipped)
		{
			int numt = triads.Count;
			Array.Clear(idsFlipped, 0, numt);

			int flipped = 0;
			for (int t = 0; t < numt; t++)
			{
				int t2;
				if (FlipTriangle(triads, t, out t2))
				{
					flipped += 2;
					idsFlipped[t] = true;
					idsFlipped[t2] = true;
				}
			}

			return flipped;
		}

		/// <summary>
		/// Перевернуть(Flip) треугольники, несоответствующие триангуляции Делоне
		/// </summary>
		private int FlipTriangles(List<Triad> triads, bool[] idsToTest, bool[] idsFlipped)
		{
			int numt = triads.Count;
			Array.Clear(idsFlipped, 0, numt);

			int flipped = 0;
			for (int t = 0; t < numt; t++)
			{
				if (idsToTest[t])
				{
					int t2;
					if (FlipTriangle(triads, t, out t2))
					{
						flipped += 2;
						idsFlipped[t] = true;
						idsFlipped[t2] = true;
					}
				}
			}

			return flipped;
		}

		/// <summary>
		/// Перевернуть(Flip) треугольники, несоответствующие триангуляции Делоне
		/// </summary>
		private int FlipTriangles(List<Triad> triads, bool[] idsToTest, Set<int> idsFlipped)
		{
			int numt = triads.Count;
			idsFlipped.Clear();

			int flipped = 0;
			for (int t = 0; t < numt; t++)
			{
				if (idsToTest[t])
				{
					int t2;
					if (FlipTriangle(triads, t, out t2))
					{
						flipped += 2;
						idsFlipped.Add(t);
						idsFlipped.Add(t2);
					}
				}
			}

			return flipped;
		}

		private int FlipTriangles(List<Triad> triads, Set<int> idsToTest, Set<int> idsFlipped)
		{
			int flipped = 0;
			idsFlipped.Clear();

			foreach (int t in idsToTest)
			{
				int t2;
				if (FlipTriangle(triads, t, out t2))
				{
					flipped += 2;
					idsFlipped.Add(t);
					idsFlipped.Add(t2);
				}
			}

			return flipped;
		}

		/// <summary>
		/// Снова проверить треугольник  с его 3 соседями и перевернуть его всесте с любым из соседей, чья
		/// противоположная точка находится внутри окружности, описанной вокруг данного треугольника
		/// </summary>
		/// <param name="triads">The triads</param>
		/// <param name="triadIndexToTest">The index of the triad to test</param>
		/// <param name="triadIndexFlipped">Index of adjacent triangle it was flipped with (if any)</param>
		/// <returns>true if the triad was flipped with any of its neighbours</returns>
		private bool FlipTriangle(List<Triad> triads, int triadIndexToTest, out int triadIndexFlipped)
		{
			int oppositeVertex, edge1, edge2, edge3, edge4;
			triadIndexFlipped = 0;

			Triad tri = triads[triadIndexToTest];
			// test all 3 neighbours of tri 

			if (tri.bc >= 0)
			{
				triadIndexFlipped = tri.bc;
				Triad t2 = triads[triadIndexFlipped];
				// find relative orientation (shared limb).
				t2.FindAdjacency(tri.b, triadIndexToTest, out oppositeVertex, out edge3, out edge4);
				if (tri.InsideCircumcircle(_points[oppositeVertex]))
				{  // not valid in the Delaunay sense.
					edge1 = tri.ab;
					edge2 = tri.ac;
					if (edge1 != edge3 && edge2 != edge4)
					{
						int tria = tri.a, trib = tri.b, tric = tri.c;
						tri.Initialize(tria, trib, oppositeVertex, edge1, edge3, triadIndexFlipped, _points);
						t2.Initialize(tria, tric, oppositeVertex, edge2, edge4, triadIndexToTest, _points);

						// change knock on triangle labels.
						if (edge3 >= 0)
							triads[edge3].ChangeAdjacentIndex(triadIndexFlipped, triadIndexToTest);
						if (edge2 >= 0)
							triads[edge2].ChangeAdjacentIndex(triadIndexToTest, triadIndexFlipped);
						return true;
					}
				}
			}


			if (tri.ab >= 0)
			{
				triadIndexFlipped = tri.ab;
				Triad t2 = triads[triadIndexFlipped];
				// find relative orientation (shared limb).
				t2.FindAdjacency(tri.a, triadIndexToTest, out oppositeVertex, out edge3, out edge4);
				if (tri.InsideCircumcircle(_points[oppositeVertex]))
				{  // not valid in the Delaunay sense.
					edge1 = tri.ac;
					edge2 = tri.bc;
					if (edge1 != edge3 && edge2 != edge4)
					{
						int tria = tri.a, trib = tri.b, tric = tri.c;
						tri.Initialize(tric, tria, oppositeVertex, edge1, edge3, triadIndexFlipped, _points);
						t2.Initialize(tric, trib, oppositeVertex, edge2, edge4, triadIndexToTest, _points);

						// change knock on triangle labels.
						if (edge3 >= 0)
							triads[edge3].ChangeAdjacentIndex(triadIndexFlipped, triadIndexToTest);
						if (edge2 >= 0)
							triads[edge2].ChangeAdjacentIndex(triadIndexToTest, triadIndexFlipped);
						return true;
					}
				}
			}

			if (tri.ac >= 0)
			{
				triadIndexFlipped = tri.ac;
				Triad t2 = triads[triadIndexFlipped];
				// find relative orientation (shared limb).
				t2.FindAdjacency(tri.a, triadIndexToTest, out oppositeVertex, out edge3, out edge4);
				if (tri.InsideCircumcircle(_points[oppositeVertex]))
				{  // not valid in the Delaunay sense.
					edge1 = tri.ab;   // .ac shared limb
					edge2 = tri.bc;
					if (edge1 != edge3 && edge2 != edge4)
					{
						int tria = tri.a, trib = tri.b, tric = tri.c;
						tri.Initialize(trib, tria, oppositeVertex, edge1, edge3, triadIndexFlipped, _points);
						t2.Initialize(trib, tric, oppositeVertex, edge2, edge4, triadIndexToTest, _points);

						// change knock on triangle labels.
						if (edge3 >= 0)
							triads[edge3].ChangeAdjacentIndex(triadIndexFlipped, triadIndexToTest);
						if (edge2 >= 0)
							triads[edge2].ChangeAdjacentIndex(triadIndexToTest, triadIndexFlipped);
						return true;
					}
				}
			}

			return false;
		}
	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Sml.DataStructures.Data;
using Sml.Geometry.Data;
using Sml.Graphs.Data;

/*
  copyright s-hull.org 2011
  released under the contributors beerware license

  contributors: Phil Atkin, Dr Sinclair, Jaroslav Gorin.
*/
namespace Sml.Geometry.Algorithms.Triangulation
{
	public class Triangulator
    {
        /// <summary>
        /// Параметр, введенный для оптимизации работы алгоритма:
        /// Отношение количества треугольников, которые необходимо повернуть, 
        /// к общему количеству треугольников 
        /// </summary>
        private const double Fraction = 0.3f;

        /// <summary>
        /// Параметр используемый при удалении дублирующих точек
        /// </summary>
	    private const double Epsilon = 0.05;


        private readonly List<SmlSPoint2D> _points;
        private readonly bool _rejectDuplicatePoints;
	    private readonly Graph<int> _deloneyGraph = new Graph<int>();

        /// <summary>
        /// Выпуклая оболочка
        /// </summary>
        private readonly Hull _hull = new Hull();

        /// <summary>
        /// Треугольники триангуляции
        /// </summary>
        private readonly List<Triad> _triads = new List<Triad>();

        /// <summary>
        /// Инициализация нового экзепляра класса <see cref="Triangulator"/>
        /// </summary>
        /// <param name="points">Точки для триангуляции.</param>
		/// <remarks>Список не должен содержать дубликатов!. Don't check for duplicate points</remarks>
        public Triangulator(List<SmlSPoint2D> points)
        {
            _points = points;
            _rejectDuplicatePoints = false;
        }

        /// <summary>
		/// Инициализация нового экзепляра класса <see cref="Triangulator"/>.
        /// </summary>
        /// <param name="points">Точки для триангуляции.</param>
        /// <param name="rejectDuplicatePoints">если <c>true</c>, удаляем дублирующиеся точки.
        ///  Optionally check for duplicate points
        /// </param>
        public Triangulator(List<SmlSPoint2D> points, bool rejectDuplicatePoints)
        {
            _points = points;
            _rejectDuplicatePoints = rejectDuplicatePoints;
        }

        /// <summary> Получить выпуклую оболочку </summary>
        /// <value> Выпуклая оболочка </value>
        public Hull Hull
        {
            get { return _hull; }
        }

        ///<summary> Получить треугольники триангуляции</summary>
        /// <value> Треугольники триангуляции </value>
        public List<Triad> Triangles
        {
            get { return _triads; }
        }

	    public Graph<int> DeloneyGraph
	    {
	        get { return _deloneyGraph; }
	    }

	    /// <summary>
        /// Основные вычисления.
        /// </summary>
        /// <param name="hullOnly">если <c>true</c>, то строим только оболочку.</param>
        private void MainCalculate(bool hullOnly)
        {
            _hull.Clear();
            _triads.Clear();

            if (_points.Count < 3)
                throw new TriangulateLogicalExeption("Number of points supplied must be >= 3");

            TPoint[] pointList = new TPoint[_points.Count];
            
            Triad firstTriad;
            int ptCount = GenerateSeedTriad(pointList, _points.Count, out firstTriad);

            _triads.Add(firstTriad);
            InitializeSeedHull();

            // Добавляет новую точку в оболочку, удаляя внутреннюю, и создает треугольники
	        for (int k = 3; k < ptCount; k++)
	            AddPointToTriangulation(hullOnly, pointList, k);
        }

	    /// <summary>
        /// Добавление k-ой точки в выпуклую оболочку и, если hullOnly == false, в триангуляцию.
        /// </summary>
        /// <param name="hullOnly">if set to <c>true</c> [hull only].</param>
        /// <param name="pointList">The point list.</param>
        /// <param name="k">The k.</param>
        private void AddPointToTriangulation(bool hullOnly, TPoint[] pointList, int k)
        {
            int newPtNumber = pointList[k].Number;
            HullVertex newPt = new HullVertex(_points, newPtNumber);

			IndexesTable iTable = new IndexesTable();
            //List<int> ptIndexList = new List<int>();
            //List<int> triadIndexList = new List<int>();

			int newPtNumberInHull = AddNewPtInTriangulation(newPt, iTable);

            // Если строим только оболочку, то выходим здесь
            if (hullOnly) return;
          
            //Номер, который должен быть присвоен первому ДОБАВЛЯЕМОМУ треугольнику на данной итерации
            int startedTriadCount = _triads.Count;

			AddTriadsGeneratedByNewPoint(newPtNumber, iTable);
            ModifyNewHullPointTriadIndex(startedTriadCount, newPtNumberInHull);
        }

        /// <summary>
        /// Добавление новой точки в триангуляцию 
        /// </summary>
        /// <param name="newPt">Новая точка.</param>
		/// <param name="iTable">Таблица индексов.</param>
        /// <returns>Номер(индекс) новой точки в выпуклой оболочке</returns>
		private int AddNewPtInTriangulation(HullVertex newPt, IndexesTable iTable)
        {
			// номер новой точки в оболочке
            int newPtNumberInHull; 

            double dx = newPt.X - _hull[0].X;
            double dy = newPt.Y - _hull[0].Y; // outwards pointing from hull[0] to pt.

            if (_hull.EdgeVisibleFrom(0, dx, dy))
            {
                // starting with a visible hull facet !!!
                // check to see if segment numh is also visible
                if (_hull.EdgeVisibleFrom(_hull.Count - 1, dx, dy))
                {
                    newPtNumberInHull = 0;
					AddPointVisibleAt0AndPenultPoints(newPt, iTable);
                }
                else
                {
                    newPtNumberInHull = 1; // keep pt hull[0]
					AddPointVisibleAt0PointAndInvisiblePenultPoint(newPt, iTable);
                }
            }
            else
				newPtNumberInHull = AddPointInvisibleAtPoint0(newPt, iTable);
            return newPtNumberInHull;
        }

        /// <summary>
        /// Для новой точки выпуклой оболочки задать номер треугольника инцидентного ей
        /// </summary>
        /// <param name="startedTriadCount">Номер первого добавленного на этом шаге треугольника.</param>
        /// <param name="newPtNumberInHull">Номер новой точки в выпуклой оболочке.</param>
        private void ModifyNewHullPointTriadIndex(int startedTriadCount, int newPtNumberInHull)
        {
            _hull[newPtNumberInHull].TriadIndex = _triads.Count - 1;
            if (newPtNumberInHull > 0)
                _hull[newPtNumberInHull - 1].TriadIndex = startedTriadCount;
            else
                _hull[_hull.Count - 1].TriadIndex = startedTriadCount;
        }

        /// <summary>
        /// Добавление новых треугольников, порожденных добавлением новой точки.
        /// </summary>
        /// <param name="newPtNumber">Номер новой точки в ptIndexList.</param>
		/// <param name="iTable">Таблица индексов.</param>
		private void AddTriadsGeneratedByNewPoint(int newPtNumber, IndexesTable iTable)
        {
			for (int p = 0; p < iTable.PointsIndexes.Count - 1; p++)
            {
				AddNewTriad(p, newPtNumber, iTable);
            }
            // Last edge is on the outside
            _triads[_triads.Count - 1].ac = -1;
        }

		/// <summary>
		/// Добавление нового треугольника
		/// </summary>
		/// <param name="p">Номер точки выпуклой оболочки.</param>
		/// <param name="newPtNumber">Номер новой точки в ptIndexList.</param>
		/// <param name="iTable">Таблица индексов.</param>
		private void AddNewTriad(int p, int newPtNumber, IndexesTable iTable)
        {
			Triad trx = new Triad(newPtNumber, iTable.PointsIndexes[p], iTable.PointsIndexes[p + 1]);
            trx.FindCircumcirclePrecisely(_points);

			trx.bc = iTable.TriadIndexes[p];
            if (p > 0)
                trx.ab = _triads.Count - 1;
            trx.ac = _triads.Count + 1;

			Triad neighborTriad = _triads[iTable.TriadIndexes[p]];
			ModifyNeighbor(trx, neighborTriad);
            _triads.Add(trx);
        }

        /// <summary>
        /// Модифицировать соседний треугольник 
        /// </summary>
        /// <param name="trx">Добавляемый треугольник.</param>
        /// <param name="txx">Соседний треугольник.</param>
		private void ModifyNeighbor(Triad trx, Triad txx)
        {
            //txx.a и txx.b
            if ((trx.b == txx.a && trx.c == txx.b) | (trx.b == txx.b && trx.c == txx.a))
                txx.ab = _triads.Count;
                //txx.a и txx.c
            else if ((trx.b == txx.a && trx.c == txx.c) | (trx.b == txx.c && trx.c == txx.a))
                txx.ac = _triads.Count;
                //txx.b и txx.c
            else if ((trx.b == txx.b && trx.c == txx.c) | (trx.b == txx.c && trx.c == txx.b))
                txx.bc = _triads.Count;
        }

        /// <summary>
        /// Добавление новой точки в триангуляцию, при условии, что она 
        /// видима из 0 точки оболочки и не видима из предпоследней точки оболочки
        /// </summary>
        /// <param name="newPt">Новая точка оболочки.</param>
		/// <param name="iTable">Таблица индексов.</param>
		private void AddPointVisibleAt0PointAndInvisiblePenultPoint(HullVertex newPt, IndexesTable iTable)
        {
			iTable.AddHullVertex(_hull[0]);

            for (int h = 1; h < _hull.Count; h++)
            {
                // if segment h is visible delete h  
				iTable.AddHullVertex(_hull[h]);
                if (_hull.EdgeVisibleFrom(h, newPt))
                {
                    // visible
                    _hull.RemoveAt(h);
                    h--;
                }
                else
                {
                    // первый невидимый
                    _hull.Insert(h, newPt);
                    break;
                }
            }
        }

        /// <summary>
        /// Добавление новой точки в триангуляцию, при условии, что она видима из 0 и предпоследней точек оболочки
        /// </summary>
        /// <param name="newPt">Точка которую хотим добавить.</param>
		/// <param name="iTable">Таблица индексов.</param>
		private void AddPointVisibleAt0AndPenultPoints(HullVertex newPt, IndexesTable iTable)
        {
            // visible.
            iTable.PointsIndexes.Add(_hull[_hull.Count - 1].PointsIndex);
			iTable.TriadIndexes.Add(_hull[_hull.Count - 1].TriadIndex);

            for (int h = 0; h < _hull.Count - 1; h++)
            {
                // if segment h is visible delete h
				iTable.PointsIndexes.Add(_hull[h].PointsIndex);
				iTable.TriadIndexes.Add(_hull[h].TriadIndex);
                if (_hull.EdgeVisibleFrom(h, newPt))
                {
                    _hull.RemoveAt(h);
                    h--;
                }
                else
                {
                    // quit on invisibility
                    _hull.Insert(0, newPt);
                    break;
                }
            }
            // look backwards through the hull structure
            for (int h = _hull.Count - 2; h > 0; h--)
            {
                // if segment h is visible delete h + 1
                if (_hull.EdgeVisibleFrom(h, newPt))
                {
					iTable.PointsIndexes.Insert(0, _hull[h].PointsIndex);
					iTable.TriadIndexes.Insert(0, _hull[h].TriadIndex);
                    _hull.RemoveAt(h + 1); // erase end of chain
                }
                else
                    break; // quit on invisibility
            }
        }

		/// <summary>
		/// Добавление новой точки в триангуляцию, при условии, что она не видима из 0ой точки оболочки
		/// </summary>
		/// <param name="newPt">The new pt.</param>
		/// <param name="iTable">Таблица индексов.</param>
		/// <returns></returns>
		private int AddPointInvisibleAtPoint0(HullVertex newPt, IndexesTable iTable)
        {
			int e1 = -1; // означает что точка внутри выпуклой оболчки
            int e2 = _hull.Count;

            for (int h = 1; h < _hull.Count; h++)
            {
                if (_hull.EdgeVisibleFrom(h, newPt))
                {
                    if (e1 < 0)
                        e1 = h; // Первая точка оболочки, из которой видна(visible) новая точка.
                }
                else
                {
                    if (e1 > 0)
                    {
                        // Первая точка (после е1) оболочки, из которой уже НЕ видна(invisible) новая точка.
                        e2 = h;
                        break;
                    }
                }
            }

            // triangle pidx starts at e1 and ends at e2 (inclusive).	
            if (e2 < _hull.Count)
                for (int e = e1; e <= e2; e++)
                {
					iTable.AddHullVertex(_hull[e]);
                }
            else
            {
                for (int e = e1; e < e2; e++)
                {
					iTable.AddHullVertex(_hull[e]);
                }
				iTable.PointsIndexes.Add(_hull[0].PointsIndex);
            }

            // erase elements e1+1 : e2-1 inclusive.
            // удалить элементы e1+1 : e2-1 включительно.
            if (e1 < e2 - 1)
                _hull.RemoveRange(e1 + 1, e2 - e1 - 1);

            // insert newHullPt at location e1+1.
            _hull.Insert(e1 + 1, newPt);
			return e1 + 1;
        }

        /// <summary>
        /// Проинициализировать "зерновую" оболочку.
        /// </summary>
        private void InitializeSeedHull()
        {
            _hull.Add(new HullVertex(_points, _triads[0].a));
            _hull.Add(new HullVertex(_points, _triads[0].b));
            _hull.Add(new HullVertex(_points, _triads[0].c));
        }

        /// <summary>
        /// Генерация "зерна" алгоритма.
        /// </summary>
        /// <param name="pointList">Список точек.</param>
        /// <param name="ptCount">Количество точек (после отсеивания дубликатов).</param>
        /// <param name="firstTriad">Треугольник, являющийся "зерном" алгоритма.</param>
        /// <returns></returns>
        private int GenerateSeedTriad(TPoint[] pointList, int ptCount, out Triad firstTriad)
        {
            // Choose first point as the seed
            for (int k = 0; k < ptCount; k++)
            {
                pointList[k].Number = k;
                pointList[k].Distance2ToCenter = _points[0].DistanceSqrTo(_points[k]);
            }

            // Sort by distance to seed point
            TPointComparer comparer = new TPointComparer();
            Array.Sort(pointList, comparer);

            // Duplicates are more efficiently rejected now we have sorted the vertices
            if (_rejectDuplicatePoints)
                ptCount = RejectDuplicatePoints(pointList, ptCount);

            int mid = -1;
            double circumCenterX = 0;
            double circumCenterY = 0;

            // Find the point which, with the first two points, creates the triangle with the smallest circumcircle
            double romin2 = double.MaxValue;
            firstTriad = new Triad(pointList[0].Number, pointList[1].Number, 2);

            for (int kc = 2; kc < ptCount; kc++)
            {
                firstTriad.c = pointList[kc].Number;
                if (firstTriad.FindCircumcirclePrecisely(_points) && firstTriad.circumcircleR2 < romin2)
                {
                    mid = kc;
                    // Centre of the circumcentre of the seed triangle
                    romin2 = firstTriad.circumcircleR2;
                    circumCenterX = firstTriad.circumcircleX;
                    circumCenterY = firstTriad.circumcircleY;
                }
                else if (romin2 * 4 < pointList[kc].Distance2ToCenter)
                    break;
            }

            if(mid == -1)
                throw new TriangulateLogicalExeption("Все точки лежат на одной прямой.");

            // Change the indices, if necessary, to make the 2th point produce the smallest circumcircle with the 0th and 1th
            if (mid != 2)
            {
                SwapMidAndSecondPositions(pointList, mid);
            }

            // Sort the remainder according to their distance from its centroid
            // Re-measure the points' distances from the centre of the circumcircle
            SmlSPoint2D center = new SmlSPoint2D(circumCenterX, circumCenterY);
            for (int k = 3; k < ptCount; k++)
                pointList[k].Distance2ToCenter = _points[pointList[k].Number].DistanceSqrTo(center);

            // Sort the _other_ points in order of distance to circumcentre
            Array.Sort(pointList, 3, ptCount - 3, comparer);

            // These three points are our seed triangle
            firstTriad.c = pointList[2].Number;
            firstTriad.MakeClockwise(_points);
            firstTriad.FindCircumcirclePrecisely(_points);

            return ptCount;
        }

        /// <summary>
        /// Поставить точку под индексом Mid на 2ую позицию.
        /// </summary>
        /// <param name="pointList">The point list.</param>
        /// <param name="mid">Номер точки, образующей с первыми 2мя минимальную окружность.</param>
        private static void SwapMidAndSecondPositions(TPoint[] pointList, int mid)
        {
            TPoint midPoint = pointList[mid];
            Array.Copy(pointList, 2, pointList, 3, mid - 2);
            pointList[2] = midPoint;
        }

        /// <summary>
        /// Исключить повторяющиеся точки из дальнейшего рассмотрения.
        /// </summary>
        /// <param name="pointList">The point list.</param>
        /// <param name="ptCount">Общее количество точек.</param>
        /// <returns>
        /// Количество неповторяющихся точек
        /// </returns>
        private int RejectDuplicatePoints(TPoint[] pointList, int ptCount)
        {
            // Search backwards so each removal is independent of any other
            for (int k = ptCount - 2; k >= 0; k--)
            {
                // If the points are identical then their distances will be the same,
                // so they will be adjacent in the sorted list
                if ((Math.Abs(_points[pointList[k].Number].X - _points[pointList[k + 1].Number].X) < Epsilon) &&
                    (Math.Abs(_points[pointList[k].Number].Y - _points[pointList[k + 1].Number].Y) < Epsilon))
                {
                    // Duplicates are expected to be rare, so this is not particularly efficient
                    Array.Copy(pointList, k + 2, pointList, k + 1, ptCount - k - 2);
                    
                    ptCount--;
                }
            }

            Debug.WriteLine((_points.Count - ptCount).ToString() + " duplicate points rejected");

            if (ptCount < 3)
                throw new TriangulateLogicalExeption("Number of unique points supplied must be >= 3");

            return ptCount;
        }
        
        /// <summary>
        /// Return the convex hull of the supplied points,
        /// </summary>
        /// <returns></returns>
        public void CalculateConvexHull()
        {
            MainCalculate(true);
        }

        /// <summary>
        /// Return the Delaunay triangulation of the supplied points
        /// </summary>
        public void Triangulate()
        {
            MainCalculate(false);
			FlipTriangles();
        }

		private void FlipTriangles()
		{
			TriadFlipper tf = new TriadFlipper(_triads, _points);
			tf.DoWork(Fraction);
		}

        public void ConvertTriadsToGraph()
        {
            Dictionary<long, SmlPair<int, int>> edgeTable = new Dictionary<long, SmlPair<int, int>>();
           
            foreach (Triad triad in _triads)
            {
                List<int> list = new List<int>(3) { triad.a, triad.b, triad.c };
                list.Sort();

                long hash1 = ((long)(list[0]) << 32) + list[1];
                edgeTable[hash1] = new SmlPair<int, int>(list[0], list[1]);

                long hash2 = ((long)(list[0]) << 32) + list[2];
                edgeTable[hash2] = new SmlPair<int, int>(list[0], list[2]);

                long hash3 = ((long)(list[1]) << 32) + list[2];
                edgeTable[hash3] = new SmlPair<int, int>(list[1], list[2]);
            }

            Dictionary<int, Vertex<int>> vertexTable = new Dictionary<int, Vertex<int>>();
            foreach (SmlPair<int, int> edge in edgeTable.Values)
            {
                if(!vertexTable.ContainsKey(edge.First))
                {
                    Vertex<int> firstVertex = _deloneyGraph.AddVertex(edge.First);
                    vertexTable.Add(edge.First, firstVertex);
                }

                if (!vertexTable.ContainsKey(edge.Second))
                {
                    Vertex<int> secondVertex = _deloneyGraph.AddVertex(edge.Second);
                    vertexTable.Add(edge.Second, secondVertex);
                }

                double distance = _points[edge.First].DistanceTo(_points[edge.Second]);
                _deloneyGraph.AddEdge(vertexTable[edge.First], vertexTable[edge.Second], distance);
            }
		}
    }
}

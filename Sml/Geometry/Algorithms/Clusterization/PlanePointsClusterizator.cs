// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;

using Sml.DataStructures.Data;
using Sml.Geometry.Algorithms.Triangulation;
using Sml.Geometry.Data;
using Sml.Graphs.Algorithms;
using Sml.Graphs.Data;

namespace Sml.Geometry.Algorithms.Clusterization
{
    public class PlanePointsClusterizator
    {
        private readonly List<SmlSPoint2D> _pointsList;
        
        /// <summary>
        /// Список кластеров - результат кластеризации
        /// </summary>
        private readonly List<ClusterInfo> _clusterList = new List<ClusterInfo>();

        /// <summary>
        /// Ребра между кластерами
        /// </summary>
        private readonly List<Edge<int>> _edgesBetweenClusters = new List<Edge<int>>();

        private Dictionary<Edge<int>, SmlPair<ClusterInfo, ClusterInfo>> _relationshipBetweenClusters = new Dictionary<Edge<int>, SmlPair<ClusterInfo, ClusterInfo>>();

        /// <summary>
        /// МОД (Минимальное остовное дерево)
        /// строится во время работы алгоритма кластеризации
        /// </summary>
        private Graph<int> _mst;

        /// <summary>
        /// Коэффициент, равный отношению текущего рассматриваемого ребра к 
        /// минимальному смежному ребру.
        /// </summary>
        private const double ClusteringFactor = 5;

        /// <summary>
        /// Коэффициент, аналогичный коэффициенту ClusteringFactor, но используемый в алгоритме агломеративной кластеризации.
        /// </summary>
        private const double AgglomerativeClusteringFactor = 5;

        public PlanePointsClusterizator(List<SmlSPoint2D> pointsList)
        {
            _pointsList = pointsList;
        }

        /// <summary>
        /// Получить список кластеров
        /// </summary>
        /// <value>
        /// Список кластеров
        /// </value>
        public List<ClusterInfo> ClusterList
        {
            get { return _clusterList; }
        }

        /// <summary>
        /// Получить ребра между кластерами
        /// </summary>
        /// <value>
        /// Ребра между кластерами
        /// </value>
        public List<Edge<int>> EdgesBetweenClusters
        {
            get { return _edgesBetweenClusters; }
        }

        /// <summary>
        /// Получить МОД (MST)
        /// </summary>
        /// <value>
        /// МОД (MST)
        /// </value>
        public Graph<int> MST
        {
            get { return _mst; }
        }

        public List<SmlSPoint2D> PointsList
        {
            get { return _pointsList; }
        }

        /// <summary>
        /// Кластеризация на основе теории графов.
        /// </summary>
        public void GraphClusterization()
        {
            CalculateClusterGraphs();
        }

        /// <summary>
        /// Кластеризация на основе теории графов + дальнейшая агломеративная кластеризация на основе 
        /// средних длин ребер внутри кластера (остовного дерева кластера).
        /// </summary>
        public void AgglomerativeClusterization()
        {
            CalculateClusterGraphs();
            AgglomerateGraphClusters();
        }

        /// <summary>
        /// Вычислить графы кластеров. Кластеризация на основе теории графов, на основе ЕМОД.
        /// </summary>
        private void CalculateClusterGraphs()
        {
            _mst = BuildMST();
                      
            foreach (Edge<int> edge in MST.Edges)
            {
                Edge<int> minEdge = GetNeighborMinEdge(edge);

                if (minEdge == null) continue;

                if (IsEdgeToRemove(edge, minEdge))
                    EdgesBetweenClusters.Add(edge);
            }

            RemoveEdgesFromMST(MST);
            GenerateClusters(MST);
        }

        /// <summary>
        /// Построить МОД (Минимальное остовное дерево).
        /// </summary>
        /// <returns>МОД (Минимальное остовное дерево)</returns>
        private Graph<int> BuildMST()
        {
            Triangulator triangulator = new Triangulator(PointsList, true);
            triangulator.Triangulate();
            triangulator.ConvertTriadsToGraph();
            Graph<int> deloneyGraph = triangulator.DeloneyGraph;

            if (deloneyGraph.Vertices.Count == 0) throw new Exception("Граф без вершин");

            Vertex<int> firstVertex = GetFirstVertex(deloneyGraph);

            Graph<int> mst = GraphAlgorithms.PrimsAlgorithm(deloneyGraph, firstVertex);
            return mst;
        }

        private static Vertex<int> GetFirstVertex(Graph<int> deloneyGraph)
        {
            Vertex<int> firstVertex = null;
            foreach (Vertex<int> vertex in deloneyGraph.Vertices)
            {
                firstVertex = vertex;
                break;
            }
            return firstVertex;
        }

        /// <summary>
        ///  Получение соседнего минимального ребра.
        /// </summary>
        private static Edge<int> GetNeighborMinEdge(Edge<int> edge)
        {
            double minLength = double.MaxValue;
            Edge<int> minEdge = null;

            List<Edge<int>> outputEdges = new List<Edge<int>>(edge.To.OutputEdges);
            outputEdges.AddRange(edge.From.OutputEdges);

            foreach (Edge<int> outputEdge in outputEdges)
                if (minLength > outputEdge.Weight && edge != outputEdge) //&& outputEdge.Weight > 50
                {
                    minLength = outputEdge.Weight;
                    minEdge = outputEdge;
                }
            return minEdge;
        }

        /// <summary>
        /// Условие для удаления ребра из ЕМОД.
        /// </summary>
        private static bool IsEdgeToRemove(Edge<int> edge, Edge<int> minEdge)
        {
            if (ClusteringFactor * minEdge.Weight < edge.Weight)
                return true;
            return false;
        }

        /// <summary>
        /// Условие для удаления ребра из ЕМОД.
        /// Анализируется не только соседние ребра, но и соседи минимального из соседних ребер.
        /// </summary>
        private static bool IsEdgeToRemove2(Edge<int> edge, Edge<int> minEdge)
        {
            if (ClusteringFactor * minEdge.Weight < edge.Weight)
            {
                Edge<int> neighborEdge = GetNeighborMinEdge(minEdge);
                if (ClusteringFactor * neighborEdge.Weight < edge.Weight)
                    return true;
            }
                
            return false;
        }

        /// <summary>
        /// Удаление ребер из МОД.
        /// </summary>
        private void RemoveEdgesFromMST(Graph<int> mst)
        {
            foreach (Edge<int> edge in EdgesBetweenClusters)
                mst.RemoveEdge(edge);
        }

        /// <summary>
        /// Создание кластеров на основе Минимального остовного дерева.
        /// </summary>
        private void GenerateClusters(Graph<int> mst)
        {
            Dictionary<Vertex<int>, List<Edge<int>>> neighborVertexTable = GenerateNeighborVertexTable();
            Dictionary<Vertex<int>, Vertex<int>> vertexMappingTable = new Dictionary<Vertex<int>, Vertex<int>>();

            foreach (Vertex<int> vertex in mst.Vertices)
                vertex.Tag = false;

            foreach (Vertex<int> vertex in mst.Vertices)
            {
                if ((bool) vertex.Tag)
                    continue;

                List<Vertex<int>> vertexesGivedBirthToCluster;
                ClusterInfo clusterInfo = CreateClusterWhichIncludeVertex(vertex, vertexMappingTable, out vertexesGivedBirthToCluster);

                InitializeClusterRelationships(neighborVertexTable, vertexesGivedBirthToCluster, clusterInfo);

                _clusterList.Add(clusterInfo);
            }

            UpdateClusterRelationships(vertexMappingTable);

        }

        private void UpdateClusterRelationships(Dictionary<Vertex<int>, Vertex<int>> vertexMappingTable)
        {
            Dictionary<Edge<int>, SmlPair<ClusterInfo, ClusterInfo>> newRelationshipBetweenClusters = new Dictionary<Edge<int>, SmlPair<ClusterInfo, ClusterInfo>>(_relationshipBetweenClusters.Count);
            foreach (KeyValuePair<Edge<int>, SmlPair<ClusterInfo, ClusterInfo>> oldRelation in _relationshipBetweenClusters)
            {
                Edge<int> oldEdge = oldRelation.Key;
                Edge<int> newEdge = new Edge<int>(vertexMappingTable[oldEdge.From], vertexMappingTable[oldEdge.To]);
                newEdge.Weight = oldEdge.Weight;

                newRelationshipBetweenClusters.Add(newEdge, oldRelation.Value);
            }

            _relationshipBetweenClusters = newRelationshipBetweenClusters;
        }

        private Dictionary<Vertex<int>, List<Edge<int>>> GenerateNeighborVertexTable()
        {
            Dictionary<Vertex<int>, List<Edge<int>>> vertexTable = new Dictionary<Vertex<int>, List<Edge<int>>>(EdgesBetweenClusters.Count*2);
            foreach (Edge<int> edge in EdgesBetweenClusters)
            {
                if (vertexTable.ContainsKey(edge.From))
                    vertexTable[edge.From].Add(edge);
                else
                    vertexTable.Add(edge.From, new List<Edge<int>> {edge});

                if (vertexTable.ContainsKey(edge.To))
                    vertexTable[edge.To].Add(edge);
                else
                    vertexTable.Add(edge.To, new List<Edge<int>> { edge });
            }
            return vertexTable;
        }

        /// <summary>
        /// Проинициализировать связи кластера с другими кластерами.
        /// </summary>
        private void InitializeClusterRelationships(Dictionary<Vertex<int>, List<Edge<int>>> vertexTable, List<Vertex<int>> vertexesGivedBirthToCluster, ClusterInfo clusterInfo)
        {
            foreach (Vertex<int> vert in vertexesGivedBirthToCluster)
            {
                if (vertexTable.ContainsKey(vert))
                {
                    foreach (Edge<int> edge in vertexTable[vert])
                    {
                        if (_relationshipBetweenClusters.ContainsKey(edge))
                            _relationshipBetweenClusters[edge].Second = clusterInfo;
                        else
                            _relationshipBetweenClusters.Add(edge, new SmlPair<ClusterInfo, ClusterInfo>(clusterInfo, null));
                    }
                }
            }
        }
   

        /// <summary>
        /// Создать кластер, содержащий вершину vertex.
        /// </summary>
        private ClusterInfo CreateClusterWhichIncludeVertex(Vertex<int> vertex, Dictionary<Vertex<int>, Vertex<int>> vertexMappingTable, out List<Vertex<int>> vertexesGivedBirthToClusterList)
        {
            Graph<int> cluster = new Graph<int>();
            vertexesGivedBirthToClusterList = new List<Vertex<int>> { vertex };
            List<Vertex<int>> activeVertexes = new List<Vertex<int>> { vertex };

            Dictionary<Edge<int>, object> edgeSet = new Dictionary<Edge<int>, object>();

            while (activeVertexes.Count != 0)
            {
                Vertex<int> newVertex = activeVertexes[0];
                foreach (Edge<int> edge in newVertex.OutputEdges)
                {
                    edgeSet[edge] = null;
                    Vertex<int> partnerVertex = edge.GetPartnerVertex(newVertex);
                    if (!(bool)partnerVertex.Tag)
                        activeVertexes.Add(partnerVertex);
                }

                newVertex.Tag = true;
                Vertex<int> addedVertex = cluster.AddVertex(newVertex.Data);

                vertexMappingTable.Add(newVertex, addedVertex);
                vertexesGivedBirthToClusterList.Add(newVertex);
                activeVertexes.Remove(newVertex);
            }

            foreach (Edge<int> edge in edgeSet.Keys)
                cluster.AddEdge(vertexMappingTable[edge.From], vertexMappingTable[edge.To], edge.Weight);

            return new ClusterInfo(cluster);
        }



        /// <summary>
        /// Агломеративная кластеризация.
        /// </summary>
        private void AgglomerateGraphClusters()
        {
            Dictionary<ClusterInfo, double> clusterTable = CalculateAvarageEdgeWeightInClusters();

            List<Edge<int>> edgesToAgglomerate = GetEdgesToAgglomerate(clusterTable);

            Dictionary<int, List<ClusterInfo>> unionClusterTable = GroupClusters(edgesToAgglomerate);

            RemoveFromClusterList(unionClusterTable);

            Dictionary<Vertex<int>, Vertex<int>> vertexMappingTable = new Dictionary<Vertex<int>, Vertex<int>>();
            Dictionary<int, ClusterInfo> unitedClusters = CombineClusters(unionClusterTable, vertexMappingTable);

            AddAgglomerativeEdges(unitedClusters, edgesToAgglomerate, vertexMappingTable);

            _clusterList.AddRange(new List<ClusterInfo>(unitedClusters.Values));
        }

        /// <summary>
        /// Удалить из списка кластеров, кластеры являющиеся состаными частями объединенных кластеров.
        /// </summary>
        private void RemoveFromClusterList(Dictionary<int, List<ClusterInfo>> unionClusterTable)
        {
            foreach (KeyValuePair<int, List<ClusterInfo>> pair in unionClusterTable)
            {
                foreach (ClusterInfo clusterInfo in pair.Value)
                {
                    _clusterList.Remove(clusterInfo);
                }
            }
        }

        /// <summary>
        /// Добавление ребер, ранее разделяющих кластеры
        /// </summary>
        private static void AddAgglomerativeEdges(Dictionary<int, ClusterInfo> unitedClusters, List<Edge<int>> edgesToAgglomerate, Dictionary<Vertex<int>, Vertex<int>> vertexMappingTable)
        {
            foreach (Edge<int> edge in edgesToAgglomerate)
            {
                int unionClusterNumber = (int) edge.Tag;
                Graph<int> clusterToAddEdge = unitedClusters[unionClusterNumber].Cluster;
                Edge<int> newEdge = new Edge<int>(vertexMappingTable[edge.From],vertexMappingTable[edge.To]);
                newEdge.Weight = edge.Weight;
                clusterToAddEdge.AddEdge(newEdge);
            }
        }

        /// <summary>
        /// Объединить кластеры.
        /// </summary>
        private static Dictionary<int, ClusterInfo> CombineClusters(Dictionary<int, List<ClusterInfo>> unionClusterTable, Dictionary<Vertex<int>, Vertex<int>> vertexMappingTable)
        {
            Dictionary<int, ClusterInfo> unitedClusters = new Dictionary<int, ClusterInfo>();
            foreach (KeyValuePair<int, List<ClusterInfo>> pair in unionClusterTable)
            {
                Graph<int> cluster = new Graph<int>(false);
                
                foreach (ClusterInfo clusterInfo in pair.Value)
                {
                    foreach (Vertex<int> vertex in clusterInfo.Cluster.Vertices)
                    {
                        Vertex<int> newVertex = cluster.AddVertex(vertex.Data);
                        vertexMappingTable.Add(vertex, newVertex);
                    }

                    foreach (Edge<int> edge in clusterInfo.Cluster.Edges)
                    {
                        Edge<int> newEdge = new Edge<int>(vertexMappingTable[edge.From],vertexMappingTable[edge.To]);
                        newEdge.Weight = edge.Weight;
                        cluster.AddEdge(newEdge);
                    }
                }

                ClusterInfo unitedCluster = new ClusterInfo(cluster);
                unitedClusters.Add(pair.Key, unitedCluster);
            }
            return unitedClusters;
        }

        /// <summary>
        /// Сгруппировать кластеры.
        /// </summary>
        private Dictionary<int, List<ClusterInfo>> GroupClusters(List<Edge<int>> edgesToAgglomerate)
        {
            int unionClusterCount = 0;
            Dictionary<int, List<ClusterInfo>> unionClusterTable = new Dictionary<int, List<ClusterInfo>>();
            Dictionary<int,List<Edge<int>>> edgesInUnionTable = new Dictionary<int, List<Edge<int>>>();
            foreach (Edge<int> edge in edgesToAgglomerate)
            {
                ClusterInfo firstCluster = _relationshipBetweenClusters[edge].First;
                ClusterInfo secondCluster = _relationshipBetweenClusters[edge].Second;

                if (firstCluster.NumberOfUnionCluster != int.MinValue && secondCluster.NumberOfUnionCluster != int.MinValue)
                {
                    List<ClusterInfo> clustersInSecondUnion = unionClusterTable[secondCluster.NumberOfUnionCluster];
                    unionClusterTable[firstCluster.NumberOfUnionCluster].AddRange(clustersInSecondUnion);

                    unionClusterTable.Remove(secondCluster.NumberOfUnionCluster);
 
                    List<Edge<int>> edges = edgesInUnionTable[secondCluster.NumberOfUnionCluster];
                    edgesInUnionTable[firstCluster.NumberOfUnionCluster].AddRange(edges);
                    edgesInUnionTable.Remove(secondCluster.NumberOfUnionCluster);
                    edgesInUnionTable[firstCluster.NumberOfUnionCluster].Add(edge);

                    foreach (ClusterInfo clusterInfo in clustersInSecondUnion)
                        clusterInfo.NumberOfUnionCluster = firstCluster.NumberOfUnionCluster;
                }
                else if (firstCluster.NumberOfUnionCluster != int.MinValue)
                {
                    secondCluster.NumberOfUnionCluster = firstCluster.NumberOfUnionCluster;
                    edgesInUnionTable[firstCluster.NumberOfUnionCluster].Add(edge);
                                        
                    List<ClusterInfo> clusterInfos = unionClusterTable[firstCluster.NumberOfUnionCluster];
                    clusterInfos.Add(secondCluster);
                }
                else if (secondCluster.NumberOfUnionCluster != int.MinValue)
                {
                    firstCluster.NumberOfUnionCluster = secondCluster.NumberOfUnionCluster;
                    edgesInUnionTable[secondCluster.NumberOfUnionCluster].Add(edge);
                    
                    List<ClusterInfo> clusterInfos = unionClusterTable[secondCluster.NumberOfUnionCluster];
                    clusterInfos.Add(firstCluster);
                }
                else
                {
                    firstCluster.NumberOfUnionCluster = unionClusterCount;
                    secondCluster.NumberOfUnionCluster = unionClusterCount;
                    edgesInUnionTable[unionClusterCount] = new List<Edge<int>> { edge };

                    List<ClusterInfo> clusters = new List<ClusterInfo> {firstCluster, secondCluster};
                    unionClusterTable.Add(unionClusterCount, clusters);
                    unionClusterCount++;
                }
            }

            foreach (KeyValuePair<int, List<Edge<int>>> pair in edgesInUnionTable)
                foreach (Edge<int> edge in pair.Value)
                    edge.Tag = pair.Key;

            return unionClusterTable;
        }

        private List<Edge<int>> GetEdgesToAgglomerate(Dictionary<ClusterInfo, double> clusterTable)
        {
            List<Edge<int>> edgesToAgglomerate = new List<Edge<int>>();
            foreach (KeyValuePair<Edge<int>, SmlPair<ClusterInfo, ClusterInfo>> edgePair in _relationshipBetweenClusters)
            {
                Edge<int> edge = edgePair.Key;
                ClusterInfo clusterInfo1 = edgePair.Value.First;
                ClusterInfo clusterInfo2 = edgePair.Value.Second;
                double avarageEdgeWeight1 = clusterTable[clusterInfo1];
                double avarageEdgeWeight2 = clusterTable[clusterInfo2];

                if (AgglomerativeClusteringFactor*avarageEdgeWeight1 > edge.Weight &&
                    AgglomerativeClusteringFactor*avarageEdgeWeight2 > edge.Weight)
                {
                    edgesToAgglomerate.Add(edge);
                }
            }

            return edgesToAgglomerate;
        }

        /// <summary>
        /// Вычислить средний вес ребер внутри кластеров.
        /// </summary>
        private Dictionary<ClusterInfo, double> CalculateAvarageEdgeWeightInClusters()
        {
            Dictionary<ClusterInfo, double> clusterTable = new Dictionary<ClusterInfo, double>(_clusterList.Count);
            foreach (ClusterInfo clusterInfo in _clusterList)
            {
                double avarageEdgeLength = 0;
                ICollection<Edge<int>> edges = clusterInfo.Cluster.Edges;

                foreach (Edge<int> edge in edges)
                    avarageEdgeLength += edge.Weight;

                avarageEdgeLength /= edges.Count;
                clusterTable.Add(clusterInfo, avarageEdgeLength);
            }
            return clusterTable;
        }
    }
}

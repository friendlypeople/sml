// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Sml.Geometry.Data;

namespace Sml.Geometry
{
    public static class GeomUtils
    {
        /// <summary>
        /// Точность округления по умолчанию
        /// </summary>
        public static double Delta = 1e-4f;
        
		/// <summary>
		/// Преобразование углов, больших чем два пи по модулю
		/// </summary>
		/// <param name="a">преобразуемый угол</param>
		/// <returns></returns>
		public static double Revolution(double a)
        {
			a = Math.Abs(((2 * Math.PI + a) / (2 * Math.PI) - Math.Floor((2 * Math.PI + a) / (2 * Math.PI))) * (2 * Math.PI));
            return a;
        }

		/// <summary>
		/// Округление до целого без точности
		/// </summary>
		/// <param name="a">Округляемое</param>
		/// <returns>результат округления</returns>
		public static double RoundingError(double a)
		{
            double a1 = Math.Floor(a);
            double a2 = Math.Ceiling(a);
            if (Math.Abs(Math.Abs(a) - Math.Abs(a1)) < Delta)
                return a1;
            if (Math.Abs(Math.Abs(a2) - Math.Abs(a)) < Delta)
                return a2;
            return a;       
		}

        /// <summary>
        /// Округление до целого позволяет задавать точнось
        /// </summary>
        /// <param name="a">Округляемое</param>
        /// <param name="delta">Точность</param>
        /// <returns>Результат округления</returns>
		public static double RoundingError(double a, double delta)
        {
            double a1 = Math.Floor(a);
            double a2 = Math.Ceiling(a);
            if (Math.Abs(Math.Abs(a) - Math.Abs(a1)) < delta)
                return a1;
            if (Math.Abs(Math.Abs(a2) - Math.Abs(a)) < delta)
                return a2;
            return a;
        }

		/// <summary>
		/// Метод позволяет определить, можно ли через 3 точки построить две перпендикулярные прямые
		/// </summary>
		/// <param name="index">Индекс точки в массиве, через которую проходит пересечение прямых</param>
		/// <param name="points">Массив трех точек</param>
		/// <returns>Возможно или нет</returns>
		public static bool IsToPerpendLinesExsit(SmlPoint2D[] points, out int index)
		{
		    SmlLine2D l1 = new SmlLine2D(points[0], points[1]);
		    SmlLine2D l2 = new SmlLine2D(points[1], points[2]);
		    SmlLine2D l3 = new SmlLine2D(points[2], points[0]);
		    if (l1.IsPerpendicular(l2))
            {
                index = 1;
                return true;
            }
		    if (l1.IsPerpendicular(l3))
            {
                index = 0;
                return true;
            }
            if (l2.IsPerpendicular(l3))
            {
                index = 2;
                return true;
            }
			index = -1;
		    return false;
		}

        /// <summary>
        /// Определение четвертой вершины параллелограмма
        /// </summary>
        /// <param name="pnts">Массив первых трех вершин</param>
        /// <param name="index">Индекс точки массива, соответсвующей смежной вершине (противоположной определяемой)</param>
        /// <returns>Четвертая вершина параллелограмма</returns>
        /// <exception cref="IndexOutOfRangeException">Вершина с таким индексом в массиве не найдена</exception>
        public static SmlPoint2D FindLastVertex(SmlPoint2D[] pnts, int index)
        {
            SmlPoint2D p = new SmlPoint2D();
            switch (index)
            {
                case 0:
                    {
                        SmlLine2D l1 = (new SmlLine2D(pnts[0], pnts[1])).GetParLine(pnts[2]);
                        SmlLine2D l2 = (new SmlLine2D(pnts[0], pnts[2])).GetParLine(pnts[1]);
                        l1.CrossPoint(l2, ref p);
                        break;
                    }
                case 1:
                    {
                        SmlLine2D l1 = (new SmlLine2D(pnts[1], pnts[0])).GetParLine(pnts[2]);
                        SmlLine2D l2 = (new SmlLine2D(pnts[1], pnts[2])).GetParLine(pnts[0]);
                        l1.CrossPoint(l2, ref p);
                        break;
                    }
                case 2:
                    {
                        SmlLine2D l1 = (new SmlLine2D(pnts[2], pnts[0])).GetParLine(pnts[1]);
                        SmlLine2D l2 = (new SmlLine2D(pnts[2], pnts[1])).GetParLine(pnts[0]);
                        l1.CrossPoint(l2, ref p);
                        break;
                    }
                default:
                    {
                        throw new IndexOutOfRangeException("Вершина с таким индексом в массиве не найдена");
                    }
            }
            return p;
        }

		///<summary>
		/// Пересчет полярных координат
		///</summary>
		public static void CartesianToPolar(double x, double y, out double alpha, out double d)
		{
            d = RoundingError(Math.Sqrt(Math.Pow(x, 2.0) + Math.Pow(y, 2.0)));
			alpha = CalculateAtan(x, y);
			//if ((x == 0) && (y == 0))
			//{
			//    alpha = 0;
			//    return; 
			//}
			//if (x == 0)
			//{
			//    alpha = (y > 0) ? Math.PI / 2 : 3 * Math.PI / 2;
			//    return; 
			//}
			//if (y < 0)
			//    alpha += 2 * Math.PI;
		}

		///<summary>
		/// Пересчет полярных координат
		///</summary>
		public static void CartesianToPolar(double[] coords, double[] polar)
		{
			CartesianToPolar(coords[0], coords[1], out polar[0], out polar[1]);
		}

		private static double CalculateAtan(double x, double y)
		{
			double fi = RoundingError(Math.Atan2(y, x));
			if ((x == 0) && (y == 0))
				fi = 0;
			else if (x == 0)
				fi = (y > 0) ? Math.PI / 2 : 3 * Math.PI / 2;
			else if (y < 0)
				fi += 2 * Math.PI;
			return fi;
		}

    	///<summary>
		/// Пересчет сферических координат
		///</summary>
		public static void CartesianToSpherical(double x, double y, double z, out double fi, out double r, out double teta)
		{
			r = RoundingError(Math.Sqrt(x * x + y * y + z * z));
			fi = CalculateAtan(x, y);

			double y1 = Math.Sqrt((x * x + y * y));
			teta = RoundingError(Math.Atan2(y1, z));
			if (z == 0) teta = 0;	
		}

		///<summary>
		/// Пересчет сферических координат
		///</summary>
		public static void CartesianToSpherical(double[] coord, double[] sphere)
		{
			CartesianToSpherical(coord[0], coord[1], coord[2], out sphere[0], out sphere[1], out sphere[2]);
		}

		///<summary>
		/// Пересчет декартовых координат
		/// </summary>
		public static void PolarToCartesian(double alpha, double d, out double x, out double y)
		{
            x = RoundingError(d * Math.Cos(alpha));
            y = RoundingError(d * Math.Sin(alpha));
		}

		///<summary>
		/// Пересчет декартовых координат
		/// </summary>
		public static void PolarToCartesian(double alpha, double d, double[] coords)
		{
			coords[0] = RoundingError(d * Math.Cos(alpha));
			coords[1] = RoundingError(d * Math.Sin(alpha));
		}

		///<summary>
		/// Пересчет декартовых координат
		/// </summary>
		public static void PolarToCartesian(double[] polar, double[] coords)
		{
			PolarToCartesian(polar[0], polar[1], coords);
		}

		///<summary>
		/// Пересчет декартовых координат
		/// </summary>
		public static void SphericalToCartesian(double fi, double r, double teta, out double x, out double y, out double z)
		{
			x = RoundingError(r * Math.Sin(teta) * Math.Cos(fi));
			y = RoundingError(r * Math.Sin(teta) * Math.Sin(fi));
			z = RoundingError(r * Math.Cos(teta));
		}

		///<summary>
		/// Пересчет декартовых координат
		/// </summary>
		public static void SphericalToCartesian(double[] sphere, double[] coords)
		{
			SphericalToCartesian(sphere[0], sphere[1], sphere[2], out coords[0], out coords[1], out coords[2]);
		}

		/// <summary>
		/// Поиск ограничивающего прямоугольника
		/// </summary>
		/// <param name="point2Ds">Список точек</param>
		/// <returns></returns>
		public static SmlBoundaryRect2D CreateBoundaryRectangle(List<SmlPoint2D> point2Ds)
		{
			double[] d = FindMinMaxPoint(point2Ds.ToArray(), 0, point2Ds.Count - 1);
			return new SmlBoundaryRect2D(new SmlPoint2D(d[0], d[1]), new SmlPoint2D(d[2], d[3]));
		}

		/// <summary>
		/// Поиск левой нижней и правой верхней точек описанного многоугольника 
		/// </summary>
		/// <param name="point2Ds">The point2 ds.</param>
		/// <returns>Возвращает [0] - Минимальную точку [1] - Максимальную точку</returns>
		public static SmlMinMaxPoints FindMinMaxPoint(params SmlPoint2D[] point2Ds)
		{
			double[] d = FindMinMaxPoint(point2Ds, 0, point2Ds.Length - 1);

			SmlMinMaxPoints minMaxPoints;
			minMaxPoints.MinPoint = new SmlPoint2D(d[0], d[1]);
			minMaxPoints.MaxPoint = new SmlPoint2D(d[2], d[3]);
			
			return minMaxPoints;
		}

    	private static double[] FindMinMaxPoint(SmlPoint2D[] point2Ds, int l, int r)
		{
			if (l == r)
				return new[] { point2Ds[l].X, point2Ds[l].Y, point2Ds[l].X, point2Ds[l].Y };

			int m = (l + r) / 2;
			double[] i = FindMinMaxPoint(point2Ds, l, m);
			double[] j = FindMinMaxPoint(point2Ds, m + 1, r);

			double[] ret = new double[4];
			ret[0] = (i[0] < j[0]) ? i[0] : j[0];
			ret[1] = (i[1] < j[1]) ? i[1] : j[1];
			ret[2] = (i[2] > j[2]) ? i[2] : j[2];
			ret[3] = (i[3] > j[3]) ? i[3] : j[3];
			return ret;
		}

		/// <summary>
		/// Преобразование радиан в градусы.
		/// </summary>
		/// <param name="radians">Радианы.</param>
		/// <returns>Градусы</returns>
		public static double ConvertFromRadiansToDegrees(double radians)
		{
			return radians * (180 / Math.PI);
		}

		/// <summary>
		/// Преобразование из градусов в радианы.
		/// </summary>
		/// <param name="degrees">Градусы.</param>
		/// <returns></returns>
		public static double ConvertFromDegreesToRadians(double degrees)
		{
			return degrees * (Math.PI / 180);
		}
    }
}

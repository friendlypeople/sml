// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Diagnostics;
using Sml.Common;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Реализации вестора в пространстве
	/// </summary>
	[DebuggerDisplay("X = {X} Y = {Y} Z = {Z} Fi = {Fi} R = {R} Teta = {Teta}")]
	public class SmlVector3D : SmlPoint3DImpl
	{
		 /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
		public SmlVector3D() { }


		/// <summary>
		/// Создание вектора по координатам
		/// </summary>
		/// <param name="x">Абсцисса вектора</param>
		/// <param name="y">Ордината вектора</param>
		/// <param name="z">Аппликата вектора</param>
		public SmlVector3D(double x, double y, double z) : base(x, y, z) { }


		/// <summary>
        /// Конструктор копирования
        /// </summary>
        /// <param name="v">Копируемый вектор</param>
		public SmlVector3D(SmlVector3D v) : base(v) { }


		/// <summary>
		/// Инициализация экземпляра класса <see cref="SmlVector3D"/>.
		/// </summary>
		/// <param name="v">Вектор на плоскости</param>
		public SmlVector3D(SmlVector2D v) : base(v.X, v.Y, 0) { }

		/// <summary>
		/// Инициализация экземпляра класса <see cref="SmlVector3D"/>.
		/// </summary>
		/// <param name="p">Точка на плоскости</param>
		public SmlVector3D(SmlPoint2D p) : base(p.X, p.Y, 0) { }

		/// <summary>
		/// Инициализация экземпляра класса <see cref="SmlVector3D"/>.
		/// </summary>
		/// <param name="p">Точка в пространстве</param>
		public SmlVector3D(SmlPoint3D p) : base(p.X, p.Y, p.Z) { }

		 /// <summary>
        ///Конструктор вектора по двум точкам p2------>p1
        /// </summary>
        /// <param name="p1">Конец вектора</param>
        /// <param name="p2">Начало вектора</param>
		public SmlVector3D(SmlPoint3D p1, SmlPoint3D p2)
        {
        	SmlVector3D vector2D = p1 - p2;
			_cartesC[0] = GeomUtils.RoundingError(vector2D.X);
			_cartesC[1] = GeomUtils.RoundingError(vector2D.Y);
			CartesianToSpherical();
        }

		/// <summary>
		/// Единичный орт-вектор по оси абсцисс (оси ОХ)
		/// </summary>
		public static SmlVector3D XAxis
		{
			get { return new SmlVector3D(1, 0, 0); }
		}

		/// <summary>
		/// Единичный орт-вектор по оси ординат (оси ОY)
		/// </summary>
		public static SmlVector3D YAxis
		{
			get { return new SmlVector3D(0, 1, 0); }
		}

		/// <summary>
		/// Единичный орт-вектор по оси ординат (оси ОY)
		/// </summary>
		public static SmlVector3D ZAxis
		{
			get { return new SmlVector3D(0, 0, 1); }
		}

		/// <summary>
		/// Проверяет, является ли вектор нулевым
		/// </summary>
		/// <value><c>true</c> если вектор нулевой; иначе,<c>false</c>.</value>
		public bool IsZeroVector
		{
			get { return Math.Abs(X) < GeomUtils.Delta && Math.Abs(Y) < GeomUtils.Delta && Math.Abs(Z) < GeomUtils.Delta; }
		}

		/// <summary>
		///Получить или задать длину вектора
		/// </summary>
		/// <value>Длина вектора</value>
		public double Length
		{
			get { return R; }
			set { R = value; }
		}

		/// <summary>
		/// Квадрат длины вектора
		/// </summary>
		public double LengthSquared
		{
			get { return R * R; }
		}

		/// <summary>
		/// Угол между вызывающим вектором и вектором v
		/// </summary>
		/// <param name="vec">Вектор, с которым вычисляется угол</param>
		/// <returns>Угол между векторами</returns>
		/// <remarks>Угол вычисляется в диапозоне от 0 до 180 градусов</remarks>
		public SmlAngle GetAngleTo(SmlVector3D vec)
		{
			if (vec.IsZeroVector || IsZeroVector)
				throw new InfinityException("Невозможно определить угол между нулевым вектором");
			double u = ScalarProduct(this, vec);
			double v = Length * vec.Length;

			return new SmlAngle(Math.Acos(u / v));
		}

		/// <summary>
		/// Преобразовать вектор в точку - конец вектора
		/// </summary>
		/// <returns></returns>
		public SmlPoint3D ToPoint3D()
		{
			return new SmlPoint3D(X, Y, Z);
		}

		/// <summary>
		/// Преобразовать вектор в точку - конец вектора
		/// </summary>
		/// <returns></returns>
		public SmlPoint2D ToPoint2D()
		{
			return new SmlPoint2D(X, Y);
		}

		/// <summary>
		/// Создает вектор перпендикулярный данному, используя для направления опорную точку
		/// </summary>
		/// <param name="pt">Опорная точка задающая направление.</param>
		/// <returns>Вектор перпендикулярный исходному</returns>
		public SmlVector3D CreatePerpendicular(SmlPoint3D pt)
		{
			SmlVector3D vec = pt.ToSmlVector3D();
			if (IsCollinear(vec))
				throw new Exception("Нельзя создать перпендикуляр на двух коллиниарных векторах");

			SmlVector3D orto = vec.OrtoProject(this);
			return pt - orto.ToPoint3D();
		}

		/// <summary>
		/// Получить ортогональную проекцию текущего вектора на заданную ось.
		/// </summary>
		/// <param name="axis">Ось, на которую осуществляется проекция</param>
		/// <returns>Полученная проекция</returns>
		public SmlVector3D OrtoProject(SmlVector3D axis)
		{
			SmlVector3D i = axis.UnitCreate();
			double sc = ScalarProduct(axis);
			return i * sc;
		}

		/// <summary>
		/// Оператор сложения векторов
		/// </summary>
		/// <param name="v1">1-е слагаемое</param>
		/// <param name="v2">2-е слагаемое</param>
		/// <returns>Сумма векторов</returns>
		static public SmlVector3D operator +(SmlVector3D v1, SmlVector3D v2)
		{
			return new SmlVector3D(GeomUtils.RoundingError(v1.X + v2.X),
				GeomUtils.RoundingError(v1.Y + v2.Y),
				GeomUtils.RoundingError(v1.Z + v2.Z)
				);
		}

		/// <summary>
		/// Оператор сложения векторов
		/// </summary>
		/// <param name="v1">1-е слагаемое</param>
		/// <param name="v2">2-е слагаемое</param>
		/// <returns>Сумма векторов</returns>
		static public SmlVector3D operator +(SmlVector3D v1, SmlVector2D v2)
		{
			return new SmlVector3D(GeomUtils.RoundingError(v1.X + v2.X),
				GeomUtils.RoundingError(v1.Y + v2.Y), v1.Z);
		}

		/// <summary>
		/// Оператор разности двух векторов
		/// </summary>
		/// <param name="v1">Вектор-уменьшаемое</param>
		/// <param name="v2">Вектор-вычитаемое</param>
		/// <returns>Вектор-разность</returns>
		static public SmlVector3D operator -(SmlVector3D v1, SmlVector3D v2)
		{
			return new SmlVector3D(
				GeomUtils.RoundingError(v1.X - v2.X),
				GeomUtils.RoundingError(v1.Y - v2.Y),
				GeomUtils.RoundingError(v1.Z - v2.Z));
		}

		/// <summary>
		/// Оператор разности двух векторов
		/// </summary>
		/// <param name="v1">Вектор-уменьшаемое</param>
		/// <param name="v2">Вектор-вычитаемое</param>
		/// <returns>Вектор-разность</returns>
		static public SmlVector3D operator -(SmlVector3D v1, SmlVector2D v2)
		{
			return new SmlVector3D(
				GeomUtils.RoundingError(v1.X - v2.X),
				GeomUtils.RoundingError(v1.Y - v2.Y), v1.Z);
		}

		/// <summary>
		/// Умножение вектора на число
		/// </summary>
		/// <param name="v">умножаемый вектор</param>
		/// <param name="k">число-множитель</param>
		/// <returns></returns>
		static public SmlVector3D operator *(SmlVector3D v, double k)
		{
			return new SmlVector3D(GeomUtils.RoundingError(v.X * k),
				GeomUtils.RoundingError(v.Y * k),
				GeomUtils.RoundingError(v.Z * k));
		}

		/// <summary>
		/// Деление вектора на число
		/// </summary>
		/// <param name="v">Делимый вектор</param>
		/// <param name="k">число-множитель</param>
		/// <returns></returns>
		static public SmlVector3D operator /(SmlVector3D v, double k)
		{
			return new SmlVector3D(GeomUtils.RoundingError(v.X / k),
				GeomUtils.RoundingError(v.Y / k),
				GeomUtils.RoundingError(v.Z / k));
		}

		/// <summary>
		/// Скалярное умножение двух векторов
		/// </summary>
		/// <param name="v1">первый перемножаемый вектор</param>
		/// <param name="v2">второй перемножаемый вектор</param>
		/// <returns>Скаляр - результат перемножения</returns>
		static public double ScalarProduct(SmlVector3D v1, SmlVector3D v2)
		{
			return GeomUtils.RoundingError(v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z);
		}

        /// <summary>
		/// Скалярное умножение двух векторов
		/// </summary>
		/// <param name="v">перемножаемый вектор</param>
		/// <returns>Скаляр - результат перемножения</returns>
		public double ScalarProduct(SmlVector3D v)
		{
			return GeomUtils.RoundingError(X * v.X + Y * v.Y + Z * v.Z);
		}

		/// <summary>
		/// Смешанное произведение трех векторов
		/// </summary>
		/// <param name="v1">первый перемножаемый вектор</param>
		/// <param name="v2">второй перемножаемый вектор</param>
		/// <param name="v3">третий перемножаемый вектор</param>
		/// <returns>Скаляр - результат перемножения</returns>
		/// <remarks>(v1, [v2, v3])</remarks>
		static public double ScalarTripleProduct(SmlVector3D v1, SmlVector3D v2, SmlVector3D v3)
		{
			return ScalarProduct(v1, VectorProduct(v2, v3));
		}

		/// <summary>
		/// Смешанное произведение трех векторов
		/// </summary>
		/// <param name="v1">первый перемножаемый вектор</param>
		/// <param name="v2">второй перемножаемый вектор</param>
		/// <returns>Скаляр - результат перемножения</returns>
		/// <remarks>(this, [v1, v2])</remarks>
		public double ScalarTripleProduct(SmlVector3D v1, SmlVector3D v2)
		{
			return ScalarProduct(this, VectorProduct(v1, v2));
		}
		
		/// <summary>
		/// Векторное умножение двух векторов
		/// </summary>
		/// <param name="v1">первый перемножаемый вектор</param>
		/// <param name="v2">второй перемножаемый вектор</param>
		/// <returns>Вектор - результат перемножения</returns>
		static public SmlVector3D VectorProduct(SmlVector3D v1, SmlVector3D v2)
		{
			return new SmlVector3D(v1.Z * v2.Y - v1.Y * v2.Z,
								v1.X * v2.Z - v1.Z * v2.X,
								v1.Y * v2.X - v1.X * v2.Y);
		}

		/// <summary>
		/// Векторное умножение двух векторов
		/// </summary>
		/// <param name="v">перемножаемый вектор</param>
		/// <returns>Вектор - результат перемножения</returns>
		public SmlVector3D VectorProduct(SmlVector3D v)
		{
			return VectorProduct(this, v);
		}

		/// <summary>
		/// Перепендикулярен ли вызывающий вектор с вектором v
		/// </summary>
		/// <param name="v">Сравниваемый вектор</param>
		/// <param name="delta">Точность</param>
		/// <returns> <c>true</c> если v перпендикулярен; иначе, <c>false</c>.  </returns>
		public bool IsPerpendicular(SmlVector3D v, double delta)
		{
			if (IsZeroVector || v.IsZeroVector)
				return true;
			SmlAngle angle = GetAngleTo(v);
			return angle.IsRightAngle(delta);
		}

		/// <summary>
		/// Перепендикулярен ли вызывающий вектор с вектором v
		/// </summary>
		/// <param name="v">Сравниваемый вектор</param>
		/// <returns> <c>true</c> если v перпендикулярен; иначе, <c>false</c>.  </returns>
		public bool IsPerpendicular(SmlVector3D v)
		{
			return IsPerpendicular(v, GeomUtils.Delta);
		}

		/// <summary>
		/// Коллинеарен ли вызывающий вектор с вектором v
		/// </summary>
		/// <param name="v">Сравниваемый вектор</param>
		/// <param name="delta">Точность</param>
		/// <returns><c>true</c> если коллинеарен; иначе, <c>false</c>.</returns>
		/// <remarks>Нулевой вектор коллиниарен любому</remarks>
		public bool IsCollinear(SmlVector3D v, double delta)
		{
			if (IsZeroVector || v.IsZeroVector)
				return true;

			SmlAngle angle = GetAngleTo(v);
			return angle.IsZeroAngle(delta) || angle.IsStraightAngle(delta);
		}

		

		/// <summary>
		/// Коллинеарен ли вызывающий вектор с вектором v
		/// </summary>
		/// <param name="v">Сравниваемый вектор</param>
		/// <returns><c>true</c> если коллинеарен; иначе, <c>false</c>.</returns>
		/// <remarks>Нулевой вектор коллиниарен любому</remarks>
		public bool IsCollinear(SmlVector3D v)
		{
			return IsCollinear(v, GeomUtils.Delta);
		}

		/// <summary>
		/// Сонаправлен ли вызывающий вектор с вектором v
		/// </summary>
		/// <param name="v">Сравниваемый вектор</param>
		/// <returns> <c>true</c> если соноправлен; иначе, <c>false</c>. </returns>
		public bool IsCodirectional(SmlVector3D v)
		{
			return IsCodirectional(v, GeomUtils.Delta);
		}


		/// <summary>
		/// Сонаправлен ли вызывающий вектор с вектором v
		/// </summary>
		/// <param name="v">Сравниваемый вектор</param>
		/// <param name="delta">Точность</param>
		/// <returns><c>true</c> если вектор сонаправлен вектору v; иначе, <c>false</c>.</returns>
		public bool IsCodirectional(SmlVector3D v, double delta)
		{
			if (IsZeroVector || v.IsZeroVector)
				return true;

			SmlAngle angle = GetAngleTo(v);
			return angle.IsZeroAngle(delta);
		}

		/// <summary>
		/// Разворот вызывающего вектора в противоположную сторону
		/// </summary>
		public void Negate()
		{
			_cartesC[0] = -_cartesC[0];
			_cartesC[1] = -_cartesC[1];
			_cartesC[2] = -_cartesC[2];
			CartesianToSpherical();
		}

		/// <summary>
		/// Создание противоположного вектора
		/// </summary>
		/// <returns></returns>
		public SmlVector2D NegateCreate()
		{
			return new SmlVector2D(-X, -Y);
		}

		/// <summary>
		/// Поворот вектора вокруг оси ОХ.
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Результирующий вектор.</returns>
		public SmlVector3D RotateX(double angle)
		{
			return RotateX(new SmlAngle(angle));
		}

		/// <summary>
		/// Поворот вектора вокруг оси ОХ.
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Результирующий вектор.</returns>
		public SmlVector3D RotateX(SmlAngle angle)
		{
			return new SmlVector3D(X, angle.Cos * Y - angle.Sin * Z, angle.Sin * Y + angle.Cos * Z);
		}

		/// <summary>
		/// Поворот вектора вокруг оси ОY.
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Результирующий вектор.</returns>
		public SmlVector3D RotateY(double angle)
		{
			return RotateY(new SmlAngle(angle));
		}

		/// <summary>
		/// Поворот вектора вокруг оси ОY.
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Результирующий вектор.</returns>
		public SmlVector3D RotateY(SmlAngle angle)
		{
			return new SmlVector3D(angle.Cos * X + angle.Sin * Z, Y, angle.Cos * Z - angle.Sin * X);
		}

		/// <summary>
		/// Поворот вектора вокруг оси ОZ.
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Результирующий вектор.</returns>
		public SmlVector3D RotateZ(double angle)
		{
			return RotateZ(new SmlAngle(angle));
		}

		/// <summary>
		/// Поворот вектора вокруг оси ОZ.
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Результирующий вектор.</returns>
		public SmlVector3D RotateZ(SmlAngle angle)
		{
			return new SmlVector3D(angle.Cos * X - angle.Sin * Y, angle.Sin * X + angle.Cos * Y, Z);
		}

		/// <summary>
		/// Равны ли два вектора
		/// </summary>
		/// <param name="v1">Первый вектор</param>
		/// <param name="v2">Второй вектор</param>
		/// <returns></returns>
		static public bool operator ==(SmlVector3D v1, SmlVector3D v2)
		{
			return EqualsImpl(v1, v2);
		}

		/// <summary>
		/// Преобразование вызывающего вектора к единичному
		/// </summary>
		public void Unit()
		{
			R = 1.0;
			SphericalToCartesian();
		}

		/// <summary>
		/// Создание сонаправленного единичного вектора
		/// </summary>
		/// <returns>Созданный еденичный вектор</returns>
		public SmlVector3D UnitCreate()
		{
			SmlVector3D v = new SmlVector3D(this);
			v.Unit();
			return v;
		}

		/// <summary>
		/// Не равны ли два вектора
		/// </summary>
		/// <param name="v1">первый вектор</param>
		/// <param name="v2">второй вектор</param>
		/// <returns></returns>
		static public bool operator !=(SmlVector3D v1, SmlVector3D v2)
		{
			return !(v1 == v2);
		}

		/// <summary>
		/// Больше ли первый вектор чем второй
		/// </summary>
		/// <param name="v1">Первый вектор</param>
		/// <param name="v2">Второй вектор</param>
		/// <returns>Результат сравнения</returns>
		static public bool operator > (SmlVector3D v1, SmlVector3D v2)
		{
			if (v1.X == v2.X)
			{
				if (v1.Y == v2.Y)
					return v1.Z > v2.Z;
				return v1.Y > v2.Y;
			}
			return v1.X > v2.X;
		}

		/// <summary>
		/// Меньше ли первый вектор чем второй
		/// </summary>
		/// <param name="v1">Первый вектор</param>
		/// <param name="v2">Второй вектор</param>
		/// <returns>Результат сравнения</returns>
		static public bool operator < (SmlVector3D v1, SmlVector3D v2)
		{
			if (v1.X == v2.X)
			{
				if (v1.Y == v2.Y)
					return v1.Z < v2.Z;
				return v1.Y < v2.Y;
			}
			return v1.X < v2.X;
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns> A <see cref="System.String"/> that represents this instance. </returns>
		public override string ToString()
		{
			return "(" + X + ";" + Y + ";" + Z + ")";
		}

		/// <summary>
		/// Equalses the specified other.
		/// </summary>
		/// <param name="other">The other.</param>
		/// <returns></returns>
		public bool Equals(SmlVector3D other)
		{
			return EqualsImpl(this, other);
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof (SmlVector3D)) return false;
			return Equals((SmlVector3D) obj);
		}

		

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>
		/// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
		/// </returns>
		public override int GetHashCode()
		{
			return HashCode.GetHashCode(X.GetHashCode(), Y.GetHashCode(), Z.GetHashCode());
		}
	}
}

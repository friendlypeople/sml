// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Diagnostics;
using Sml.Common;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Представление плоскости в пространстве
	/// </summary>
	[DebuggerDisplay("A = {A} B = {B} C = {C} D = {D}")]
	public class SmlPlane
	{
		private readonly double[] _koeffs = new double[4];


		/// <summary>
		/// Конструктор копирования
		/// </summary>
		/// <param name="plane">Исходная плоскость</param>
		public SmlPlane(SmlPlane plane)
		{
			for (int i = 0; i < _koeffs.Length; i++)
			{
				_koeffs[i] = plane._koeffs[i];
			}
		}

		/// <summary>
		/// Конструктор создания плоскости по нормали
		/// </summary>
		/// <param name="normal">Нормаль к плоскости</param>
		/// <param name="d">Коэффициент D уровнения плоскости</param>
		public SmlPlane(SmlVector3D normal, double d)
		{
			_koeffs[0] = normal.X;
			_koeffs[1] = normal.Y;
			_koeffs[2] = normal.Z;
			_koeffs[3] = d;
		}

		/// <summary>
		/// Конструктор создания плоскости по нормали
		/// </summary>
		/// <param name="normal">Нормаль к плоскости</param>
		/// <param name="pointOnPlane">Точка на плоскости.</param>
		public SmlPlane(SmlVector3D normal, SmlPoint3D pointOnPlane)
		{
			_koeffs[0] = normal.X;
			_koeffs[1] = normal.Y;
			_koeffs[2] = normal.Z;
			D = - normal.ScalarProduct(pointOnPlane.ToSmlVector3D());
		}

		/// <summary>
		/// Конструктор создания плоскости по прямой пепедикулярной плоскости и точки на плоскости
		/// </summary>
		/// <param name="line">Прямая пепедикулярная плоскости</param>
		/// <param name="pointOnPlane">Точка на плоскости.</param>
		public SmlPlane(SmlLine3D line, SmlPoint3D pointOnPlane)
		{
			_koeffs[0] = line.GuidingVector.X;
			_koeffs[1] = line.GuidingVector.Y;
			_koeffs[2] = line.GuidingVector.Z;
			D = -line.GuidingVector.ScalarProduct(pointOnPlane.ToSmlVector3D());
		}

		/// <summary>
		/// Конструктор создания плоскости по 3 точкам
		/// </summary>
		/// <param name="pt1">Точка на плоскости.</param>
		/// <param name="pt2">Точка на плоскости.</param>
		/// <param name="pt3">Точка на плоскости.</param>
		public SmlPlane(SmlPoint3D pt1, SmlPoint3D pt2, SmlPoint3D pt3)
		{
			SmlVector3D vec1 = pt2 - pt1;
			SmlVector3D vec2 = pt3 - pt1;
			SmlVector3D normal = vec1.VectorProduct(vec2);
			normal.Unit();
			_koeffs[0] = normal.X;
			_koeffs[1] = normal.Y;
			_koeffs[2] = normal.Z;
			D = -normal.ScalarProduct(pt1.ToSmlVector3D());
		}

		/// <summary>
		/// Конструктор создания плоскости по коффициентам
		/// </summary>
		/// <param name="a">Коэффициент A уровнения плоскости</param>
		/// <param name="b">Коэффициент B уровнения плоскости</param>
		/// <param name="c">Коэффициент C уровнения плоскости</param>
		/// <param name="d">Коэффициент D уровнения плоскости</param>
		public SmlPlane(double a, double b, double c, double d)
		{
			A = a;
			B = b;
			C = c;
			D = d;
		}

		/// <summary>
		/// Коэффициент A уровнения плоскости Ax + By + Cz + D
		/// </summary>
		public double A
		{
			get { return _koeffs[0]; }
			set { _koeffs[0] = value; }
		}

		/// <summary>
		/// Коэффициент B уровнения плоскости Ax + By + Cz + D
		/// </summary>
		public double B
		{
			get { return _koeffs[1]; }
			set { _koeffs[1] = value; }
		}

		/// <summary>
		/// Коэффициент C уровнения плоскости Ax + By + Cz + D
		/// </summary>
		public double C
		{
			get { return _koeffs[2]; }
			set { _koeffs[2] = value; }
		}

		/// <summary>
		/// Коэффициент D уровнения плоскости Ax + By + Cz + D
		/// </summary>
		public double D
		{
			get { return _koeffs[3]; }
			set { _koeffs[3] = value; }
		}

		/// <summary>
		/// Gets or sets the plane normal vector.
		/// </summary>
		/// <value>The plane normal vector.</value>
		public SmlVector3D Normal
		{
			get
			{
				return new SmlVector3D (A, B, C);
			}

			set
			{
				A = value.X;
				B = value.Y;
				C = value.Z;
			}
		}

		/// <summary>
		/// Создать плоскость с противоположно направленной нормалью 
		/// </summary>
		public void Negate()
		{
			for (int i = 0; i < _koeffs.Length; i++)
			{
				_koeffs[i] = - _koeffs[i];
			}
		}

		/// <summary>
		/// Создать плоскость с противоположно направленной нормалью 
		/// </summary>
		/// <returns>плоскость с противоположно направленной нормалью</returns>
		public SmlPlane NegateCreate()
		{
			return new SmlPlane(-A, -B, -C, -D);
		}

		/// <summary>
		/// Определить угол между плоскостью
		/// </summary>
		/// <param name="plane">Плоскость с которой определяется угол</param>
		/// <returns>Угол между плоскостью</returns>
		public SmlAngle GetAngleTo(SmlPlane plane)
		{
			if (Normal.IsZeroVector || plane.Normal.IsZeroVector)
				throw new InfinityException("Невозможно определить угол для нулевого вектора");
			double u = SmlVector3D.ScalarProduct(Normal, plane.Normal);
			double v = Normal.Length * plane.Normal.Length;
			return SmlAngle.CreateCosAngle(u / v);
		}

		/// <summary>
		/// Расстояние до параллельной плоскости 
		/// </summary>
		/// <param name="pl">Плоскость</param>
		/// <returns>Расстояние до плоскости </returns>
		public double Distance(SmlPlane pl)
		{
			if (!IsParallel(pl))
				throw new Exception("Плоскости не параллельны");
			
			double u = Math.Abs(D - pl.D);
			double v = Math.Sqrt(A * A + B * B + C * C);
			return u / v;
		}

		/// <summary>
		/// Определяет параллельность плоскости к заданной
		/// </summary>
		/// <param name="pl">Сравниваемая плоскость</param>
		/// <returns><c>true</c>если плоскости параллельны; иначе, <c>false</c>.</returns>
		public bool IsParallel(SmlPlane pl)
		{
			return Normal.IsCollinear(pl.Normal);
		}

		/// <summary>
		/// Определяет параллельность плоскости к заданной
		/// </summary>
		/// <param name="pl">Сравниваемая плоскость</param>
		/// <param name="delta">Точность</param>
		/// <returns><c>true</c>если плоскости параллельны; иначе, <c>false</c>.</returns>
		public bool IsParallel(SmlPlane pl, double delta)
		{
			return Normal.IsCollinear(pl.Normal, delta);
		}

		/// <summary>
		/// Определяет перпендикулярность плоскости к заданной
		/// </summary>
		/// <param name="v">Сравниваемая плоскость</param>
		/// <param name="delta">Точность</param>
		/// <returns> <c>true</c> если плоскости перпендикулярны; иначе, <c>false</c>.</returns>
		public bool IsPerpendicular(SmlPlane v, double delta)
		{
			return Math.Abs(SmlVector3D.ScalarProduct(Normal, v.Normal)) < delta;
		}

		/// <summary>
		/// Определяет перпендикулярность плоскости к заданной
		/// </summary>
		/// <param name="v">Сравниваемая плоскость</param>
		/// <returns> <c>true</c> если плоскости перпендикулярны; иначе, <c>false</c>.</returns>
		public bool IsPerpendicular(SmlPlane v)
		{
			return IsPerpendicular(v, GeomUtils.Delta);
		}


		/// <summary>
		/// Расстояние до точки 
		/// </summary>
		/// <param name="p">Точка</param>
		/// <returns>Расстояние до точки </returns>
		public double Distance(SmlPoint3D p)
		{
			double temp = A * p.X + B * p.Y + C * p.Z + D;
			return temp / p.R;
		}

		private static bool EqualsImpl(SmlPlane v1, SmlPlane v2)
		{
			if (ReferenceEquals(v1, v2)) return true;
			if (ReferenceEquals(v1, null)) return false;
			if (ReferenceEquals(null, v2)) return false;
			return ((v1.A == v2.A) && (v1.B == v2.B) && (v1.C == v2.C) && (v1.D == v2.D));
		}

		/// <summary>
		/// Плоскость ОXY
		/// </summary>
		public static SmlPlane XYPlane = new SmlPlane(SmlVector3D.ZAxis, SmlPoint3D.Zero);
		/// <summary>
		/// Плоскость ОYZ
		/// </summary>
		public static SmlPlane YZPlane = new SmlPlane(SmlVector3D.XAxis, SmlPoint3D.Zero);
		/// <summary>
		/// Плоскость ОZX
		/// </summary>
		public static SmlPlane ZXPlane = new SmlPlane(SmlVector3D.YAxis, SmlPoint3D.Zero);


		/// <summary>
		/// Determines whether the planes are equal.
		/// </summary>
		/// <param name="value1">A plane.</param>
		/// <param name="value2">A plane.</param>
		/// <returns>True if the planes are equal, otherwise false.</returns>
		public static bool operator ==(SmlPlane value1, SmlPlane value2)
		{
			return EqualsImpl(value1, value2);
				
		}

		/// <summary>
		/// Determines whether the planes are not equal.
		/// </summary>
		/// <param name="value1">A plane.</param>
		/// <param name="value2">A plane.</param>
		/// <returns>True if the planes are not equal, otherwise false.</returns>
		public static bool operator !=(SmlPlane value1, SmlPlane value2)
		{
			return !(value1 == value2);
		}

		/// <summary>
		/// Equalses the specified other.
		/// </summary>
		/// <param name="other">The other.</param>
		/// <returns></returns>
		public bool Equals(SmlPlane other)
		{
			return EqualsImpl(this, other);
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof (SmlPlane)) return false;
			return Equals((SmlPlane) obj);
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>
		/// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
		/// </returns>
		public override int GetHashCode()
		{
			return HashCode.GetHashCode(A.GetHashCode(), B.GetHashCode(), C.GetHashCode(), D.GetHashCode());
		}
	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Класс описывающий треугольник
	/// </summary>
	public class SmlTriangle2D : SmlPolygonBase2D
	{
		/// <summary>
		/// Создание нового экземпляра класса <see cref="SmlTriangle2D"/>.
		/// </summary>
		public SmlTriangle2D() { }

		/// <summary>
		/// Создание нового экземпляра класса <see cref="SmlTriangle2D"/>.
        /// </summary>
        /// <param name="first">Базовая точка</param>
        /// <param name="p2">Вторая точка</param>
		/// <param name="p3">Третья точка</param>
        /// <exception cref="InvalidOperationException">Точки лежат на одной прямой</exception>
		/// <remarks>Конструктор треугольника по трем точкам. Первой вершиной полигона по индексу будет первый аргумент.</remarks>
		public SmlTriangle2D(SmlPoint2D first, SmlPoint2D p2, SmlPoint2D p3)
        {
        	Set(first, p2, p3);
        }

		/// <summary>
		/// Создание нового экземпляра класса <see cref="SmlTriangle2D"/>.
		/// </summary>
		/// <param name="trngl">Копируемый треугольник</param>
		public SmlTriangle2D(SmlTriangle2D trngl) : base(trngl) { }

		/// <summary>
		/// Проверка на возможность создания треугольника по 3 точкам
		/// </summary>
		/// <param name="p1">Первая вершина</param>
		/// <param name="p2">Вторая вершина</param>
		/// <param name="p3">Третия вершина</param>
		/// <returns>
		///   <c>true</c> если треугольник можно создать по указанным точкам; иначе,<c>false</c>.
		/// </returns>
		public static bool CanCreate(SmlPoint2D p1, SmlPoint2D p2, SmlPoint2D p3)
		{
			SmlLine2D line = new SmlLine2D(p1, p3);
			return !line.IsOn(p2);
		}


		protected void Set(SmlPoint2D first, SmlPoint2D p2, SmlPoint2D p3)
		{
			SmlPoint2D[] pnts = { first, p2, p3 };
			if (!CanCreate(first, p2, p3))
				throw new InvalidOperationException("Заданные лежат на одной прямой");
			
			_polygon.Clear();
			foreach (SmlPoint2D point2D in pnts)
				_polygon.AddLast(point2D);
		}

		
		/// <summary>
		/// Получить периметр треугольника
		/// </summary>
		public double Perimetr
		{
			get
			{
				double per = 0;
				foreach (SmlSegment2D edge in GetEdges())
					per += edge.Length;
				return per;
			}
		}

		/// <summary>
		/// Получить площадь треугольника
		/// </summary>
		public virtual double Area
		{
			get
			{
				double halfPerimetr = Perimetr / 2;
				double area = halfPerimetr;
				foreach (SmlSegment2D edge in GetEdges())
					area *= (halfPerimetr - edge.Length);
				return area;
			}
		}

		/// <summary>
		/// Трансформирование
		/// </summary>
		/// <param name="matrix">Матрица трансформации</param>
		public void TransformBy(SmlMatrix2D matrix)
		{
			TransformByImpl(matrix);
		}
	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Базовый класс для реализации линейных примитивов
	/// </summary>
	public abstract class SmlLine3DBase
	{
		protected SmlVector3D	_vector = new SmlVector3D();
		protected SmlPoint3D		_point = new SmlPoint3D();

		/// <summary>
		/// Создание экземпляра класса <see cref="SmlLine3D"/>.
		/// </summary>
		protected SmlLine3DBase() { }

		/// <summary>
		/// Создание экземпляра класса <see cref="SmlLine3D"/>.
		/// </summary>
		/// <param name="pt1">Первая точка лежащая на прямой.</param>
		/// <param name="pt2">Вторая точка лежащая на прямой.</param>
		protected SmlLine3DBase(SmlPoint3D pt1, SmlPoint3D pt2)
		{
			_point = new SmlPoint3D(pt1);
			_vector = pt2 - pt1;
		}

		/// <summary>
		/// Создание экземпляра класса <see cref="SmlLine3D"/>.
		/// </summary>
		/// <param name="pt">Точка лежащая на прямой.</param>
		/// <param name="vec">Направляющий вектор.</param>
		protected SmlLine3DBase(SmlPoint3D pt, SmlVector3D vec)
		{
			_vector = new SmlVector3D(vec);
			_point = new SmlPoint3D(pt);
		}

		/// <summary>
		/// Создание экземпляра класса <see cref="SmlLine3D"/>.
		/// </summary>
		/// <param name="line">Исходная линия для создания экземпляра.</param>
		/// <remarks>Конструктор копирования</remarks>
		protected SmlLine3DBase(SmlLine3DBase line)
		{
			_vector = new SmlVector3D(line._vector);
			_point = new SmlPoint3D(line._point);
		}


		/// <summary>
		/// Угол между прямой и осью OX
		/// </summary>
		public SmlAngle XAxisAngle
		{
			get
			{
				if (_vector.IsZeroVector)
					throw new InfinityException("Невозможно определить угол для нулевого вектора");
				return SmlAngle.CreateCosAngle(_vector.X / _vector.Length);
			}
		}

		/// <summary>
		/// Угол между прямой и осью OY
		/// </summary>
		public SmlAngle YAxisAngle
		{
			get
			{
				if (_vector.IsZeroVector)
					throw new InfinityException("Невозможно определить угол для нулевого вектора");
				return SmlAngle.CreateCosAngle(_vector.Y / _vector.Length);
			}
		}

		/// <summary>
		/// Угол между прямой и осью OZ
		/// </summary>
		public SmlAngle ZAxisAngle
		{
			get
			{
				if (_vector.IsZeroVector)
					throw new InfinityException("Невозможно определить угол для нулевого вектора");
				return SmlAngle.CreateCosAngle(_vector.Z / _vector.Length);
			}
		}

		/// <summary>
		/// Определить угол между прямыми
		/// </summary>
		/// <param name="line">Прямая с которой определяется угол</param>
		/// <returns>Угол между прямыми</returns>
		public SmlAngle GetAngleTo(SmlLine3DBase line)
		{
			if (_vector.IsZeroVector || line._vector.IsZeroVector)
				throw new InfinityException("Невозможно определить угол для нулевого вектора");
			double u = SmlVector3D.ScalarProduct(_vector, line._vector);
			double v = _vector.Length * line._vector.Length;
			return SmlAngle.CreateCosAngle(u / v);
		}

		/// <summary>
		/// Определить угол между плоскостью
		/// </summary>
		/// <param name="plane">Плоскость с которой определяется угол</param>
		/// <returns>Угол между плоскостью</returns>
		public SmlAngle GetAngleTo(SmlPlane plane)
		{
			if (_vector.IsZeroVector || plane.Normal.IsZeroVector)
				throw new InfinityException("Невозможно определить угол для нулевого вектора");
			double u = SmlVector3D.ScalarProduct(_vector, plane.Normal);
			double v = _vector.Length * plane.Normal.Length;
			return SmlAngle.CreateCosAngle(u / v);
		}

		/// <summary>
		/// Определяет перпендикулярность между прямыми
		/// </summary>
		/// <param name="line">Прямая, для которой проверяется условие</param>
		/// <returns><c>true</c>если линии перпендикулярны; иначе,<c>false</c>.</returns>
		public bool IsPerpendicular(SmlLine3DBase line)
		{
			return _vector.IsPerpendicular(line._vector);
		}

		/// <summary>
		/// Определяет перпендикулярность между прямыми
		/// </summary>
		/// <param name="line">Прямая, для которой проверяется условие</param>
		/// <param name="delta">Точность измерения в радианах.</param>
		/// <returns><c>true</c>если линии перпендикулярны; иначе,<c>false</c>.</returns>
		public bool IsPerpendicular(SmlLine3DBase line, double delta)
		{
			return _vector.IsPerpendicular(line._vector, delta);
		}

		/// <summary>
		/// Определяет перпендикулярность между прямой и плоскостью
		/// </summary>
		/// <param name="plane">Плоскость, для которой проверяется условие</param>
		/// <returns><c>true</c>если линия перпендикулярна плоскости; иначе,<c>false</c>.</returns>
		public bool IsPerpendicular(SmlPlane plane)
		{
			return _vector.IsCollinear(plane.Normal);
		}

		/// <summary>
		/// Определяет перпендикулярность между прямой и плоскостью
		/// </summary>
		/// <param name="plane">Плоскость, для которой проверяется условие</param>
		/// <param name="delta">Точность измерения в радианах.</param>
		/// <returns><c>true</c>если линия перпендикулярна плоскости; иначе,<c>false</c>.</returns>
		public bool IsPerpendicular(SmlPlane plane, double delta)
		{
			return _vector.IsCollinear(plane.Normal, delta);
		}


		/// <summary>
		/// Определяет параллельность между прямой и плоскостью
		/// </summary>
		/// <param name="plane">Плоскость, для которой проверяется условие</param>
		/// <returns><c>true</c>если прямая параллельна плоскости; иначе,<c>false</c>.</returns>
		public bool IsParallel(SmlPlane plane)
		{
			return _vector.IsPerpendicular(plane.Normal);
		}

        /// <summary>
		/// Определяет параллельность между прямой и плоскостью
		/// </summary>
		/// <param name="plane">Плоскость, для которой проверяется условие</param>
		/// <param name="delta">Точность измерения в радианах.</param>
		/// <returns><c>true</c>если прямая параллельна плоскости; иначе,<c>false</c>.</returns>
		public bool IsParallel(SmlPlane plane, double delta)
		{
			return _vector.IsPerpendicular(plane.Normal, delta);
		}

		/// <summary>
		/// Определяет параллельность между прямыми
		/// </summary>
		/// <param name="line">Прямая, для которой проверяется условие</param>
		/// <returns><c>true</c>если прямые параллельны; иначе,<c>false</c>.</returns>
		public bool IsParallel(SmlLine3DBase line)
		{
			return _vector.IsCodirectional(line._vector);
		}

		/// <summary>
		/// Определяет параллельность между прямыми
		/// </summary>
		/// <param name="line">Прямая, для которой проверяется условие</param>
		/// <param name="delta">Точность измерения в радианах.</param>
		/// <returns><c>true</c>если прямые параллельны; иначе,<c>false</c>.</returns>
		public bool IsParallel(SmlLine3DBase line, double delta)
		{
			return _vector.IsCodirectional(line._vector, delta);
		}

		/// <summary>
		/// Определяет, скрещиваются ли прямые
		/// </summary>
		/// <param name="line">>Прямая, для которой проверяется условие</param>
		/// <returns><c>true</c>если прямые скрещиваются; иначе,<c>false</c>.</returns>
		/// <remarks>Две прямые скрещиваются, если тройка векторов, содержит пару коллинеарных векторов</remarks>
		public bool IsSkew(SmlLine3DBase line)
		{
			return IsSkew(line, GeomUtils.Delta);
		}

		/// <summary>
		/// Определяет, скрещиваются ли прямые
		/// </summary>
		/// <param name="line">&gt;Прямая, для которой проверяется условие</param>
		/// <param name="delta">Точность измерения в радианах.</param>
		/// <returns>  <c>true</c>если прямые скрещиваются; иначе,<c>false</c>.</returns>
		/// <remarks>Две прямые скрещиваются, если тройка векторов, содержит пару коллинеарных векторов</remarks>
		public bool IsSkew(SmlLine3DBase line, double delta)
		{
			SmlVector3D vec = line._point - _point;
			return (vec.IsCollinear(_vector, delta) ||
				vec.IsCollinear(line._vector, delta) ||
				_vector.IsCollinear(line._vector, delta));
		}

		/// <summary>
		/// Определяет, пересекаются ли прямые
		/// </summary>
		/// <param name="line">&gt;Прямая, для которой проверяется условие</param>
		/// <returns><c>true</c>если прямые пересекаются; иначе,<c>false</c>.</returns>
		public bool IsIntersect(SmlLine3DBase line)
		{
			return !(IsParallel(line)) && !(IsSkew(line));
		}

		/// <summary>
		/// Определяет, пересекаются ли прямые
		/// </summary>
		/// <param name="line">&gt;Прямая, для которой проверяется условие</param>
		/// <param name="delta">Точность измерения в радианах.</param>
		/// <returns>  <c>true</c>если прямые пересекаются; иначе,<c>false</c>.</returns>
		public bool IsIntersect(SmlLine3DBase line, double delta)
		{
			return !(IsParallel(line, delta)) && !(IsSkew(line, delta));
		}



		/// <summary>
		/// Расстояние от точки до прямой
		/// </summary>
		/// <param name="pt">Точка для определения расстояния</param>
		/// <returns>Расстояние от точки до прямой</returns>
		public double DistanceTo(SmlPoint3D pt)
		{
			if (_vector.IsZeroVector)
				throw new InfinityException("Направляющий вектор прямой - нулевой");
			SmlVector3D vec = pt - _point;
			double u = SmlVector3D.VectorProduct(_vector, vec).Length;
			double v = _vector.Length;
			return u / v;
		}

		/// <summary>
		/// Расстояние от прямой до прямой
		/// </summary>
		/// <param name="line">Прямая для определения расстояния</param>
		/// <returns>Расстояние от прямой до прямой</returns>
		public double DistanceTo(SmlLine3DBase line)
		{
			if (_vector.IsZeroVector || line._vector.IsZeroVector)
				throw new InfinityException("Направляющий вектор прямой - нулевой");

			SmlVector3D vec = _point - line._point;
			double u = Math.Abs(SmlVector3D.ScalarTripleProduct(vec, _vector, line._vector));
			double v = SmlVector3D.VectorProduct(_vector, line._vector).Length;
			return u / v;
		}

		/// <summary>
		/// Лежит ли точка на прямой
		/// </summary>
		/// <param name="p">исследуемая точка</param>
		/// <returns><c>true</c> если точка лежит на прямой; иначе <c>false</c>.</returns>
		public bool IsOn(SmlPoint3D p)
		{
			return IsOn(p, GeomUtils.Delta);
		}

		/// <summary>
		/// Лежит ли точка на прямой
		/// </summary>
		/// <param name="p">Исследуемая точка</param>
		/// <param name="delta">The delta.</param>
		/// <returns><c>true</c> если точка лежит на прямой; иначе <c>false</c>.</returns>
		public bool IsOn(SmlPoint3D p, double delta)
		{
			return IsOnImpl(p, delta);
		}

		protected virtual bool IsOnImpl(SmlPoint3D p, double delta)
		{
			SmlVector3D vec = p - _point;
			return _vector.IsCollinear(vec, delta);
		}

		/// <summary>
		/// Точка пересечения прямых (вызывающей и аргумента)
		/// </summary>
		/// <param name="line">Пересекаемая прямая</param>
		/// <param name="p">Точка пересечения</param>
		/// <returns>Пересекаются ли прямые</returns>
		/// <remarks>
		/// Решаем методом Крамера СЛАУ составленную из параметрических уравнений прямых
		/// <para>rx = ax*t - bx*u</para>
		/// <para>ry = ay*t - by*u</para>
		/// <para>где r - вектор из одной точки на прямой в другую,
		///  a - направляющий вектор первой прямой, b - направляющий вектор второй прямой</para>
		/// </remarks>
		public virtual bool CrossPoint(SmlLine3DBase line, out SmlPoint3D p)
		{
			p = new SmlPoint3D();
			if (IsParallel(line) || IsSkew(line))
				return false;
			SmlVector3D r = line._point - _point;
			SmlVector3D a = _vector;
			SmlVector3D b = line._vector;

			double delta = -a.X * b.Y + b.X * a.Y;
			double delta1 = -r.X * b.Y + b.X * r.Y;
			//double delta2 = a.X * r.Y - r.X * a.Y;

			double t = delta1 / delta;
			//double u = delta2 / delta;

			p = new SmlPoint3D(_point.X + _vector.X * t, _point.Y + _vector.Y * t, _point.Z + _vector.Z * t);
			return true;
		}

		/// <summary>
		/// Transforms the by.
		/// </summary>
		/// <param name="matrix">The matrix.</param>
		public void TransformBy(SmlMatrix3D matrix)
		{
			_point.TransformBy(matrix);
			_vector.TransformBy(matrix);
		}
	}
}

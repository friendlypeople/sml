// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Sml.Algebra.Data;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Базовое представление точки в пространстве
	/// </summary>
	public abstract class SmlPoint3DImpl : IComparable<SmlPoint3DImpl>
	{
		protected readonly double[] _cartesC = new double[3];
		protected readonly double[] _sphereC = new double[3];

		/// <summary>
		/// Конструктор точки по координатам
		/// </summary>
		/// <param name="x">Абсцисса точки</param>
		/// <param name="y">Ордината точки</param>
		/// <param name="z">Аппликата точки</param>
		protected SmlPoint3DImpl(double x, double y, double z)
		{
			_cartesC[0] = x;
			_cartesC[1] = y;
			_cartesC[2] = z;
			CartesianToSpherical();
		}

		///<summary>
		///Конструктор по умолчанию
		/// </summary>
		protected SmlPoint3DImpl() { }

		/// <summary>
		/// Перегруженный конструктор копирования
		/// </summary>
		/// <param name="point">точка c которой производится копирование</param>
		protected SmlPoint3DImpl(SmlPoint3DImpl point)
		{
			CloneImpl(point, this);
		}

		private void CloneImpl(SmlPoint3DImpl src, SmlPoint3DImpl dst)
		{
			for (int i = 0; i < 3; i++)
			{
				dst._cartesC[i] = src._cartesC[i];
				dst._sphereC[i] = src._sphereC[i];
			}
		}

		protected static bool EqualsImpl(SmlPoint3DImpl v1, SmlPoint3DImpl v2)
		{
			if (ReferenceEquals(v1, v2)) return true;
			if (ReferenceEquals(v1, null)) return false;
			if (ReferenceEquals(null, v2)) return false;
			return ((v1.X == v2.X) && (v1.Y == v2.Y) && (v1.Z == v2.Z));
		}

		/// <summary>
		/// Aбсцисса точки
		/// </summary>
		public double X
		{
			get { return _cartesC[0]; }
			set
			{
				_cartesC[0] = value;
				CartesianToSpherical();
			}
		}

		/// <summary>
		/// Ордината точки
		/// </summary>
		public double Y
		{
			get { return _cartesC[1]; }
			set
			{
				_cartesC[1] = value;
				CartesianToSpherical();
			}
		}

		/// <summary>
		/// Аппликата точки
		/// </summary>
		public double Z
		{
			get { return _cartesC[2]; }
			set
			{
				_cartesC[2] = value;
				CartesianToSpherical();
			}
		}

		/// <summary>
		/// Получить или задать угол между положительным направлением оси ОХ и проекцией направлением на точку в плоскости OXY
		/// </summary>
		/// <value>Угол между положительным направлением оси ОХ и проекцией направлением на точку в плоскости OXY</value>
		/// <remarks>Используется как в сферической так и в цилиндрической системе координат. Угол азимута</remarks>
		public double Fi
		{
			get {  return _sphereC[0]; }
			set
			{
				_sphereC[0] = value;
				SphericalToCartesian();
			}
		}

		/// <summary>
		/// Получить или задать угол между положительным направлением оси ОZ и направлением на точку
		/// </summary>
		/// <value>Угол между положительным направлением оси ОZ и направлением на точку</value>
		/// <remarks>Используется в сферической системе координат. Угол зенита</remarks>
		public double Teta
		{
			get { return _sphereC[2]; }
			set
			{
				_sphereC[2] = value;
				SphericalToCartesian();
			}
		}

		/// <summary>
		/// Получить или задать расстояние до точки в сферической системе координат
		/// </summary>
		/// <value> Расстояние до точки</value>
		/// <remarks>Используется в сферической системе координат</remarks>
		public double R
		{
			get { return _sphereC[1]; }
			set
			{
				_sphereC[1] = value;
				SphericalToCartesian();
			}
		}

		/// <summary>
		/// Получить или задать расстояние до точки в цилиндрической системе координат
		/// </summary>
		/// <value> Расстояние до точки</value>
		/// <remarks>Используется в цилиндрической системе координат</remarks>
		public double Ro
		{
			get { return _sphereC[1] * Math.Sin(_sphereC[2]);  }
			set
			{
				_sphereC[1] = Math.Sqrt(Math.Pow(value, 2) + Math.Pow(Z, 2));
				SphericalToCartesian();
			}
		}

		/// <summary>
		/// Привести точку по сферическим координатам
		/// </summary>
		/// <param name="fi">Угол между положительным направлением оси ОХ и проекцией направлением на точку в плоскости OXY</param>
		/// <param name="r">Растояние до точки от начала координат</param>
		/// <param name="teta">Угол между положительным направлением оси ОZ и направлением на точку</param>
		public void SetSpherical(double fi, double r, double teta)
		{
			_sphereC[0] = fi;
			_sphereC[1] = r;
			_sphereC[2] = teta;
			SphericalToCartesian();
		}

		/// <summary>
		/// Привести точку по цилиндрическим координатам
		/// </summary>
		/// <param name="fi">Угол между положительным направлением оси ОХ и проекцией направлением на точку в плоскости OXY</param>
		/// <param name="r">Растояние до проекции точки на плоскость OXY от начала координат</param>
		/// <param name="z">Высота</param>
		public void SetCylindrical(double fi, double r, double z)
		{
			_sphereC[0] = fi;
			_sphereC[1] = Math.Sqrt(Math.Pow(r, 2) + Math.Pow(z, 2));

			double teta = 0;
			if (z != 0)
				teta = GeomUtils.RoundingError(Math.Atan2(r, z));
			_sphereC[2] = teta;
			SphericalToCartesian();
		}

		/// <summary>
		/// Привести точку к декартовым координатам
		/// </summary>
		/// <param name="x">Абсцисса точки</param>
		/// <param name="y">Ордината точки</param>
		/// <param name="z">Аппликата точки</param>
		public void SetCartesian(double x, double y, double z)
		{
			_cartesC[0] = x;
			_cartesC[1] = y;
			_cartesC[2] = z;
			CartesianToSpherical();
		}

		protected void CartesianToSpherical()
		{
			GeomUtils.CartesianToSpherical(_cartesC, _sphereC);
		}

		protected void SphericalToCartesian()
		{
			GeomUtils.SphericalToCartesian(_sphereC, _cartesC);
		}

		/// <summary>
		/// Трансфорирование точки
		/// </summary>
		/// <param name="matrix">Матрица трансформации</param>
		public void TransformBy(SmlMatrix3D matrix)
		{
			Matrix pmat = new Matrix(1, 4);
			pmat[0, 0] = X;
			pmat[0, 1] = Y;
			pmat[0, 2] = Z;
			pmat[0, 3] = 1;

			Matrix rmat = pmat * matrix;
			X = rmat[0, 0];
			Y = rmat[0, 1];
			Z = rmat[0, 2];
		}

		public int CompareTo(SmlPoint3DImpl p)
		{
			int res;
			if (X.CompareTo(p.X) == 0)
				res = Y.CompareTo(p.Y) == 0 ? Z.CompareTo(p.Z) : Y.CompareTo(p.Y);
			else
				res = X.CompareTo(p.X);
			return res;
		}
	}
}

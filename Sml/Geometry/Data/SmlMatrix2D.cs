// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Sml.Algebra.Data;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Матрица афиновых преобразований на плоскости
	/// </summary>
    public class SmlMatrix2D : Matrix
    {
        private SmlMatrix2D() : base(3, 3) { }

		/// <summary>
		/// Конструктор копирования матрицы
		/// </summary>
		/// <param name="m">The m.</param>
        public SmlMatrix2D(SmlMatrix2D m) : base(m) { }

        /// <summary>
        /// Получить матрицу линейного оператора над пространством
        /// </summary>
        /// <value>Матрица линейного оператора над пространством</value>
        public Matrix TMatrix
        {
            get { return GetSubMatrixLimited(0, 1, 0, 1); }
        }

        /// <summary>
        /// Получить вектор переноса
        /// </summary>
        /// <value>Вектор переноса</value>
        public Matrix TVector
        {
            get { return GetSubMatrixLimited(0, 1, 2, 2); }
        }

		/// <summary>
		/// Матрица переноса
		/// </summary>
		/// <param name="vector">Вектор переноса базиса</param>
		/// <returns></returns>
        public static SmlMatrix2D Displacement(SmlVector2D vector)
        {
            SmlMatrix2D matrix = new SmlMatrix2D();

			matrix.SetNativeForm(	1,		  0,		0,
									0,		  1,		0,
									vector.X, vector.Y, 1 );
            return matrix;
        }

		/// <summary>
		/// Матрица переноса
		/// </summary>
		/// <param name="startPt">Исходная точка</param>
		/// <param name="endPt">Конечная точка</param>
		/// <returns></returns>
        public static SmlMatrix2D Displacement(SmlPoint2D startPt, SmlPoint2D endPt)
        {
            SmlVector2D vec = endPt - startPt;
            return Displacement(vec);
        }

		/// <summary>
		/// Поворот точки на заданный угол
		/// </summary>
		/// <param name="pt">Базовая точка</param>
		/// <param name="angle">Угол в радианах</param>
		/// <returns></returns>
		public static SmlMatrix2D Rotation(SmlPoint2D pt, double angle)
		{
			return Rotation(pt, new SmlAngle(angle));
		}

		/// <summary>
		/// Поворот точки на заданный угол
		/// </summary>
		/// <param name="pt">Базовая точка</param>
		/// <param name="ang">Угол в радианах</param>
		/// <returns></returns>
		public static SmlMatrix2D Rotation(SmlPoint2D pt, SmlAngle ang)
		{
			SmlMatrix2D matrix = new SmlMatrix2D();

			matrix.SetNativeForm(	ang.Cos,						  ang.Sin,							   0,
									-ang.Sin,						  ang.Cos,							   0,
									pt.X*(ang.Cos + 1) + pt.Y*ang.Sin, -pt.X*ang.Sin - pt.Y*(ang.Cos - 1), 1);

			return matrix;
		}

		/// <summary>
		/// Масштабирование относительно заданной точки
		/// </summary>
		/// <param name="pt">Базовая точка</param>
		/// <param name="sc">Масштабный коэффициент</param>
		/// <returns></returns>
		public static SmlMatrix2D Scaling(SmlPoint2D pt, double sc)
		{
			SmlMatrix2D matrix = new SmlMatrix2D();

			matrix.SetNativeForm( sc,			 0,				0,
								  0,			 sc,			0,
								  (1 - sc)*pt.X, (1 - sc)*pt.Y, 1);

			return matrix;
		}
    }
}

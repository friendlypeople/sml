// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;

namespace Sml.Geometry.Data
{
	/// <summary>
    /// Класс полигон - многоугольник с произвольным количеством вершин
    /// </summary>
    public class SmlPolygon2D : SmlPolygonBase2D
	{
		/// <summary>
        /// Констуктор полигона по умолчанию
        /// </summary>
        public SmlPolygon2D() { }

		/// <summary>
        /// Конструктор полигона по списку точек
        /// </summary>
        /// <param name="points">Список точек</param>
		public SmlPolygon2D(List<SmlPoint2D> points) : base(points) { }

        /// <summary>
        /// Конструктор полигона по списку точек
        /// </summary>
        /// <param name="points">Список точек</param>
        public SmlPolygon2D(List<SmlSPoint2D> points) : base(points) { }

		/// <summary>
		/// Вставляет новую вершину в полигон
		/// </summary>
		/// <param name="index">Номер вершины, после которой производится вставка</param>
		/// <param name="p">Точка, по которой вставляется новая вершина</param>
		/// <exception cref="IndexOutOfRangeException">Вершина с таким индексом в полигоне не найдена</exception>
		public void Insert(int index, SmlPoint2D p)
		{
			InsertImpl(index, p);
		}

		/// <summary>
		/// Вставляет новую вершину в полигон
		/// Если полигон пустой, вставляет вершину первой
		/// </summary>
		/// <param name="index">Индекс вершины, после которой производится вставка</param>
		/// <param name="x">Абсцисса вставляемой вершины</param>
		/// <param name="y">Ордината вставляемой вершины</param>
		/// <exception cref="InfinityException">Вершины с таким индексом не существует</exception>
		public void Insert(int index, double x, double y)
		{
			InsertImpl(index, x,y);
		}

		/// <summary>
		/// Удаляет вершину с таким индексом из полигона
		/// </summary>
		/// <param name="index">Индекс вершины</param>
		/// <returns>Удаленная вершина как точка</returns>
		/// <exception cref="IndexOutOfRangeException">Вершина с таким индексом в полигоне не найдена</exception>
		/// <exception cref="InvalidOperationException">Полигон не содержит ни одной вершины</exception>
		public SmlPoint2D Remove(int index)
		{
			ExistVertexesCheck();
            ExistIndexCheck(index);
		    LinkedListNode<SmlPoint2D> lln = VertexNode(index);
		    _polygon.Remove(lln);
		    return lln.Value;
		}

        /// <summary>
        /// Удаляет вершину в точке р
        /// </summary>
        /// <param name="p">Точка, в которой содержится вершина</param>
        /// <returns>Истинно если вершина в точке</returns>
        /// <exception cref="InvalidOperationException">Полигон не содержит ни одной вершины</exception>
        public bool Remove(SmlPoint2D p)
        {
            ExistVertexesCheck();
            if (IsVertex(p))
            {
                LinkedListNode<SmlPoint2D> lln = VertexNode(p);
                if (lln != null) _polygon.Remove(lln);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Разделение полигона по хорде, проходящей через вершины полигона с соответствующими индексами
        /// </summary>
        /// <param name="index1">Индекс первой вершины</param>
        /// <param name="index2">Индекс второй вершины</param>
        /// <returns>Полигон, полученный в результате разделения вызывающего</returns>
        /// <exception cref="InvalidOperationException">Вызывающий полигон не содержит вершин или содержит одну вершину</exception>
        /// <exception cref="IndexOutOfRangeException">Хотя бы одна вершина с таким индексом не найдена</exception>
        /// <exception cref="InfinityException">Индексы вершин совпадают. Определение секущей хорды невозможно</exception>
        public SmlPolygon2D Split(int index1, int index2)
        {
            ExistVertexesCheck();
            if (_polygon.Count == 1)
                throw new InvalidOperationException("Полигон содержит одну вершину. Разбиение невозможно.");
            ExistIndexCheck(index1);
            ExistIndexCheck(index2);
            if (index1 == index2)
                throw new InfinityException("Индексы вершин совпадают. Определение секущей хорды невозможно");
            LinkedListNode<SmlPoint2D> lln1;
            LinkedListNode<SmlPoint2D> lln2;
            SmlPolygon2D plg = new SmlPolygon2D();
            if (index1 < index2)
            {
                for (int i = index1, j = -1; i <= index2; i++, j++)
                    plg.Insert(j, this[i]);
                lln1 = VertexNode(index1 + 1);
                for (int i = index1 + 1; i <= index2 - 1; i++)
                {
                    lln2 = lln1.Next;
                    _polygon.Remove(lln1);
                    lln1 = lln2;
                }
                return plg;
            }
            for (int i = index2, j = -1; i <= index1; i++, j++)
                plg.Insert(j, this[i]);
            lln1 = VertexNode(index2 + 1);
            for (int i = index2 + 1; i <= index1 - 1; i++)
            {
                lln2 = lln1.Next;
                _polygon.Remove(lln1);
                lln1 = lln2;
            }
            return plg;
        }

		/// <summary>
		/// Трансформирование
		/// </summary>
		/// <param name="matrix">Матрица трансформации</param>
		public void TransformBy(SmlMatrix2D matrix)
		{
			TransformByImpl(matrix);
		}

       

    }

}

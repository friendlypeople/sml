// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;

namespace Sml.Geometry.Data
{
	///<summary>
    /// ������������� �� ���������
    ///</summary>
	public class SmlRect2D : SmlQuadrangle2D
    {
		/// <summary>
		/// ����������� �������������� �� ���������
		/// </summary>
		public SmlRect2D() { }

        /// <summary>
        /// ����������� �������������� �� ���� ������. ������ �������� �������� �� ������� ����� ������ ��������.
        /// </summary>
        /// <param name="first">������� �����</param>
        /// <param name="p2">������ ����� (������ �� ������� ����� � ��� ���� ��� ������)</param>
		/// <param name="p3">������ �����</param>
        /// <exception cref="InvalidOperationException">����� �� �������� ���������������� ������</exception>
        public SmlRect2D(SmlPoint2D first, SmlPoint2D p2, SmlPoint2D p3)
        {
        	Set(first, p2, p3);
        }

		/// <summary>
		/// ����������� �������������� �� ���� ������. ������ �������� �������� �� ������� ����� ������ ��������.
		/// </summary>
		private SmlRect2D(SmlPoint2D[] pts)
		{
			Set(pts[0], pts[1], pts[2]);
		}

		/// <summary>
		/// ����������� �������������� �� ���� ������, ���������� ������ ����� ��������������, � ����� �������
		/// ����� ��������������. ������������� �����������, ������������ �� ����� ��������������, ��� ������� ���
		/// ������, ������� � ������� ��� ������ ���� ������� �����, ������� ��� ������ ���� � ���������� ��������
		/// ��� ������ ���� ������� �����, ������ ���� ������������ ���������������!!!
		/// </summary>
		/// <param name="p1">������� �����</param>
		/// <param name="p2">������ ����� (������ �� ������� ����� � ��� ���� ��� ������)</param>
		/// <param name="d">����� ������� �����</param>
		public SmlRect2D(SmlPoint2D p1, SmlPoint2D p2, double d)
		{
			SmlVector2D v1 = new SmlVector2D(p2, p1);
			SmlVector2D v2 = v1.PerpendicularPositive();
			v2.D = d;
			SmlPoint2D p3 = (p2.AsVector() + v2).ToPoint();
			Set(p1, p2, p3);
		}

		/// <summary>
		/// ����������� ��������������, ���������������� �� ������������ ����
		/// </summary>
		/// <param name="p1">������� ����� ��������������</param>
		/// <param name="p2">��������������� ����� �������������</param>
		public SmlRect2D(SmlPoint2D p1, SmlPoint2D p2)
		{
			Set(p1, new SmlPoint2D(p2.X, p1.Y), p2);
		}

		/// <summary>
		/// �������� �� ����������� �������� �������������� �� 2 ������
		/// </summary>
		/// <param name="p1">������� ����� ��������������</param>
		/// <param name="p2">��������������� ����� �������������</param>
		public static bool CanCreate(SmlPoint2D p1, SmlPoint2D p2)
		{
			int ind;
			SmlPoint2D[] pnts = { p1, new SmlPoint2D(p2.X, p1.Y), p2 };
			return GeomUtils.IsToPerpendLinesExsit(pnts, out ind);
		}

		/// <summary>
		/// ����������� ��������������, ���������������� �� ������������ ����
		/// </summary>
		/// <param name="p1">������� ����� ��������������</param>
		/// <param name="width">������</param>
		/// <param name="height">������</param>
		public SmlRect2D(SmlPoint2D p1, double width, double height)
		{
			SmlPoint2D p2 = new SmlPoint2D(p1.X + width, p1.Y + height);
			Set(p1, new SmlPoint2D(p2.X, p1.Y), p2);
		}

		/// <summary>
		/// ����������� ����������� ��������������
		/// </summary>
		/// <param name="rct">���������� �������������</param>
		public SmlRect2D(SmlRect2D rct) : base(rct) { }


    	protected void Set(SmlPoint2D first, SmlPoint2D p2, SmlPoint2D p3)
        {
            SmlPoint2D[] pnts = { first, p2, p3 };
            int ind;

            if (!GeomUtils.IsToPerpendLinesExsit(pnts, out ind))
                throw new InvalidOperationException("�������� ����� �� �������� ���������������� ������");
            SmlPoint2D p = GeomUtils.FindLastVertex(pnts, ind);
            
			_polygon.Clear();
    		foreach (SmlPoint2D point2D in pnts)
    			_polygon.AddLast(point2D);
			_polygon.AddLast(p);
        }

		/// <summary>
		/// �������� ������ ��������������
		/// </summary>
		/// <value>������</value>
		public virtual double Width
		{
			get
			{ 
				SmlVector2D vect = new SmlVector2D(this[1], this[0]);
				return vect.D;
			}
		}

		/// <summary>
		/// �������� ������ ��������������
		/// </summary>
		/// <value>������</value>
		public virtual double Height
		{
			get
			{
				SmlVector2D vect = new SmlVector2D(this[3], this[0]);
				return vect.D;
			}
		}

        
        /// <summary>
        /// ������� ��������������
        /// </summary>
		public virtual double Area
		{
			get
			{
				SmlVector2D vect1 = new SmlVector2D(this[1], this[0]);
				SmlVector2D vect2 = new SmlVector2D(this[3], this[0]);
				return vect1.D * vect2.D;
			}
		}

		/// <summary>
		/// �����������������
		/// </summary>
		/// <param name="matrix">������� �������������</param>
		public void TransformBy(SmlMatrix2D matrix)
		{
			TransformByImpl(matrix);
		}

		/// <summary>
		/// ������� ������ ��������������
		/// </summary>
		/// <param name="param">���������</param>
		/// <param name="inside">������ ���� <c>true</c> [inside].</param>
		/// <returns></returns>
		public SmlRect2D Offset(double param, bool inside)
		{
			SmlPoint2D[] nPoints = new SmlPoint2D[3];
			for (int i = 0; i < 3; i++)
			{
				SmlPoint2D bPt = this[i];
				SmlVector2D vector = CalculateOffsetOrtVector(bPt, GetNextIndex(i), GetPrevIndex(i), param, inside);
				nPoints[i] = bPt + vector;
			}
			return new SmlRect2D(nPoints);
		}


		private SmlVector2D CalculateOffsetOrtVector(SmlPoint2D bPt, int inPt1, int inpt2, double param, bool inside)
		{
			SmlPoint2D pt1 = this[inPt1];
			SmlPoint2D pt2 = this[inpt2];
			SmlVector2D vector1 = new SmlVector2D(pt1, bPt);
			vector1.Unit();
			SmlVector2D vector2 = new SmlVector2D(pt2, bPt);
			vector2.Unit();
			SmlVector2D vec = vector1 + vector2;
			vec = vec * param;
			if (!inside) vec.Negate();
			return vec;
		}

		
    }

}

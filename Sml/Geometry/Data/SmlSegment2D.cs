// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;

namespace Sml.Geometry.Data
{
	[Serializable]
	public class SmlSegment2D : SmlLine2D
	{
		/// <summary>
		/// Стартовая точка
		/// </summary>
		protected SmlPoint2D _startPoint;

		/// <summary>
		/// Конечная точка
		/// </summary>
		protected SmlPoint2D _endPoint;

		/// <summary>
		/// Конструктор отрезка по умолчанию
		/// </summary>
		public SmlSegment2D()
		{
			_startPoint = new SmlPoint2D();
			_endPoint = new SmlPoint2D();
		}

		/// <summary>
		/// Конструктор отрезка по двум точкам, р1 - стартовая, р2 - конечная точки отрезка
		/// </summary>
		/// <param name="p1">Стартовая точка</param>
		/// <param name="p2">Конечная точка</param>
		public SmlSegment2D(SmlPoint2D p1, SmlPoint2D p2)
			: base(p1, p2)
		{
			_startPoint = p1;
			_endPoint = p2;
		}

		/// <summary>
		/// Конструктор копирования отрезка
		/// </summary>
		/// <param name="s">Копируемый отрезок</param>
		public SmlSegment2D(SmlSegment2D s)
			: base(s.StartPoint, s.EndPoint)
		{
			_startPoint = s.StartPoint;
			_endPoint = s.EndPoint;
		}

		/// <summary>
		/// Возвращает стартовую точку
		/// </summary>
		public SmlPoint2D StartPoint
		{
			get
			{
				return _startPoint;
			}
			set
			{
				base.Set(value, _endPoint);
				_startPoint = value;
			}
		}

		/// <summary>
		/// Возвращает конечную точку
		/// </summary>
		public SmlPoint2D EndPoint
		{
			get { return _endPoint; }
			set
			{
				base.Set(_startPoint, value);
				_endPoint = value;
			}
		}

		/// <summary>
		/// Длина отрезка
		/// </summary>
		public double Length
		{
			get { return _startPoint.Distance(_endPoint); }
		}

		/// <summary>
		/// Задает отрезок по двум точкам
		/// </summary>
		/// <param name="p1">стартовая точка</param>
		/// <param name="p2">конечная точка</param>
		public override void Set(SmlPoint2D p1, SmlPoint2D p2)
		{
			base.Set(p1, p2);
			_startPoint = p1;
			_endPoint = p2;
		}

		/// <summary>
		/// Задает отрезок по другому отрезку
		/// </summary>
		/// <param name="s">Задающий отрезок</param>
		public override void Set(SmlSegment2D s)
		{
			base.Set(s.StartPoint, s.EndPoint);
			_startPoint = s.StartPoint;
			_endPoint = s.EndPoint;
		}

		public override void Set(SmlPoint2D p, SmlVector2D v)
		{
			SmlPoint2D second = (p.AsVector() + v).ToPoint();
			Set(p, second);
		}

		/// <summary>
		/// Вычисляет значение абсциссы точки на отрезке по значению ее ординаты
		/// </summary>
		/// <param name="x">Возвращаемая абсцисса точки</param>
		/// <param name="y">Ордината точки</param>
		/// <returns>истинно если точка имееет абсциссу и точка лежит на отрезке</returns>
		/// <exception cref="InfinityException">У горизонтального отрезка при соответствующей ординате значение абсциссы точно определить невозможно</exception>
		public override bool XValue(ref double x, double y)
		{
			SmlRay2D r1 = new SmlRay2D(_startPoint, _endPoint);
			SmlRay2D r2 = new SmlRay2D(_endPoint, _startPoint);
			try
			{
				return r1.XValue(ref x, y) && r2.XValue(ref x, y);
			}
			catch (InfinityException)
			{
				throw new InfinityException("Горизонтальный отрезок. При этой ординате значение абсциссы может быть любым из бесконечного и ограниченного сверху и снизу множества.");
			}
		}


		/// <summary>
		/// Вычисляет значение ординаты точки на отрезке по значению ее абсциссы
		/// </summary>
		/// <param name="x">Абсцисса точки</param>
		/// <param name="y">Возвращаемая ордината точки</param>
		/// <returns>истинно если точка имеет ординату и точка лежит на отрезке</returns>
		/// <exception cref="InfinityException">У вертикального отрезка при соответсвующей абсциссе значение ординаты точно определить невозможно</exception>
		public override bool YValue(double x, ref double y)
		{
			SmlRay2D r1 = new SmlRay2D(_startPoint, _endPoint);
			SmlRay2D r2 = new SmlRay2D(_endPoint, _startPoint);
			try
			{
				return r1.YValue(x, ref y) && r2.YValue(x, ref y);
			}
			catch (InfinityException)
			{
				throw new InfinityException(
					"Вертикальный отрезок. При этой абсциссе значение ординаты может быть любым из бесконечного и ограниченного сверху и снизу множества.");
			}
		}

		/// <summary>
		/// Лежит ли точка на отрезке
		/// </summary>
		/// <param name="p">точка</param>
		/// <returns></returns>
		public override bool IsOn(SmlPoint2D p)
		{
			SmlRay2D r1 = new SmlRay2D(StartPoint, EndPoint);
			SmlRay2D r2 = new SmlRay2D(EndPoint, StartPoint);
			return (r1.IsOn(p) && r2.IsOn(p));
		}

		/// <summary>
		/// Является ли вертикальным отрезок
		/// </summary>
		/// <param name="delta">Допуск (точность) - изменение по X на каждые 100 мм по Y..</param>
		/// <returns>
		/// 	<c>true</c> если отрезок вертикальный; иначе, <c>false</c>.
		/// </returns>
		public bool IsVertical(double delta)
		{
			double relativeHeight = Length / 100;
			double deltaX = Math.Abs(StartPoint.X - EndPoint.X);

			return deltaX <= relativeHeight * delta;
		}

		/// <summary>
		/// Является ли горизонтальным отрезок
		/// </summary>
		/// <param name="delta">Допуск(точность) - изменение выстоты на каждые 100 мм по X.</param>
		/// <returns>
		/// 	<c>true</c> если отрезок горизонтальный; иначе, <c>false</c>.
		/// </returns>
		public bool IsHorizontal(double delta)
		{
			double relativeLength = Length / 100;
			double deltaY = Math.Abs(StartPoint.Y - EndPoint.Y);

			return deltaY <= relativeLength * delta;
		}

		/// <summary>
		/// Возвращает расположение точки относительно вызывающего отрезка
		/// </summary>
		/// <param name="p">Исследуемая точка</param>
		/// <returns>Расположение точки</returns>
		public Location Locate(SmlPoint2D p)
		{
			SmlVector2D v1 = (_endPoint - _startPoint);
			SmlVector2D v2 = (p - _startPoint);
			SmlVector2D v3 = (p - _endPoint);
			if (base.IsOn(p))
			{
				if (p == _startPoint)
					return Location.Origin;
				if (p == _endPoint)
					return Location.Destination;
				if (v1.IsCodirectional(v2))
				{
					if (v3.IsCodirectional(v2))
						return Location.Beyond;
					return Location.Between;
				}
				return Location.Behind;
			}
			if (v1.Orientation(v2) == Orientation.CounterClockwise)
				return Location.Left;
			return Location.Right;
		}

		/// <summary>
		/// Возвращает случайную точку отрезка
		/// </summary>
		/// <returns>Случайная точка отрезка</returns>
		public override SmlPoint2D PointOn()
		{
			Random rnd = new Random();
			double b = _startPoint.X > _endPoint.X ? _endPoint.X : _startPoint.X;
			double a = Math.Abs(_startPoint.X - _endPoint.X);
			double x = 0;
			double y = 0;
			if (a != 0)
			{
				x = rnd.Next(100);
				x = b + (x - Math.Floor(x / a) * a);
				base.YValue(x, ref y);
				return new SmlPoint2D(x, y);
			}
			b = _startPoint.Y > _endPoint.Y ? _endPoint.Y : _startPoint.Y;
			a = Math.Abs(_startPoint.Y - _endPoint.Y);
			if (a != 0)
			{
				y = rnd.Next(100);
				y = b + y - Math.Floor(y / a) * a;
				return new SmlPoint2D(_startPoint.X, y);
			}
			return _startPoint;
		}

		/// <summary>
		/// Поворот отрезка ПЧС вокруг точки р на угол alpha
		/// </summary>
		/// <param name="alpha">Угол поворота</param>
		/// <param name="p">Точка вращения</param>
		public override void RotateNegative(double alpha, SmlPoint2D p)
		{
			SmlRay2D r1 = new SmlRay2D(StartPoint, EndPoint);
			SmlRay2D r2 = new SmlRay2D(EndPoint, StartPoint);
			r1.RotateNegative(alpha, p);
			r2.RotateNegative(alpha, p);
			Set(r1.StartPoint, r2.StartPoint);
		}

		/// <summary>
		/// Возвращает отрезок, который представляет собой вызывающий отрезок, 
		/// повернутый на угол alpha ПЧС вокруг точки р
		/// </summary>
		/// <param name="alpha">Угол поворота</param>
		/// <param name="p">Точка поворота</param>
		/// <returns>Повернутый отрезок</returns>
		public new SmlSegment2D RotateNegativeCreate(double alpha, SmlPoint2D p)
		{
			SmlSegment2D s = new SmlSegment2D(this);
			s.RotateNegative(alpha, p);
			return s;
		}

		/// <summary>
		/// Поворот отрезка ПрЧС на угол alpha вокруг точки р
		/// </summary>
		/// <param name="alpha">Угол поворота</param>
		/// <param name="p">Точка вращения</param>
		public override void RotatePositive(double alpha, SmlPoint2D p)
		{
			SmlRay2D r1 = new SmlRay2D(StartPoint, EndPoint);
			SmlRay2D r2 = new SmlRay2D(EndPoint, StartPoint);
			r1.RotatePositive(alpha, p);
			r2.RotatePositive(alpha, p);
			Set(r1.StartPoint, r2.StartPoint);
		}

		/// <summary>
		/// Возвращает отрезок, который представляет собой вызывающий отрезок, 
		/// повернутый на угол alpha ПрЧС вокруг точки р
		/// </summary>
		/// <param name="alpha">Угол поворота</param>
		/// <param name="p">Точка поворота</param>
		/// <returns>Повернутый отрезок</returns>
		public new SmlSegment2D RotatePositiveCreate(double alpha, SmlPoint2D p)
		{
			SmlSegment2D s = new SmlSegment2D(this);
			s.RotatePositive(alpha, p);
			return s;
		}

		/// <summary>
		/// Вычисляет точку пересечения вызывающего отрезка и прямой
		/// </summary>
		/// <param name="l">Прямая</param>
		/// <param name="p">Точка пересечения</param>
		/// <returns>Истинно если пересекаются</returns>
		public override bool CrossPoint(SmlLine2D l, ref SmlPoint2D p)
		{
			SmlRay2D r1 = new SmlRay2D(_startPoint, _endPoint);
			SmlRay2D r2 = new SmlRay2D(_endPoint, _startPoint);
			return r1.CrossPoint(l, ref p) && r2.CrossPoint(l, ref p);
		}

		/// <summary>
		/// Вычисляет точку пересечения вызывающего отрезка и другого отрезка
		/// </summary>
		/// <param name="s">Отрезок</param>
		/// <param name="p">Точка пересечения</param>
		/// <returns>Истинно если пересекаются</returns>
		public override bool CrossPoint(SmlSegment2D s, ref SmlPoint2D p)
		{
			SmlRay2D r1 = new SmlRay2D(s.StartPoint, s.EndPoint);
			//SmlRay2D r2 = new SmlRay2D(s.EndPoint, s.StartPoint);
			CrossPoint(r1, ref p);
			//CrossPoint(r2, ref p);
			return IsOn(p) && s.IsOn(p);
		}

		/// <summary>
		/// Возвращает точку пересечения вызывающего отрезка и луча
		/// </summary>
		/// <param name="r">Пересекаемый луч</param>
		/// <param name="p">Точка пересечения</param>
		/// <returns></returns>
		public override bool CrossPoint(SmlRay2D r, ref SmlPoint2D p)
		{
			SmlRay2D r1 = new SmlRay2D(_startPoint, _endPoint);
			//  SmlRay2D r2 = new SmlRay2D(_endPoint, _startPoint);
			return r1.CrossPoint(r, ref p);// && r2.CrossPoint(r, ref p);
		}

		/// <summary>
		/// Расстояние от вызывающего отрезка до точки
		/// </summary>
		/// <param name="p">Точка</param>
		/// <returns>Расстояние</returns>
		public override double Distance(SmlPoint2D p)
		{
			double d1 = (new SmlRay2D(StartPoint, EndPoint)).Distance(p);
			double d2 = (new SmlRay2D(EndPoint, StartPoint)).Distance(p);
			return d1 >= d2 ? d1 : d2;
		}

		public override string ToString()
		{
			return "StartPt " + StartPoint + " EndPt " + EndPoint;
		}

		/// <summary>
		/// Эквивалентность сегментов
		/// </summary>
		/// <param name="seSmlent">Сегмент для сравнения</param>
		/// <returns>  	<c>true</c> если эквивалентны ; иначе, <c>false</c>.  </returns>
		public bool IsEquals(SmlSegment2D seSmlent)
		{
			return IsEquals(seSmlent, GeomUtils.Delta);
		}

		/// <summary>
        /// Эквивалентность сегментов
		/// </summary>
		/// <param name="seSmlent">Сегмент для сравнения</param>
		/// <param name="tolerance">Точность</param>
		/// <returns>  	<c>true</c> если эквивалентны ; иначе, <c>false</c>. </returns>
		public bool IsEquals(SmlSegment2D seSmlent, double tolerance)
		{
			return StartPoint.IsEquals(seSmlent.StartPoint, tolerance)
				&& EndPoint.IsEquals(seSmlent.EndPoint, tolerance);
		}
	}
}

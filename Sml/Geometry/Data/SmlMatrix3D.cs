// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Sml.Algebra.Data;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Матрица афиновых преобразований в пространстве
	/// </summary>
	public class SmlMatrix3D : Matrix
	{
		private SmlMatrix3D() : base(4, 4) { }

		/// <summary>
		/// Конструктор копирования матрицы
		/// </summary>
		/// <param name="m">The m.</param>
		public SmlMatrix3D(SmlMatrix3D m) : base(m) { }

        /// <summary>
        /// Получить матрицу линейного оператора над пространством
        /// </summary>
        /// <value>Матрица линейного оператора над пространством</value>
        public Matrix TMatrix
        {
            get { return GetSubMatrixLimited(0, 2, 0, 2); }
        }

        /// <summary>
        /// Получить вектор переноса
        /// </summary>
        /// <value>Вектор переноса</value>
        public Matrix TVector
        {
            get { return GetSubMatrixLimited(0, 2, 3, 3); }
        }

		/// <summary>
		/// Матрица переноса
		/// </summary>
		/// <param name="vector">Вектор переноса базиса</param>
		/// <returns></returns>
		public static SmlMatrix3D Displacement(SmlVector3D vector)
		{
			SmlMatrix3D matrix = new SmlMatrix3D();
			matrix.SetNativeForm(
                                      1,		0,		  0,	0,
								      0,		1,		  0,	0,
								      0,		0,		  1,	0,
								  vector.X, vector.Y, vector.Z, 1);
			return matrix;
		}

		/// <summary>
		/// Матрица поворота относительно оси OX
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Сформированная матрица</returns>
		public static SmlMatrix3D RotateOX(SmlAngle angle)
		{
			SmlMatrix3D matrix = new SmlMatrix3D();
			matrix.SetNativeForm( 
                                  1,      0,		 0,	    0,
								  0,  angle.Cos, angle.Sin, 0,
								  0, -angle.Sin, angle.Cos, 0,
								  0,      0,		 0,		1);
			return matrix;
		}

		/// <summary>
		/// Матрица поворота относительно оси OX
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Сформированная матрица</returns>
		public static SmlMatrix3D RotateOX(double angle)
		{
			return RotateOX(new SmlAngle(angle));
		}

		/// <summary>
		/// Матрица поворота относительно оси OY
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Сформированная матрица</returns>
		public static SmlMatrix3D RotateOY(SmlAngle angle)
		{
			SmlMatrix3D matrix = new SmlMatrix3D();
			matrix.SetNativeForm(
                                  angle.Cos, 0, -angle.Sin, 0,
								      0,     1,      0,		0,
								  angle.Sin, 0,  angle.Cos,	0,
								      0,	 0,      0,		1);
			
			return matrix;
		}

		/// <summary>
		/// Матрица поворота относительно оси OY
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Сформированная матрица</returns>
		public static SmlMatrix3D RotateOY(double angle)
		{
			return RotateOY(new SmlAngle(angle));
		}

		/// <summary>
		/// Матрица поворота относительно оси OZ
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Сформированная матрица</returns>
		public static SmlMatrix3D RotateOZ(SmlAngle angle)
		{
			SmlMatrix3D matrix = new SmlMatrix3D();

			matrix.SetNativeForm(
                                  angle.Cos, angle.Sin, 0, 0,
								 -angle.Sin, angle.Cos, 0, 0,
								      0,	      0,	1, 0,
								      0,		  0,	0, 1);
			return matrix;
		}

		/// <summary>
		/// Матрица поворота относительно произвольной оси OZ
		/// </summary>
		/// <param name="vec">Вектор задающий ось поворота.</param>
		/// <param name="ang">Угол поворота.</param>
		/// <returns>Сформированная матрица </returns>
		public static SmlMatrix3D Rotate(SmlVector3D vec, SmlAngle ang)
		{
			SmlMatrix3D matrix = new SmlMatrix3D();
			double l = vec.X;
			double m = vec.Y;
			double n = vec.Z;

			matrix.SetNativeForm(
					l*l+ang.Cos*(1-l*l),	l*(1-ang.Cos)*m+ n*ang.Sin, l*(1-ang.Cos)*n-m*ang.Sin,  0,
				l*(1-ang.Cos)*m-n*ang.Sin,		m*m+ang.Cos*(1-m*m),	m*(1-ang.Cos)*n+l*ang.Sin,  0,
				l*(1-ang.Cos)*n+m*ang.Sin,	m*(1-ang.Cos)*n-l*ang.Sin,		n*n+ang.Cos*(1-n*n),	0,
							 0,							 0,							 0,				1);
			return matrix;
		}

		/// <summary>
		/// Матрица поворота относительно оси OZ
		/// </summary>
		/// <param name="angle">Угол поворота.</param>
		/// <returns>Сформированная матрица</returns>
		public static SmlMatrix3D RotateOZ(double angle)
		{
			return RotateOZ(new SmlAngle(angle));
		}

		/// <summary>
		/// Матрица отражения относительно плоскости XOY
		/// </summary>
		/// <returns>Сформированная матрица</returns>
		public static SmlMatrix3D MirrorXY()
		{
			SmlMatrix3D matrix = new SmlMatrix3D();

			matrix.SetNativeForm(
                                 1, 0,  0, 0,
								 0, 1,  0, 0,
								 0, 0, -1, 0,
								 0, 0,  0, 1);
			return matrix;
		}

		/// <summary>
		/// Матрица отражения относительно плоскости YOZ
		/// </summary>
		/// <returns>Сформированная матрица</returns>
		public static SmlMatrix3D MirrorYZ()
		{
			SmlMatrix3D matrix = new SmlMatrix3D();

			matrix.SetNativeForm( -1, 0, 0, 0,
								  0,  1, 0, 0,
								  0,  0, 1, 0,
								  0,  0, 0, 1);
			return matrix;
		}

		/// <summary>
		/// Матрица отражения относительно плоскости ZOX
		/// </summary>
		/// <returns>Сформированная матрица</returns>
		public static SmlMatrix3D MirrorZX()
		{
			SmlMatrix3D matrix = new SmlMatrix3D();

			matrix.SetNativeForm(1, 0,  0, 0,
								 0, -1, 0, 0,
								 0, 0,  1, 0,
								 0, 0,  0, 1);
			return matrix;
		}

		/// <summary>
		/// Масштабирование относительно заданной точки
		/// </summary>
		/// <param name="pt">Базовая точка</param>
		/// <param name="sc">Масштабный коэффициент</param>
		/// <returns></returns>
		public static SmlMatrix3D Scaling(SmlPoint3D pt, double sc)
		{
			SmlMatrix3D matrix = new SmlMatrix3D();

			matrix.SetNativeForm(    sc,          0,            0,      0,
								      0,         sc,            0,      0,
								      0,          0,           sc,      0,
								 (1-sc)*pt.X, (1-sc)*pt.Y, (1-sc)*pt.Z, 1 );
			return matrix;
		}
	}
}

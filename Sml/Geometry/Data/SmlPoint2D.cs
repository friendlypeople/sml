// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Diagnostics;
using Sml.Common;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Представление точки на плоскости
	/// </summary>
	[Serializable]
	[DebuggerDisplay("X = {X} Y = {Y} Alpha = {Alpha} D = {D}")]
	public class SmlPoint2D : SmlPoint2DImpl 
	{
		///<summary>
		///Конструктор по умолчанию
		/// </summary>
		public SmlPoint2D() { }

		/// <summary>
		/// Перегруженный конструктор копирования
		/// </summary>
		/// <param name="point">точка c которой производится копирование</param>
		public SmlPoint2D(SmlPoint2D point) : base(point) { }

		/// <summary>
		/// Конструктор точки по координатам
		/// </summary>
		/// <param name="x">Абсцисса точки</param>
		/// <param name="y">Ордината точки</param>
		public SmlPoint2D(double x, double y) : base(x, y) { }

        ///<summary>
        ///Конструктор по SmlSPoint2D
        /// </summary>
        public SmlPoint2D(SmlSPoint2D sPoint2D) : base(sPoint2D.X, sPoint2D.Y) { }

	    /// <summary>
		/// Преобразовать точку в вектор
		/// </summary>
		/// <returns></returns>
		public SmlVector2D AsVector()
		{
			return new SmlVector2D(X, Y);
		}

		/// <summary>
		/// Расстояние до точки 
		/// </summary>
		/// <param name="p">точка</param>
		/// <returns></returns>
		public double Distance(SmlPoint2D p)
		{
			return Math.Abs(Math.Sqrt(Math.Pow(X - p.X, 2) + Math.Pow(Y - p.Y, 2)));
		}

		/// <summary>
		/// Расстояние до прямой
		/// </summary>
		/// <param name="l">прямая</param>
		/// <returns></returns>
		public double Distance(SmlLine2D l)
		{
			double t = Math.Sqrt(Math.Pow(l.A, 2) + Math.Pow(l.B, 2));
			return Math.Abs((l.A*X + l.B*Y + l.C)/t);
		}

		/// <summary>
		/// Расстояние до луча
		/// </summary>
		/// <param name="r">луч</param>
		/// <returns></returns>
		public double Distance(SmlRay2D r)
		{
			//TODO Реализовать
			return 0;
		}

		/// <summary>
		/// Расстояние до отрезка
		/// </summary>
		/// <param name="s">отрезок</param>
		/// <returns></returns>
		public double Distance(SmlSegment2D s)
		{
			//TODO Реализовать
			return 0;
		}

		/// <summary>
		/// Нулевая точка
		/// </summary>
		public static SmlPoint2D Zero
		{
			get { return new SmlPoint2D(); }
		}

		/// <summary>
		/// Сумма для точки
		/// </summary>
		/// <param name="p1">первое слагаемое</param>
		/// <param name="p2">второе слагаемое</param>
		/// <returns></returns>
		public static SmlVector2D operator +(SmlPoint2D p1, SmlPoint2D p2)
		{
			return new SmlVector2D(GeomUtils.RoundingError(p1.X + p2.X), GeomUtils.RoundingError(p1.Y + p2.Y));
		}

		/// <summary>
		/// Сумма для точки
		/// </summary>
		/// <param name="p1">первое слагаемое</param>
		/// <param name="vec">Слагаемы вектор</param>
		/// <returns></returns>
		public static SmlPoint2D operator +(SmlPoint2D p1, SmlVector2D vec)
		{
			return new SmlPoint2D(GeomUtils.RoundingError(p1.X + vec.X), GeomUtils.RoundingError(p1.Y + vec.Y));
		}

		/// <summary>
		/// Разность для точки
		/// </summary>
		/// <param name="p1">Уменьшаемое</param>
		/// <param name="p2">Вычитаемое</param>
		/// <returns></returns>
		public static SmlVector2D operator -(SmlPoint2D p1, SmlPoint2D p2)
		{
			return new SmlVector2D(GeomUtils.RoundingError(p1.X - p2.X), GeomUtils.RoundingError(p1.Y - p2.Y));
		}

		/// <summary>
		/// Разность для точки
		/// </summary>
		/// <param name="p1">Уменьшаемое</param>
		/// <param name="vec">Вычитаемое</param>
		/// <returns></returns>
		public static SmlPoint2D operator -(SmlPoint2D p1, SmlVector2D vec)
		{
			return new SmlPoint2D(GeomUtils.RoundingError(p1.X - vec.X), GeomUtils.RoundingError(p1.Y - vec.Y));
		}

		/// <summary>
		/// Умножить точку на число
		/// </summary>
		/// <param name="p">Точка</param>
		/// <param name="k">Множитель</param>
		/// <returns></returns>
		public static SmlPoint2D operator *(SmlPoint2D p, double k)
		{
			return new SmlPoint2D(p.X*k, p.Y*k);
		}

		/// <summary>
		/// Равны ли две точки
		/// </summary>
		/// <param name="p1">Точка</param>
		/// <param name="p2">Точка</param>
		/// <returns></returns>
		public static bool operator ==(SmlPoint2D p1, SmlPoint2D p2)
		{
			if (ReferenceEquals(p1, p2)) return true;
			if (ReferenceEquals(p1, null)) return false;
			if (ReferenceEquals(null, p2)) return false;
			return ((p1.X == p2.X) && (p1.Y == p2.Y));
		}

		/// <summary>
		/// Не равны ли две точки
		/// </summary>
		/// <param name="p1">Точка</param>
		/// <param name="p2">Точка</param>
		/// <returns></returns>
		public static bool operator !=(SmlPoint2D p1, SmlPoint2D p2)
		{
			return !(p1 == p2);
		}

		/// <summary>
		/// Эквивалентность точек
		/// </summary>
		/// <param name="point">Сравниваемая точка</param>
		/// <returns><c>true</c> если эквивалентны ; иначе, <c>false</c>. </returns>
		public bool IsEquals(SmlPoint2D point)
		{
			return IsEquals(point, GeomUtils.Delta);
		}

		/// <summary>
		/// Эквивалентность точек
		/// </summary>
		/// <param name="point">Сравниваемая точка</param>
		/// <param name="tolerance">Точность</param>
		/// <returns><c>true</c> если эквивалентны ; иначе, <c>false</c>. </returns>
		public bool IsEquals(SmlPoint2D point, double tolerance)
		{
			return (Math.Abs((X - point.X)) <= tolerance && Math.Abs((Y - point.Y)) <= tolerance);
		}

		/// <summary>
		/// Больше ли точка р1 точки р2
		/// </summary>
		/// <param name="p1">Точка</param>
		/// <param name="p2">Точка</param>
		/// <returns></returns>
		public static bool operator >(SmlPoint2D p1, SmlPoint2D p2)
		{
			return ((p1.X > p2.X) || ((p1.X == p2.X) && (p1.Y > p2.Y)));
		}

		/// <summary>
		/// Меньше ли точка р1 точи р2
		/// </summary>
		/// <param name="p1">Первая точка</param>
		/// <param name="p2">Вторая точка</param>
		/// <returns>Результат сравнения</returns>
		public static bool operator <(SmlPoint2D p1, SmlPoint2D p2)
		{
			return ((p1.X < p2.X) || ((p1.X == p2.X) && (p1.Y < p2.Y)));
		}

		/// <summary>
		/// Местоположение точки относительно ограниченного точками p1 и р2 отрезка
		/// </summary>
		/// <param name="p1">Точка</param>
		/// <param name="p2">Точка</param>
		/// <returns></returns>
		public Location Locate(SmlPoint2D p1, SmlPoint2D p2)
		{
			return Location.Right;
		}

		/// <summary>
		/// Определение местоположения относительно отрезка S
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public Location Locate(SmlSegment2D s)
		{
			return Location.Right;
		}

		/// <summary>
		/// Возврашает эту точку в новой системе координат
		/// </summary>
		/// <param name="p">Начало отсчета новой системы координат относительно старой</param>
		/// <returns></returns>
		public SmlPoint2D InNewOrigin(SmlPoint2D p)
		{
			return new SmlPoint2D(X - p.X, Y - p.Y);
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns> A <see cref="System.String"/> that represents this instance. </returns>
		public override string ToString()
		{
			return "(" + X + ";" + Y + ")";
		}

		/// <summary>
		/// Equalses the specified other.
		/// </summary>
		/// <param name="other">The other.</param>
		/// <returns></returns>
		public bool Equals(SmlPoint2D other)
		{
			return EqualsImpl(this, other);
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.NullReferenceException">
		/// The <paramref name="obj"/> parameter is null.
		///   </exception>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof (SmlPoint2D)) return false;
			return Equals((SmlPoint2D) obj);
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>
		/// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
		/// </returns>
		public override int GetHashCode()
		{
			return HashCode.GetHashCode(X.GetHashCode(), Y.GetHashCode());
		}
	}
}

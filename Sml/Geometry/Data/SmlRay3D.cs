// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System.Diagnostics;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Представление луча в пространстве
	/// </summary>
	[DebuggerDisplay("P = {_point} Vector = {_vector}")]
	public class SmlRay3D : SmlLine3DBase
	{
		/// <summary>
		/// Создание экземпляра класса <see cref="SmlRay3D"/>.
		/// </summary>
		public SmlRay3D() { }

		/// <summary>
		/// Создание экземпляра класса <see cref="SmlRay3D"/>.
		/// </summary>
		/// <param name="pt1">Первая точка лежащая на прямой.</param>
		/// <param name="pt2">Вторая точка лежащая на прямой.</param>
		public SmlRay3D(SmlPoint3D pt1, SmlPoint3D pt2) : base(pt1, pt2) { }

		/// <summary>
		/// Создание экземпляра класса <see cref="SmlRay3D"/>.
		/// </summary>
		/// <param name="pt">Точка лежащая на прямой.</param>
		/// <param name="vec">Направляющий вектор.</param>
		public SmlRay3D(SmlPoint3D pt, SmlVector3D vec) : base(pt, vec) { }

		/// <summary>
		/// Создание экземпляра класса <see cref="SmlRay3D"/>.
		/// </summary>
		/// <param name="ray">Исходный луч для создания экземпляра.</param>
		/// <remarks>Конструктор копирования</remarks>
		public SmlRay3D(SmlRay3D ray) : base(ray) { }




		protected override bool IsOnImpl(SmlPoint3D p, double delta)
		{
			bool bres = base.IsOnImpl(p, delta);
			if (!bres) return false;

			SmlVector3D check = _point - p;
			if (check.IsZeroVector) return true;
			return !check.IsCodirectional(_vector, delta);
		}

		/// <summary>
		/// Точка пересечения луча и прямой
		/// </summary>
		/// <param name="line">Пересекаемая прямая</param>
		/// <param name="p">Точка пересечения</param>
		/// <returns>Пересекаются ли прямая с лучем</returns>
		public override bool CrossPoint(SmlLine3DBase line, out SmlPoint3D p)
		{
			bool bres = base.CrossPoint(line, out p);
			if (!bres) return false;
			return IsOn(p);
		}

		/// <summary>
		/// Точка пересечения двух лучей
		/// </summary>
		/// <param name="ray">Пересекаемый луч</param>
		/// <param name="p">Точка пересечения</param>
		/// <returns><c>true</c>если лучи пересекаются; иначе,<c>false</c></returns>
		public bool CrossPoint(SmlRay3D ray, out SmlPoint3D p)
		{
			bool bres = base.CrossPoint(ray, out p);
			if (!bres) return false;
			return IsOn(p) && ray.IsOn(p);
		}

		/// <summary>
		/// Получить или задать начальную точку луча.
		/// </summary>
		/// <value>  Начальную точку луча  </value>
		public SmlPoint3D StartPoint
		{
			get { return _point; }
			set { _point = value; }
		}

		/// <summary>
		/// Получить или задать направляющий вектор для луча
		/// </summary>
		/// <value>Направляющий вектор для для</value>
		public SmlVector3D GuidingVector
		{
			get { return _vector; }
			set { _vector = value; }
		}

		/// <summary>
		/// Луч соответсвующий полуоси OX
		/// </summary>
		public static SmlRay3D XAxis
		{
			get { return new SmlRay3D(new SmlPoint3D(), SmlVector3D.XAxis); }
		}

		/// <summary>
		/// Луч соответсвующий полуоси OY
		/// </summary>
		public static SmlRay3D YAxis
		{
			get { return new SmlRay3D(new SmlPoint3D(), SmlVector3D.YAxis); }
		}

		/// <summary>
		/// Луч соответсвующий полуоси OZ
		/// </summary>
		public static SmlRay3D ZAxis
		{
			get { return new SmlRay3D(new SmlPoint3D(), SmlVector3D.ZAxis); }
		}
	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;

namespace Sml.Geometry.Data
{
	///<summary>
	///Базовый класс
	///</summary>
	public abstract class SmlPolygonBase2D : SmlTaggedObject
	{
		/// <summary>
		/// Двусвязный список точек - вершин полигона
		/// </summary>
		protected LinkedList<SmlPoint2D> _polygon = new LinkedList<SmlPoint2D>();

		///<summary>
		/// Ограничивающий прямоугольник
		///</summary>
		public virtual SmlBoundaryRect2D GetBoundaryRect()
		{
			return GeomUtils.CreateBoundaryRectangle(Vertexes);
		}

		/// <summary>
		/// Получить или задать Orientation
		/// </summary>
		/// <value>Orientation</value>
		public Orientation Orientation
		{
			get
			{
				if (_polygon.Count <= 2)
					return Orientation.CounterClockwise;

				SmlVector2D first = new SmlVector2D(this[0], this[1]);
				SmlVector2D second = new SmlVector2D(this[2], this[1]);
				return first.Orientation(second);
			}
		}
		/// <summary>
		/// Инициализация нового экзепляра  <see cref="SmlPolygonBase2D"/> class.
		/// </summary>
		protected SmlPolygonBase2D() { }

		/// <summary>
		/// Конструктор копирования полигона
		/// </summary>
		/// <param name="plg">Копируемый полигон</param>
		protected SmlPolygonBase2D(SmlPolygonBase2D plg)
		{
			foreach (SmlPoint2D vertex in plg.Vertexes)
				_polygon.AddLast(new SmlPoint2D(vertex));
		}

		/// <summary>
		/// Конструктор полигона по списку точек
		/// </summary>
		/// <param name="points">Список точек</param>
		protected SmlPolygonBase2D(List<SmlPoint2D> points)
		{
			if (points == null) throw new ArgumentNullException("points");
			if (points.Count == 0)
				throw new InvalidOperationException("Полигон не содержит ни одной вершины");
			InsertImpl(0, new SmlPoint2D(points[0]));
			for (int i = 1; i < points.Count; i++)
				InsertImpl(i - 1, new SmlPoint2D(points[i]));
		}

        /// <summary>
        /// Конструктор полигона по списку точек
        /// </summary>
        /// <param name="points">Список точек</param>
        protected SmlPolygonBase2D(List<SmlSPoint2D> points)
        {
            if (points == null) throw new ArgumentNullException("points");
            if (points.Count == 0)
                throw new InvalidOperationException("Полигон не содержит ни одной вершины");
            InsertImpl(0, new SmlPoint2D(points[0]));
            for (int i = 1; i < points.Count; i++)
                InsertImpl(i - 1, new SmlPoint2D(points[i]));
        }


		/// <summary>
		/// Выпуклый полигон или нет
		/// </summary>
		/// <exception cref="InvalidOperationException">Полигон не содержит вершин</exception>
		public bool IsConvex
		{
			get
			{
				ExistVertexesCheck();
				int i = 0;
				for (LinkedList<SmlPoint2D>.Enumerator num = _polygon.GetEnumerator(); num.MoveNext(); i++)
					if (!IsConvexVertex(i))
						return false;
				return true;
			}
		}

		/// <summary>
		/// Количество вершин полигона
		/// </summary>
		public int VertexCount
		{
			get { return _polygon.Count; }
		}

		/// <summary>
		/// Возвращает вершины полигона (readonly)
		/// </summary>
		/// <value>The vertexes.</value>
		public List<SmlPoint2D> Vertexes
		{
			get
			{
				List<SmlPoint2D> vrtxs = new List<SmlPoint2D>();
				foreach (SmlPoint2D point2D in _polygon)
					vrtxs.Add(point2D);
				return vrtxs;
			}
		}

		/// <summary>
		/// Минимальная вершина полигона (левая нижняя вершина)
		/// </summary>
		/// <exception cref="InvalidOperationException">Полигон пустой, вершин не содержит</exception>
		public SmlPoint2D MinVertex
		{
			get
			{
				ExistVertexesCheck();
				SmlPoint2D p1 = new SmlPoint2D(_polygon.First.Value);
				foreach (SmlPoint2D p in _polygon)
				{
					if (p < p1)
						p1 = new SmlPoint2D(p);
				}
				return p1;
			}
		}

		/// <summary>
		/// Проверка наличия в полигоне хотя бы одной вершины
		/// </summary>
		/// <exception cref="InvalidOperationException">В случае если вершин в полигоне не найдено</exception>
		protected void ExistVertexesCheck()
		{
			if (!(_polygon.Count > 0))
				throw new InvalidOperationException("Полигон не содержит ни одной вершины");
		}

		/// <summary>
		/// Проверка наличия в полигоне вершины с таким индексом
		/// </summary>
		/// <param name="index">Проверяемый индекс</param>
		/// <exception cref="IndexOutOfRangeException">В случае если вершина с таким индексом не найдена</exception>
		protected void ExistIndexCheck(int index)
		{
			if (index < _polygon.Count) return;
			throw new IndexOutOfRangeException("Вершина с таким значением индекса не найдена");
		}

		/// <summary>
		/// Возвращает узел связанного списка вершин по индексу
		/// </summary>
		/// <param name="index">Индекс вершины</param>
		/// <returns>Узел списка</returns>
		protected LinkedListNode<SmlPoint2D> VertexNode(int index)
		{
			LinkedListNode<SmlPoint2D> lln = _polygon.First;
			if (index == 0) return lln;

			int i = 1;
			while ((lln = lln.Next) != null)
			{
				if (i == index) return lln;
				i++;
			}
			throw new IndexOutOfRangeException("Вершина с таким значением индекса не найдена");
		}

		/// <summary>
		/// Возвращает узел связанного списка вершин если точка является вершиной
		/// </summary>
		/// <param name="p">Исследуемая точка</param>
		/// <returns>Узел списка вершин</returns>
		protected LinkedListNode<SmlPoint2D> VertexNode(SmlPoint2D p)
		{
			LinkedListNode<SmlPoint2D> lln = _polygon.First;
			for (LinkedList<SmlPoint2D>.Enumerator num = _polygon.GetEnumerator(); num.MoveNext(); lln = lln.Next)
				if (num.Current == p)
					break;
			return lln;
		}

		/// <summary>
		/// Итератор для списка вершин полигона
		/// </summary>
		/// <returns>Итератор</returns>
		public IEnumerator<SmlPoint2D> GetEnumerator()
		{
			for (int i = 0; i < _polygon.Count; i++)
				yield return VertexNode(i).Value;
		}

		/// <summary>
		/// Является ли выпуклой вершина по заданному индексу
		/// </summary>
		/// <param name="index">Индекс исследуемой вершины</param>
		/// <returns>Выпуклая или нет</returns>
		/// <exception cref="IndexOutOfRangeException">Вершины с таким индексом в этом полигоне нет</exception>
		///<exception cref="InvalidOperationException">Полигон пустой, вершин не содержит</exception>
		public bool IsConvexVertex(int index)
		{
			ExistVertexesCheck();
			ExistIndexCheck(index);
			if (_polygon.Count <= 2)
				return true;
			LinkedListNode<SmlPoint2D> current = VertexNode(index);
			LinkedListNode<SmlPoint2D> previos = current == _polygon.First ? _polygon.Last : current.Previous;
			LinkedListNode<SmlPoint2D> next = current == _polygon.Last ? _polygon.First : current.Next;
			return new SmlVector2D(next.Value, current.Value).Orientation(new SmlVector2D(previos.Value, current.Value)) ==
				   Orientation.CounterClockwise;
		}

		/// <summary>
		/// Индексатор для доступа к вершинам полигона по индексу. Индекс начинается с нуля.
		/// </summary>
		/// <param name="index">Индекс вершины</param>
		/// <returns>Вершина по указанному индексу</returns>
		/// <exception cref="InvalidOperationException">Полигон не содержит вершин</exception>
		/// <exception cref="IndexOutOfRangeException">Вершины с таким индексом не существует</exception>
		public SmlPoint2D this[int index]
		{
			get
			{
				ExistVertexesCheck();
				ExistIndexCheck(index);
				LinkedListNode<SmlPoint2D> lln = VertexNode(index);
				return lln.Value;
			}
			set
			{
				ExistVertexesCheck();
				ExistIndexCheck(index);
				LinkedListNode<SmlPoint2D> lln = VertexNode(index);
				lln.Value = value;
			}
		}

		/// <summary>
		/// Трансформирование
		/// </summary>
		/// <param name="matrix">Матрица трансформации</param>
		protected void TransformByImpl(SmlMatrix2D matrix)
		{
			foreach (SmlPoint2D pt in _polygon) pt.TransformBy(matrix);
		}


		/// <summary>
		/// Возвращает ребро полигона между вершинами с указанным индексом и последующей
		/// </summary>
		/// <param name="index">Индекс вершины</param>
		/// <returns>Ребро полигона</returns>
		/// <exception cref="InvalidOperationException">Полигон не содержит вершин</exception>
		/// <exception cref="IndexOutOfRangeException">Вершины с таким индексом не найдено</exception>
		public SmlSegment2D GetEdge(int index)
		{
			ExistVertexesCheck();
			ExistIndexCheck(index);
			if (_polygon.Count == 1)
				return new SmlSegment2D(this[0], this[0]);
			LinkedListNode<SmlPoint2D> lln1 = VertexNode(index);
			LinkedListNode<SmlPoint2D> lln2 = lln1 == _polygon.Last ? _polygon.First : lln1.Next;
			return new SmlSegment2D(lln1.Value, lln2.Value);
		}

		/// <summary>
		/// Возвращает индекс вершины при первом совпадении с точкой р
		/// </summary>
		/// <param name="p">Точка, для которой ищется индекс</param>
		/// <param name="index">Возвращаемый индекс вершины</param>
		/// <returns>Истинно если, вершина, соответсвующая точке р, найдена</returns>
		/// <exception cref="InvalidOperationException">Полигон не имеет вершин</exception>
		public bool Index(SmlPoint2D p, out int index)
		{
			ExistVertexesCheck();
			index = 0;
			for (LinkedList<SmlPoint2D>.Enumerator num = _polygon.GetEnumerator(); num.MoveNext(); index++)
				if (num.Current == p)
					return true;
			return false;
		}

		/// <summary>
		/// Сервисный метод для определения факта пересечения ребра полигона и горизонтального луча в положительную полуось ОХ
		/// </summary>
		/// <param name="i">Индекс вершины, между которой и ее последователем берется ребро</param>
		/// <param name="p">Точка, из которой берется луч по направлению положительной полуоси ОХ</param>
		/// <returns>Пересекается ребро с лучом или нет</returns>
		/// <exception cref="InvalidOperationException">Полигон не содержит вершин</exception>
		/// <exception cref="IndexOutOfRangeException">Вершины с таким индексом не найдено</exception>
		/// <exception cref="InfinityException">Точка лежит на периметре полигона</exception>
		protected bool IsEdgeCrossing(int i, SmlPoint2D p)
		{
			SmlSegment2D s = GetEdge(i);
			switch (s.Locate(p))
			{
				case Location.Left:
					return ((Math.Round(s.StartPoint.Y, 10) < Math.Round(p.Y, 10)) &&
							(Math.Round(p.Y, 10) <= Math.Round(s.EndPoint.Y, 10)));
				case Location.Right:
					return ((Math.Round(s.EndPoint.Y, 10) < Math.Round(p.Y, 10)) &&
							(Math.Round(p.Y, 10) <= Math.Round(s.StartPoint.Y, 10)));
				case Location.Between:
				case Location.Origin:
				case Location.Destination:
					return true;
			}
			return false;
		}

		/// <summary>
		/// Является ли точка р одной из вершин полигона
		/// </summary>
		/// <param name="p">исследуемая точка</param>
		/// <returns>Истинно если является</returns>
		/// <exception cref="InvalidOperationException">Полигон пустой, не имеет точек</exception>
		public bool IsVertex(SmlPoint2D p)
		{
			ExistVertexesCheck();
			foreach (SmlPoint2D p1 in _polygon)
				if (p1 == p)
					return true;
			return false;
		}


		/// <summary>
		/// Находится ли полигон внутри полигона
		/// </summary>
		/// <param name="polygon">Полигон.</param>
		/// <returns>
		/// 	<c>true</c> если операции выполнена успешна, иначе <c>false</c>
		/// </returns>
		/// <exception cref="InvalidOperationException">Полигон не содержит ни одной вершины, либо одну вершинy</exception>
		public bool IsInside(SmlPolygonBase2D polygon)
		{
			foreach (SmlPoint2D pt in polygon)
			{
				if (!IsInside(pt))
					return false;
			}
			return true;
		}

		/// <summary>
		/// Находится ли точка внутри полигона
		/// </summary>
		/// <param name="p">Исследуемая точка</param>
		/// <returns></returns>
		/// <exception cref="InvalidOperationException">Полигон не содержит ни одной вершины, либо одну вершинy</exception>
		public bool IsInside(SmlPoint2D p)
		{
			ExistVertexesCheck();
			if (_polygon.Count == 1)
				throw new InvalidOperationException("Полигон содержит только одну вершину.");
			bool parity = false;
			for (int i = 0; i < _polygon.Count; i++)
			{
				try
				{
					parity = parity ^ IsEdgeCrossing(i, p);
				}
				catch (InfinityException) { return true; }
			}
			return parity;
		}

		/// <summary>
		/// Получить список ребер полигона
		/// </summary>
		/// <returns>Список ребер полигона</returns>
		public List<SmlSegment2D> GetEdges()
		{
			List<SmlSegment2D> fl = new List<SmlSegment2D>();

			if (VertexCount == 0) return fl;

			for (int i = 0; i < VertexCount - 1; i++)
			{
				SmlPoint2D pt1 = VertexNode(i).Value;
				SmlPoint2D pt2 = VertexNode(i + 1).Value;
				fl.Add(new SmlSegment2D(pt1, pt2));
			}

			SmlPoint2D tpt1 = VertexNode(VertexCount - 1).Value;
			SmlPoint2D tpt2 = VertexNode(0).Value;
			fl.Add(new SmlSegment2D(tpt1, tpt2));
			return fl;
		}

		/// <summary>
		/// Определяет пересечение полигона с отезком
		/// </summary>
		/// <param name="s">Отрезок</param>
		/// <returns>
		/// 	<c>true</c> если пересекает пересекает полигон, иначе, <c>false</c>.
		/// </returns>
		public virtual bool HasIntersect(SmlSegment2D s)
		{
			List<SmlSegment2D> fl1 = GetEdges();
			foreach (SmlSegment2D seSmlent2D in fl1)
			{
				SmlPoint2D pt = new SmlPoint2D();
				if (seSmlent2D.CrossPoint(s, ref pt))
					return true;
			}
			return false;
		}

		/// <summary>
		/// Определяет пересечение полигона с отезком
		/// </summary>
		/// <param name="s">Отрезок</param>
		/// <returns>
		/// 	<c>true</c> если пересекает пересекает полигон, иначе, <c>false</c>.
		/// </returns>
		public virtual bool HasIntersect2(SmlSegment2D s)
		{
			if (IsInside(s.StartPoint) && !IsInside(s.EndPoint))
				return true;
			if (!IsInside(s.StartPoint) && IsInside(s.EndPoint))
				return true;
			return false;
		}

		/// <summary>
		/// Определяет пересечение 2 полигонов
		/// </summary>
		/// <param name="polygon">Полигон</param>
		/// <returns>
		/// 	<c>true</c> если полигоны пересекаются, иначе, <c>false</c>.
		/// </returns>
		public bool HasIntersect(SmlPolygonBase2D polygon)
		{
			ExistVertexesCheck();
			polygon.ExistVertexesCheck();

			// Проверим ребра полигонов на пересечение            
			List<SmlSegment2D> fl2 = polygon.GetEdges();
			foreach (SmlSegment2D seSmlent2D in fl2)
			{
				if (HasIntersect(seSmlent2D))
					return true;
			}

			return false;
		}

		/// <summary>
		/// Вставляет новую вершину в полигон
		/// </summary>
		/// <param name="index">Номер вершины, после которой производится вставка</param>
		/// <param name="p">Точка, по которой вставляется новая вершина</param>
		/// <exception cref="IndexOutOfRangeException">Вершина с таким индексом в полигоне не найдена</exception>
		protected void InsertImpl(int index, SmlPoint2D p)
		{
			if (_polygon.Count == 0)
			{
				_polygon.AddFirst(p);
				return;
			}
			ExistIndexCheck(index);
			////Если в списке две вершины, третью надо вставить так, чтобы вершины были положительно ориентированными (ПрЧС)
			//if (_polygon.Count == 2)
			//{
			//    //Если вектора, образованные начальной вершиной в списке и последующей, текущей вершиной и вставляемой вершиной, 
			//    //являются положительно ориентированными
			//    if (new Vector2D(_polygon.First.Next.Value,_polygon.First.Value)
			//            .Orientation(new Vector2D(p,_polygon.First.Value)) == Orientation.CounterClockwise)
			//    {
			//        lln = lln.Next;
			//        _polygon.AddAfter(lln, p);
			//        return;
			//    }
			//    _polygon.AddAfter(lln, p);
			//    return;
			//}
			LinkedListNode<SmlPoint2D> lln = VertexNode(index);
			_polygon.AddAfter(lln, p);
		}

		/// <summary>
		/// Вставляет новую вершину в полигон
		/// Если полигон пустой, вставляет вершину первой
		/// </summary>
		/// <param name="index">Индекс вершины, после которой производится вставка</param>
		/// <param name="x">Абсцисса вставляемой вершины</param>
		/// <param name="y">Ордината вставляемой вершины</param>
		/// <exception cref="InfinityException">Вершины с таким индексом не существует</exception>
		protected void InsertImpl(int index, double x, double y)
		{
			InsertImpl(index, new SmlPoint2D(x, y));
		}

		protected int GetNextIndex(int index)
		{
			index++;
			if (index == VertexCount) return 0;
			return index;
		}

		protected int GetPrevIndex(int index)
		{
			index--;
			if (index == -1) return VertexCount - 1;
			return index;
		}

		/// <summary>
		/// Равны ли два полигона
		/// </summary>
		/// <param name="p1">Первый полигон</param>
		/// <param name="p2">Второй полигон</param>
		/// <returns></returns>
		static public bool operator ==(SmlPolygonBase2D p1, SmlPolygonBase2D p2)
		{
			if (ReferenceEquals(p1, p2)) return true;
			if (ReferenceEquals(p1, null)) return false;
			if (ReferenceEquals(null, p2)) return false;

			if (p1._polygon.Count == p2._polygon.Count)
			{
				for (int i = 0; i < p1._polygon.Count; i++)
				{
					if (p1[i] != p2[i])
						return false;
				}
				return true;
			}

			return false;
		}

		/// <summary>
		/// Не равны ли два полигона
		/// </summary>
		/// <param name="p1">Первый полигон</param>
		/// <param name="p2">Второй полигон</param>
		/// <returns></returns>
		static public bool operator !=(SmlPolygonBase2D p1, SmlPolygonBase2D p2)
		{
			return !(p1 == p2);
		}
	}
}

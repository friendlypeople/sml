// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Представление линии на плоскости
	/// </summary>
    [Serializable]
    public class SmlLine2D : SmlTaggedObject
    {
    	/// <summary>
		/// член при Х(канонич. ур-е прямой Ax + By + C = 0)
    	/// </summary>
		protected double _a;

		/// <summary>
        /// член при У(канонич. ур-е прямой Ax + By + C = 0)
		/// </summary>
    	protected double _b;


		/// <summary>
        /// св. член(канонич. ур-е прямой Ax + By + C = 0)
		/// </summary>
    	protected double _c;
		
    	/// <summary>
    	/// Коэффициент А канонического уравнения прямой
    	/// </summary>
		public double A
    	{
    		get { return _a; }
            set { _a = value; }
    	}

		/// <summary>
		/// Коэффициент B канонического уравнения прямой
		/// </summary>
		public double B
		{
			get { return _b; }
            set { _b = value; }
		}

		/// <summary>
		/// Коэффициент C канонического уравнения прямой
		/// </summary>
		public double C
		{
			get { return _c; }
            set { _c = value; }
		}

        /// <summary>
        /// Тангенс угла наклона прямой к положит. оси ОХ(ур-е с угл. коэф.)
        /// </summary>
        /// <exception cref="InfinityException">Для вертикальной прямой тангенс угла не определен</exception>
        public double K
        {
            get 
            {
                if (Math.Abs(_b - 0) < GeomUtils.Delta)
                    throw new InfinityException("Вертикальная прямая");
                return GeomUtils.RoundingError(-A/B);
            }
        }

        /// <summary>
        /// Свободный член уравнения прямой(ур-е с угл. коэф.)
        /// </summary>
        public double D
        {
            get
            {
				if (Math.Abs(_b - 0) < GeomUtils.Delta)
                    return GeomUtils.RoundingError(-C/A);
                return GeomUtils.RoundingError(-C/B);
            }
        }

		/// <summary>
		/// Задание прямой по двум точкам
		/// </summary>
		/// <param name="p1">первая задающая точка</param>
		/// <param name="p2">вторая задающая точка</param>
		public virtual void Set(SmlPoint2D p1, SmlPoint2D p2)
		{
			SmlLine2D l = new SmlLine2D(p1,p2);
		    _a = l.A;
		    _b = l.B;
		    _c = l.C;
		}

        /// <summary>
        /// Задание линейного обекта по точке и вектору
        /// </summary>
        /// <param name="p">задающая точка</param>
        /// <param name="v">задающий вектор</param>
        public virtual void Set(SmlPoint2D p, SmlVector2D v)
        {
            SmlLine2D l = new SmlLine2D(p, (p.AsVector() + v).ToPoint());
            _a = l.A;
            _b = l.B;
            _c = l.C;
        }

		/// <summary>
		/// Задание прямой по другой прямой
		/// </summary>
		/// <param name="l">Задающая прямая</param>
		public virtual void Set(SmlLine2D l)
		{
		    _a = l.A;
		    _b = l.B;
		    _c = l.C;
		}

        /// <summary>
        /// Задание прямой по лучу
        /// </summary>
        /// <param name="r">Задающий луч</param>
        public virtual void Set(SmlRay2D r)
        {
            SmlLine2D l = new SmlLine2D((SmlLine2D) r);
            _a = l.A;
            _b = l.B;
            _c = l.C;
        }

        /// <summary>
        /// Задание прямой по отрезку
        /// </summary>
        /// <param name="s">Задающий отрезок</param>
        public virtual void Set(SmlSegment2D s)
        {
            SmlLine2D l = new SmlLine2D((SmlLine2D) s);
            _a = l.A;
            _b = l.B;
            _c = l.C;
        }

		/// <summary>
		/// Конструктор прямой по умолчанию
		/// </summary>
        public SmlLine2D()
		{
			_a = 0;
			_b = 0;
			_c = 0;
		}
        
        /// <summary>
		/// Конструктор прямой по двум точкам
		/// </summary>
		/// <param name="p1">точка</param>
		/// <param name="p2">точка</param>
		public SmlLine2D (SmlPoint2D p1, SmlPoint2D p2)
		{
            _a = p2.Y - p1.Y;
            _b = p1.X - p2.X;
            _c = GeomUtils.RoundingError(-p1.X*(p2.Y - p1.Y) + p1.Y*(p2.X - p1.X));
        }

        /// <summary>
        /// Конструктор прямой по точке и вектору
        /// </summary>
        /// <param name="p">Задающая точка</param>
        /// <param name="v">Задающий вектор</param>
        public SmlLine2D(SmlPoint2D p,SmlVector2D v)
        {
            if (p == new SmlPoint2D(0,0))
            {
				SmlLine2D l = new SmlLine2D(p, v.ToPoint());
                _a = l.A;
                _b = l.B;
                _c = l.C;
            }
            else
            {
				SmlLine2D l = new SmlLine2D(p, (p.AsVector() + v).ToPoint());
                _a = l.A;
                _b = l.B;
                _c = l.C;
            }
        }

        /// <summary>
        /// Конструктор копирования
        /// </summary>
        /// <param name="l">Задающая прямая</param>
        public SmlLine2D (SmlLine2D l)
        {
            _a = l.A;
            _b = l.B;
            _c = l.C;
        }

        /// <summary>
        /// Конструктор линии по лучу
        /// </summary>
        /// <param name="r">Задающий луч</param>
        public SmlLine2D (SmlRay2D r)
        {
            SmlLine2D l = new SmlLine2D((SmlLine2D) r);
            _a = l.A;
            _b = l.B;
            _c = l.C;
        }

        /// <summary>
        /// Конструктор линии по отрезку
        /// </summary>
        /// <param name="s">Задающий отрезко</param>
        public SmlLine2D (SmlSegment2D s)
        {
            SmlLine2D l = new SmlLine2D((SmlLine2D) s);
            _a = l.A;
            _b = l.B;
            _c = l.C;
        }

		/// <summary>
		/// Вычисляет значение абсциссы точки на прямой по значению его ординаты
		/// </summary>
		/// <param name="x">возвращает значение абсциссы точки</param>
		/// <param name="y">ордината точки</param>
		/// <returns>истинно если точка имеет абсциссу</returns>
		/// <exception cref="InfinityException">На горизонтальной прямой на соответсвующей ординате значение абсциссы может быть любым</exception>
		public  virtual bool XValue(ref double x, double y)
		{
			if (Math.Abs(_a - 0) > GeomUtils.Delta)
            {
            	x = GeomUtils.RoundingError((-_b*y - _c)/_a);
                return true;
            }
			if (Math.Abs(y - GeomUtils.RoundingError(-_c / _b)) < GeomUtils.Delta)
                throw new InfinityException(
                    "Горизонтальная прямая. Значение абсциссы при этой ординате может быть любым");
		    x = 0;
            return false;
		}

        /// <summary>
        /// Поворот прямой ПрЧС
        /// </summary>
        /// <param name="alpha">Угол поворота</param>
        /// <param name="p">Точка, вокруг которой производится поворот</param>
        public virtual void RotatePositive(double alpha, SmlPoint2D p)
        {
            SmlRay2D r = new SmlRay2D();
            try
            {
                r.Set(PointOn(), new SmlVector2D(GeomUtils.RoundingError(Math.Atan(K))));
            }
            catch (InfinityException)
            {
                r.Set(PointOn(), new SmlVector2D(Math.PI / 2));
            }

            r.RotatePositive(alpha, p);
            Set(r);
        }

        /// <summary>
        /// Поворот прямой ПЧС
        /// </summary>
        /// <param name="alpha">Угол поворота</param>
        /// <param name="p">Точка, вокруг которой производится поворот</param>
        public virtual void RotateNegative(double alpha, SmlPoint2D p)
        {
            SmlRay2D r = new SmlRay2D();
            try
            {
                r.Set(PointOn(), new SmlVector2D(GeomUtils.RoundingError(Math.Atan(K))));
            }
            catch (InfinityException)
            {
				r.Set(PointOn(), new SmlVector2D(Math.PI / 2));
            }
            
            r.RotateNegative(alpha,p);
            Set(r);
        }

        /// <summary>
        /// Возвращает повернутую ПрЧС прямую
        /// </summary>
        /// <param name="alpha">Угол поворота</param>
        /// <param name="p">Точка, вокруг которой производится поворот</param>
        /// <returns></returns>
        public SmlLine2D RotatePositiveCreate(double alpha, SmlPoint2D p)
        {
            SmlRay2D r = new SmlRay2D();
            try
            {
                r.Set(PointOn(), new SmlVector2D(GeomUtils.RoundingError(Math.Atan(K))));
            }
            catch (InfinityException)
            {
                r.Set(PointOn(), new SmlVector2D(Math.PI / 2));
            }
            r.RotatePositive(alpha, p);
            return new SmlLine2D(r);
        }

        /// <summary>
        /// Возвращает повернутую ПЧС прямую
        /// </summary>
        /// <param name="alpha">Угол поворота</param>
        /// <param name="p">Точка, вокруг которой производится поворот</param>
        /// <returns></returns>
        public SmlLine2D RotateNegativeCreate(double alpha, SmlPoint2D p)
		{
            SmlRay2D r = new SmlRay2D();
            try
            {
                r.Set(PointOn(), new SmlVector2D(GeomUtils.RoundingError(Math.Atan(K))));
            }
            catch (InfinityException)
            {
                r.Set(PointOn(), new SmlVector2D(Math.PI / 2));
            }
            r.RotateNegative(alpha, p);
            return new SmlLine2D(r);
		}

    	/// <summary>
		/// Вычисляет значение ординаты точки на линейном объекте по значению его абсциссы
		/// </summary>
		/// <param name="x">абсцисса точки</param>
		/// <param name="y">возвращает значение ординаты точки</param>
		/// <returns>истинно если точка имеет ординату</returns>
		/// <exception cref="InfinityException">У вертикальной прямой при соответсвующей абсциссе значение ординаты точно определить невозможно</exception>
		public virtual bool YValue(double x, ref double y)
		{
			if (Math.Abs(_b - 0) > GeomUtils.Delta)
            {
            	y = GeomUtils.RoundingError((-_a*x - _c)/_b);
                return true;
            }
			if (Math.Abs(x - GeomUtils.RoundingError(-_c / _a)) < GeomUtils.Delta)
                throw new InfinityException(
                    "Вертикальная прямая. При этом значении абсциссы значение ординаты может быть любым");
            y = 0;
            return false;
		}

        /// <summary>
        /// Возвращает биссектрису угла, образованного пересечением вызывающей и другой прямой ПЧС
        /// </summary>
        /// <param name="l1">Пересекаемая прямая</param>
        /// <param name="l2">Биссектрисса</param>
        /// <returns>Возможно ли получить биссектрису</returns>
        public bool NegativeBisectorCreate(SmlLine2D l1, out SmlLine2D l2)
        {
            l2 = new SmlLine2D();
            if (IsParallel(l1))
                return false;
            l2.A = GeomUtils.RoundingError(_a + l1.A);
            l2.B = GeomUtils.RoundingError(_b + l1.B);
            l2.C = GeomUtils.RoundingError(_c + l1.C);
            return true;
        }

        /// <summary>
        /// Возвращает биссектрису угла, образованного пересечением вызывающей и другой прямой ПЧС
        /// </summary>
        /// <param name="l1">Пересекаемая прямая</param>
        /// <param name="l2">Биссектрисса</param>
        /// <returns>Возможно ли получить биссектрису</returns>
        public bool PositiveBisectorCreate(SmlLine2D l1, out SmlLine2D l2)
        {
            l2 = new SmlLine2D();
            if (IsParallel(l1))
                return false;
            l2.A = GeomUtils.RoundingError(_a - l1.A);
            l2.B = GeomUtils.RoundingError(_b - l1.B);
            l2.C = GeomUtils.RoundingError(_c - l1.C);
            return true;
        }

		/// <summary>
		/// Лежит ли точка на прямой
		/// </summary>
		/// <param name="p">Исследуемая точка</param>
		/// <returns><c>true</c> если точка лежит на прямой; иначе <c>false</c>.</returns>
		public virtual bool IsOn(SmlPoint2D p)
		{
			double roundingError = GeomUtils.RoundingError(_a*p.X + _b*p.Y + C);
			double abs = Math.Abs(roundingError);
			return (abs < 0.000001);
		}

		/// <summary>
		/// Перпендикулярны ли прямые
		/// </summary>
		/// <param name="l">Исследуемая прямая</param>
		/// <returns></returns>
		public bool IsPerpendicular (SmlLine2D l)
		{
			if (Math.Abs(_a) < GeomUtils.Delta && Math.Abs(_b) < GeomUtils.Delta &&
				Math.Abs(l.A) < GeomUtils.Delta && Math.Abs(l.B) < GeomUtils.Delta)
				return true;

			if ((Math.Abs(_a) < GeomUtils.Delta && Math.Abs(_b) < GeomUtils.Delta) ^
				(Math.Abs(l.A) < GeomUtils.Delta && Math.Abs(l.B) < GeomUtils.Delta))
				return false;
			return (Math.Abs(GeomUtils.RoundingError(_a * l.A + _b * l.B)) < GeomUtils.Delta);
		}

        /// <summary>
        /// Является ли горизотальной прямая
        /// </summary>
        /// <returns></returns>
        public bool IsHorizontal()
        {
			return (Math.Abs(_a) < GeomUtils.Delta);
        }

        /// <summary>
        /// Является ли вертикальной прямая
        /// </summary>
        /// <returns></returns>
        public bool IsVertical()
        {
			return (Math.Abs(_b) < GeomUtils.Delta);
        }
        
		/// <summary>
		/// Параллельны ли прямые
		/// </summary>
		/// <param name="l">Исследуемая прямая</param>
		/// <returns></returns>
		public bool IsParallel(SmlLine2D l)
		{
			if (Math.Abs(_a) < GeomUtils.Delta && Math.Abs(_b) < GeomUtils.Delta &&
				Math.Abs(l.A) < GeomUtils.Delta && Math.Abs(l.B) < GeomUtils.Delta)
				return true;
			if ((Math.Abs(_a) < GeomUtils.Delta && Math.Abs(_b) < GeomUtils.Delta) ^
				(Math.Abs(l.A) < GeomUtils.Delta && Math.Abs(l.B) < GeomUtils.Delta))
				return false;
		    return (Math.Abs(GeomUtils.RoundingError(_a*l.B - l.A*_b)) < GeomUtils.Delta);
		}

		/// <summary>
		/// Точка пересечения прямых (вызывающей и аргумента)
		/// </summary>
		/// <param name="l">Пересекаемая прямая</param>
		/// <param name="p">Точка пересечения</param>
		/// <returns>Пересекаются ли прямые</returns>
		public virtual bool CrossPoint(SmlLine2D l, ref SmlPoint2D p)
		{
			if ((Math.Abs(GeomUtils.RoundingError(_a - l.A)) < GeomUtils.Delta) &&
				(Math.Abs(GeomUtils.RoundingError(_b - l.B)) < GeomUtils.Delta) &&
				(Math.Abs(GeomUtils.RoundingError(_c - l.C)) < GeomUtils.Delta))
				throw new InfinityException(
					"MGeometryFramework: Прямые совпадают. Любую точку на них можно считать точкой пересечения!");
			if (IsParallel(l))
				return false;
			//p.Set(GeomUtils.RoundingError((_b*l.C - l.B*_c)/(_a*l.B - l.A*_b)),
			//      GeomUtils.RoundingError((_a*l.C - l.A*_c)/(l.A*_b - _a*l.B)));

			double pX = (_b*l.C - l.B*_c)/(_a*l.B - l.A*_b);
			double pY = (_a*l.C - l.A*_c)/(l.A*_b - _a*l.B);
			p.SetCartesian(pX, pY);

			return true;
		}

		/// <summary>
        /// Точка пересечения вызывающей прямой и луча
        /// </summary>
        /// <param name="r">Исследуемый на пересечение луч</param>
        /// <param name="p">точка пересечения</param>
        /// <returns>Пересекаются ли прямая и луч</returns>
        public virtual bool CrossPoint(SmlRay2D r, ref SmlPoint2D p)
        {
            return r.CrossPoint(this,ref p);
        }

        /// <summary>
        /// Точка пересечения вызывающей прямой и отрезка
        /// </summary>
        /// <param name="s">Исследуемый на пересечение отрезок</param>
        /// <param name="p">Точка пересечения</param>
        /// <returns>Пересекаются ли прямая и отрезок</returns>
        public virtual bool CrossPoint(SmlSegment2D s, ref SmlPoint2D p)
        {
            return s.CrossPoint(this, ref p);
        }

		/// <summary>
		/// Расстояние от вызывающей прямой до точки
		/// </summary>
		/// <param name="p">точка</param>
		/// <returns></returns>
		public virtual double Distance(SmlPoint2D p)
		{
		    return Math.Abs((_a*p.X + _b*p.Y + _c)/Math.Sqrt(Math.Pow(_a, 2) + Math.Pow(B, 2)));
		}

		/// <summary>
		/// Расстояние между двумя !!!ПАРАЛЛЕЛЬНЫМИ!!! прямыми
		/// </summary>
		/// <param name="l">Прямая, параллельная вызывающей</param>
		/// <param name="d">Возвращаемое расстояние</param>
		/// <returns>результат возвращается если прямые параллельны</returns>
		public bool Distance(SmlLine2D l, ref double d)
		{
            if (IsParallel(l))
            {
                d = Math.Abs((_c - l.C)/Math.Sqrt(Math.Pow(_a, 2) + Math.Pow(_b, 2))); 
                return true;
            }
            return false;
		}

        /// <summary>
        /// Возвращает прямую, параллельную вызывающей и проходящей через заданную точку
        /// </summary>
        /// <param name="p">Точка, через которую проходит параллельная прямая</param>
        /// <returns></returns>
        public SmlLine2D GetParLine(SmlPoint2D p)
        {
			SmlLine2D l = new SmlLine2D();
			l.A = _a;
        	l.B = _b;
        	l.C = GeomUtils.RoundingError(-_a*p.X - _b*p.Y);
			return l;
        }

        /// <summary>
        /// Возвращает прямую, перепендикулярную вызывающей и проходящей через заданную точку
        /// </summary>
        /// <param name="p">Точка, через которую проходит перпендикулярная прямая</param>
        /// <returns></returns>
        public SmlLine2D GetPerpendLine(SmlPoint2D p)
        {
            SmlLine2D l = new SmlLine2D();
            l.A = _b;
            l.B = -_a;
            l.C = _a*p.Y - _b*p.Y;
            return l;
        }

        /// <summary>
        /// Возвращает угол между вызывающей прямой и другой прямой ПрЧС
        /// </summary>
        /// <param name="l">Прямая, между которой и вызывающей ищется угол</param>
        /// <returns>угол</returns>
        public double AnglePositive(SmlLine2D l)
        {
        	if (IsPerpendicular(l))
                return Math.PI/2;
            if (Math.Atan(GeomUtils.RoundingError((A * l.B - l.A * B) / (A * l.A + B * l.B))) >= 0)
                return Math.Atan(GeomUtils.RoundingError((A * l.B - l.A * B) / (A * l.A + B * l.B)));
           return (Math.PI + Math.Atan(GeomUtils.RoundingError((A*l.B - l.A*B)/(A*l.A + B*l.B))));
        }

        /// <summary>
        /// Возвращает угол между вызывающей прямой и другой прямой ПрЧС
        /// </summary>
        /// <param name="l">Прямая, между которой и вызывающей ищется угол</param>
        /// <returns>угол</returns>
        public double AngleNegative(SmlLine2D l)
        {
            if (IsPerpendicular(l))
                return Math.PI/2;
            if (Math.Atan(GeomUtils.RoundingError((_a * l.B - l.A * _b) / (_a * l.A + _b * l.B))) < 0)
                return Math.Abs(Math.Atan(GeomUtils.RoundingError((A*l.B - l.A*B)/(A*l.A + B*l.B))));
            return (Math.PI - Math.Atan(GeomUtils.RoundingError((A * l.B - l.A * B) / (A * l.A + B * l.B))));
        }

        /// <summary>
        /// Возвращает случайную точку на прямой
        /// </summary>
        /// <returns></returns>
        public virtual SmlPoint2D PointOn()
        {
			Random rnd = new Random();
			double c = 0;
        	double c1 = 0;
			if (YValue(rnd.Next(100), ref c))
			{
				if (XValue(ref c,rnd.Next(100)))
				{
					c = rnd.Next(100);
					YValue(c,ref c1);
					return new SmlPoint2D(c, c1);
				}
				YValue(rnd.Next(100), ref c);
				return new SmlPoint2D(rnd.Next(100),c);
			}
        	XValue(ref c, c1);
        	return new SmlPoint2D(c,rnd.Next(100));
        }
    }
}

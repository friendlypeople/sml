// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Diagnostics;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Представление угла
	/// </summary>
	[DebuggerDisplay("Radians = {Radians} Degrees = {Degrees}")]
	public class SmlAngle : IComparable<SmlAngle> 
	{
		private double _radian;

		/// <summary>
		/// Инициализация нового экземпляра класса <see cref="SmlAngle"/>.
		/// </summary>
		public SmlAngle() { }

		/// <summary>
		/// Инициализация нового экземпляра класса <see cref="SmlAngle"/>.
		/// </summary>
		/// <param name="radians">Значение угла в радианах.</param>
		public SmlAngle(double radians)
		{
			_radian = radians; 
		}

		/// <summary>
		/// Инициализация нового экземпляра класса <see cref="SmlAngle"/>.
		/// </summary>
		/// <param name="angle">Угол с исходными данными</param>
		public SmlAngle(SmlAngle angle)
		{
			CloneImpl(angle, this);
		}

		private bool InternalComapre(double value, double delta)
		{
			return Math.Abs(Math.Abs(_radian) - value) < delta;
		}

		private void CloneImpl(SmlAngle src, SmlAngle dst)
		{
			dst._radian = src._radian;
		}

		private static bool EqualsImpl(SmlAngle a1, SmlAngle a2)
		{
			if (ReferenceEquals(a1, a2)) return true;
			if (ReferenceEquals(a1, null)) return false;
			if (ReferenceEquals(null, a2)) return false;
			return a1._radian == a2._radian;
		}

		/// <summary>
		/// Получить иди задать угол в радианах
		/// </summary>
		/// <value>Значение угла в радианах</value>
		public double Radians
		{
			get { return _radian; }
			set { _radian = value; }
		}

		/// <summary>
		/// Получить иди задать угол в градусах
		/// </summary>
		/// <value>Значение угла в радианах</value>
		public double Degrees
		{
			get { return GeomUtils.ConvertFromRadiansToDegrees(_radian); }
			set { _radian = GeomUtils.ConvertFromDegreesToRadians(value); }
		}

		/// <summary>
		/// Получить синус угла
		/// </summary>
		public double Sin
		{
			get { return Math.Sin(_radian); }
		}

		/// <summary>
		/// Получить косинус угла
		/// </summary>
		public double Cos
		{
			get { return Math.Cos(_radian); }
		}

		

		/// <summary>
		/// Является ли угол острым
		/// </summary>
		/// <value><c>true</c> если данный угол острый; иначе, <c>false</c>. </value>
		public bool IsAcuteAngle()
		{
			return _radian > 0 && _radian < Math.PI / 2;
		}

		/// <summary>
		/// Является ли угол тупым
		/// </summary>
		/// <value><c>true</c> если данный угол тупой; иначе, <c>false</c>. </value>
		public bool IsObtuseAngle()
		{
			return _radian > Math.PI / 2 && _radian < Math.PI;
		}

		/// <summary>
		/// Является ли угол невыпуклым с заданной точностью от 180 до 360
		/// </summary>
		/// <returns><c>true</c> если угол невыпуклый, иначе<c>false</c>.</returns>
		public bool IsReflexAngle()
		{
			return _radian > Math.PI && _radian < 2 * Math.PI;
		}

		/// <summary>
		/// Является ли угол отрицательным
		/// </summary>
		/// <returns><c>true</c> если угол отрицательный, иначе<c>false</c>.</returns>
		public bool IsNegateAngle()
		{
			return _radian < 0;
		}

		/// <summary>
		/// Является ли угол прямым
		/// </summary>
		/// <returns><c>true</c> если угол прямой, иначе<c>false</c>.</returns>
		public bool IsRightAngle()
		{
			return IsRightAngle(GeomUtils.Delta);
		}

		
		/// <summary>
		/// Является ли угол прямым с заданной точностью
		/// </summary>
		/// <param name="delta">Точность измерения в радианах</param>
		/// <returns><c>true</c> если угол прямой, иначе<c>false</c>.</returns>
		public bool IsRightAngle(double delta)
		{
			return InternalComapre(Math.PI / 2, delta);
		}

		/// <summary>
		/// Является ли угол развернутым с заданной точностью
		/// </summary>
		/// <returns><c>true</c> если угол развернутый, иначе<c>false</c>.</returns>
		public bool IsStraightAngle()
		{
			return IsStraightAngle(GeomUtils.Delta);
		}

		/// <summary>
		/// Является ли угол развернутым с заданной точностью
		/// </summary>
		/// <param name="delta">Точность измерения в радианах</param>
		/// <returns><c>true</c> если угол развернутый, иначе<c>false</c>.</returns>
		public bool IsStraightAngle(double delta)
		{
			return InternalComapre(Math.PI, delta);
		}

		/// <summary>
		/// Является ли угол развернутым с заданной точностью
		/// </summary>
		/// <returns><c>true</c> если угол нулевой, иначе<c>false</c>.</returns>
		public bool IsZeroAngle()
		{
			return IsZeroAngle(GeomUtils.Delta);
		}

		/// <summary>
		/// Является ли угол развернутым с заданной точностью
		/// </summary>
		/// <param name="delta">Точность измерения в радианах</param>
		/// <returns><c>true</c> если угол нулевой, иначе<c>false</c>.</returns>
		public bool IsZeroAngle(double delta)
		{
			return InternalComapre(0, delta);
		}

		/// <summary>
		/// Является ли угол полным с заданной точностью
		/// </summary>
		/// <returns><c>true</c> если угол полный, иначе<c>false</c>.</returns>
		public bool IsFullAngle()
		{
			return IsFullAngle(GeomUtils.Delta);
		}

		/// <summary>
		/// Является ли угол полным с заданной точностью
		/// </summary>
		/// <param name="delta">Точность измерения в радианах</param>
		/// <returns><c>true</c> если угол полный, иначе<c>false</c>.</returns>
		public bool IsFullAngle(double delta)
		{
			return InternalComapre(2 * Math.PI, delta);
		}

		/// <summary>
		/// Проверяет положителен ли угол и находится ли он в диапазоне от 0 до 360
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is normal angle]; otherwise, <c>false</c>.
		/// </returns>
		public bool IsNormalAngle()
		{
			return _radian <= 2 * Math.PI && _radian >= 0;
		}

		/// <summary>
		/// Получить угол, сумма которого с исходным равна 360 градусам
		/// </summary>
		/// <returns>Угол, сумма которого с исходным равна 360 градусам</returns>
		public SmlAngle GetConjugateAngle()
		{
			if (!IsNormalAngle())
				throw new Exception("Угол отрицательный или больше 360 градусов");
			return new SmlAngle(2 * Math.PI - _radian);
		}

		/// <summary>
		/// Делает угол положительным и приводит угол в диапазон от 0 до 360
		/// </summary>
		public void Normalize()
		{
			_radian = GeomUtils.Revolution(_radian);
			//if (IsNegateAngle())
			//    _radian = 2 * Math.PI + _radian;
			
		}

		/// <summary>
		/// Делает угол положительным и приводит его в диапазон от 0 до 360
		/// </summary>
		public SmlAngle NormalizeCreate()
		{
			SmlAngle ang = Clone();
			ang.Normalize();
			return ang;
		}

		/// <summary>
		/// Клонирование угла
		/// </summary>
		/// <returns>Созданная копия угла</returns>
		public SmlAngle Clone()
		{
			SmlAngle ang = new SmlAngle();
			CloneImpl(this, ang);
			return ang;
		}

		/// <summary>
		/// Создать полный угол (360 градусов)
		/// </summary>
		/// <returns>Полный угол</returns>
		public static SmlAngle CreateFullAngle()
		{
			return new SmlAngle(2 * Math.PI);
		}

		/// <summary>
		/// Создать развернутый угол (180 градусов)
		/// </summary>
		/// <returns>Развернутый угол</returns>
		public static SmlAngle CreateStraightAngle()
		{
			return new SmlAngle(Math.PI);
		}

		/// <summary>
		/// Создать развернутый угол (90 градусов)
		/// </summary>
		/// <returns>Развернутый угол</returns>
		public static SmlAngle CreateRightAngle()
		{
			return new SmlAngle(Math.PI / 2);
		}

		/// <summary>
		/// Создает угол по значению,делает положительным и приводит его в диапазон от 0 до 360
		/// </summary>
		/// <param name="value">Значение угла</param>
		/// <returns>Созданный угол</returns>
		public static SmlAngle CreateNormalizedAngle(double value)
		{
			SmlAngle ang = new SmlAngle(value);
			ang.Normalize();
			return ang;
		}

		/// <summary>
		/// Создание угла по значению его косинуса.
		/// </summary>
		/// <param name="cos">Значение косинуса угла</param>
		/// <returns>Созданный угол</returns>
		public static SmlAngle CreateCosAngle(double cos)
		{
			return new SmlAngle(Math.Acos(cos));
		}

		/// <summary>
		/// Создание угла по значению его синуса.
		/// </summary>
		/// <param name="sin">Значение синуса угла</param>
		/// <returns>Созданный угол</returns>
		public static SmlAngle CreateSinAngle(double sin)
		{
			return new SmlAngle(Math.Asin(sin));
		}

		public int CompareTo(SmlAngle other)
		{
			return _radian.CompareTo(other._radian);
		}

		/// <summary>
		/// Проверка эквивалентности одного угла другому
		/// </summary>
		/// <param name="other">Угол для сравнения</param>
		public bool Equals(SmlAngle other)
		{
			return EqualsImpl(this, other);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof(SmlAngle)) return false;
			return Equals((SmlAngle)obj);
		}

		public override int GetHashCode()
		{
			return _radian.GetHashCode();
		}

		/// <summary>
		/// Равны ли два угла
		/// </summary>
		/// <param name="v1">Первый угол</param>
		/// <param name="v2">Второй угол</param>
		/// <returns></returns>
		static public bool operator ==(SmlAngle v1, SmlAngle v2)
		{
			return EqualsImpl(v1, v2);
		}

		/// <summary>
		/// Не равны ли два угла
		/// </summary>
		/// <param name="v1">первый угол</param>
		/// <param name="v2">второй угол</param>
		/// <returns></returns>
		public static bool operator !=(SmlAngle v1, SmlAngle v2)
		{
			return !(v1 == v2);
		}

		/// <summary>
		/// Оператор сложения углов
		/// </summary>
		/// <param name="a1">1-е слагаемое</param>
		/// <param name="a2">2-е слагаемое</param>
		/// <returns>Сумма углов</returns>
		static public SmlAngle operator +(SmlAngle a1, SmlAngle a2)
		{
			return new SmlAngle(a1._radian + a2._radian);
		}

		/// <summary>
		/// Оператор сложения углов
		/// </summary>
		/// <param name="a1">1-е слагаемое</param>
		/// <param name="a2">Значение угла в радианах</param>
		/// <returns>Сумма углов</returns>
		static public SmlAngle operator +(SmlAngle a1, double a2)
		{
			return new SmlAngle(a1._radian + a2);
		}

		/// <summary>
		/// Оператор разности углов
		/// </summary>
		/// <param name="a1">Угол-уменьшаемое</param>
		/// <param name="a2">Угол-вычитаемое</param>
		/// <returns>Угол-разность</returns>
		static public SmlAngle operator -(SmlAngle a1, SmlAngle a2)
		{
			return new SmlAngle(a1._radian - a2._radian);
		}

		/// <summary>
		/// Оператор разности углов
		/// </summary>
		/// <param name="a1">Угол-уменьшаемое</param>
		/// <param name="a2">Значение угла в радианах</param>
		/// <returns>Угол-разность</returns>
		static public SmlAngle operator -(SmlAngle a1, double a2)
		{
			return new SmlAngle(a1._radian - a2);
		}

		/// <summary>
		/// Умножение угла
		/// </summary>
		/// <param name="a1">умножаемый угол</param>
		/// <param name="a2">угол-множитель</param>
		/// <returns>Результат перемножения</returns>
		static public SmlAngle operator *(SmlAngle a1, SmlAngle a2)
		{
			return new SmlAngle(a1._radian * a2._radian);
		}

		/// <summary>
		/// Умножение угла
		/// </summary>
		/// <param name="a">умножаемый угол</param>
		/// <param name="k">Значение угла в радианах</param>
		/// <returns>Результат перемножения</returns>
		static public SmlAngle operator *(SmlAngle a, double k)
		{
			return new SmlAngle(a._radian * k);
		}

		/// <summary>
		/// Деление вектора на число
		/// </summary>
		/// <param name="a1">Делимый угол</param>
		/// <param name="a2">Угол-делитель</param>
		/// <returns>Результат деления</returns>
		static public SmlAngle operator /(SmlAngle a1, SmlAngle a2)
		{
			return new SmlAngle(a1._radian / a2._radian);
		}


		/// <summary>
		/// Деление вектора на число
		/// </summary>
		/// <param name="a">Делимый угол</param>
		/// <param name="k">Угол-делитель</param>
		/// <returns></returns>
		static public SmlAngle operator /(SmlAngle a, double k)
		{
			return new SmlAngle(a._radian / k);
		}

		/// <summary>
		/// Больше ли первый угол чем второй
		/// </summary>
		/// <param name="a1">Первый угол</param>
		/// <param name="a2">Второй угол</param>
		/// <returns>Результат сравнения</returns>
		static public bool operator >(SmlAngle a1, SmlAngle a2)
		{
			return a1._radian > a2._radian;
		}

		/// <summary>
		/// Меньше ли первый угол чем второй
		/// </summary>
		/// <param name="a1">Первый угол</param>
		/// <param name="a2">Второй угол</param>
		/// <returns>Результат сравнения</returns>
		static public bool operator <(SmlAngle a1, SmlAngle a2)
		{
			return a1._radian < a2._radian;
		}

		/// <summary>
		/// Преобразование из SmlAngle в значение угла в радианах
		/// </summary>
		/// <param name="angle">Угол</param>
		/// <returns>Значение угла в радианах</returns>
		public static implicit operator double(SmlAngle angle)
		{
			return angle._radian;
		}
		
	}


}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Sml.Algebra.Data;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Базовое представление точки на плоскости
	/// </summary>
	public abstract class SmlPoint2DImpl : IComparable<SmlPoint2DImpl> 
	{
		protected readonly double[] _cartesC = new double[2];
		protected readonly double[] _polarC = new double[2];

		///<summary>
		///Конструктор по умолчанию
		/// </summary>
		protected SmlPoint2DImpl() { }

		/// <summary>
		/// Перегруженный конструктор копирования
		/// </summary>
		/// <param name="point">точка c которой производится копирование</param>
		protected SmlPoint2DImpl(SmlPoint2DImpl point)
		{
			CloneImpl(point, this);
		}

		/// <summary>
		/// Конструктор точки по координатам
		/// </summary>
		/// <param name="x">Абсцисса точки</param>
		/// <param name="y">Ордината точки</param>
		protected SmlPoint2DImpl(double x, double y)
		{
			_cartesC[0] = x;
			_cartesC[1] = y;
			CartesianToPolar();
		}

		protected void CloneImpl(SmlPoint2DImpl src, SmlPoint2DImpl dst)
		{
			for (int i = 0; i < 2; i++)
			{
				dst._cartesC[i] = src._cartesC[i];
				dst._polarC[i] = src._polarC[i];
			}
		}

		protected static bool EqualsImpl(SmlPoint2DImpl v1, SmlPoint2DImpl v2)
		{
			if (ReferenceEquals(v1, v2)) return true;
			if (ReferenceEquals(v1, null)) return false;
			if (ReferenceEquals(null, v2)) return false;
			return ((v1.X == v2.X) && (v1.Y == v2.Y));
		}

		/// <summary>
		/// абсцисса точки
		/// </summary>
		public double X
		{
			get { return _cartesC[0]; }
			set 
			{
				_cartesC[0] = value;
				CartesianToPolar();
			}
		}

		/// <summary>
		/// ордината точки
		/// </summary>
		public double Y
		{
			get { return _cartesC[1]; }
			set 
			{
				_cartesC[1] = value;
				CartesianToPolar();
			}
		}

		/// <summary>
		/// Угол между вектором на точку и положительным направлением оси ОХ
		/// </summary>
		public double Alpha
		{
			get { return _polarC[0]; }
			set
			{
				_polarC[0] = value;
				PolarToCartesian();
			}
		}

		/// <summary>
		/// Расстояние между точкой и началом координат
		/// </summary>
		public double D
		{
			get { return _polarC[1]; }
			set
			{
				_polarC[1] = value;
				PolarToCartesian();
			}
		}

		/// <summary>
		/// Привести точку к координатам
		/// </summary>
		/// <param name="x">абсцисса точки</param>
		/// <param name="y">ордината точки</param>
		public void SetCartesian(double x, double y)
		{
			_cartesC[0] = x;
			_cartesC[1] = y;
			CartesianToPolar();
		}

		/// <summary>
		/// Привести точку по полярным координатам
		/// </summary>
		/// <param name="alpha">Угол между положительным направлением оси ОХ и направлением на точку</param>
		/// <param name="d">Расстояние от начала координат до точки</param>
		public void SetPolar(double alpha, double d)
		{
			_polarC[0] = alpha;
			_polarC[1] = d;
			PolarToCartesian();
		}


		protected void PolarToCartesian()
		{
			GeomUtils.PolarToCartesian(_polarC, _cartesC);
		}

		protected void CartesianToPolar()
		{
			GeomUtils.CartesianToPolar(_cartesC, _polarC);
		}

		/// <summary>
		/// Трансфорирование точки
		/// </summary>
		/// <param name="matrix">Матрица трансформации</param>
		public void TransformBy(SmlMatrix2D matrix)
		{
			Matrix pmat = new Matrix(1, 3);
			pmat[0, 0] = X;
			pmat[0, 1] = Y;
			pmat[0, 2] = 1;

			Matrix rmat = pmat * matrix;
			X = rmat[0, 0];
			Y = rmat[0, 1];
		}

		/// <summary>
		/// Сравнение двух точек
		/// </summary>
		/// <param name="p">Точка для сравнения</param>
		/// <returns></returns>
		public int CompareTo(SmlPoint2DImpl p)
		{
			return X.CompareTo(p.X) == 0
					? Y.CompareTo(p.Y)
					: X.CompareTo(p.X);
		}
	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Реализация прямоугольника, ориентированного по осям X и Y
	/// </summary>
	public class SmlBoundaryRect2D : SmlQuadrangle2D
	{
		/// <summary>
		/// Конструктор по умолчанию
		/// </summary>
		public SmlBoundaryRect2D() { }

		/// <summary>
		/// Конструктор прямоугольника, ориентированного по координатным осям
		/// </summary>
		/// <param name="p1">Базовая точка прямоугольника</param>
		/// <param name="p2">Противоположная точка прямоугольник</param>
		public SmlBoundaryRect2D(SmlPoint2D p1, SmlPoint2D p2)
		{
			Set(p1, p2);
		}

		/// <summary>
		/// Конструктор прямоугольника, ориентированного по координатным осям
		/// </summary>
		/// <param name="p1">Базовая точка прямоугольника</param>
		/// <param name="width">Ширина.</param>
		/// <param name="height">Высота.</param>
		public SmlBoundaryRect2D(SmlPoint2D p1, double width, double height) : this(p1, new SmlPoint2D(p1.X + width, p1.Y + height)) { }


		/// <summary>
		/// Конструктор квадрата, ориентированного по координатным осям
		/// </summary>
		/// <param name="p1">Базовая точка прямоугольника</param>
		/// <param name="width">Ширина.</param>
		public SmlBoundaryRect2D(SmlPoint2D p1, double width) : this(p1, width, width) { }

		/// <summary>
		/// Ширина прямоугольника
		/// </summary>
		public double Width
		{
			get { return Math.Abs(BasePoint.X - OppositePoint.X); }
		}

		/// <summary>
		/// Высота прямоугольника
		/// </summary>
		public double Height
		{
			get { return Math.Abs(BasePoint.Y - OppositePoint.Y); }
		}

		/// <summary>
		/// Создает оффсет прямоугольника
		/// </summary>
		/// <param name="param">Дистанция</param>
		/// <param name="inside">Внутрь если <c>true</c> [inside].</param>
		/// <returns></returns>
		public SmlBoundaryRect2D Offset(double param, bool inside)
		{
			SmlPoint2D pt1;
			SmlPoint2D pt2;
			SmlPoint2D temp = OppositePoint;
			if (inside)
			{
				pt1 = new SmlPoint2D(BasePoint.X + param, BasePoint.Y + param);
				pt2 = new SmlPoint2D(temp.X - param, temp.Y - param);
			}
			else
			{
				pt1 = new SmlPoint2D(BasePoint.X - param, BasePoint.Y - param);
				pt2 = new SmlPoint2D(temp.X + param, temp.Y + param);
			}
			return new SmlBoundaryRect2D(pt1, pt2);
		}

		/// <summary>
		/// Площадь прямоугольника
		/// </summary>
		public double Area
		{
			get { return Width * Height; }
		}

		/// <summary>
		/// Определяет пересечение полигона с отезком
		/// </summary>
		/// <param name="s">Отрезок</param>
		/// <returns>
		/// 	<c>true</c> если пересекает полигон, иначе, <c>false</c>.
		/// </returns>
		public override bool HasIntersect(SmlSegment2D s)
		{
			if (s.StartPoint.X > OppositePoint.X && s.EndPoint.X > OppositePoint.X)
				return false;

			if (s.StartPoint.X < BasePoint.X && s.EndPoint.X < BasePoint.X)
				return false;

			if (s.StartPoint.Y > OppositePoint.Y && s.EndPoint.Y > OppositePoint.Y)
				return false;

			if (s.StartPoint.Y < BasePoint.Y && s.EndPoint.Y < BasePoint.Y)
				return false;

			return base.HasIntersect(s);
		}

		protected void Set(SmlPoint2D basePt, SmlPoint2D opositePt)
		{
			SmlPoint2D thirdPt = basePt.Y > opositePt.Y ? new SmlPoint2D(opositePt.X, basePt.Y) : new SmlPoint2D(basePt.X, opositePt.Y);

			SmlPoint2D[] pnts = { basePt, thirdPt, opositePt };
			int ind;

			if (!GeomUtils.IsToPerpendLinesExsit(pnts, out ind))
				throw new InvalidOperationException("Заданные точки не образуют перпендикулярные прямые");

			SmlPoint2D p = GeomUtils.FindLastVertex(pnts, ind);
			_polygon.Clear();
			foreach (SmlPoint2D point2D in pnts)
				_polygon.AddLast(point2D);
			_polygon.AddLast(p);
		}
	}
}

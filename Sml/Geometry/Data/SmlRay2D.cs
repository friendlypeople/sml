// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;

namespace Sml.Geometry.Data
{
	public class SmlRay2D : SmlLine2D
	{
		/// <summary>
		/// Стартовая точка луча
		/// </summary>
		protected SmlPoint2D _startPoint;

		/// <summary>
		/// Направляющий вектор луча
		/// </summary>
		protected SmlVector2D _directVector;

		/// <summary>
		/// Конструктор луча по умолчанию
		/// </summary>
		public SmlRay2D()
		{
			_startPoint = new SmlPoint2D();
			_directVector = new SmlVector2D();
		}

		/// <summary>
		/// Конструктор луча по стартовой точке и направляющему вектору
		/// </summary>
		/// <param name="p">стартовая точка</param>
		/// <param name="v">направляющий вектор</param>
		public SmlRay2D(SmlPoint2D p, SmlVector2D v)
			: base(p, v)
		{
			_startPoint = p;
			_directVector = v;
		}


		/// <summary>
		/// Конструктор луча по двум точкам
		/// </summary>
		/// <param name="p1">Стартовая точка луча</param>
		/// <param name="p2">Точка, задающая направление луча</param>
		public SmlRay2D(SmlPoint2D p1, SmlPoint2D p2)
			: base(p1, p2)
		{
			_startPoint = p1;
			_directVector = p2.AsVector() - p1.AsVector();
		}

		/// <summary>
		/// Конструктор копирования луча
		/// </summary>
		/// <param name="r">Копируемый луч</param>
		public SmlRay2D(SmlRay2D r)
			: base(r)
		{
			_startPoint = r.StartPoint;
			_directVector = r.DirectVector;
		}

		/// <summary>
		/// Конструктор луча по отрезку
		/// </summary>
		/// <param name="s">Задающий отрезок. Старотовая точка отрезка - </param>
		public SmlRay2D(SmlSegment2D s)
			: base(s)
		{
			_startPoint = s.StartPoint;
			_directVector = s.EndPoint.AsVector() - s.StartPoint.AsVector();
		}

		/// <summary>
		/// Задание луча по точке и вектору
		/// </summary>
		/// <param name="p">Задающая точка - стартовая точка луча</param>
		/// <param name="v">Задающий вектор - направляющий вектор луча</param>
		public override void Set(SmlPoint2D p, SmlVector2D v)
		{
			SmlRay2D r = new SmlRay2D(p, v);
			base.Set(r);
			_startPoint = r.StartPoint;
			_directVector = r.DirectVector;
		}

		/// <summary>
		/// Задание луча по двум точкам
		/// </summary>
		/// <param name="p1">Задающая точка - стартовая точка луча</param>
		/// <param name="p2">Задающая точка - точка, задающая направление луча</param>
		public override void Set(SmlPoint2D p1, SmlPoint2D p2)
		{
			SmlRay2D r = new SmlRay2D(p1, p2);
			base.Set(r);
			_startPoint = r.StartPoint;
			_directVector = r.DirectVector;
		}

		/// <summary>
		/// Задание луча по отрезку
		/// </summary>
		/// <param name="s">Задающий отрезок. Стартовая точка отрезка - стартовая точка луча</param>
		public override void Set(SmlSegment2D s)
		{
			SmlRay2D r = new SmlRay2D(s);
			base.Set(s);
			_startPoint = r.StartPoint;
			_directVector = r.DirectVector;
		}

		/// <summary>
		/// Задание луча по другому лучу
		/// </summary>
		/// <param name="r">Задающий луч</param>
		public override void Set(SmlRay2D r)
		{
			base.Set(r);
			_startPoint = r.StartPoint;
			_directVector = r.DirectVector;
		}

		/// <summary>
		/// Возвращает значение абсциссы точки на луче
		/// </summary>
		/// <param name="x">Абсцисса точки</param>
		/// <param name="y">Ордината точки</param>
		/// <returns>Истинно если точка с такой ординатой может быть найдена на луче</returns>
		/// <exception cref="InfinityException">На горизонтальном луче при соответсвующей ординате значение абсциссы задать точно невозможно</exception>
		public override bool XValue(ref double x, double y)
		{
			try
			{
				if (base.XValue(ref x, y))
					return IsOn(new SmlPoint2D(x, y));
				return false;
			}
			catch (InfinityException)
			{
				throw new InfinityException(
					"Горизонтальный луч. При этой ординате значение абсциссы может быть одним из бесконечного и ограниченного сверху или снизу множества.");
			}
		}

		/// <summary>
		/// Возвращает значение ординаты точки на луче
		/// </summary>
		/// <param name="x">Абсцисса точки</param>
		/// <param name="y">Ордината точки</param>
		/// <returns>Истинно если точка с такой абсциссой может быть найдена на луче</returns>
		/// <exception cref="InfinityException">У вертикального луча при соответствующей абсциссе ординату точно определить невозможно</exception>
		public override bool YValue(double x, ref double y)
		{

			try
			{
				if (base.YValue(x, ref y))
					return IsOn(new SmlPoint2D(x, y));
				return false;
			}
			catch (InfinityException)
			{
				throw new InfinityException(
					"Вертикальный луч. При этой абсциссе значение ординаты может быть любым из бесконечного множества");
			}
		}

		/// <summary>
		/// Поворот луча ПЧС на угол alpha вокруг точки р
		/// </summary>
		/// <param name="alpha">Угол поворота</param>
		/// <param name="p">Точка вращения</param>
		public override void RotateNegative(double alpha, SmlPoint2D p)
		{
			SmlPoint2D p1 = ((StartPoint.AsVector() - p.AsVector()).RotateNegativeCreate(alpha) + p.AsVector()).ToPoint();
			SmlVector2D v = DirectVector.RotateNegativeCreate(alpha);
			Set(p1, v);
		}

		/// <summary>
		/// Возвращает луч, повернутый относительно вызывающего на угол alpha ПЧС вокруг точки р
		/// </summary>
		/// <param name="alpha">Угол поворота</param>
		/// <param name="p">Точка вращения</param>
		/// <returns></returns>
		public new SmlRay2D RotateNegativeCreate(double alpha, SmlPoint2D p)
		{
			SmlRay2D r = new SmlRay2D(this);
			r.RotateNegative(alpha, p);
			return r;
		}

		/// <summary>
		/// Поворот луча ПрЧС на угол alpha вокруг точки р
		/// </summary>
		/// <param name="alpha">Угол поворота</param>
		/// <param name="p">Точка вращения</param>
		public override void RotatePositive(double alpha, SmlPoint2D p)
		{
			SmlPoint2D p1 = ((StartPoint.AsVector() - p.AsVector()).RotatePositiveCreate(alpha) + p.AsVector()).ToPoint();
			SmlVector2D v = DirectVector.RotatePositiveCreate(alpha);
			Set(p1, v);
		}

		/// <summary>
		/// Возвращает луч, повернутый относительно вызывающего на угол alpha ПрЧС вокруг точки р
		/// </summary>
		/// <param name="alpha">Угол поворота</param>
		/// <param name="p">Точка вращения</param>
		/// <returns></returns>
		public new SmlRay2D RotatePositiveCreate(double alpha, SmlPoint2D p)
		{
			SmlRay2D r = new SmlRay2D(this);
			r.RotatePositive(alpha, p);
			return r;
		}

		/// <summary>
		/// Пересечение луча с прямой
		/// </summary>
		/// <param name="l">Пересекаемая прямая</param>
		/// <param name="p">точка пересечения</param>
		/// <returns>Пересекается ли луч с прямой или нет</returns>
		public override bool CrossPoint(SmlLine2D l, ref SmlPoint2D p)
		{
			try
			{
				if (!base.CrossPoint(l, ref p))
					return false;
			}
			catch (InfinityException)
			{
				p = _startPoint;
				return true;
				//throw new InfinityException("Луч и прямая совпадают. Любую точку луча можно считать точкой их пересечения");
			}
			if (GeomUtils.RoundingError(_c) != 0)
			{
				if (_directVector.Orientation(p.AsVector()) == Orientation.CounterClockwise)
				{
					if ((_directVector.AnglePositive(p.AsVector())) <= (_directVector.AnglePositive(_startPoint.AsVector())))
						return true;
					return false;
				}
				if (_directVector.AngleNegative(p.AsVector()) <= _directVector.AngleNegative(_startPoint.AsVector()))
					return true;
				return false;
			}
			if (_directVector.IsCodirectional(_startPoint.AsVector()))
				return ((_startPoint < p) || (_startPoint == p));
			return ((_startPoint > p) || (_startPoint == p));
		}

		/// <summary>
		/// Пересечение двух лучей
		/// </summary>
		/// <param name="r">Пересекаемый луч</param>
		/// <param name="p">Точка пересечения</param>
		/// <returns>Пересекаются ли лучи или нет</returns>
		public override bool CrossPoint(SmlRay2D r, ref SmlPoint2D p)
		{
			return (CrossPoint(new SmlLine2D(r), ref p) && r.CrossPoint(new SmlLine2D(this), ref p));
		}

		/// <summary>
		/// Пересечение вызывающего луча и отрезка
		/// </summary>
		/// <param name="s">Пересекаемый отрезок</param>
		/// <returns>Пересекается ли луч с отрезком или нет</returns>
		public bool CrossPoint(SmlSegment2D s)
		{
			return true;
		}

		/// <summary>
		/// Расстояние от луча до точки
		/// </summary>
		/// <param name="p">Точка</param>
		/// <returns>Расстояние</returns>
		public override double Distance(SmlPoint2D p)
		{
			SmlVector2D v = new SmlVector2D(p.AsVector() - StartPoint.AsVector());
			SmlVector2D v1 = DirectVector.PerpendicularNegative();
			SmlPoint2D p1 = new SmlPoint2D();
			if (DirectVector.Orientation(v) == Orientation.CounterClockwise)
			{
				if (CrossPoint((new SmlRay2D(p, v1)), ref p1))
					return base.Distance(p);
				return StartPoint.Distance(p);
			}
			v1 = DirectVector.PerpendicularPositive();
			if (CrossPoint((new SmlRay2D(p, v1)), ref p1))
				return base.Distance(p);
			return StartPoint.Distance(p);
		}



		/// <summary>
		/// Находится ли точка на луче
		/// </summary>
		/// <param name="p">Исследуемая точка</param>
		/// <returns>Находится или нет</returns>
		public override bool IsOn(SmlPoint2D p)
		{
			SmlVector2D vect = p - StartPoint;
			return base.IsOn(p) && DirectVector.IsCodirectional(vect);
		}

		/// <summary>
		/// Возвращает случайную точку луча
		/// </summary>
		/// <returns>Точка, лежащая на луче</returns>
		public override SmlPoint2D PointOn()
		{
			Random rnd = new Random();
			double x;
			double y = 0;
			if (_directVector.X > 0)
			{
				x = rnd.Next(100) + _startPoint.X;
				base.YValue(x, ref y);
				return new SmlPoint2D(x, y);
			}
			if (DirectVector.X < 0)
			{
				x = StartPoint.X - rnd.Next(100);
				base.YValue(x, ref y);
				return new SmlPoint2D(x, y);
			}
			if (DirectVector.Y < 0)
				return new SmlPoint2D(_startPoint.X, _startPoint.Y - rnd.Next(100));
			return new SmlPoint2D(_startPoint.X, _startPoint.Y + rnd.Next(100));
		}

		/// <summary>
		/// Стартовая точка луча
		/// </summary>
		public SmlPoint2D StartPoint
		{
			get { return _startPoint; }
			set
			{
				base.Set(value, _directVector);
				_startPoint = value;
			}
		}

		/// <summary>
		/// Направляющий вектор луча
		/// </summary>
		public SmlVector2D DirectVector
		{
			get { return _directVector; }
			set
			{
				base.Set(_startPoint, value);
				_directVector = value;
			}
		}
	}
}

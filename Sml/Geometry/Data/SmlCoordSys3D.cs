// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Diagnostics;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Представление трехмерного ортогонального базиса
	/// </summary>
	[DebuggerDisplay("O = {_center} i = {_i} j = {_j} k = {_k}")]
	public class SmlCoordSys3D
	{
		private SmlVector3D _i = SmlVector3D.XAxis;
		private SmlVector3D _j = SmlVector3D.YAxis;
		private SmlVector3D _k = SmlVector3D.ZAxis;
		private SmlPoint3D _center = new SmlPoint3D();

		/// <summary>
		/// Инициализация нового экземпляра класса <see cref="SmlCoordSys3D"/>.
		/// </summary>
		public SmlCoordSys3D() { }

		/// <summary>
		/// Инициализация нового экземпляра класса <see cref="SmlCoordSys3D"/>.
		/// </summary>
		/// <param name="cs3d">Исходная система координат</param>
		public SmlCoordSys3D(SmlCoordSys3D cs3d)
		{
			_i = new SmlVector3D(cs3d._i);
			_j = new SmlVector3D(cs3d._j);
			_k = new SmlVector3D(cs3d._k);
			_center = new SmlPoint3D(cs3d._center);
		}

		/// <summary>
		/// Инициализация нового экземпляра класса <see cref="SmlCoordSys3D"/>.
		/// </summary>
		/// <param name="center">Центр нового базиса.</param>
		/// <param name="xAxis">Ось Ox нового базиса.</param>
		/// <param name="vec">Вектор не колиниарный xAxis, служащий направлением для оси Oy.</param>
		public SmlCoordSys3D(SmlPoint3D center, SmlVector3D xAxis, SmlVector3D vec)
			: this(center, xAxis, center + vec)
		{
		}

		/// <summary>
		/// Инициализация нового экземпляра класса <see cref="SmlCoordSys3D"/>.
		/// </summary>
		/// <param name="center">Центр нового базиса.</param>
		/// <param name="xAxis">Ось Ox нового базиса.</param>
		/// <param name="pt">Точка на плоскости Oxy, служащая направлением для оси Oy.</param>
		public SmlCoordSys3D(SmlPoint3D center, SmlVector3D xAxis, SmlPoint3D pt)
		{
			_center = center;
			_i = xAxis.UnitCreate();
			_j = CreateYAxis(center, xAxis, pt);
			_k = CreateZAxis(_i, _j);
		}

		/// <summary>
		/// Инициализация нового экземпляра класса <see cref="SmlCoordSys3D"/>.
		/// </summary>
		/// <param name="center">Центр нового базиса.</param>
		/// <param name="xPt">Точка на плоскости Oxy, служащая направлением для оси Ox.</param>
		/// <param name="yPt">Точка на плоскости Oxy, служащая направлением для оси Oy.</param>
		public SmlCoordSys3D(SmlPoint3D center, SmlPoint3D xPt, SmlPoint3D yPt)
			: this (center, xPt - center, yPt - center) { }

		private SmlVector3D CreateYAxis(SmlPoint3D center, SmlVector3D xAxis, SmlPoint3D pt)
		{
			SmlVector3D vec = pt - center;
			if (vec.IsCollinear(xAxis))
				throw new Exception("Нельзя создать базис на двух коллиниарных векторах");
			SmlVector3D orto = vec.OrtoProject(xAxis);
			SmlPoint3D p = center + orto;
			return (pt - p).UnitCreate();
		}

		private SmlVector3D CreateZAxis(SmlVector3D xAxis, SmlVector3D yAxis)
		{
			SmlVector3D zAxis = SmlVector3D.VectorProduct(xAxis, yAxis);
			zAxis.Unit();
			return zAxis;
		}
	}


}

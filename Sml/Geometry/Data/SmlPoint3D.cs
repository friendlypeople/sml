// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Sml.Common;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Представление точки в пространстве
	/// </summary>
	public class SmlPoint3D : SmlPoint3DImpl
	{
		/// <summary>
		/// Конструктор точки по координатам
		/// </summary>
		/// <param name="x">Абсцисса точки</param>
		/// <param name="y">Ордината точки</param>
		/// <param name="z">Аппликата точки</param>
		public SmlPoint3D(double x, double y, double z) : base(x, y, z) { }

		///<summary>
		///Конструктор по умолчанию
		/// </summary>
		public SmlPoint3D() { }

		/// <summary>
		/// Перегруженный конструктор копирования
		/// </summary>
		/// <param name="point">точка c которой производится копирование</param>
		public SmlPoint3D(SmlPoint3D point) :base(point) { }

		/// <summary>
		/// Нулевая точка
		/// </summary>
		public static SmlPoint3D Zero
		{
			get { return new SmlPoint3D(); }
		}


		/// <summary>
		/// Преобразовать к представлению точки на плоскости
		/// </summary>
		/// <returns>Представление точки на плоскости</returns>
		public SmlPoint2D ToSmlPoint2D()
		{
			return new SmlPoint2D(X, Y);
		}

        /// <summary>
        /// Преобразовать к представлению точки на плоскости
        /// </summary>
        /// <returns>Представление точки на плоскости</returns>
        public SmlSPoint2D ToSmlSPoint2D()
        {
            return new SmlSPoint2D(X, Y);
        }

		/// <summary>
		/// Преобразовать к представлению точки на плоскости
		/// </summary>
		/// <returns>Представление точки на плоскости</returns>
		public SmlVector3D ToSmlVector3D()
		{
			return new SmlVector3D(X, Y, Z);
		}


		/// <summary>
		/// Расстояние до точки 
		/// </summary>
		/// <param name="p">точка</param>
		/// <returns>Расстояние до точки </returns>
		public double Distance(SmlPoint3D p)
		{
			return Math.Abs(Math.Sqrt(Math.Pow(X - p.X, 2) + Math.Pow(Y - p.Y, 2) + Math.Pow(Z - p.Z, 2)));
		}

		/// <summary>
		/// Расстояние до плоскости 
		/// </summary>
		/// <param name="pl">Плоскость</param>
		/// <returns>Расстояние до плоскости </returns>
		public double Distance(SmlPlane pl)
		{
			double temp = pl.A * X + pl.B * Y + pl.C * Z + pl.D;
			return temp / R;
		}

		/// <summary>
		/// Равны ли две точки
		/// </summary>
		/// <param name="p1">Точка</param>
		/// <param name="p2">Точка</param>
		/// <returns></returns>
		public static bool operator ==(SmlPoint3D p1, SmlPoint3D p2)
		{
			return EqualsImpl(p1, p2);
		}

		/// <summary>
		/// Не равны ли две точки
		/// </summary>
		/// <param name="p1">Точка</param>
		/// <param name="p2">Точка</param>
		/// <returns></returns>
		public static bool operator !=(SmlPoint3D p1, SmlPoint3D p2)
		{
			return !(p1 == p2);
		}

		/// <summary>
		/// Эквивалентность точек
		/// </summary>
		/// <param name="point">Сравниваемая точка</param>
		/// <returns><c>true</c> если эквивалентны ; иначе, <c>false</c>. </returns>
		public bool IsEquals(SmlPoint3D point)
		{
			return IsEquals(point, 0);
		}

		/// <summary>
		/// Эквивалентность точек
		/// </summary>
		/// <param name="point">Сравниваемая точка</param>
		/// <param name="tolerance">Точность</param>
		/// <returns>  	<c>true</c> если эквивалентны ; иначе, <c>false</c>. </returns>
		public bool IsEquals(SmlPoint3D point, double tolerance)
		{
			return (Math.Abs((X - point.X)) < tolerance &&
				Math.Abs((Y - point.Y)) < tolerance &&
				Math.Abs((Z - point.Z)) < tolerance
				);
		}

		/// <summary>
		/// Больше ли точка р1 точки р2
		/// </summary>
		/// <param name="p1">Точка</param>
		/// <param name="p2">Точка</param>
		/// <returns></returns>
		public static bool operator >(SmlPoint3D p1, SmlPoint3D p2)
		{
			return ((p1.X > p2.X) || 
				((p1.X == p2.X) && (p1.Y > p2.Y)) ||
				((p1.X == p2.X) && (p1.Y == p2.Y) && (p1.Z > p2.Z)));
		}

		/// <summary>
		/// Меньше ли точка р1 точи р2
		/// </summary>
		/// <param name="p1">Первая точка</param>
		/// <param name="p2">Вторая точка</param>
		/// <returns>Результат сравнения</returns>
		public static bool operator <(SmlPoint3D p1, SmlPoint3D p2)
		{
			return ((p1.X < p2.X) ||
				((p1.X == p2.X) && (p1.Y < p2.Y)) ||
				((p1.X == p2.X) && (p1.Y == p2.Y) && (p1.Z < p2.Z)));
		}

		/// <summary>
		/// Сумма для точки
		/// </summary>
		/// <param name="p1">первое слагаемое</param>
		/// <param name="p2">второе слагаемое</param>
		/// <returns></returns>
		public static SmlVector3D operator +(SmlPoint3D p1, SmlPoint3D p2)
		{
			return new SmlVector3D(GeomUtils.RoundingError(p1.X + p2.X),
				GeomUtils.RoundingError(p1.Y + p2.Y),
				GeomUtils.RoundingError(p1.Z + p2.Z));
		}

		/// <summary>
		/// Сумма для точки
		/// </summary>
		/// <param name="p1">первое слагаемое</param>
		/// <param name="vec">Слагаемы вектор</param>
		/// <returns></returns>
		public static SmlPoint3D operator +(SmlPoint3D p1, SmlVector3D vec)
		{
			return new SmlPoint3D(GeomUtils.RoundingError(p1.X + vec.X),
				GeomUtils.RoundingError(p1.Y + vec.Y),
				GeomUtils.RoundingError(p1.Z + vec.Z)
				);
		}

		/// <summary>
		/// Разность для точки
		/// </summary>
		/// <param name="p1">Уменьшаемое</param>
		/// <param name="p2">Вычитаемое</param>
		/// <returns></returns>
		public static SmlVector3D operator -(SmlPoint3D p1, SmlPoint3D p2)
		{
			return new SmlVector3D(GeomUtils.RoundingError(p1.X - p2.X),
				GeomUtils.RoundingError(p1.Y - p2.Y),
				GeomUtils.RoundingError(p1.Z - p2.Z)
				);
		}

		/// <summary>
		/// Разность для точки
		/// </summary>
		/// <param name="p1">Уменьшаемое</param>
		/// <param name="vec">Вычитаемое</param>
		/// <returns></returns>
		public static SmlPoint3D operator -(SmlPoint3D p1, SmlVector3D vec)
		{
			return new SmlPoint3D(GeomUtils.RoundingError(p1.X - vec.X),
				GeomUtils.RoundingError(p1.Y - vec.Y),
				GeomUtils.RoundingError(p1.Z - vec.Z)
				);
		}

		/// <summary>
		/// Умножить точку на число
		/// </summary>
		/// <param name="p">Точка</param>
		/// <param name="k">Множитель</param>
		/// <returns></returns>
		public static SmlPoint3D operator *(SmlPoint3D p, double k)
		{
			return new SmlPoint3D(p.X * k, p.Y * k, p.Z * k);
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns> A <see cref="System.String"/> that represents this instance. </returns>
		public override string ToString()
		{
			return "(" + X + ";" + Y + ";" + Z + ")";
		}

		/// <summary>
		/// Equalses the specified other.
		/// </summary>
		/// <param name="other">The other.</param>
		/// <returns></returns>
		public bool Equals(SmlPoint3D other)
		{
			return EqualsImpl(this, other);
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof (SmlPoint3D)) return false;
			return Equals((SmlPoint3D) obj);
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>
		/// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
		/// </returns>
		public override int GetHashCode()
		{
			return HashCode.GetHashCode(X.GetHashCode(), Y.GetHashCode(), Z.GetHashCode());
		}
	}
}

// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Diagnostics;
using Sml.Common;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Реализации вестора на плоскости
	/// </summary>
	[DebuggerDisplay("X = {X} Y = {Y} Alpha = {Alpha} D = {D}")]
    public class SmlVector2D : SmlPoint2DImpl
    {
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
		public SmlVector2D() { }


		/// <summary>
        /// Создание вектора по координатам
        /// </summary>
        /// <param name="x">Абсцисса вектора</param>
        /// <param name="y">Ордината вектора</param>
        public SmlVector2D(double x, double y) :base (x, y)  { }

        /// <summary>
        /// Создание вектора по углу между вектором и положительным напрвлением оси ОХ
        /// </summary>
        /// <param name="alpha">Задающий вектор</param>
        public SmlVector2D(double alpha)
        {
            _polarC[0] = GeomUtils.Revolution(alpha);
			_polarC[1] = 1;
			PolarToCartesian();
        }

        /// <summary>
        /// Конструктор копирования
        /// </summary>
        /// <param name="v">Копируемый вектор</param>
        public SmlVector2D(SmlVector2D v) : base(v) { }

		/// <summary>
		/// Конструктор создания вектора из точки
		/// </summary>
		/// <param name="v">Исходная точка</param>
		public SmlVector2D(SmlPoint2D v) : base(v) { }

		/// <summary>
		/// Конструктор копирования
		/// </summary>
		/// <param name="v">Копируемый вектор</param>
		public SmlVector2D(SmlVector3D v) : base(v.X, v.Y) { }

		/// <summary>
		/// Конструктор создания вектора из точки
		/// </summary>
		/// <param name="v">Исходная точка</param>
		public SmlVector2D(SmlPoint3D v) : base(v.X, v.Y) { }

        /// <summary>
        ///Конструктор вектора по двум точкам p2------>p1
        /// </summary>
        /// <param name="p1">Конец вектора</param>
        /// <param name="p2">Начало вектора</param>
        public SmlVector2D(SmlPoint2D p1, SmlPoint2D p2)
        {
        	SmlVector2D vector2D = p1 - p2;
			_cartesC[0] = GeomUtils.RoundingError(vector2D.X);
			_cartesC[1] = GeomUtils.RoundingError(vector2D.Y);
			CartesianToPolar();
        }

        /// <summary>
        /// Единичный орт-вектор по оси абсцисс (оси ОХ)
        /// </summary>
        public static SmlVector2D XAxis
        {
			get { return new SmlVector2D(1, 0); }
        }

        /// <summary>
        /// Единичный орт-вектор по оси ординат (оси ОY)
        /// </summary>
        public static SmlVector2D YAxis
        {
            get { return new SmlVector2D(0, 1); }
        }

		/// <summary>
		/// Длина вектора
		/// </summary>
		public double Length
		{
			get { return D; }
			set { D = value; }
		}

		/// <summary>
		/// Квадрат длины вектора
		/// </summary>
		public double LengthSquared
		{
			get { return D*D; }
		}

		/// <summary>
		/// Проверяет, является ли вектор нулевым
		/// </summary>
		/// <value><c>true</c> если вектор нулевой; иначе,<c>false</c>.</value>
		public bool IsZeroVector
		{
			get { return Math.Abs(X) < GeomUtils.Delta && Math.Abs(Y) < GeomUtils.Delta; }
		}

		/// <summary>
        /// Преобразовать вектор в точку - конец вектора
        /// </summary>
        /// <returns></returns>
        public SmlPoint2D ToPoint()
        {
            return new SmlPoint2D(X, Y);
        }

        /// <summary>
        /// Оператор сложения векторов
        /// </summary>
        /// <param name="v1">1-е слагаемое</param>
        /// <param name="v2">2-е слагаемое</param>
        /// <returns>Сумма векторов</returns>
        static public SmlVector2D operator + (SmlVector2D v1, SmlVector2D v2)
        {
            return new SmlVector2D(GeomUtils.RoundingError(v1.X + v2.X), GeomUtils.RoundingError(v1.Y + v2.Y));
        }

        /// <summary>
        /// Оператор разности двух векторов
        /// </summary>
        /// <param name="v1">Вектор-уменьшаемое</param>
        /// <param name="v2">Вектор-вычитаемое</param>
        /// <returns>Вектор-разность</returns>
        static public SmlVector2D operator - (SmlVector2D v1, SmlVector2D v2)
        {
        	return new SmlVector2D(GeomUtils.RoundingError(v1.X - v2.X), GeomUtils.RoundingError(v1.Y - v2.Y));
        }

        /// <summary>
        /// Умножение вектора на число
        /// </summary>
        /// <param name="v">умножаемый вектор</param>
        /// <param name="k">число-множитель</param>
        /// <returns></returns>
        static public SmlVector2D operator * (SmlVector2D v, double k)
        {
        	return new SmlVector2D(GeomUtils.RoundingError(v.X*k), GeomUtils.RoundingError(v.Y*k));
        }


		/// <summary>
		/// Деление вектора на число
		/// </summary>
		/// <param name="v">Делимый вектор</param>
		/// <param name="k">число-делитель</param>
		/// <returns></returns>
		static public SmlVector2D operator /(SmlVector2D v, double k)
		{
			return new SmlVector2D(GeomUtils.RoundingError(v.X / k), GeomUtils.RoundingError(v.Y / k));
		}

        /// <summary>
        /// Скалярное умножение двух векторов
        /// </summary>
        /// <param name="v1">первый перемножаемый вектор</param>
        /// <param name="v2">второй перемножаемый вектор</param>
        /// <returns>Скаляр - результат перемножения</returns>
        static public double ScalarProduct(SmlVector2D v1, SmlVector2D v2)
        {
            return GeomUtils.RoundingError(v1.X * v2.X + v1.Y * v2.Y);
        }

		/// <summary>
        /// Скалярное умножение двух векторов
        /// </summary>
        /// <param name="v">второй перемножаемый вектор</param>
        /// <returns>Скаляр - результат перемножения</returns>
        public double ScalarProduct(SmlVector2D v)
        {
            return ScalarProduct(this, v);
        }

		/// <summary>
		/// Псевдоскалярное произведение векторов
		/// </summary>
		/// <param name="v1">первый перемножаемый вектор</param>
		/// <param name="v2">второй перемножаемый вектор</param>
		/// <returns>Скаляр - результат перемножения </returns>
		public static double QuasiScalarProduct(SmlVector2D v1, SmlVector2D v2)
		{
			return v1.X * v2.Y - v1.Y * v2.X;
		}

		/// <summary>
		/// Псевдоскалярное произведение векторов
		/// </summary>
		/// <param name="v">второй перемножаемый вектор</param>
		/// <returns>Скаляр - результат перемножения </returns>
		public double QuasiScalarProduct(SmlVector2D v)
		{
			return QuasiScalarProduct(this, v);
		}

		/// <summary>
        /// Равны ли два вектора
        /// </summary>
        /// <param name="v1">Первый вектор</param>
        /// <param name="v2">Второй вектор</param>
        /// <returns></returns>
        static public bool operator == (SmlVector2D v1, SmlVector2D v2)
        {
			if (ReferenceEquals(v1, v2)) return true;
			if (ReferenceEquals(v1, null)) return false;
			if (ReferenceEquals(null, v2)) return false;
			return ((v1.X == v2.X) && (v1.Y == v2.Y));
        }

        /// <summary>
        /// Не равны ли два вектора
        /// </summary>
        /// <param name="v1">первый вектор</param>
        /// <param name="v2">второй вектор</param>
        /// <returns></returns>
        static public bool operator != (SmlVector2D v1, SmlVector2D v2)
        {
			return !(v1 == v2);
        }

        /// <summary>
        /// Больше ли первый вектор чем второй
        /// </summary>
        /// <param name="v1">Первый вектор</param>
        /// <param name="v2">Второй вектор</param>
        /// <returns>Результат сравнения</returns>
        static public bool operator > (SmlVector2D v1, SmlVector2D v2)
        {
        	return ((v1.X > v2.X) || ((v1.X == v2.X) && (v1.Y > v2.Y)));
        }

        /// <summary>
        /// Меньше ли первый вектор чем второй
        /// </summary>
        /// <param name="v1">Первый вектор</param>
        /// <param name="v2">Второй вектор</param>
        /// <returns>Результат сравнения</returns>
        static public bool operator < (SmlVector2D v1, SmlVector2D v2)
        {
        	return ((v1.X < v2.X) || ((v1.X == v2.X) && (v1.Y < v2.Y)));
        }

		/// <summary>
		/// Определение направления ориентации с другим вектором(по направлению часовой стрелки или против)
		/// </summary>
		/// <param name="v">Вектор, с которым определяется ориентация</param>
		/// <returns></returns>
		public Orientation Orientation(SmlVector2D v)
		{
			if (0 <= GeomUtils.RoundingError(X * v.Y - v.X * Y))
				return Data.Orientation.CounterClockwise;
			return Data.Orientation.Clockwise;
		}

        /// <summary>
        /// Перепендикулярен ли вызывающий вектор с вектором v
        /// </summary>
        /// <param name="v">Сравниваемый вектор</param>
		/// <returns><c>true</c> если вектора перепендикулярны, иначе, <c>false</c>.</returns>
		/// <remarks>Нулевой вектор перепендикулярен любому</remarks>
        public bool IsPerpendicular(SmlVector2D v)
        {
			return IsPerpendicular(v, GeomUtils.Delta);
        }

		/// <summary>
		/// Перепендикулярен ли вызывающий вектор с вектором v
		/// </summary>
		/// <param name="v">Сравниваемый вектор</param>
		/// <param name="delta">Допустимое отклонение в радианах</param>
		/// <returns><c>true</c> если вектора перепендикулярны, иначе, <c>false</c>.</returns>
		/// <remarks>Нулевой вектор перепендикулярен любому</remarks>
		public bool IsPerpendicular(SmlVector2D v, double delta)
		{
			if (IsZeroVector || v.IsZeroVector)
				return true;
			SmlAngle angle = GetAngleTo(v);
			return angle.IsRightAngle(delta);
		}

		/// <summary>
		/// Коллинеарен ли вызывающий вектор с вектором v
		/// </summary>
		/// <param name="v">Сравниваемый вектор</param>
		/// <param name="delta">Точность</param>
		/// <returns><c>true</c> если коллинеарен; иначе, <c>false</c>.</returns>
		/// <remarks>Нулевой вектор коллиниарен любому</remarks>
        public bool IsCollinear(SmlVector2D v, double delta)
        {
			if (IsZeroVector || v.IsZeroVector)
				return true;

			SmlAngle angle = GetAngleTo(v);
			return angle.IsZeroAngle(delta) || angle.IsStraightAngle(delta);
        }

		/// <summary>
		/// Коллинеарен ли вызывающий вектор с вектором v
		/// </summary>
		/// <param name="v">Сравниваемый вектор</param>
		/// <returns> <c>true</c> если коллинеарен; иначе, <c>false</c>. </returns>
		public bool IsCollinear(SmlVector2D v)
		{
			return IsCollinear(v, GeomUtils.Delta);
		}

		/// <summary>
		/// Сонаправлен ли вызывающий вектор с вектором v
		/// </summary>
		/// <param name="v">Сравниваемый вектор</param>
		/// <param name="delta">Точность</param>
		/// <returns> <c>true</c> если соноправлен; иначе, <c>false</c>. </returns>
        public bool IsCodirectional(SmlVector2D v, double delta)
        {
			if (IsZeroVector || v.IsZeroVector)
				return true;

			SmlAngle angle = GetAngleTo(v);
			return angle.IsZeroAngle(delta);
        }

		/// <summary>
		/// Сонаправлен ли вызывающий вектор с вектором v
		/// </summary>
		/// <param name="v">Сравниваемый вектор</param>
		/// <returns> <c>true</c> если соноправлен; иначе, <c>false</c>. </returns>
		public bool IsCodirectional(SmlVector2D v)
		{
			return IsCodirectional(v, GeomUtils.Delta);
		}

		/// <summary>
		/// Угол между вызывающим вектором и вектором v
		/// </summary>
		/// <param name="vec">Вектор, с которым вычисляется угол</param>
		/// <returns>Угол между векторами</returns>
		/// <remarks>Угол вычисляется в диапозоне от 0 до 180 градусов</remarks>
		public SmlAngle GetAngleTo(SmlVector2D vec)
		{
			if (vec.IsZeroVector || IsZeroVector)
				throw new InfinityException("Невозможно определить угол между нулевым вектором");
			double u = ScalarProduct(this, vec);
			double v = Length * vec.Length;

            double d = GeomUtils.RoundingError(u / v);
		    return new SmlAngle(Math.Acos(d));
		}

		/// <summary>
		/// Получить угол между вызывающим вектором и осью координат Ох
		/// </summary>
		/// <returns>Угол между вызывающим вектором и осью координат Ох</returns>
		public SmlAngle GetAngle()
		{
			return new SmlAngle(Alpha);
		}

		/// <summary>
		/// Угол между вызывающим вектором и вектором v ПрЧС, радианы
		/// </summary>
		/// <param name="v">Вектор, с которым вычисляется угол</param>
		/// <returns>Угол между векторами</returns>
		public SmlAngle AnglePositive(SmlVector2D v)
		{
			double alpha = GetAngleTo(v);
			if (Orientation(v) == Data.Orientation.CounterClockwise)
				return new SmlAngle(alpha);

			return new SmlAngle(2 * Math.PI - alpha);
		}

		/// <summary>
		/// Угол между вызывающим вектором и  вектором v ПЧС, радианы
		/// </summary>
		/// <param name="v">Вектор, с которым вычисляется угол</param>
		/// <returns>Угол между векторами</returns>
		public SmlAngle AngleNegative(SmlVector2D v)
		{
			double alpha = GetAngleTo(v);

			if (Orientation(v) == Data.Orientation.CounterClockwise)
				return new SmlAngle(2 * Math.PI - alpha);
			return new SmlAngle(alpha);
		}

        /// <summary>
        /// Перпендикуляр к вызывающему вектору ПрЧС
        /// </summary>
		/// <returns>Вектор, перпендикулярный вызывающему ПЧС</returns>
        public SmlVector2D PerpendicularPositive()
        {
			SmlVector2D v = new SmlVector2D(Y, -X);
			if (Orientation(v) == Data.Orientation.CounterClockwise)
				return v;
			v.Negate();
			return v;
        }

        /// <summary>
        /// Перпендикуляр к вызывающему вектору ПЧС
        /// </summary>
        /// <returns>Вектор, перпендикулярный вызывающему ПЧС</returns>
        public SmlVector2D PerpendicularNegative()
        {
			SmlVector2D v = PerpendicularPositive();
			v.Negate();
			return v;
        }
        
        /// <summary>
        /// Разворот вызывающего вектора в противоположную сторону
        /// </summary>
        public void Negate()
        {
			_cartesC[0] = -_cartesC[0];
			_cartesC[1] = -_cartesC[1];
			CartesianToPolar();
        }

        /// <summary>
        /// Создание противоположного вектора
        /// </summary>
        /// <returns></returns>
        public SmlVector2D NegateCreate()
        {
           return new SmlVector2D(-X,-Y);
        }

        /// <summary>
        /// Преобразование вызывающего вектора к единичному
        /// </summary>
		public void Unit()
        {
        	D = 1.0;
			PolarToCartesian();
        }

    	/// <summary>
        /// Создание сонаправленного единичного вектора
        /// </summary>
        /// <returns></returns>
        public SmlVector2D UnitCreate()
        {
    		SmlVector2D v = new SmlVector2D(this);
			v.Unit();
			return v;
        }

        /// <summary>
        /// Разворот вызывающего вектора на угол angle ПрЧС
        /// </summary>
        /// <param name="angle">угол поворота</param>
        public void RotatePositive(double angle)
        {
			Alpha += angle;
			Alpha = GeomUtils.Revolution(Alpha);
			PolarToCartesian();
        }

        /// <summary>
        /// Разворот вызывающего вектора на угол angle ПЧС
        /// </summary>
        /// <param name="angle">угол поворота</param>
        public void RotateNegative(double angle)
        {
			Alpha -= angle;
			Alpha = GeomUtils.Revolution(Alpha);
			PolarToCartesian();
        }

        /// <summary>
        /// Создание вектора, повернутого на угол angle ПрЧС относительно вызывающего вектора
        /// </summary>
        /// <param name="angle">угол поворота</param>
        /// <returns>Повернутый вектор</returns>
		public SmlVector2D RotatePositiveCreate(double angle)
        {
            SmlVector2D v = new SmlVector2D(X, Y);
			v.RotatePositive(angle);
			return v;
        }

        /// <summary>
        /// Создание вектора, повернутого на угол angle ПЧС относительно вызывающего вектора
        /// </summary>
        /// <param name="angle">угол поворота</param>
        /// <returns>Повернутый вектор</returns>
		public SmlVector2D RotateNegativeCreate(double angle)
        {
        	SmlVector2D v = new SmlVector2D(X, Y);
			v.RotateNegative(angle);
			return v;
        }

        /// <summary>
        /// Создание вектора-биссектрисы выпуклого угла между вызывающим вектором и векторм v
        /// </summary>
        /// <param name="v">Вектор, между которым и вызывающим строится бисектрисса</param>
        /// <returns>Вектор по биссектриссе выпуклого угла</returns>
        public SmlVector2D ConvexBisectorCreate(SmlVector2D v)
        {
			if (IsCollinear(v))
			{
				if (IsCodirectional(v))
					return this;
				return PerpendicularPositive();
			}
			return (UnitCreate() + v.UnitCreate());
        }

        /// <summary>
        /// Создание вектора-биссектрисы вогнутого угла между вызывающим вектором и векторм v
        /// </summary>
		/// <param name="v">Вектор, между которым и вызывающим строится бисектрисса</param>
        /// <returns>Вектор по биссектриссе вогнутого угла</returns>
        public SmlVector2D ConcaveBisectorCreate (SmlVector2D v)
        {
			SmlVector2D v1 = ConvexBisectorCreate(v);
        	v1.Negate();
			return v1; 
        }

		/// <summary>
		/// Equalses the specified other.
		/// </summary>
		/// <param name="other">The other.</param>
		/// <returns></returns>
		public bool Equals(SmlVector2D other)
		{
			return EqualsImpl(this, other);
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="T:System.NullReferenceException">
		/// The <paramref name="obj"/> parameter is null.
		///   </exception>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof (SmlVector2D)) return false;
			return Equals((SmlVector2D) obj);
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>
		/// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
		/// </returns>
		public override int GetHashCode()
		{
			return HashCode.GetHashCode(X.GetHashCode(), Y.GetHashCode());
		}
    }

}

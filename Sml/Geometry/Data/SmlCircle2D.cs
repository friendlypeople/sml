// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;

namespace Sml.Geometry.Data
{
	/// <summary>
	/// Класс описывающий окружность
	/// </summary>
	public class SmlCircle2D : SmlTaggedObject
	{
		private SmlPoint2D _center = new SmlPoint2D();
		private double _radius;
		private SmlPoint2D _internalPoint = new SmlPoint2D();

		/// <summary>
		/// Создание нового экземпляра класса <see cref="SmlCircle2D"/>.
		/// </summary>
		/// <param name="center">Центр окружности</param>
		/// <param name="radius">Радиус окружности</param>
		public SmlCircle2D(SmlPoint2D center, double radius)
		{
			_center = center;
			CheckZeroOrNegativeRadius(radius);
			_radius = radius;
			_internalPoint = GenerateArcPoint(_center, _radius, 0);
		}

		/// <summary>
		/// Создание нового экземпляра класса <see cref="SmlCircle2D"/>.
		/// </summary>
		public SmlCircle2D()
		{
			_radius = 1;
			_internalPoint = GenerateArcPoint(_center, _radius, 0);
		}

		/// <summary>
		/// Получить или задать радиус окружности
		/// </summary>
		/// <value> Радиус окружности </value>
		public double Radius
		{
			get { return _radius; }
			set
			{
				CheckZeroOrNegativeRadius(value);
				_radius = value;
				_internalPoint = GenerateArcPoint(_center, _radius, 0);
			}
		}

		/// <summary>
		/// Получить или задать центр окружности
		/// </summary>
		/// <value>Центр окружности</value>
		public SmlPoint2D Center
		{
			get { return _center; }
			set
			{ 
				_center = value;
				_internalPoint = GenerateArcPoint(_center, _radius, 0);
			}
		}

		/// <summary>
		/// Получить диаметр окружности
		/// </summary>
		public double Diameter
		{
			get { return 2 * _radius; }
		}

		/// <summary>
		/// Получить площадь окружности
		/// </summary>
		public double Area 
		{
			get { return Math.PI * _radius * _radius; }
		}

		/// <summary>
		/// Получить диаметр окружности
		/// </summary>
		public double Length
		{
			get { return 2 * Math.PI * _radius; }
		}

		private void CheckZeroOrNegativeRadius(double r)
		{
			if (r <=0 )
				throw new Exception("Невозможно создать окружность нулевого или отрицательного радиуса");
		}

		private SmlPoint2D GenerateArcPoint(SmlPoint2D p0, double r, double fi)
		{
			double x = p0.X + Math.Round(r * Math.Cos(fi));
			double y = p0.Y - Math.Round(r * Math.Sin(fi));
			return new SmlPoint2D(x, y);
		}

		/// <summary>
		/// Получить лист точек равномерно лежащих на окружности
		/// </summary>
		/// <param name="count">Колличество точек лежащих на окружности</param>
		/// <returns>Лист точек</returns>
		public List<SmlPoint2D> GetPointsOnCircle(uint count)
		{
			if (count == 0) count = 1;
			List<SmlPoint2D> list = new List<SmlPoint2D>();
			double fi = 2* Math.PI / count;
			double angle = 0;
			for (int i = 0; i < count; i++)
			{
				list.Add(GenerateArcPoint(_center, _radius, angle));
				angle += fi;
			}
			return list;
		}

		///<summary>
		/// Ограничивающий прямоугольник
		///</summary>
		public SmlBoundaryRect2D GetBoundaryRect()
		{
			return GeomUtils.CreateBoundaryRectangle(GetPointsOnCircle(4));
		}

		/// <summary>
		/// Трансформирование
		/// </summary>
		/// <param name="matrix">Матрица трансформации</param>
		public void TransformBy(SmlMatrix2D matrix)
		{
			_center.TransformBy(matrix);
			_internalPoint.TransformBy(matrix);
			SmlVector2D vec = _internalPoint - _center;
			_radius = vec.D;
		}

		/// <summary>
		/// Проверяет, лежит ли точка на окружности
		/// </summary>
		/// <param name="pt">Исследуемая точка</param>
		/// <returns>
		///		<c>true</c> если точка лежит на окружности, иначе <c>false</c>.
		/// </returns>
		public bool IsOn(SmlPoint2D pt)
		{
			SmlVector2D vec = pt - _center;
			return Math.Abs(vec.D - _radius) < GeomUtils.Delta;
		}

		/// <summary>
		/// Проверяет, лежит ли точка на окружности или внутри нее
		/// </summary>
		/// <param name="pt">Исследуемая точка</param>
		/// <returns>
		///		<c>true</c> если точка лежит на окружности или внутри нее, иначе <c>false</c>.
		///  </returns>
		public bool IsInside(SmlPoint2D pt)
		{
			SmlVector2D vec = pt - _center;
			return vec.D <= _radius + GeomUtils.Delta;
		}
	}
}

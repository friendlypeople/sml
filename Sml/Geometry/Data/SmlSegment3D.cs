// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


namespace Sml.Geometry.Data
{
	/// <summary>
	/// Представление отрезка в пространстве
	/// </summary>
	public class SmlSegment3D : SmlLine3DBase
	{
		private SmlPoint3D _endPoint = new SmlPoint3D();

		/// <summary>
		/// Создание экземпляра класса <see cref="SmlSegment3D"/>.
		/// </summary>
		public SmlSegment3D() { }

		/// <summary>
		/// Создание экземпляра класса <see cref="SmlSegment3D"/>.
		/// </summary>
		/// <param name="pt1">Первая точка лежащая на прямой.</param>
		/// <param name="pt2">Вторая точка лежащая на прямой.</param>
		public SmlSegment3D(SmlPoint3D pt1, SmlPoint3D pt2) : base(pt1, pt2)
		{
			_endPoint = pt2;
		}

		/// <summary>
		/// Создание экземпляра класса <see cref="SmlSegment3D"/>.
		/// </summary>
		/// <param name="pt">Точка лежащая на прямой.</param>
		/// <param name="vec">Направляющий вектор.</param>
		public SmlSegment3D(SmlPoint3D pt, SmlVector3D vec) : base(pt, vec)
		{
			_endPoint = pt + vec;
		}

		/// <summary>
		/// Создание экземпляра класса <see cref="SmlSegment3D"/>.
		/// </summary>
		/// <param name="seg">Исходный луч для создания экземпляра.</param>
		/// <remarks>Конструктор копирования</remarks>
		public SmlSegment3D(SmlSegment3D seg) : base(seg)
		{
			_endPoint = new SmlPoint3D(seg._endPoint);
		}

		/// <summary>
		/// Получить или задать конечную точку луча.
		/// </summary>
		/// <value>Конечная точка отрезка</value>
		public SmlPoint3D EndPoint
		{
			get { return _endPoint; }
			set { _endPoint = value; }
		}

		/// <summary>
		/// Получить или задать начальную точку луча.
		/// </summary>
		/// <value>Начальная точка отрезка</value>
		public SmlPoint3D StartPoint
		{
			get { return _point; }
			set { _point = value; }
		}

		protected override bool IsOnImpl(SmlPoint3D p, double delta)
		{
			bool bres = base.IsOnImpl(p, delta);
			if (!bres) return false;

			SmlVector3D check1 = _point - p;
			SmlVector3D check2 = _endPoint - p;
			if (check1.IsZeroVector || check2.IsZeroVector) return true;
			return !check1.IsCodirectional(check2, delta);
		}

		public override bool CrossPoint(SmlLine3DBase line, out SmlPoint3D p)
		{
			bool bres = base.CrossPoint(line, out p);
			if (!bres) return false;
			return IsOn(p);
		}

		/// <summary>
		/// Точка пересечения отрезка с лучем
		/// </summary>
		/// <param name="ray">Пересекаемый луч</param>
		/// <param name="p">Точка пересечения</param>
		/// <returns><c>true</c>если отрезок пересекается с лучем; иначе,<c>false</c></returns>
		public bool CrossPoint(SmlRay3D ray, out SmlPoint3D p)
		{
			bool bres = base.CrossPoint(ray, out p);
			if (!bres) return false;
			return ray.IsOn(p) && IsOn(p);
		}

		/// <summary>
		/// Точка пересечения двух отрезков
		/// </summary>
		/// <param name="seg">Пересекаемый отрезок</param>
		/// <param name="p">Точка пересечения</param>
		/// <returns><c>true</c>если отрезки пересекаются; иначе,<c>false</c></returns>
		public bool CrossPoint(SmlSegment3D seg, out SmlPoint3D p)
		{
			bool bres = base.CrossPoint(seg, out p);
			if (!bres) return false;
			return IsOn(p) && seg.IsOn(p);
		}
	}
}

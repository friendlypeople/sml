// Copyright (c) 2012 Виталий Сайдин, Александр Яковенко, Ярослав Горин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Alexander Yakovenko, Yaroslav Gorin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Sml.Geometry.Data;

namespace Sml.AutoCAD.Bridge
{
	public static class Bridge
	{
		public static Point3d ToPoint3d(SmlPoint2D point)
		{
			return new Point3d(point.X, point.Y, 0);
		}

        public static Point3d ToPoint3d(SmlPoint3D point)
        {
            return new Point3d(point.X, point.Y, point.Z);
        }

		public static Point2d ToPoint2d(SmlPoint2D point)
		{
			return new Point2d(point.X, point.Y);
		}

		public static SmlPoint2D ToSmlPoint2D(Point3d point)
		{
			return new SmlPoint2D(point.X, point.Y);
		}

		public static SmlPoint2D ToSmlPoint2D(Point2d point)
		{
			return new SmlPoint2D(point.X, point.Y);
		}

        public static SmlSPoint2D ToSmlSPoint2D(Point3d point)
        {
            return new SmlSPoint2D(point.X, point.Y);
        }

        public static SmlSPoint2D ToSmlSPoint2D(Point2d point)
        {
            return new SmlSPoint2D(point.X, point.Y);
        }

		public static Polyline ToPolyline(SmlPolygonBase2D polygon)
		{
			Polyline poly = new Polyline(polygon.VertexCount);
			for (int i = 0; i < polygon.VertexCount; i++)
			{
				poly.AddVertexAt(i, ToPoint2d(polygon[i]), 0, -1, -1);
			}
			poly.Closed = true;
			return poly;
		}

		public static Line ToLine(SmlSegment2D segment)
		{
			return new Line(ToPoint3d(segment.StartPoint), ToPoint3d(segment.EndPoint));
		}

		/// <summary>
		/// Конвертировать 2 точки в сегмант.
		/// </summary>
		/// <param name="pt1">The PT1.</param>
		/// <param name="pt2">The PT2.</param>
		/// <returns></returns>
        public static SmlSegment2D ToSmlSegment2D(Point2d pt1, Point2d pt2)
		{
            return new SmlSegment2D(ToSmlPoint2D(pt1), ToSmlPoint2D(pt2));
		}

		/// <summary>
		/// Конвертировать 2 точки в сегмант.
		/// </summary>
		/// <param name="pt1">The PT1.</param>
		/// <param name="pt2">The PT2.</param>
		/// <returns></returns>
        public static SmlSegment2D ToSmlSegment2D(Point3d pt1, Point3d pt2)
		{
            return new SmlSegment2D(ToSmlPoint2D(pt1), ToSmlPoint2D(pt2));
		}

		/// <summary>
		/// Converts the line to segment.
		/// </summary>
		/// <param name="l">Линия.</param>
		/// <returns></returns>
        public static SmlSegment2D ToSmlSegment2D(Line l)
		{
			return ToSmlSegment2D(l.StartPoint, l.EndPoint);
		}
	}
}
